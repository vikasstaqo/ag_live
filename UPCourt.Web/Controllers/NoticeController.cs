﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeController : BaseMVCController
    {
        [AllowAnonymous]
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "addbasicindex")]
        public ActionResult addbasicindex(NoticeMvcModel model)
        {
            return View(model);
        }

        // GET: Notice
        public ActionResult Index()
        {
            var dbData = NoticeDataService.GetLite(db).OrderBy(i => i.NoticeDate).ToList();
            return View(NoticeMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: Notice/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Notice/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            InitilizeGovermentDepartments(null);
            var model = new NoticeMvcModel();           

            model.NoticeRespondents = new List<NoticeRespondentMvcModel>();                
            model.NoticeRespondents.Add(new NoticeRespondentMvcModel());

            model.NoticeBasicIndexs = new List<NoticeBasicIndexMvcModel>();
            model.NoticeBasicIndexs.Add(new NoticeBasicIndexMvcModel());
            return View(model);
        }

        // POST: Notice/Create
        [AllowAnonymous]
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(NoticeMvcModel model)
        {
            return View(model);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        // GET: Notice/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Notice/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Notice/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Notice/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Add()
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, "Name", "Name");
            ViewBag.GovermentDepartments2 = mvc;
            return View();
        }

        public JsonResult GovtDept()
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);

            return Json(mvc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddNotice(string petitioner, string counselor, string notice, string respondent)
        {
            string petitionerName = "";
            Guid petitionerId = new Guid();
            string noticeNo = "";

            Notice tempNotice = new Notice();

            var serializeData = JsonConvert.DeserializeObject<List<Petitioner>>(petitioner);
            var serializeCounselorData = JsonConvert.DeserializeObject<List<NoticeCounselor>>(counselor);
            var serializeNoticeData = JsonConvert.DeserializeObject<List<Notice>>(notice);
            var serializeRespondentData = JsonConvert.DeserializeObject<List<NoticeRespondentMvcModel>>(respondent);

            foreach (var data in serializeData)
            {
                Petitioner tempPetitioner = new Petitioner();
                tempPetitioner.PetitionerId = Guid.NewGuid();
                tempPetitioner.Name = data.Name;
                tempPetitioner.Address = data.Address;
                tempPetitioner.ContactNo = data.ContactNo;
                tempPetitioner.Email = data.Email;
                tempPetitioner.District = data.District;
                tempPetitioner.IDProof = data.IDProof;
                tempPetitioner.IDProofNumber = data.IDProofNumber;
                tempPetitioner.ModifyBy = data.Name;

                petitionerName = data.Name;
                petitionerId = tempPetitioner.PetitionerId;
            }

            foreach (var data in serializeCounselorData)
            {
                NoticeCounselor tempCounselor = new NoticeCounselor();
                tempCounselor.NoticeCounselorId = Guid.NewGuid();
                tempCounselor.Name = data.Name;
                tempCounselor.Email = data.Email;
                tempCounselor.ContactNo = data.ContactNo;
                tempCounselor.EnrollmentNo = data.EnrollmentNo;
                tempCounselor.RollNo = data.RollNo;
                tempCounselor.ModifyBy = petitionerName;

            }

            foreach (var data in serializeNoticeData)
            {               
                tempNotice.NoticeType = data.NoticeType;
                tempNotice.SerialNo = NoticeBusinessService.GetSerailNo(db);
                tempNotice.NoticeNo = NoticeBusinessService.GetInvoiceNo("NOC", tempNotice.SerialNo);
                tempNotice.NoticeDate = Extended.CurrentIndianTime;
                tempNotice.Subject = data.Subject;
                tempNotice.IsContextOrder = data.IsContextOrder;

                noticeNo = tempNotice.NoticeNo;
            }

            tempNotice.NoticeId = Guid.NewGuid();

            foreach (var data in serializeRespondentData)
            {
                PartyAndRespondent tempPartyAndRespondent = new PartyAndRespondent();
                if (!data.IsPartyDepartment)
                {
                    tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                    tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    tempPartyAndRespondent.PartyType = PartyType.Individual;
                    tempPartyAndRespondent.PartyName = data.PartyName;
                    tempPartyAndRespondent.ContactNo = data.ContactNo;
                    tempPartyAndRespondent.Email = data.Email;
                    tempPartyAndRespondent.ModifyBy = petitionerName;
                }
                if (data.IsPartyDepartment)
                {
                    tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                    tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    tempPartyAndRespondent.PartyType = PartyType.Department;
                    tempPartyAndRespondent.GovernmentDepartmentId = data.GovernmentDepartmentId;
                    tempPartyAndRespondent.ModifyBy = petitionerName;
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }



    }
}
