﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    

    public class CaseStageMvcModel : BaseEntityModel<CaseStage, CaseStageMvcModel>
    {
        public Guid CaseStageId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Active")]
        public bool IsActive { get; set; }

    }

   

}