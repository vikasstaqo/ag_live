﻿using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeReportController : BaseMVCController
    {
        // GET: NoticeReport
        public ActionResult Index(NoticeCaseIndexMvcModel model)
        {
            InitilizeRegions(AppUser.User.RegionId);
            InitilizeCaseNoticeTypes(null, 0);
            InitilizeSubject(model.Subject, model.CaseNoticeTypeId);
            InitilizeNoticeType(model.NoticeTypeId, Convert.ToInt32(model.ParentNoticeType));
            InitilizeGroups(null);
            model.Notices = searchNotices(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ExportToExcel(NoticeCaseIndexMvcModel model)
        {
            var notices = searchNotices(model);
            if (notices != null)
            {
                var sb = new StringBuilder();
                var data = from n in notices
                           select new
                           {


                               NoticeNo = n.NoticeNo,
                               NoticeDate = n.NoticeDate,
                               Petitioner = PetitionerDetail(n.Petitioners),
                               Respondent = RespondentDetail(n.NoticeRespondents),
                               NoticeType = n.NoticeType,
                               NoticeSubType = n.CaseNoticeType?.Name,
                               PetitionerAdvocate = n.NoticeCounselors.Count > 0 ? n.NoticeCounselors.FirstOrDefault().Name : "",
                               District = n.CrimeDetail?.District != null ? n.CrimeDetail?.District : "",
                               Region = n.Region?.Name != null ? n.Region?.Name : "",
                               Subject = n.Subject != null ? n.Subject : "",
                               GroupName = n.Group != null && n.Group.GroupName != null ? n.Group.GroupName : "",
                               PetDistrict = n.Petitioners.Count > 0 && n.Petitioners.FirstOrDefault().District != null ? n.Petitioners.FirstOrDefault().District : "",
                           };
                var list = data.ToList();
                var grid = new System.Web.UI.WebControls.GridView
                {
                    DataSource = list
                };
                grid.DataBind();

                if (model.Export == ExportType.Excel)
                {
                    StringWriter sw = new StringWriter();
                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                    grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                    grid.RenderControl(htw);
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "attachment; filename=NoticeReport.xls");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.Write(sw.ToString());
                }
                else if (model.Export == ExportType.PDF)
                {
                    StringWriter sw = new StringWriter();
                    System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                    grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                    grid.RenderControl(htw);
                    StringReader reader = new StringReader(sw.ToString());
                    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    pdfDoc.AddHeader("Notice Report", "Notices");
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                   
                    pdfDoc.Close();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment; filename=NoticeReport.PDF");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                }

                Response.End();
            }


            DisplayMessage(MessageType.Success, "No notices found", "No notices found", "");
                      model.Notices = notices;
            return View(model);
        }

        private string PetitionerDetail(List<PetitionerMvcModel> Pet)
        {
            return Pet != null && Pet.Count > 0 ? Pet.FirstOrDefault().Name : "";
        }
        private string RespondentDetail(List<NoticeRespondentMvcModel> Res)
        {
            string res = "";
            if (Res != null && Res.Count > 0)
            {
                foreach (NoticeRespondentMvcModel objNRM in Res)
                {
                    if (objNRM?.GovernmentDepartmentId != null)
                        res = res + "," + objNRM?.GovermentDepartment.POCName + "(" + objNRM?.GovermentDepartment.Name + ") ";
                    else
                        res = res + "," + objNRM.PartyName;
                }
                res = res.Remove(0, 1);
            }
            return res;
        }
        private List<NoticeMvcModel> searchNotices(NoticeCaseIndexMvcModel model)
        {
            if (model != null)
            {
                var dbData = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB")
                    .OrderByDescending(i => i.NoticeCreateDate).ToList();

                if (model.NoticeNo != null)
                    dbData = dbData.Where(x => x.NoticeNo.Contains(model.NoticeNo)).ToList();

                if (model.NoticeFromDate.HasValue)
                    dbData = dbData.Where(x => x.NoticeDate.Value.Date >= model.NoticeFromDate.Value.Date).ToList();

                if (model.NoticeToDate.HasValue)
                    dbData = dbData.Where(x => x.NoticeDate.Value.Date <= model.NoticeToDate.Value.Date).ToList();

                if (model.ParentNoticeType != 0)
                    dbData = dbData.Where(x => x.NoticeType == model.ParentNoticeType).ToList();

                if (model.NoticeTypeId != null && model.NoticeTypeId != Guid.Empty)
                    dbData = dbData.Where(x => x.CaseNoticeTypeId == model.NoticeTypeId).ToList();

                if (model.Subject != null && model.Subject != "")
                    dbData = dbData.Where(x => x.Subject == model.Subject).ToList();

                if (model.GroupId != null && model.GroupId != Guid.Empty)
                    dbData = dbData.Where(x => x.GroupID == model.GroupId).ToList();

                if (model.RegionId != null && model.RegionId != Guid.Empty)
                    dbData = dbData.Where(x => x.RegionId == model.RegionId).ToList();

                return NoticeMvcModel.CopyFromEntityList(dbData.OrderBy(x => x.NoticeCreateDate).ToList(), 0);
            }
            return null;
        }

        #region private methods


        #region data initialisers
        private List<SelectListItem> GetAllStatus()
        {
            var AllStatuses = CaseStatusDataService.GetLite(db).Where(x => x.Name.ToLower().Contains("fresh case")).ToList();
            var CaseStatuses = new List<SelectListItem>();
            for (int i = 0; i < AllStatuses.Count; i++)
            {
                CaseStatuses.Add(new SelectListItem()
                {
                    Text = AllStatuses[i].Name,
                    Value = AllStatuses[i].CaseStatusId.ToString()
                });
            }
            return CaseStatuses;
        }
        private void InitilizeDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Department = new SelectList(mvc, "GovernmentDepartmentId", "Name", selectedValue);
        }
        private void InitilizeNoticeType(object selectedValue, int noticeType)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(n => n.IsActive == true && n.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);

        }
        private void InitilizeSubject(object selectedValue, Guid? CaseNoticeType)
        {
            var dbData = SubjectDataService.GetLite(db).Where(s => s.CaseNoticeTypeId == CaseNoticeType).OrderBy(i => i.Name).ToList();
            var mvc = SubjectMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Subjects = new SelectList(dbData, nameof(Subject.Name), nameof(Subject.Name), selectedValue);
        }
        private void InitilizeGroups(object selectedValue)
        {
            var dbData = GroupDataService.GetLite(db).OrderBy(i => i.GroupName).ToList();
            var mvc = GroupMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Groups = new SelectList(dbData, nameof(Group.GroupID), nameof(Group.GroupName), selectedValue);
        }
        private void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeGovDesignations(object selectedValue)
        {
            var dbData = GovDesignationDataService.GetLite(db).OrderBy(i => i.DesignationName).ToList();
            var mvc = GovDesignationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovDesignations = new SelectList(mvc, nameof(GovDesignationMvcModel.GovernmentDesignationId)
                , nameof(GovDesignationMvcModel.DesignationName), selectedValue);
        }
        private void InitilizeCaseNoticeTypes(object selectedValue, int noticeType)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(n => n.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);
        }
        private void InitilizeRegions(object selectedValue)
        {

            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);

        }
        
        #endregion

        #endregion
    }
}