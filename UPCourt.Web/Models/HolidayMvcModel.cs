﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class HolidayMvcModel : BaseEntityModel<Holiday, HolidayMvcModel>
    {
        public Guid HolidayId { get; set; }
        public string HolidayName { get; set; }
        public string HStatus { get; set; }
        public string Hdate { get; set; }
        public string Hyear { get; set; }
    }
    public class HolidayIndexMvcModel
    {
        public Guid HolidayId { get; set; }
        public DateTime? Hdate { get; set; }
        public string HolidayName { get; set; }
        public string HStatus { get; set; }
        public string Hyear { get; set; }
        public List<HolidayMvcModel> HolidayDocs { get; set; }

       
    }
  
}