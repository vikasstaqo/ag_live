﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class HolidayController : BaseMVCController
    {
        // GET: Holiday
        public ActionResult Index()
        {
            var dbData = HolidayListDataService.GetLite(db).ToList();
            return View();
        }
        public ActionResult Create()
        {
            Initilizeholiday(null);           
            return View();
        }
        private void Initilizeholiday(object selectedValue)
        {
            var dbData = HolidayListDataService.GetLite(db).OrderBy(i => i.HolidayName).ToList();
            var mvc = HolidayMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.HolidayList = new SelectList(mvc, nameof(HolidayMvcModel.HolidayName)
                , nameof(HolidayMvcModel.HolidayName), selectedValue);
        }
    }
}