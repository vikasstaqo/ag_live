﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UPCourt.Entity;

namespace UPCourt.Web
{
    public class RouteConfig
    {
        private static string Environment = ConfigurationManager.AppSettings["Environment"].ToString();

        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            using (DatabaseContext db = new DatabaseContext())
            {
                db.Database.Migrate();
                if (Environment != null && Environment.ToLower() == "live")
                {                                                     
                    DatabaseContextHelper.EnsureSeedDataForContext(db);
                }
            }
        }
    }
}
