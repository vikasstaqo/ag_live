﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CourtJudgeMappingDataService
    {
        public static IQueryable<CourtJudgeMapping> GetLite(DatabaseContext db)
        {
            return db.CourtJudgeMappings;
        }
        public static IQueryable<CourtJudgeMapping> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}