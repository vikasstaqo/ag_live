﻿
function ConfirmAppDelete(NoticeId) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Sure You Want Delete!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/FreshCases/Delete/" + NoticeId);
        }
    });
}
function ConfirmSumit(NoticeId) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Are Sure You Want To Submit! Once you submit the application you will not able  to make any changes to the application",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/FreshCases/ConfirmFinalSubmit/" + NoticeId);
        }
    });
}