/* global Chart:false */

$(function () {
    'use strict'
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var lastSixMonths = Array();
    var lastWeekDates = Array();
    var today = new Date();
    var d, day;
    var month;

    for (var i = 6; i > 0; i -= 1) {
        d = new Date(today.getFullYear(), (today.getMonth() + 1) - i, 1);
        lastSixMonths.push(monthNames[d.getMonth()].toUpperCase());
    }
    d = new Date();
    for (var i = 7; i > 0; i -= 1) {
        d = new Date(today.getFullYear(), today.getMonth(), today.getDate() - i);
        day = d.getDate();
        lastWeekDates.push((day == 1 || day == 21 || day == 31 ? day + 'st' : day == 2 || day == 22 ? day + 'nd' : day == 3 || day == 23 ? day + 'rd' : day + 'th') + ' ' + monthNames[d.getMonth()]);
    }
    var ticksStyle = {
        fontColor: '#495057',
        fontStyle: 'bold'
    }

    var mode = 'index'
    var intersect = true
    debugger;
    var $salesChart = $('#sales-chart')
    var activeCasesCounts = $('#activeCasesCounts').val();
    // eslint-disable-next-line no-unused-vars
    var salesChart = new Chart($salesChart, {
        type: 'bar',
        data: {
            labels: lastSixMonths, //['JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            datasets: [{
                backgroundColor: '#90ed7d',
                borderColor: '#90ed7d',
                data: activeCasesCounts
            }]
        },
        options: {
            maintainAspectRatio: false,
            tooltips: { mode: mode, intersect: intersect },
            hover: { mode: mode, intersect: intersect },
            legend: { display: false },
            scales: {
                yAxes: [{ gridLines: { display: true, lineWidth: '4px', color: 'rgba(0, 0, 0, .2)', zeroLineColor: 'transparent' }, ticks: ticksStyle }],
                xAxes: [{ display: true, gridLines: { display: false }, ticks: ticksStyle }]
            }
        }
    });


    var $salesChart2 = $('#sales-chart2');
    var disposedCasesCounts = $('#disposedCasesCounts').val();
    // eslint-disable-next-line no-unused-vars
    var salesChart2 = new Chart($salesChart2, {
        type: 'bar',
        data: {
            labels: lastSixMonths, //['JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            datasets: [
                {
                    backgroundColor: '#7cb5ec',
                    borderColor: '#7cb5ec',
                    data: disposedCasesCounts,//[1000, 2000, 3000, 2500, 2700, 2500, 3000]
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect
            },
            hover: {
                mode: mode,
                intersect: intersect
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: $.extend({
                        beginAtZero: true,
                        // Include a dollar sign in the ticks
                        callback: function (value) {
                            if (value >= 1000) {
                                value /= 1000
                            }
                            return 0
                        }
                    }, ticksStyle)
                }],
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: ticksStyle
                }]
            }
        }
    })


    var $salesChart3 = $('#sales-chart3');
    var contextOrdersCounts = $('#contextOrdersCounts').val();
    // eslint-disable-next-line no-unused-vars
    var salesChart3 = new Chart($salesChart3, {
        type: 'bar',
        data: {
            labels: lastSixMonths, //['JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            datasets: [
                {
                    backgroundColor: '#f7a35c',
                    borderColor: '#f7a35c',
                    data: contextOrdersCounts,//[1000, 2000, 3000, 2500, 2700, 2500, 3000]
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect
            },
            hover: {
                mode: mode,
                intersect: intersect
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: $.extend({
                        beginAtZero: true,

                        // Include a dollar sign in the ticks
                        callback: function (value) {
                            if (value >= 1000) {
                                value /= 1000

                            }

                            return 0
                        }
                    }, ticksStyle)
                }],
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: ticksStyle
                }]
            }
        }
    })

    var $salesChart4 = $('#sales-chart4')
    // eslint-disable-next-line no-unused-vars
    var salesChart4 = new Chart($salesChart4, {
        type: 'bar',
        data: {
            labels: lastSixMonths, //['JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            datasets: [
                {
                    backgroundColor: '#f97d09',
                    borderColor: '#f97d09',
                    data: [1000, 2000, 3000, 2500, 2700, 2500, 3000]
                }
            ]
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect
            },
            hover: {
                mode: mode,
                intersect: intersect
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: $.extend({
                        beginAtZero: true,

                        // Include a dollar sign in the ticks
                        callback: function (value) {
                            if (value >= 1000) {
                                value /= 1000

                            }

                            return 0
                        }
                    }, ticksStyle)
                }],
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: ticksStyle
                }]
            }
        }
    })



    var $visitorsChart = $('#visitors-chart');
    var pendingNoticesCounts = $('#pendingNoticesCounts').val();
    // eslint-disable-next-line no-unused-vars
    var visitorsChart = new Chart($visitorsChart, {
        data: {
            labels: lastWeekDates, //['18th', '20th', '22nd', '24th', '26th', '28th', '30th'],
            datasets: [
                {
                    type: 'line',
                    data: pendingNoticesCounts,
                    backgroundColor: 'transparent',
                    borderColor: '#007bff',
                    pointBorderColor: '#007bff',
                    pointBackgroundColor: '#007bff',
                    fill: false
                    // pointHoverBackgroundColor: '#007bff',
                    // pointHoverBorderColor    : '#007bff'
                },
                {
                    type: 'line',
                    data: pendingNoticesCounts,
                    backgroundColor: 'tansparent',
                    borderColor: '#ced4da',
                    pointBorderColor: '#ced4da',
                    pointBackgroundColor: '#ced4da',
                    fill: false
                    // pointHoverBackgroundColor: '#ced4da',
                    // pointHoverBorderColor    : '#ced4da'
                }]
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                mode: mode,
                intersect: intersect
            },
            hover: {
                mode: mode,
                intersect: intersect
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: $.extend({
                        beginAtZero: true,
                        suggestedMax: 20
                    }, ticksStyle)
                }],
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: ticksStyle
                }]
            }
        }
    })
})
