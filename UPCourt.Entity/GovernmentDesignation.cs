﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{


    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DesignationName), UserNameProp: nameof(ModifiedBy))]
    public class GovernmentDesignation : BaseMasterEntity
    {
        public GovernmentDesignation()
        {
        }
        public Guid GovernmentDesignationId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string DesignationName { get; set; }
        public GovernmentDesignationStatus Status { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }
        public string POCName { get; set; }
        public string POCContact { get; set; }
        public string POCFaxNo { get; set; }
        public string POCMobileNo { get; set; }
        public string POCEmail { get; set; }
        public bool IsAssignUser { get; set; }

    }
    public enum GovernmentDesignationStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(OICName), UserNameProp: nameof(ModifiedBy))]
    public class GovernmentDepartmentOic : BaseMasterEntity
    {
        public GovernmentDepartmentOic()
        {
        }
        public Guid GovernmentdepartmentOICID { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public Guid GovernmentDesignationId { get; set; }
        public string OICName { get; set; }
        public GovernmentDepartmentOicStatus Status { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }
        public GovernmentDesignation GovernmentDesignation { get; set; }
        public string POCName { get; set; }
        public string POCContact { get; set; }
        public string POCFaxNo { get; set; }
        public string POCMobileNo { get; set; }
        public string POCEmail { get; set; }
        public bool IsAssignUser { get; set; }

    }
    public enum GovernmentDepartmentOicStatus
    {
        Active = 1,
        Inactive = 0
    }
}
