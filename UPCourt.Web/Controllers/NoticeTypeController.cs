﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeTypeController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanView)]
        // GET: Court
        public ActionResult Index()
        {
            return View(CaseNoticeTypeMvcModel.CopyFromEntityList(NoticeTypeDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        private void InitialiseParentTypes(object selectedValue)
        {
            var enumData = from Entity.ParentNoticeType e in Enum.GetValues(typeof(Entity.ParentNoticeType)) select new { Id = (int)e, Name = e.ToString() };
            ViewBag.ParentNoticeTypes = new SelectList(enumData, "Id", "Name", selectedValue);
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanView)]
        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = NoticeTypeDataService.GetDetail(db).Where(i => i.CaseNoticeTypeId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitialiseParentTypes(id);
            return View(CaseNoticeTypeMvcModel.CopyFromEntity(dbData, 0));
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanCreate)]
        // GET: Court/Create
        public ActionResult Create()
        {
            CaseNoticeTypeMvcModel model = new CaseNoticeTypeMvcModel();
            InitialiseParentTypes(null);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanCreate)]
        // POST: Court/Create
        [HttpPost]
        public ActionResult Create(CaseNoticeTypeMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var count = NoticeTypeDataService.GetLite(db).Where(i => i.Name == model.Name).FirstOrDefault();
            if (count != null)
            {
                ModelState.AddModelError(nameof(model.Name), "This Name Already Exist");
                return View(model);
            }
            CaseNoticeType tempCaseNoticeType = new CaseNoticeType
            {
                CaseNoticeTypeId = Guid.NewGuid(),
                DisplayOrder = MasterBusinessService.GetCaseNoticeTypesDisplayOrder(db),
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            InitializeData(tempCaseNoticeType, model, true);

            db.CaseNoticeTypes.Add(tempCaseNoticeType);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanEdit)]
        // GET: AGDepartment/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(i => i.CaseNoticeTypeId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitialiseParentTypes(id);
            return View(CaseNoticeTypeMvcModel.CopyFromEntity(dbData, 0));
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanEdit)]
        // POST: CaseStatus/Edit/5
        [HttpPost]
        public ActionResult Edit(CaseNoticeTypeMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = NoticeTypeDataService.GetLite(db).Where(i => i.CaseNoticeTypeId == model.CaseNoticeTypeId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitializeData(dbData, model, false);
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        private void InitializeData(CaseNoticeType tempCaseNoticeType, CaseNoticeTypeMvcModel model, bool IsNew)
        {
            tempCaseNoticeType.Name = model.Name;
            tempCaseNoticeType.IsActive = model.IsActive;
            tempCaseNoticeType.ParentNoticeType = model.ParentNoticeType;
            if (!IsNew)
            {
                tempCaseNoticeType.CaseNoticeTypeId = model.CaseNoticeTypeId;
            }
        }

        [PageRoleAuthorizarion(PageName.NoticeType, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(i => i.CaseNoticeTypeId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
