﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPCourt.Web.Core
{
    public class DataAnotationHelper
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DateGreaterThanAttribute : ValidationAttribute, IClientValidatable
    {

        private string DateToCompareFieldName { get; set; }

        public DateGreaterThanAttribute(string dateToCompareFieldName)
        {
            DateToCompareFieldName = dateToCompareFieldName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime laterDate = (DateTime)value;

            DateTime earlierDate = (DateTime)validationContext.ObjectType.GetProperty(DateToCompareFieldName).GetValue(validationContext.ObjectInstance, null);

            if (laterDate > earlierDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(string.Format("{0} precisa ser menor!", DateToCompareFieldName));
            }
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = string.Format("{0} precisa ser menor!", DateToCompareFieldName),
                ValidationType = "dategreaterthan"
            };

            clientValidationRule.ValidationParameters.Add("datetocomparefieldname", DateToCompareFieldName);

            return new[] { clientValidationRule };
        }

    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DateLessThanAttribute : ValidationAttribute, IClientValidatable
    {

        private string DateFromCompareFieldName { get; set; }

        public DateLessThanAttribute(string dateFromCompareFieldName)
        {
            DateFromCompareFieldName = dateFromCompareFieldName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime GreaterDate = (DateTime)value;

            DateTime earlierDate = (DateTime)validationContext.ObjectType.GetProperty(DateFromCompareFieldName).GetValue(validationContext.ObjectInstance, null);

            if (earlierDate < GreaterDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(string.Format("{0} precisa ser menor!", DateFromCompareFieldName));
            }
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = string.Format("{0} precisa ser menor!", DateFromCompareFieldName),
                ValidationType = "datelessthan"
            };

            clientValidationRule.ValidationParameters.Add("datetocomparefieldname", DateFromCompareFieldName);

            return new[] { clientValidationRule };
        }

    }
}