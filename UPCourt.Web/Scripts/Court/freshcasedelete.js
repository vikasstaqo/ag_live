﻿function ConfirmDelete(NoticeNo, NoticeId) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete Notice No '" + NoticeNo + "' !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/FreshCases/Delete/" + NoticeId);
        }
    });
}

function ConfirmAppDelete(NoticeId) {
  
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete Application Document !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/FreshCases/Delete/" + NoticeId);
        }
    });
}
function ConfirmSumit(NoticeId) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to Final Submit Application Document !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/FreshCases/ConfirmFinalSubmit/" + NoticeId);
        }
    });
}

