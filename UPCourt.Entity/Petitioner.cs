﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog:true,DisplayProp:nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class Petitioner
    { 
        public Guid PetitionerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string District { get; set; }

        public string CaseDistrict { get; set; }
        public IDProofType IDProof { get; set; }
        public string IDProofNumber { get; set; }        
        public string ModifyBy { get; set; }
        public bool IsFirst { get; set; }
        public Guid NoticeId { get; set; }
        public Guid? SalutationId { get; set; }
        public Guid? ParantageSalutationId { get; set; }         
        public int? Age { get; set; }
        public string ParantageName { get; set; }
        public string City { get; set; }
        public string MobileNo { get; set; }
        public string Pincode { get; set; }
        public Notice NoticePetitioner { get; set; }
        public int? SNo { get; set; }
    }
    public enum IDProofType
    {
        [Description("Adhaar Card")]
        AadharID = 0,
        [Description("Votar Card")]
        VoterID = 1,
        [Description("PAN Card")]
        PANID = 2,
        [Description("Driving License")]
        DrivingLicense = 3
    }
}
