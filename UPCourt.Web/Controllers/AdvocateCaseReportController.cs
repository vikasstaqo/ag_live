﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class AdvocateCaseReportController : BaseMVCController
    {
        // GET: AdvocateCaseReport
        public ActionResult Index(AdvocateCaseFilterMvcModel AdvocatecaseFilterMvcModel)
        {
            if (!IsValidPermission(rModel, PageName.CaseReport, PagePermissionType.CanView))
            {
                return RedirectToAction("Index", "Home");
            }
            InitilizeCaseStatus(AdvocatecaseFilterMvcModel.CaseStatusActive);

            InitilizeAdvocate(AdvocatecaseFilterMvcModel.UserId);
            InitilizeRegion(AdvocatecaseFilterMvcModel.RegionId);


            List<Case> acctiveCases = new List<Case>();
            acctiveCases = CaseDataService.GetDetail(db).Where(x => x.CaseStatus.Type == CaseStatusType.Normal).OrderBy(i => i.Notice.NoticeDate).ToList();


            if (AdvocatecaseFilterMvcModel.CaseStatusActive != null && AdvocatecaseFilterMvcModel.CaseStatusActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.CaseStatusId == AdvocatecaseFilterMvcModel.CaseStatusActive).OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AdvocatecaseFilterMvcModel.UserId != null && AdvocatecaseFilterMvcModel.UserId.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.Advocate?.UserId == AdvocatecaseFilterMvcModel.UserId).OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AdvocatecaseFilterMvcModel.RegionId != null && AdvocatecaseFilterMvcModel.RegionId.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.Region?.RegionId == AdvocatecaseFilterMvcModel.RegionId).OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AdvocatecaseFilterMvcModel.UserId != null && AdvocatecaseFilterMvcModel.UserId.ToString() != "")
            {
                var dbAdvocateName = AdvocateDataService.GetDetail(db, AppUser).Where(x => x.UserId == AdvocatecaseFilterMvcModel.UserId).ToList();
                if (dbAdvocateName.Any())
                {
                    foreach (var p in dbAdvocateName)
                    {
                        ViewBag.AdvocateName = "Advocate Name : " + p.Name;
                    }
                }
            }
            else
            {
                ViewBag.AdvocateName = "";
            }
            AdvocatecaseFilterMvcModel.AcctiveCases = CaseMvcModel.CopyFromEntityList(acctiveCases, 2);
            AdvocatecaseFilterMvcModel.CaseCountList = AdvocatecaseFilterMvcModel.AcctiveCases.GroupBy(info => info.CaseStatus.Name)
                        .Select(group => new AdvocateCaseCount
                        {
                            CaseStatus = group.Key,
                            Count = group.Count()
                        })
                        .OrderBy(x => x.CaseStatus).ToList();

            return View(AdvocatecaseFilterMvcModel);
        }
        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Normal).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }
        public void InitilizeAdvocate(object selectedValue)
        {
            var dbData = AdvocateDataService.GetDetail(db, AppUser).Where(x => x.UserRole.Name == "Advocate").ToList();
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Advocate = new SelectList(mvc, "UserId", "Name", selectedValue);
        }
        public void InitilizeRegion(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Region = new SelectList(mvc, "RegionId", "Name", selectedValue);
        }
    }
}