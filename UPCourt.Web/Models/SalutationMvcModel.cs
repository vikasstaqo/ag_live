﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class SalutationMvcModel : BaseEntityModel<Salutation, SalutationMvcModel>
    {
        public Guid SalutationId { get; set; }
        public string Name { get; set; }
        public SalutationStatus Status { get; set; }

    }
}