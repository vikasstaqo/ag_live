﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class SubjectController : BaseMVCController
    {
        // GET: Subject
        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(SubjectMvcModel.CopyFromEntityList(SubjectDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        // GET: Subject/Create
        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new SubjectMvcModel();
            model.IsActive = true;
            InitialiseParentTypes(null);
            InitilizeNoticeType(model.ParentNoticeType);
            return View(model);
        }
        private void InitialiseParentTypes(object selectedValue)
        {
            var enumData = from Entity.ParentNoticeType e in Enum.GetValues(typeof(Entity.ParentNoticeType)) select new { Id = (int)e, Name = e.ToString() };
            ViewBag.ParentNoticeTypes = new SelectList(enumData, "Id", "Name", selectedValue);
        }

        public void InitilizeNoticeType(object selectedValue)
        {
            var dbData = NoticeTypeDataService.GetLite(db).OrderBy(i => i.Name);
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.NoticeType = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);

        }

        [HttpGet]
        public ActionResult GetNoticeTypes(int pid)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(i => (int)i.ParentNoticeType == pid);
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(SubjectMvcModel model)
        {
            InitialiseParentTypes(model.ParentNoticeType);
            InitilizeNoticeType(model.CaseNoticeType);
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Subject tempData = new Subject()
            {
                SubjectId = Guid.NewGuid(),
                Name = model.Name,
                Description = model.Description,
                IsActive = model.IsActive,
                ParentNoticeType = model.ParentNoticeType,
                CaseNoticeTypeId = model.CaseNoticeTypeId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetSubjectsDisplayOrder(db)
            };
            db.Subjects.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = SubjectDataService.GetLite(db).Where(i => i.SubjectId == id).FirstOrDefault();
            if (dbData == null)            {                return RedirectToAction("Index");            }
            SubjectMvcModel model = SubjectMvcModel.CopyFromEntity(dbData, 0);
            InitilizeNoticeType(model.CaseNoticeType);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(SubjectMvcModel model)
        {
            InitialiseParentTypes(model.ParentNoticeType);
            InitilizeNoticeType(model.CaseNoticeType);
            if (!ModelState.IsValid) { return View(model); }
            var dbData = SubjectDataService.GetLite(db).Where(i => i.SubjectId == model.SubjectId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Description = model.Description;
            dbData.IsActive = model.IsActive;
            dbData.ParentNoticeType = model.ParentNoticeType;
            dbData.CaseNoticeTypeId = model.CaseNoticeTypeId;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        public ActionResult Details(Guid id)
        {
            var dbData = SubjectDataService.GetLite(db).Where(i => i.SubjectId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(SubjectMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(SubjectMvcModel model)
        {
            var duplicateCheck = SubjectDataService.GetLite(db).FirstOrDefault(i => i.SubjectId != model.SubjectId && i.CaseNoticeTypeId == model.CaseNoticeTypeId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{ model.Name } already exists in same Notice Type ");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = SubjectDataService.GetLite(db).Where(i => i.SubjectId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }

    }
}
