﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class NotificationsModel : BaseEntityModel<Entity.Notification, NotificationsModel>
    {
        public Guid NotificationId { get; set; }
        [Display(Name = "Notification Source")]
        public Guid SourceEntityId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Notification SourceType is required.")]
        [Display(Name = "Notification Entity")]
        public int SourceEntityType { get; set; }

        [Display(Name = "Target User")]
        public Guid TargetEntityId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Notification Target UserType is required.")]
        public Guid TargetEntityType { get; set; }

        [Display(Name = "Status")]
        public Entity.NotificationStatus Status { get; set; }

        [Required]
        public string Message { get; set; }
        public string ModifyBy { get; set; }
        public string CreationDate { get; set; }
        [Display(Name = "Notification Type")]
        public int NotificationType { get; set; }
        public string DaysElapsed { get; set; }
        public List<Guid> AssignedUsers { get; set; }
        [Display(Name = "Route Value")]
        public string RouteValue { get; set; }
        [Display(Name = "User Type")]
        public ICollection<Entity.UserRole> UserType { get; set; }
    }

    public class NotificationsMvcModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public static class ExtensionMethods
    {
        public static System.Web.Mvc.SelectList ToSelectList<TEnum>(this TEnum obj)
        where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            return new SelectList(Enum.GetValues(typeof(TEnum))
            .OfType<Enum>()
            .Select(x => new SelectListItem
            {
                Text = Enum.GetName(typeof(TEnum), x),
                Value = (Convert.ToInt32(x))
                .ToString()
            }), "Value", "Text");
        }
    }
}