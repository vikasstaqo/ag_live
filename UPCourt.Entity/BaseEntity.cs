﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public class BaseMasterEntity
    {
        public int DisplayOrder { get; set; }
        public bool IsDeleted { get; set; }
        [EntityChangeTrackerIgnore]
        public DateTime CreatedOn { get; set; }
        [EntityChangeTrackerIgnore]
        public Guid? CreatedBy { get; set; }
        [EntityChangeTrackerIgnore]
        public DateTime ModifiedOn { get; set; }
        [EntityChangeTrackerIgnore]
        public Guid? ModifiedBy { get; set; }
    }

    public class BaseTransEntity
    {
        public bool IsDeleted { get; set; }
        [EntityChangeTrackerIgnore]
        public DateTime CreatedOn { get; set; }
        [EntityChangeTrackerIgnore]
        public Guid? CreatedBy { get; set; }
    }

}
