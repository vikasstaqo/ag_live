﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class IPCSectionMvcModel : BaseEntityModel<IPCSection, IPCSectionMvcModel>
    {
        public Guid IPCSectionId { get; set; }
        [Required]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }
        public string Description { get; set; }
        public IPCSectionStatus Status { get; set; }
    }
}