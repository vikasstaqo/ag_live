﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using CompareAttribute = System.Web.Mvc.CompareAttribute;

namespace UPCourt.Web.Models
{
    public class UserLoginRequestModel
    {
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class UserMvcModelExternal : BaseEntityModel<User, UserMvcModel>
    {
        public Guid UserId { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        public string Name { get; set; }
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Incorrect email format.")]
        public string Email { get; set; }

        [Required]
        public UserGender Gender { get; set; }

        [Required]

        [RegularExpression(@"^(\d{12}|\d{16})$", ErrorMessage = "Not a valid adhar number")]
        public string Aadhar { get; set; }
        public string LoginId { get; set; }
   
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirmation Password is required.")]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        [Obsolete]
        public string ConfirmPassword { get; set; }

        [Required]
        public UserStatus Status { get; set; }
        [Required]
        [Display(Name = "User Type")]
        public UserType UserType { get; set; }
        public List<SelectListItem> UserTypeList { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        [Display(Name = "Role")]
        public Guid UserRoleId { get; set; }
        [Display(Name = "AG-Department")]
        public Guid? AGDepartmentId { get; set; }
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        public UserRoleMvcModel UserRole { get; set; }


        public AGDepartmentMvcModel AGDepartment { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }

        public Guid? DesignationId { get; set; }
        public DesignationResponseModel Designation { get; set; }

        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        public RegionMvcModel Region { get; set; }

        public LoginAS LoginAs { get; set; }
        public override UserMvcModel EntityToResponse(User entity, UserMvcModel model)
        {
            model.UserRole = UserRoleMvcModel.CopyFromEntity(entity.UserRole, 0);
            model.AGDepartment = AGDepartmentMvcModel.CopyFromEntity(entity.AGDepartment, 0);
            model.GovermentDepartment = GovermentDepartmentMvcModel.CopyFromEntity(entity.GovernmentDepartment, 0);
            model.Designation = DesignationResponseModel.CopyFromEntity(entity.Designation, 0);
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, 0);
            return model;
        }
    }
    public class UserMvcModel : BaseEntityModel<User, UserMvcModel>
    {
        public Guid UserId { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        public string Name { get; set; }
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(RegularExpressionHelper.PhoneNoValidationExpression, ErrorMessage = "Not a valid phone number")]
        public string Phone { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Incorrect email format.")]
        public string Email { get; set; }
        public UserGender Gender { get; set; }
          

        [RegularExpression(@"^(\d{12}|\d{16})$", ErrorMessage = "Not a valid adhar number")]
        public string Aadhar { get; set; }
        public string LoginId { get; set; }
    
        [RegularExpression(@"^[a-zA-Z0-9'@&#.\s]{7,10}$", ErrorMessage = "Password length must be between 7 to 10 characters which contain at least one numeric digit and a special character")]
        public string Password { get; set; }

        [Required]
        public UserStatus Status { get; set; }
  
        [Display(Name = "User Type")]
        public UserType UserType { get; set; }
        public List<SelectListItem> UserTypeList { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        [Display(Name = "Role")]
        public Guid UserRoleId { get; set; }
        [Display(Name = "AG-Department")]
        public Guid? AGDepartmentId { get; set; }
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        public UserRoleMvcModel UserRole { get; set; }


        public AGDepartmentMvcModel AGDepartment { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }

        public Guid? DesignationId { get; set; }
        public DesignationResponseModel Designation { get; set; }
      
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        public RegionMvcModel Region { get; set; }

        public LoginAS LoginAs { get; set; }
        public override UserMvcModel EntityToResponse(User entity, UserMvcModel model)
        {
            model.UserRole = UserRoleMvcModel.CopyFromEntity(entity.UserRole, 0);
            model.AGDepartment = AGDepartmentMvcModel.CopyFromEntity(entity.AGDepartment, 0);
            model.GovermentDepartment = GovermentDepartmentMvcModel.CopyFromEntity(entity.GovernmentDepartment, 0);
            model.Designation = DesignationResponseModel.CopyFromEntity(entity.Designation, 0);
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, 0);
            return model;
        }
        [Display(Name = "Enrollment No")]
        public string EnrollmentNo { get; set; }
    }


    public class UserFilterMvcModel : BaseEntityModel<User, UserFilterMvcModel>
    {

        [Display(Name = "Show Result")]
        public ShowPage Paging { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Role")]
        public Guid? UserRoleId { get; set; }

        [Display(Name = "AG-Department")]
        public Guid? AGDepartmentId { get; set; }

        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }

        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }

        [Display(Name = "User Type")]
        public UserType UserType { get; set; }
       
        public List<SelectListItem> UserTypeList { get; set; }
        public UserRoleMvcModel UserRole { get; set; }
        public AGDepartmentMvcModel AGDepartment { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }


        public RegionMvcModel Region { get; set; }
            public override UserFilterMvcModel EntityToResponse(User entity, UserFilterMvcModel model)
        {
            return model;
        }

        public ICollection<UserMvcModel> UserData { get; set; }


    }

    public enum LoginAS
    {
        AOR = 0,
        InPerson = 1
    }

}