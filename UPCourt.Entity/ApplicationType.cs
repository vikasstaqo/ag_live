﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public class ApplicationType
    {
        public Guid ApplicationTypeID { get; set; }
        public string ATName { get; set; }
        public bool IsActive { get; set; }
       
    }
}
