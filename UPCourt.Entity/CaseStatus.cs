﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifiedBy))]
    public class CaseStatus : BaseMasterEntity
    {
        public CaseStatus() { Cases = new HashSet<Case>(); }
        public Guid CaseStatusId { get; set; }
        public string Name { get; set; }
        public string HexColorCode { get; set; }
        public CaseStatusType Type { get; set; }
        public ICollection<Case> Cases { get; set; }
        public bool ShowOnDashboard { get; set; }
    }

    public enum CaseStatusType
    {
        Normal = 0,
        Dispose = 1,
        Pending = 2,
    }
}
