﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DocumentName), UserNameProp: nameof(CreatedBy))]
    public class DocumentMaster : BaseMasterEntity
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
    }
}
