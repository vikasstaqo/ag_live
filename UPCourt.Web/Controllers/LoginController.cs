﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class LoginController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }

        public LoginController() : this(new CustomUserManager())
        {
        }

        public LoginController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }


        //
        // GET: /Login
        [AllowAnonymous]
        public ActionResult PublicLogin(string returnUrl, string Region)
        {
            ViewBag.ReturnUrl = returnUrl;
            TempData["Region"] = Region;
            return View();
        }


        // POST: /PublicLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PublicLogin(LoginViewModel model, string returnUrl)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.LoginId == model.LoginName);
            if (dbUser == null)
            {
                ModelState.AddModelError(nameof(LoginViewModel.LoginName), "User does not exists.");
                return View(model);
            }
            if (dbUser != null)
            {
                var tempGivenHash = PasswordHelper.EncodePassword(model.Password, dbUser.PasswordSalt);

                if (dbUser.PasswordHash != tempGivenHash)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Password), "Invalid user name or password.");
                    return View(model);
                }
                if (dbUser.Status != UserStatus.Active)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.LoginName), "User is not active, please talk to admins");
                    return View(model);
                }
            }

            var UserRoleName = UserRoleDataService.GetLite(db).Where(x => x.UserRoleId == dbUser.UserRoleId).FirstOrDefault();

            DateTime LastSignIn = new DateTime();

            UserLogInLog lastLog = UserLogInLogDataService.GetLite(db).FirstOrDefault(i => i.UserId == dbUser.UserId && !i.IsExpired);
            if (lastLog != null)
            {
                lastLog.SignOut = Extended.CurrentIndianTime;
                lastLog.IsExpired = true;
                lastLog.IsForcedExpired = true;
                LastSignIn = lastLog.SignIn;
            }
            else
            {
                LastSignIn = DateTime.Now;
            }
            UserLogInLog newlog = new UserLogInLog
            {
                UserLogInLogId = Guid.NewGuid(),
                UserId = dbUser.UserId,
                ApiSessionToken = Guid.NewGuid(),
                SignIn = Extended.CurrentIndianTime
            };
            dbUser.LastLogin = LastSignIn;
            db.UserLogInLogs.Add(newlog);
            db.SaveChanges();
            string RegionName = Convert.ToString(TempData["Region"]);
            if (RegionName != null && RegionName != "")
            {
                var Region = RegionDataService.GetLite(db).Where(x => x.Name.Contains(RegionName)).FirstOrDefault();
                dbUser.RegionId = Region.RegionId;
                db.SaveChanges();
            }
            else
            {
                var Region = RegionDataService.GetLite(db).Where(x => x.Name.Contains("Allahabad")).FirstOrDefault();
                dbUser.RegionId = Region.RegionId;
                db.SaveChanges();
            }

            var apUser = new ApplicationUser
            {
                Id = dbUser.UserId.ToString(),
                UserName = dbUser.Name,
                UserType = dbUser.UserType,
                TempRegionId = dbUser.RegionId
            };

            UserLogInLog lastLogin = UserLogInLogDataService.GetLite(db).OrderBy(x => x.SignIn).LastOrDefault(i => i.UserId == dbUser.UserId);
                      await SignInAsync(apUser, model.RememberMe);

            if (UserRoleName.UserSystemRoleId == UserSystemRole.Admin || UserRoleName.UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                return RedirectToAction("Index", "Dashboard");
            }

            else if (UserRoleName.UserSystemRoleId == UserSystemRole.Guest)
            {
                return RedirectToAction("List", "Freshcases");
            }
            else
            {
                return RedirectToAction("Index", "Registration");
            }
        }


        //
        // GET: /Login
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                if (ModelState["Password"].Errors.Any() && model.OTPVerify)
                {
                    ModelState["Password"].Errors[0] = new ModelError("The OTP field is required.");
                }
                return View(model);
            }

            var dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.Email == model.LoginName);
            if (model.OTPVerify)
                dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.Phone == model.LoginName);
            if (dbUser == null)
            {
                ModelState.AddModelError(nameof(LoginViewModel.LoginName), "User does not exists !!.");
                return View(model);
            }
            if (dbUser != null)
            {
                if (!Request.Url.Host.ToLower().Contains("localhost"))
                {
                    if (model.OTPVerify)
                    {
                        ///TODO: When OTP option is done then work with OTP
                        if (model.Password != Session["LoginOTP"].ToString())
                        {
                            ModelState.AddModelError(nameof(LoginViewModel.Password), "Invalid OTP !!.");
                            return View(model);
                        }
                    }
                    else
                    {
                        var tempGivenHash = PasswordHelper.EncodePassword(model.Password, dbUser.PasswordSalt);

                        if (dbUser.PasswordHash != tempGivenHash)
                        {
                            ModelState.AddModelError(nameof(LoginViewModel.Password), "Invalid password !!.");
                            return View(model);
                        }
                    }
                }
                if (dbUser.Status != UserStatus.Active)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.LoginName), "User is not active, please talk to admins");
                    return View(model);
                }
            }
          

            DateTime LastSignIn = new DateTime();

            UserLogInLog lastLog = UserLogInLogDataService.GetLite(db).FirstOrDefault(i => i.UserId == dbUser.UserId && !i.IsExpired);
            if (lastLog != null)
            {
                lastLog.SignOut = Extended.CurrentIndianTime;
                lastLog.IsExpired = true;
                lastLog.IsForcedExpired = true;
                LastSignIn = lastLog.SignIn;
            }
            else
            {
                LastSignIn = DateTime.Now;
            }
            UserLogInLog newlog = new UserLogInLog
            {
                UserLogInLogId = Guid.NewGuid(),
                UserId = dbUser.UserId,
                ApiSessionToken = Guid.NewGuid(),
                SignIn = Extended.CurrentIndianTime
            };

            dbUser.LastLogin = LastSignIn;
            db.UserLogInLogs.Add(newlog);
            db.SaveChanges();

            var apUser = new ApplicationUser
            {
                Id = dbUser.UserId.ToString(),
                UserName = dbUser.Name,
                UserType = dbUser.UserType,
                TempRegionId = dbUser.RegionId,
                LastLogin = LastSignIn
            };


            await SignInAsync(apUser, model.RememberMe);
          
            return RedirectToLocal(returnUrl);
        }
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {

            //TODO: clear catch of stored
            AuthenticationManager.SignOut(Startup.CokkieName);

            var identity = await CustomUserManager.CreateIdentityAsync(user, Startup.CokkieName);

            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }


        // POST: /Login
        [HttpPost]
        public string SetRegion(string Regionid)
        {
            DateTime LastSignIn = new DateTime();
            UserLogInLog lastLog = UserLogInLogDataService.GetLite(db).FirstOrDefault(i => i.UserId == AppUser.UserId && !i.IsExpired);
            if (lastLog != null)
            {
                LastSignIn = lastLog.SignIn;
            }
            else
            {
                LastSignIn = DateTime.Now;
            }

            var apUser = new ApplicationUser
            {
                Id = AppUser.UserId.ToString(),
                UserName = AppUser.UserName,
                UserType = AppUser.UserType,
                TempRegionId = Guid.Parse(Regionid),
                LastLogin = LastSignIn
            };


            _ = SignInAsync(apUser, true);
            return "Done";
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (CustomUserManager != null)
                {
                    CustomUserManager.Dispose();
                    CustomUserManager = null;
                }


            }

            base.Dispose(disposing);
        }
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "DashBoard");
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(Startup.CokkieName);
            return RedirectToAction("Index", "Home");
        }

        private string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {

            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }

        string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };


        [HttpGet]
        [AllowAnonymous]
        public bool SendOTP(string MobileNo)
        {
            bool result = false;
            string otp = GenerateRandomOTP(6, saAllowedCharacters);
            HttpClient client = new HttpClient() { BaseAddress = new Uri("http://3.109.141.25:90/") };
            client.Timeout = TimeSpan.FromMinutes(10);
            var Apiresult = client.GetAsync("sms-template1.php?mobile=" + MobileNo + "&otp=" + otp + "");
            Apiresult.Wait();
            var content = Apiresult.Result.Content.ReadAsStringAsync();
            if (content != null)
            {
                result = true;
                Session["LoginOTP"] = otp;
            }

            return result;
        }
    }
}
