﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class ApplicationDocumentDataService
    {
        public static IQueryable<ApplicationDocument> GetLite(DatabaseContext db)
        {
            return db.ApplicationDocuments;
        }
        public static IQueryable<ApplicationDocument> GetDetail(DatabaseContext db)
        {


            var applicationdocuments = db.ApplicationDocuments
             .Include(i => i.ECourtFees)
            .Include(i => i.BasicIndexs).ThenInclude(x => x.Documents);

            return applicationdocuments;
        }

        public static IQueryable<ApplicationDocument> GetallDetail(DatabaseContext db)
        {
            var applicationdocuments = (from s in db.ApplicationDocuments
                                          join sa in db.ApplicationTypes on s.ApplicationTypeID equals sa.ApplicationTypeID
                                      
                                          select new
                                          {
                                              ApplicationTypeID = sa.ATName,
                                              ApplicantName = s.ApplicantName,
                                              FinalStatus = s.FinalStatus,
                                              ApplicationDocumentID = s.ApplicationDocumentID
                                          });

            return (IQueryable<ApplicationDocument>)applicationdocuments;
        }
    }


}