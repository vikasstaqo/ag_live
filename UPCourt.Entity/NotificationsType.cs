﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Type), UserNameProp: nameof(Type))]
    public class NotificationsType
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int NotificationTypeId { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
