﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class AGDepartmentController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(AGDepartmentMvcModel.CopyFromEntityList(AGDepartmentDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(AGDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: AGDepartment/Create
        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new AGDepartmentMvcModel();
            model.Status = DepartmentStatus.Active;
            return View(model);
        }

        // POST: AGDepartment/Create
        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(AGDepartmentMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            AGDepartment tempAGDepartment = new AGDepartment();
            tempAGDepartment.AGDepartmentId = Guid.NewGuid();
            tempAGDepartment.DisplayOrder = MasterBusinessService.GetAGDepartmentDisplayOrder(db);
            tempAGDepartment.CreatedBy = AppUser.UserId;
            tempAGDepartment.CreatedOn = DateTime.Now;
            InitializeData(tempAGDepartment, model);

            db.AGDepartments.Add(tempAGDepartment);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        // GET: AGDepartment/Edit/5
        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(AGDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: AGDepartment/Edit/5
        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(AGDepartmentMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == model.AGDepartmentId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            InitializeData(dbData, model);
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.AGDepartment, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
        private void InitializeData(AGDepartment tempAGDepartment, AGDepartmentMvcModel model)
        {
            tempAGDepartment.Name = model.Name;
            tempAGDepartment.DepartmentHeadName = model.DepartmentHeadName;
            tempAGDepartment.Status = model.Status;
            tempAGDepartment.IsDefault = model.IsDefault;
        }
        private bool CommonForInsertUpdate(AGDepartmentMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = AGDepartmentDataService.GetLite(db).FirstOrDefault(i => i.AGDepartmentId != model.AGDepartmentId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "AG-Department name already exists");
                return false;
            }
            return true;
        }
    }
}
