﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeCreateController : BaseMVCController
    {

        // GET: NoticeCreate
        public ActionResult Index()
        {

            InitilizeGovermentDepartments(null);
            InitilizeGovDesignations(null);
            InitilizeRegions(AppUser.User.RegionId);
            InitilizeCaseNoticeTypes(null, 0);
            InitilizeIPCSections(null);
            InitilizeDistrict(null);
            InitilizecaseDistrict(null);
            InitilizeSubject(null, null);
            InitilizePoliceStations(null, null);
            InitilizeGroups(null);

            var model = new NoticeMvcModel
            {
                Petitioners = new List<PetitionerMvcModel> { new PetitionerMvcModel() { IsFirst = true } },
                NoticeRespondents = new List<NoticeRespondentMvcModel> { new NoticeRespondentMvcModel() { IsFirst = true } },
                NoticeBasicIndexs = new List<NoticeBasicIndexMvcModel> { new NoticeBasicIndexMvcModel() },
                NoticeCounselors = new List<NoticeCounselorMvcModel> { new NoticeCounselorMvcModel() },
                NoticeIPCs = new List<NoticeIPCMvcModel> { new NoticeIPCMvcModel() },
                NoticeActTypes = new List<NoticeActTypeMvcModel> { new NoticeActTypeMvcModel() },
                NoticeDate = DateTime.Now,
                NoticeYear = DateTime.Now.Year,
                            };
            return View(model);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(NoticeMvcModel model)
        {
            InitilizeGovermentDepartments(model.NoticeRespondents.FirstOrDefault().GovernmentDepartmentId);
            InitilizeGovDesignations(model.NoticeRespondents.FirstOrDefault().GovernmentDesignationId);
            InitilizeRegions(model.RegionId);
            InitilizeCaseNoticeTypes(model.CaseNoticeTypeId, (int)model.NoticeType);
            InitilizeSubject(model.Subject, model.CaseNoticeTypeId);
            InitilizeIPCSections(null);
            InitilizeDistrict(model.CrimeDetail.District);
            InitilizecaseDistrict(model.CrimeDetail.District);
            InitilizePoliceStations(model.CrimeDetail.District, model.CrimeDetail.PoliceStationID);
          
            InitilizeGroups(model.GroupID);

            int FPetiner = 1;
            int FCounseler = 1;
            int FRespondent = 1;
            int FIpcs = 1;

            Notice Insert_NoticeModel = new Notice
            {
                NoticeId = Guid.NewGuid(),
                Status = NoticeStatus.Pending,
                NoticeType = model.ParentNoticeType,
                CaseNoticeTypeId = model.CaseNoticeTypeId,
                SerialNo = NoticeBusinessService.GetSerailNo(db),
                NoticeNo = model.NoticeNo, 
                NoticeDate = model.NoticeDate,
                NoticeCreateDate = Extended.CurrentIndianTime,
                NoticeYear = model.NoticeYear,
                Subject = model.Subject,
                IsContextOrder = model.IsContextOrder,
                RegionId = model.RegionId,
                GroupID = model.GroupID,
                CreatedBy = AppUser.UserId,
                CreatedOn = Extended.CurrentIndianTime,
                NoticeFrom = NoticeFrom.InternalSystem,
                ReferenceCaseNo = model.ReferenceCaseNo
            };



            //Petitioner
            foreach (var Item_PetitionerModel in model.Petitioners.Where(x => x.bActive == true))
            {
                Petitioner Insert_Petitioner = new Petitioner
                {
                    PetitionerId = Guid.NewGuid(),
                    NoticeId = Insert_NoticeModel.NoticeId,
                    Name = Item_PetitionerModel.Name,
                    Address = Item_PetitionerModel.Address,
                    ContactNo = Item_PetitionerModel.ContactNo,
                    Email = Item_PetitionerModel.Email,
                    District = Item_PetitionerModel.District,
                    CaseDistrict = Item_PetitionerModel.CaseDistrict,
                    IDProof = Item_PetitionerModel.IDProof,
                    IDProofNumber = Item_PetitionerModel.IDProofNumber,
                    IsFirst = FPetiner == 1,
                    ModifyBy = AppUser.UserName,
                    SNo = FPetiner
                };
                Insert_NoticeModel.Petitioners.Add(Insert_Petitioner);
                FPetiner += 1;
            }

            foreach (var Item_CounselorModel in model.NoticeCounselors.Where(x => x.bActive == true))
            {
                NoticeCounselor Insert_NoticeCounselor = new NoticeCounselor
                {
                    NoticeCounselorId = Guid.NewGuid(),
                    NoticeId = Insert_NoticeModel.NoticeId,
                    Name = Item_CounselorModel.Name,
                    Email = Item_CounselorModel.Email,
                    ContactNo = Item_CounselorModel.ContactNo,
                    RollNo = Item_CounselorModel.RollNo,
                    EnrollmentNo = Item_CounselorModel.EnrollmentNo,
                    ModifyBy = AppUser.UserName,
                    SNo = FCounseler
                };
                Insert_NoticeModel.Counselors.Add(Insert_NoticeCounselor);
                FCounseler += 1;
            }

            if (!model.NoticeRespondents.IsEmptyCollection())
            {
                foreach (var item_NoticeRespondents in model.NoticeRespondents.Where(x => x.bActive == true))
                {
                    PartyAndRespondent InsertPartyAndRespondent = new PartyAndRespondent();
                    InsertPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    InsertPartyAndRespondent.NoticeRespondentId = Insert_NoticeModel.NoticeId;
                    InsertPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    InsertPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    InsertPartyAndRespondent.Address = item_NoticeRespondents.Address;
                    InsertPartyAndRespondent.District = item_NoticeRespondents.District;
                    InsertPartyAndRespondent.ModifyBy = AppUser.UserName;
                    InsertPartyAndRespondent.IsFirst = FRespondent == 1;
                    if (!item_NoticeRespondents.IsPartyDepartment)
                    {
                        InsertPartyAndRespondent.PartyType = PartyType.Individual;
                        InsertPartyAndRespondent.PartyName = item_NoticeRespondents.PartyName;
                        InsertPartyAndRespondent.ContactNo = item_NoticeRespondents.ContactNo;
                        InsertPartyAndRespondent.Email = item_NoticeRespondents.Email;
                    }
                    if (item_NoticeRespondents.IsPartyDepartment)
                    {
                        InsertPartyAndRespondent.PartyType = PartyType.Department;
                        InsertPartyAndRespondent.GovernmentDepartmentId = item_NoticeRespondents.GovernmentDepartmentId;
                        InsertPartyAndRespondent.GovernmentDesignationId = item_NoticeRespondents.GovernmentDesignationId;
                    }
                    InsertPartyAndRespondent.SNo = FRespondent;
                    Insert_NoticeModel.Respondents.Add(InsertPartyAndRespondent);
                    FRespondent += 1;
                }
            }
            if (!model.NoticeBasicIndexs.IsEmptyCollection())
            {
                int OrderNo = 1;
                foreach (var item_NoticeBasicIndex in model.NoticeBasicIndexs.Where(x => x.bActive == true && x.Particulars != null && x.Particulars != ""))
                {
                    NoticeBasicIndex Insert_NoticeBasicIndex = new NoticeBasicIndex
                    {
                        NoticeBasicIndexId = Guid.NewGuid(),
                        NoticeId = Insert_NoticeModel.NoticeId,
                        Particulars = item_NoticeBasicIndex.Particulars,
                        Annexure = item_NoticeBasicIndex.Annexure,
                        IndexDate = item_NoticeBasicIndex.IndexDate
                    };
                    Insert_NoticeBasicIndex.IndexDate = item_NoticeBasicIndex.IndexDate;
                    Insert_NoticeBasicIndex.NoOfPages = 10;
                    if (item_NoticeBasicIndex.BasicIndexFile.ContentLength > 0)
                    {
                        Document Insert_Doc = new Document
                        {
                            DocumentId = Guid.NewGuid(),
                            NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId,
                            Name = Insert_NoticeBasicIndex.Annexure,
                            DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile),
                            FileType = DocumentFileType.Any,
                            EntityType = DocumentEntityType.CaseHearing,
                            Ordinal = OrderNo,
                            Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength,
                            Status = DocumentStatus.Added,
                            CreatedBy = AppUser.UserId
                        };
                        OrderNo++;
                        Insert_NoticeBasicIndex.Documents.Add(Insert_Doc);

                    }
                    Insert_NoticeModel.BasicIndexs.Add(Insert_NoticeBasicIndex);
                }
            }

            Guid crimeGuid = Guid.Empty;
            bool blnCrimeDetailAvailable = false;

            if (model.CrimeDetail != null && model.CrimeDetail.District != null)
            {
                if (model.CrimeDetail.District.Length > 0)
                {
                    blnCrimeDetailAvailable = true;
                    crimeGuid = Guid.NewGuid();
                    CrimeDetail crimeDetail = new CrimeDetail()
                    {
                        CrimeDetailID = crimeGuid,
                        CaseNo = model.CrimeDetail.CaseNo,
                        CaseNo_Court = model.CrimeDetail.CaseNo_Court,
                        CrimeNo = model.CrimeDetail.CrimeNo,
                        CrimeYear = model.CrimeDetail.CrimeYear,
                        District = model.CrimeDetail.District,
                        TrailNo_Court = model.CrimeDetail.TrailNo_Court,
                        FIRDate = model.CrimeDetail.FIRDate,
                        FIRNO = model.CrimeDetail.FIRNO,
                        IsActive = true,
                        NoticeId = Insert_NoticeModel.NoticeId,
                        PoliceStationID = model.CrimeDetail.PoliceStationID,
                        TrailNo = model.CrimeDetail.TrailNo,
                        CreatedBy = AppUser.UserId
                    };

                    Insert_NoticeModel.SectionOrAct = model.CrimeDetail.SectionOrAct;
                    Insert_NoticeModel.CrimeDetail = crimeDetail;
                }
             
            }

            if (!model.CrimeDetail.IPCSectionIdS.IsEmptyCollection())
            {
                foreach (var NoticeIPCGuid in model.CrimeDetail.IPCSectionIds_Selected)
                {
                    NoticeIPC Insert_NoticeIpc = new NoticeIPC
                    {
                        NoticeIpcId = Guid.NewGuid(),
                        NoticeId = Insert_NoticeModel.NoticeId,
                        IPCSectionId = NoticeIPCGuid,
                        SNo = FIpcs,
                        ModifyBy = AppUser.UserName
                    };
                    if (blnCrimeDetailAvailable)
                        Insert_NoticeIpc.CrimeDetailId = crimeGuid;

                    Insert_NoticeModel.IPCs.Add(Insert_NoticeIpc);
                    FIpcs += 1;
                }
            }

            
            db.Notices.Add(Insert_NoticeModel);
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string description = $"Notice '{ Insert_NoticeModel.NoticeNo }' Saved successfully";
            TimelineBusinessService.SaveTimeLine(db, Insert_NoticeModel.NoticeId, null, EventType.NoticeCreated, AppUser.UserId, description, null);

            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateNoticeCreateNotification(Insert_NoticeModel);



            DisplayMessage(MessageType.Success, $"Notice '{Insert_NoticeModel.NoticeNo}' Saved successfully");
            return RedirectToAction("Index");
        }
        string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                
                int FileSize = FileUpload.ContentLength;
                if (FileSize > 110000000)
                {
                    return "Maximum file size 100 mb will be uploaded";
                }
                else
                {
                    string fileName = Path.GetFileName(FileUpload.FileName);
                    Guid guid;
                    guid = Guid.NewGuid();

                    string UploadName = guid + "_" + fileName;
                    string FilePath = Path.Combine(Server.MapPath("~/Document/Notice/"), UploadName);
                    FileUpload.SaveAs(FilePath);
                    return UploadName;
                }
            }

            return "";
        }

        public ActionResult Edit(Guid id)
        {
            var dbData = NoticeDataService.GetDetail(db).Where(i => i.NoticeId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            var model = NoticeMvcModel.CopyFromEntity(dbData, 0);
            if (model.Petitioners.Count == 0)
                model.Petitioners = new List<PetitionerMvcModel> { new PetitionerMvcModel() { IsFirst = true } };
            if (model.NoticeRespondents.Count == 0)
                model.NoticeRespondents = new List<NoticeRespondentMvcModel> { new NoticeRespondentMvcModel() { IsFirst = true } };
            if (model.NoticeBasicIndexs.Count == 0)
                model.NoticeBasicIndexs = new List<NoticeBasicIndexMvcModel> { new NoticeBasicIndexMvcModel() };
            if (model.NoticeCounselors.Count == 0)
                model.NoticeCounselors = new List<NoticeCounselorMvcModel> { new NoticeCounselorMvcModel() };
            if (model.NoticeIPCs.Count == 0)
                model.NoticeIPCs = new List<NoticeIPCMvcModel> { new NoticeIPCMvcModel() };
            if (model.NoticeActTypes.Count == 0)
                model.NoticeActTypes = new List<NoticeActTypeMvcModel> { new NoticeActTypeMvcModel() };

            string dis = "";
            object policeId = null;
            if (model.CrimeDetail != null)
            {
                dis = dbData.CrimeDetail.District;
                policeId = dbData.CrimeDetail.PoliceStationID;
                model.CrimeDetail.SectionOrAct = dbData.SectionOrAct;
            }
            else
            {
                model.CrimeDetail = new CrimeDetailMvcModel();
            }
            InitilizeGovermentDepartments(null);
            InitilizeGovDesignations(null);
            InitilizeRegions(dbData.RegionId);
            InitilizeCaseNoticeTypes(dbData.CaseNoticeTypeId, (int)dbData.NoticeType);
            InitilizeSubject(dbData.Subject, dbData.CaseNoticeTypeId);
            InitilizeIPCSections(null);
            InitilizeDistrict(dis);
            InitilizecaseDistrict(dis);
            InitilizePoliceStations(dis, policeId);
         
            InitilizeGroups(model.GroupID);
            model.ParentNoticeType = dbData.NoticeType;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(NoticeMvcModel model)
        {
            var dbData = NoticeDataService.GetDetail(db).Where(i => i.NoticeId == model.NoticeId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }

            int FPetiner = 1;
            int FRespondent = 1;
            int FCounselor = 1;
            int FIpcs = 1;
            int FActTypes = 1;
            dbData.Status = model.Status;
            dbData.NoticeType = model.ParentNoticeType;
            dbData.CaseNoticeTypeId = model.CaseNoticeTypeId;
            dbData.NoticeNo = model.NoticeNo; 
            dbData.NoticeDate = model.NoticeDate;
            dbData.Subject = model.Subject;
            dbData.IsContextOrder = model.IsContextOrder;
            dbData.RegionId = model.RegionId;
            dbData.GroupID = model.GroupID;
            dbData.CreatedBy = AppUser.UserId;
            dbData.CreatedOn = Extended.CurrentIndianTime;
            dbData.ReferenceCaseNo = model.ReferenceCaseNo;
            //Petitioner
            //Remove Existing Deleted record
            foreach (var Deleted_PetitionerModel in model.Petitioners.Where(x => x.bActive == false))
            {
                if (Deleted_PetitionerModel.PetitionerId != null)
                {
                    Petitioner petitioner = dbData.Petitioners.Where(p => p.PetitionerId == Deleted_PetitionerModel.PetitionerId && (p.NoticeId == dbData.NoticeId)).FirstOrDefault();
                    dbData.Petitioners.Remove(petitioner);
                }
            }
            foreach (var Item_PetitionerModel in model.Petitioners.Where(x => x.bActive == true))
            {
                bool newPetitioner = false;
                Petitioner petitioner = dbData.Petitioners.Where(p => p.PetitionerId == Item_PetitionerModel.PetitionerId && (p.NoticeId == dbData.NoticeId)).FirstOrDefault();
                if (petitioner == null)
                {
                    newPetitioner = true;
                    petitioner = new Petitioner();
                    petitioner.NoticeId = dbData.NoticeId;
                    petitioner.PetitionerId = Guid.NewGuid();
                }
                if (petitioner.NoticeId == null || petitioner.NoticeId == Guid.Empty)
                    petitioner.NoticeId = dbData.NoticeId;

                petitioner.Name = Item_PetitionerModel.Name;
                petitioner.Address = Item_PetitionerModel.Address;
                petitioner.ContactNo = Item_PetitionerModel.ContactNo;
                petitioner.Email = Item_PetitionerModel.Email;
                petitioner.District = Item_PetitionerModel.District;
                petitioner.CaseDistrict = Item_PetitionerModel.CaseDistrict;
                petitioner.IDProof = Item_PetitionerModel.IDProof;
                petitioner.IDProofNumber = Item_PetitionerModel.IDProofNumber;
                petitioner.IsFirst = FPetiner == 1;
                petitioner.SNo = FPetiner;
                petitioner.ModifyBy = AppUser.UserName;

                if (newPetitioner)
                    dbData.Petitioners.Add(petitioner);

                FPetiner = FPetiner + 1;
            }

            //Counselor
            //Remove Existing Deleted record
            foreach (var Deleted_CounselorsModel in model.NoticeCounselors.Where(x => x.bActive == false))
            {
                if (Deleted_CounselorsModel.NoticeCounselorId != null)
                {
                    NoticeCounselor Counselor = dbData.Counselors.Where(p => p.NoticeCounselorId == Deleted_CounselorsModel.NoticeCounselorId && (p.NoticeId == dbData.NoticeId)).FirstOrDefault();
                    dbData.Counselors.Remove(Counselor);
                }
            }
            foreach (var Item_CounselorModel in model.NoticeCounselors.Where(x => x.bActive == true))
            {
                bool newCounselor = false;
                NoticeCounselor counselor = dbData.Counselors.Where(c => c.NoticeCounselorId == Item_CounselorModel.NoticeCounselorId).FirstOrDefault();
                if (counselor == null)
                {
                    newCounselor = true;
                    counselor = new NoticeCounselor
                    {
                        NoticeCounselorId = Guid.NewGuid(),
                        NoticeId = dbData.NoticeId,
                    };
                }
                counselor.Name = Item_CounselorModel.Name;
                counselor.Email = Item_CounselorModel.Email;
                counselor.ContactNo = Item_CounselorModel.ContactNo;
                counselor.RollNo = Item_CounselorModel.RollNo;
                counselor.EnrollmentNo = Item_CounselorModel.EnrollmentNo;
                counselor.ModifyBy = AppUser.UserName;
                counselor.SNo = FCounselor;

                if (newCounselor)
                    dbData.Counselors.Add(counselor);
                FCounselor += 1;
            }


            //Remove Existing Deleted record
            foreach (var Deleted_RespondentModel in model.NoticeRespondents.Where(x => x.bActive == false))
            {
                if (Deleted_RespondentModel.PartyAndRespondentId != null)
                {
                    PartyAndRespondent Respondent = dbData.Respondents.Where(p => p.PartyAndRespondentId == Deleted_RespondentModel.PartyAndRespondentId && (p.NoticeRespondentId == dbData.NoticeId)).FirstOrDefault();
                    dbData.Respondents.Remove(Respondent);
                }
            }
            foreach (var item_NoticeRespondents in model.NoticeRespondents.Where(x => x.bActive == true))
            {
                bool newPartyAndRespondent = false;
                PartyAndRespondent InsertPartyAndRespondent = dbData.Respondents.Where(r => r.PartyAndRespondentId == item_NoticeRespondents.PartyAndRespondentId).FirstOrDefault();
                if (InsertPartyAndRespondent == null)
                {
                    newPartyAndRespondent = true;
                    InsertPartyAndRespondent = new PartyAndRespondent();
                    InsertPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    InsertPartyAndRespondent.NoticeRespondentId = dbData.NoticeId;
                }

                InsertPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                InsertPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;

                if (!item_NoticeRespondents.IsPartyDepartment)
                {
                    InsertPartyAndRespondent.PartyType = PartyType.Individual;
                    InsertPartyAndRespondent.PartyName = item_NoticeRespondents.PartyName;
                    InsertPartyAndRespondent.ContactNo = item_NoticeRespondents.ContactNo;
                    InsertPartyAndRespondent.Email = item_NoticeRespondents.Email;
                }
                if (item_NoticeRespondents.IsPartyDepartment)
                {
                    InsertPartyAndRespondent.PartyType = PartyType.Department;
                    InsertPartyAndRespondent.GovernmentDepartmentId = item_NoticeRespondents.GovernmentDepartmentId;
                    InsertPartyAndRespondent.GovernmentDesignationId = item_NoticeRespondents.GovernmentDesignationId;
                }
                InsertPartyAndRespondent.Address = item_NoticeRespondents.Address;
                InsertPartyAndRespondent.District = item_NoticeRespondents.District;
                InsertPartyAndRespondent.ModifyBy = AppUser.UserName;
                InsertPartyAndRespondent.IsFirst = FRespondent == 1;
                InsertPartyAndRespondent.SNo = FRespondent;

                if (newPartyAndRespondent)
                    dbData.Respondents.Add(InsertPartyAndRespondent);
                FRespondent += 1;
            }


            if (!model.NoticeBasicIndexs.IsEmptyCollection())
            {
                int OrderNo = 1;
                foreach (var item_NoticeBasicIndex in model.NoticeBasicIndexs.Where(x => x.bActive == true && x.Particulars != null && x.Particulars != ""))
                {
                    bool newNoticeBasicIndex = false;
                    NoticeBasicIndex Insert_NoticeBasicIndex = dbData.BasicIndexs.Where(b => b.NoticeBasicIndexId == item_NoticeBasicIndex.NoticeBasicIndexId).FirstOrDefault();
                    if (Insert_NoticeBasicIndex == null)
                    {
                        newNoticeBasicIndex = true;
                        Insert_NoticeBasicIndex = new NoticeBasicIndex();
                        Insert_NoticeBasicIndex.NoticeBasicIndexId = Guid.NewGuid();
                        Insert_NoticeBasicIndex.NoticeId = dbData.NoticeId;
                    }

                    Insert_NoticeBasicIndex.Particulars = item_NoticeBasicIndex.Particulars;
                    Insert_NoticeBasicIndex.Annexure = item_NoticeBasicIndex.Annexure;
                    Insert_NoticeBasicIndex.IndexDate = item_NoticeBasicIndex.IndexDate;
                    Insert_NoticeBasicIndex.IndexDate = item_NoticeBasicIndex.IndexDate;
                    Insert_NoticeBasicIndex.NoOfPages = 100;

                    bool newDoc = false;
                    if (item_NoticeBasicIndex.BasicIndexFile != null && item_NoticeBasicIndex.BasicIndexFile.ContentLength > 0)
                    {
                        if (!item_NoticeBasicIndex.Documents.IsEmptyCollection())
                        {
                            if (Insert_NoticeBasicIndex.Documents.Count > 0)
                            {
                                foreach (var uDoc in Insert_NoticeBasicIndex.Documents)
                                {
                                    Document doc_Update = Insert_NoticeBasicIndex.Documents.Where(d => d.DocumentId == uDoc.DocumentId).FirstOrDefault();
                                    if (doc_Update == null)
                                    {
                                        newDoc = true;
                                        doc_Update = new Document();
                                        doc_Update.DocumentId = Guid.NewGuid();
                                        doc_Update.FileType = DocumentFileType.Any;
                                        doc_Update.EntityType = DocumentEntityType.CaseHearing;
                                        doc_Update.Ordinal = OrderNo;
                                    }


                                    doc_Update.NoticeBasicIndexId = uDoc.NoticeBasicIndexId;
                                    doc_Update.Name = uDoc.Name;
                                    doc_Update.DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile);
                                    doc_Update.Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength;
                                    doc_Update.Status = DocumentStatus.Added;
                                    doc_Update.CreatedBy = AppUser.UserId;
                                    if (newDoc)
                                        Insert_NoticeBasicIndex.Documents.Add(doc_Update);
                                    OrderNo++;
                                }
                            }
                            else
                            {
                                newDoc = true;
                                Document doc_Update = new Document();
                                doc_Update.DocumentId = Guid.NewGuid();
                                doc_Update.FileType = DocumentFileType.Any;
                                doc_Update.EntityType = DocumentEntityType.CaseHearing;
                                doc_Update.Ordinal = OrderNo;
                                doc_Update.NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId;
                                doc_Update.Name = item_NoticeBasicIndex.Documents[0].Name;
                                doc_Update.DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile);
                                doc_Update.Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength;
                                doc_Update.Status = DocumentStatus.Added;
                                doc_Update.CreatedBy = AppUser.UserId;
                                if (newDoc)
                                    Insert_NoticeBasicIndex.Documents.Add(doc_Update);
                                OrderNo++;
                            }
                        }
                        else if (item_NoticeBasicIndex.BasicIndexFile.ContentLength > 0)
                        {
                            Document Insert_Doc = new Document
                            {
                                DocumentId = Guid.NewGuid(),
                                NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId,
                                Name = Insert_NoticeBasicIndex.Annexure,
                                DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile),
                                FileType = DocumentFileType.Any,
                                EntityType = DocumentEntityType.CaseHearing,
                                Ordinal = OrderNo,
                                Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength,
                                Status = DocumentStatus.Added,
                                CreatedBy = AppUser.UserId
                            };
                            OrderNo++;
                            Insert_NoticeBasicIndex.Documents.Add(Insert_Doc);

                        }

                    }

                    if (newNoticeBasicIndex)
                        dbData.BasicIndexs.Add(Insert_NoticeBasicIndex);
                }
            }
            bool blnCrimeDetailAvailable = false;
            if (model.CrimeDetail != null && model.CrimeDetail.District != null && model.CrimeDetail.District.Length > 0)
            {

                blnCrimeDetailAvailable = true;
                if (dbData.CrimeDetail == null)
                    dbData.CrimeDetail = CreateCrimeDetailWithData(model.CrimeDetail, dbData.NoticeId);
                else
                    updateCrimeData(model.CrimeDetail, dbData.CrimeDetail);

                dbData.SectionOrAct = model.CrimeDetail.SectionOrAct;
            }



            foreach (var Deleted_ipc in NoticeIPCDataService.GetLite(db).Where(x => x.NoticeId == dbData.NoticeId))
            {
                if (Deleted_ipc.NoticeIpcId != null)
                {
                    NoticeIPC NotIpc = dbData.IPCs.Where(p => p.NoticeIpcId == Deleted_ipc.NoticeIpcId).FirstOrDefault();
                    dbData.IPCs.Remove(NotIpc);
                }
            }

            if (!model.CrimeDetail.IPCSectionIdS.IsEmptyCollection())
            {
                foreach (var NoticeIPCGuid in model.CrimeDetail.IPCSectionIds_Selected)
                {
                    NoticeIPC Insert_NoticeIpc = new NoticeIPC
                    {
                        NoticeIpcId = Guid.NewGuid(),
                        NoticeId = dbData.NoticeId,
                        IPCSectionId = NoticeIPCGuid,
                        SNo = FIpcs,
                        ModifyBy = AppUser.UserName
                    };
                    if (blnCrimeDetailAvailable)
                        Insert_NoticeIpc.CrimeDetailId = dbData.CrimeDetail.CrimeDetailID;

                    dbData.IPCs.Add(Insert_NoticeIpc);
                    FIpcs += 1;
                }
            }
                       
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateNoticeCreateNotification(dbData);
            DisplayMessage(MessageType.Success, $"Notice '{ dbData.NoticeNo }' Updated successfully");
            return RedirectToAction("Index", "NoticeCase");
        }

        private void updateCrimeData(CrimeDetailMvcModel model, CrimeDetail ent)
        {
            if (model.CrimeYear < model.FIRDate.Year)
            {

                Response.Write("<h1>Crime Year </h1>");
                ent.CaseNo = model.CaseNo;
                ent.CaseNo_Court = model.CaseNo_Court;
                ent.CrimeNo = model.CrimeNo;
                ent.CrimeYear = model.CrimeYear;
                ent.District = model.District;
                ent.TrailNo_Court = model.TrailNo_Court;
                ent.FIRDate = model.FIRDate;
                ent.FIRNO = model.FIRNO;
                ent.IsActive = true;
                ent.PoliceStationID = model.PoliceStationID;
                ent.TrailNo = model.TrailNo;
                ent.CreatedBy = AppUser.UserId;
            }
        }
        private CrimeDetail CreateCrimeDetailWithData(CrimeDetailMvcModel model, Guid NoticeId)
        {



            Response.Write("<h1>Crime Year </h1>");
            CrimeDetail crimeDetail = new CrimeDetail()
            {
                CrimeDetailID = Guid.NewGuid(),
                CaseNo = model.CaseNo,
                CaseNo_Court = model.CaseNo_Court,
                CrimeNo = model.CrimeNo,
                CrimeYear = model.CrimeYear,
                District = model.District,
                TrailNo_Court = model.TrailNo_Court,
                FIRDate = model.FIRDate,
                FIRNO = model.FIRNO,
                IsActive = true,
                NoticeId = NoticeId,
                PoliceStationID = model.PoliceStationID,
                TrailNo = model.TrailNo,
                CreatedBy = AppUser.UserId
            };

            return crimeDetail;



        }

        [HttpGet]
        public ActionResult GetDepartment(Guid? GovtDepartmentId)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == GovtDepartmentId).FirstOrDefault();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDepartmentList(int pid)
        {
            if (pid == 2)
            {
                var dbData = GovermentDepartmentDataService.GetLite(db).Where(i => i.Name.Contains("Home")).ToList();
                return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var dbData = GovermentDepartmentDataService.GetLite(db).ToList();
                return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetDepartmentDesignation(Guid? GovtDepartmentId)
        {
            var dbData = GovDesignationDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == GovtDepartmentId);
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPoliceStations(string d)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.District == d).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetNoticeTypes(int pid)
        {
            var dbData = NoticeTypeDataService.GetActive(db).Where(i => (int)i.ParentNoticeType == pid && i.IsActive == true).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSubjects(Guid ntid)
        {
            var dbData = SubjectDataService.GetLite(db).Where(i => i.CaseNoticeTypeId == ntid).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CheckNoticeNo(string NoticeNo, Guid? NoticeId)
        {
            if (NoticeId != null)
            {
                var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeNo == NoticeNo && i.NoticeId != NoticeId).FirstOrDefault();
                return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeNo == NoticeNo).FirstOrDefault();
                return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
            }
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeGovDesignations(object selectedValue)
        {
            var dbData = GovDesignationDataService.GetLite(db).OrderBy(i => i.DesignationName).ToList();
            var mvc = GovDesignationMvcModel.CopyFromEntityList(dbData, 0, 0);
            ViewBag.GovDesignations = new SelectList(mvc, nameof(GovDesignationMvcModel.GovernmentDesignationId)
                , nameof(GovDesignationMvcModel.DesignationName), selectedValue);
        }
        public void InitilizeCaseNoticeTypes(object selectedValue, int noticeType)
        {
            var dbData = NoticeTypeDataService.GetActive(db).Where(n => n.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);
        }
        public void InitilizeRegions(object selectedValue)
        {
            if (AppUser.TempRegionId != null)
            {
                var dbData = RegionDataService.GetLite(db).Where(x => x.RegionId == AppUser.TempRegionId).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }
            else
            {
                var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }
        }
        public void InitilizeIPCSections(object selectedValue)
        {
            var dbData = IPCSectionDataService.GetLite(db).Where(i => i.Status == IPCSectionStatus.Active).OrderBy(i => i.DisplayOrder).ToList();
            var mvc = IPCSectionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.IPCSections = new SelectList(mvc, nameof(IPCSection.IPCSectionId), nameof(IPCSection.SectionName), selectedValue);
            ViewBag.IPCSections_Selected = new SelectList(new List<IPCSectionMvcModel>(), nameof(IPCSection.IPCSectionId), nameof(IPCSection.SectionName), null);
        }
        public void InitilizeActTypes(object selectedValue)
        {
            var dbData = ActTypeDataService.GetLite(db).Where(x => x.ParentActBelongType == ParentActBelongType.CriminalAct).OrderBy(i => i.ActBelongName).ToList();
            var mvc = ActTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.ActTypes = new SelectList(mvc, nameof(ActType.ActTypeID), nameof(ActType.ActBelongName), selectedValue);
        }
        public void InitilizeDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
                      ViewBag.Districts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }
        public void InitilizecaseDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
            ViewBag.CaseDistricts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }
        public void InitilizeSubject(object selectedValue, Guid? CaseNoticeType)
        {
            var dbData = SubjectDataService.GetLite(db).Where(s => s.CaseNoticeTypeId == CaseNoticeType).OrderBy(i => i.Name).ToList();
            var mvc = SubjectMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Subjects = new SelectList(dbData, nameof(Subject.Name), nameof(Subject.Name), selectedValue);

        }
        private void InitilizePoliceStations(string district, object selectedValue)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(s => district == null || district.Trim() == "" || s.District == district).OrderBy(i => i.Locality).ToList();
            var mvc = PoliceStationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.PoliceStations = new SelectList(dbData, nameof(PoliceStation.PoliceStationId), nameof(PoliceStation.Locality), selectedValue);
        }
        private void InitilizeGroups(object selectedValue)
        {
            var dbData = GroupDataService.GetLite(db).OrderBy(i => i.GroupName).ToList();
            var mvc = GroupMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Groups = new SelectList(dbData, nameof(Group.GroupID), nameof(Group.GroupName), selectedValue);
        }
    }
}