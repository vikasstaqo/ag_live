﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class GovBlkMvcModel : BaseEntityModel<GovBlk, GovBlkMvcModel>
    {
        public Guid GovBlklId { get; set; }
        public string GovBlklName { get; set; }
        public GovBlkStatus Status { get; set; }
    }
}