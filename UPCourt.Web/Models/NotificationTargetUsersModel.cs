﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class NotificationTargetUsersModel : BaseEntityModel<Entity.NotificationTargetUsers, NotificationTargetUsersModel>
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public bool IsActive { get; set; }
    }
}