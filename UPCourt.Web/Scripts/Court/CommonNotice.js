﻿
function showParentNoticeTypeDetails(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetNoticeTypes",
        data: { "pid": $(obj).val() },
        success: function (response) {
            var Notice = JSON.parse(response);
            $("#CaseNoticeTypeId").empty();
            $("#CaseNoticeTypeId").append('<option value>--Select--</option>');
            Notice.forEach(function (obj, idx) {
                $("#CaseNoticeTypeId").append($("<option></option>").val(obj.CaseNoticeTypeId).html(obj.Name));
            })
        }
    });

    if ($(obj).val() == "2") {

        $("#dvCrimeDetails").show();
        $("#NoticeRespondents_0__IsPartyDepartment").attr("Checked", "checked");
        chked($("#NoticeRespondents_0__IsPartyDepartment"));

        $('#NoticeRespondents_0__GovernmentDepartmentId').val
        $("#NoticeRespondents_0__GovernmentDepartmentId option:contains(Home)").attr('selected', 'selected');
        GetDepartmentDetail($("#NoticeRespondents_0__GovernmentDepartmentId"));
        $('option', $("#NoticeRespondents_0__GovernmentDepartmentId")).not(':eq(0), :selected').remove();
    }
    else {
        $("#dvCrimeDetails").hide();
    }
}

function getPoliceStations(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetPoliceStations",
        data: { "d": $(obj).val() },
        success: function (response) {
            var Notice = JSON.parse(response);
            $("#CrimeDetail_PoliceStationID").empty();
            $("#CrimeDetail_PoliceStationID").append('<option value>--Select--</option>');
            Notice.forEach(function (obj, idx) {
                $("#CrimeDetail_PoliceStationID").append($("<option></option>").val(obj.PoliceStationId).html(obj.Locality + ' Police'));
            })
        }
    });
}

function getSubjects(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetSubjects",
        data: { "ntid": $(obj).val() },
        success: function (response) {
            var Subj = JSON.parse(response);
            $("#Subject").empty();
            $("#Subject").append('<option value>--Select--</option>');
            Subj.forEach(function (obj, idx) {
                $("#Subject").append($("<option></option>").val(obj.Name).html(obj.Name));
            })
        }
    });
}

function getSubordinateCourts(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetSubordinateCourts",
        data: { "d": $(obj).val() },
        success: function (response) {
            var Notice = JSON.parse(response);
            var objUpd = $('#' + obj.id.replace('District', 'SubordinateCourtId'));
            objUpd.empty();
            objUpd.append('<option value>--Select--</option>');
            Notice.forEach(function (obj, idx) {
                objUpd.append($("<option></option>").val(obj.PoliceStationId).html(obj.Locality + ' Police'));
            })
        }
    });
}

function setCourtTypes(obj) {
    if ($(obj).val() === "1")
        $('#rdoCourtType_HighCourt').closest('div').show();
    else {
        $('#rdoCourtType_HighCourt').closest('div').hide();
        $('#rdoCourtType_LowerCourt').prop('checked', 'true');
    }
}

function RemoveAddedItem(btn) {
    var id = $(btn).attr('id');
    $('#' + id.replace('Remove', '')).hide();
    $('#' + id.replace('Remove', '')).find("[id$=bActive]").val("false");
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhoneNo(Phone) {
    var re = /^\+?[0-9-]+$/;
    return re.test(Phone);
}

function validateAlphabateOnlyWithSpace(Name) {
    var re = /^[a-zA-Z][a-zA-Z\. ]*$/;
    return re.test(Name);
}

function validateAadhaar(AadhaarNo) {
    var re = /^\d{12}$/;
    return re.test(AadhaarNo);
}

function validatePanNo(PanNo) {
    var re = /^[A-Z]{3}P[A-Z]{1}\d{4}[A-Z]{1}$/;
    return re.test(PanNo);
}

