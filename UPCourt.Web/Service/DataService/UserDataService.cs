﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Models;

namespace UPCourt.Web.Service.DataService
{
    public class UserDataService
    {
        public static IQueryable<User> GetLite(DatabaseContext db)
        {
            return db.Users.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<User> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.UserRole)
                .ThenInclude(i => i.UserRolePagePermissions)
                .ThenInclude(i => i.PagePermission)
                .Include(i => i.AGDepartment)
                .Include(i => i.GovernmentDepartment);
        }
        public static IQueryable<User> GetDBDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.UserRole);
        }



        public static IQueryable<User> GetFiltered(DatabaseContext db, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, string HitFrom = "")
        {
            IQueryable<User> UserList;
            if (HitFrom == "DB")
                UserList = GetDBDetail(db);
            else
                UserList = GetDetail(db);
            if (UserSystemRoleId == UserSystemRole.Admin || UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                UserList = UserList.Where(x => x.RegionId == TempRegionId);
            }
            if (UserSystemRoleId == UserSystemRole.CSC || UserSystemRoleId == UserSystemRole.AdvCivil)
            {
                UserList = UserList.Where(x => x.UserRole.UserSystemRoleId == UserSystemRole.AdvCivil);
            }
            else if (UserSystemRoleId == UserSystemRole.CGA || UserSystemRoleId == UserSystemRole.AdvCriminal)
            {
                UserList = UserList.Where(x => x.UserRole.UserSystemRoleId == UserSystemRole.AdvCriminal);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptHead || UserSystemRoleId == UserSystemRole.GovDeptDegUser || UserSystemRoleId == UserSystemRole.GovDeptOIC)
            {
                UserList = UserList.Where(x => (x.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead || x.UserRole.UserSystemRoleId == UserSystemRole.GovDeptDegUser
                || x.UserRole.UserSystemRoleId == UserSystemRole.GovDeptOIC) && x.GovernmentDepartmentId == GovernmentDepartmentId);
                if (UserSystemRoleId == UserSystemRole.GovDeptDegUser)
                    UserList = UserList.Where(x => x.DesignationId == DesignationId);

            }
            return UserList;
        }

        public static String GetName(Guid? useid)
        {
            DatabaseContext db = new DatabaseContext();
            var data = GetLite(db).Where(x => x.UserId == useid).FirstOrDefault();
            if (data != null)
                return data.Name;
            else
                return "";
        }
    }
}