﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity.Core.Converter
{
    public class EnumListToStringValueConverter<E> : ValueConverter<IEnumerable<E>, string> where E : struct
    {
        public EnumListToStringValueConverter(bool ConvertValueToInt = true) : base(le => Extended.ListEnumToString(le, ConvertValueToInt), (s => Extended.StringToEnumList<E>(s)))
        {

        }
    }
}
