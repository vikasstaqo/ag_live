﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(AdvocateCourtId), UserNameProp: nameof(CreatedBy))]
    public class AdvocateCourt : BaseTransEntity
    {
        public Guid AdvocateCourtId { get; set; }
        public Guid AdvocateId { get; set; }
        public Guid CourtId { get; set; }
        public User Advocate { get; set; }
        public Court Court { get; set; }
    }


    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    public class Advocate
    {
        public Guid AdvocateId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public AdvocateStatus Status { get; set; }
        public Guid? DesignationId { get; set; }
        public string ModifyBy { get; set; }
        public int AdvCode { get; set; }
        public string AdvReg { get; set; }

    }
    public enum AdvocateStatus : byte
    {
        Active = 1,
        Inactive = 0
    }
}
