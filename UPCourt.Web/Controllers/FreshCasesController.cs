﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class FreshCasesController : BaseMVCController
    {

        // GET: NoticeCreate
        public ActionResult Index(Guid? id)
        {

            if (TempData["CurrentTab"] == null)
                TempData["CurrentTab"] = "case";

            if (id == null)
            {
                TempData["NewCase"] = "1";
                InitializeData(null);
                var model = new NoticeMvcModel
                {
                    Petitioners = new List<PetitionerMvcModel> { new PetitionerMvcModel() { IsFirst = true, } },
                    NoticeRespondents = new List<NoticeRespondentMvcModel> { new NoticeRespondentMvcModel() { IsFirst = true } },
                    NoticeBasicIndexs = new List<NoticeBasicIndexMvcModel> { new NoticeBasicIndexMvcModel() },
                    ImpungedAndSTDetails = new List<ImpungedAndSTDetailMvcModel> { new ImpungedAndSTDetailMvcModel() { DateOn = DateTime.Now } },
                    NoticeIPCs = new List<NoticeIPCMvcModel> { new NoticeIPCMvcModel() },
                    ECourtFees = new List<ECourtFeeMvcModel> { new ECourtFeeMvcModel() },
                    NoticeActTypes = new List<NoticeActTypeMvcModel> { new NoticeActTypeMvcModel() },
                    NoticeDate = DateTime.Now,
                    NoticeYear = DateTime.Now.Year,
                    RegionId = (Guid)AppUser.TempRegionId
                };


                return View(model);
            }
            else
            {
                TempData["NewCase"] = "0";
                var dbData = NoticeDataService.GetDetail(db, false).Where(i => i.NoticeId == id).FirstOrDefault();
                var model = NoticeMvcModel.CopyFromEntity(dbData, 0);
                model.Petitioners.Insert(0, new PetitionerMvcModel());
                model.NoticeRespondents.Insert(0, new NoticeRespondentMvcModel());
                model.NoticeBasicIndexs.Insert(0, new NoticeBasicIndexMvcModel());
                model.ImpungedAndSTDetails.Insert(0, new ImpungedAndSTDetailMvcModel() { DateOn = DateTime.Now });
                model.ECourtFees.Insert(0, new ECourtFeeMvcModel());
                model.NoticeActTypes.Insert(0, new NoticeActTypeMvcModel());
                Guid policeStationId = Guid.Empty;
                if (model.CrimeDetail == null)
                {
                    var crimeModel = CrimeDetailMvcModel.CopyFromEntity(CrimeDetailDataService.GetDetail(db).Where(c => c.NoticeId == model.NoticeId).FirstOrDefault(), 0);
                    model.CrimeDetail = crimeModel;
                    if (model.CrimeDetail != null)
                        policeStationId = model.CrimeDetail.PoliceStationID;
                }
                InitializeData(model);
                InitilizePoliceStations(model.District, policeStationId);
                return View(model);
            }
        }
        
        private void InitializeData(NoticeMvcModel model)
        {
            object reg = null, cnt = null, dis = null, sub = null;
            int noticeType = -1;
            if (model != null)
            {
                reg = model.RegionId;
                cnt = model.CaseNoticeTypeId;
                dis = model.District;
                sub = model.Subject;
                noticeType = (int)model.ParentNoticeType;
            }

            InitilizeGovermentDepartments(null);
            InitilizeGovDesignations(null);
            InitilizeRegions(reg);
            InitilizeCaseNoticeTypes(cnt, noticeType);
            InitilizeIPCSections(null);
            InitilizeDistrict(dis);
            InitilizeSubject(sub, 1);
            if (model != null && model.CrimeDetail != null)
                InitilizePoliceStations(model.CrimeDetail.District, model.CrimeDetail.PoliceStation);
            else if (model != null && model.District != null)
                InitilizePoliceStations(model.District, null);
            else
                InitilizePoliceStations(null, null);
            InitialiseSalutations(null, null);
            InitialiseSpecialCategories(null);
            InitialiseSubordinateCourts(null);
        }

        public ActionResult SaveNotice(NoticeMvcModel model)
        {
            Notice notice = null;
            var noticeId = Guid.Empty;
            bool newNotice = false;
            notice = NoticeDataService.GetDetail(db).Where(i => i.NoticeId == model.NoticeId).FirstOrDefault();
            if (notice == null)
            {
                newNotice = true;
                noticeId = Guid.NewGuid();
                notice = new Notice
                {
                    NoticeId = noticeId,
                    Status = NoticeStatus.Pending,
                    NoticeType = model.NoticeType,
                    CaseNoticeTypeId = model.CaseNoticeTypeId,
                    SerialNo = NoticeBusinessService.GetSerailNo(db),
                    NoticeDate = Extended.CurrentIndianTime,
                    NoticeYear = model.NoticeYear,
                    Subject = model.Subject,
                    IsContextOrder = model.IsContextOrder,
                    NoticeFrom = NoticeFrom.UserSystem,
                    District = model.District,
                    RegionId = model.RegionId,
                    CreatedBy = AppUser.UserId,
                    CreatedOn = DateTime.Now,
                };
                FillNoticeSpecialCategories(notice, model);
                notice.NoticeNo = NoticeBusinessService.GetInvoiceNo("NOC", notice.SerialNo);
            }
            else
            {
                notice.NoticeType = model.NoticeType;
                notice.CaseNoticeTypeId = model.CaseNoticeTypeId;
                notice.NoticeDate = Extended.CurrentIndianTime;
                notice.NoticeYear = model.NoticeYear;
                notice.Subject = model.Subject;
                notice.IsContextOrder = model.IsContextOrder;
                notice.NoticeFrom = NoticeFrom.UserSystem;
                notice.District = model.District;
                notice.RegionId = model.RegionId;
                notice.ModifiedBy = AppUser.UserId;
                notice.ModifiedOn = DateTime.Now;
                FillNoticeSpecialCategories(notice, model);
            }

            if (newNotice)
                db.Notices.Add(notice);

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            TempData["CurrentTab"] = "pet";
            string description = $"Notice '{ notice.NoticeNo }' Saved successfully";
            TimelineBusinessService.SaveTimeLine(db, notice.NoticeId, null, EventType.NoticeCreated, AppUser.UserId, description, null);
            DisplayMessage(MessageType.Success, $"Notice '{notice.NoticeNo}' Saved successfully");
            return RedirectToAction("Index", "FreshCases", new { id = notice.NoticeId });
        }

        private void FillNoticeSpecialCategories(Notice notice, NoticeMvcModel model)
        {
            if (model.SpecialCategories != null)
            {
                notice.NoticeSpecialcategorys.Clear();
                foreach (var SpecialCatGuid in model.SpecialCategories)
                {
                    NoticeSpecialcategory cat = new NoticeSpecialcategory()
                    {
                        NoticeSpecialcategoryId = Guid.NewGuid(),
                        NoticeId = model.NoticeId,
                        CreatedBy = AppUser.UserId,
                        CreatedOn = DateTime.Now,
                        SpecialCategoryId = SpecialCatGuid
                    };
                    notice.NoticeSpecialcategorys.Add(cat);
                }
            }
        }

        #region Petitioner
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SavePetitioner(NoticeMvcModel model)
        {
            TempData["NewCase"] = "1";
            var i = 0;
            if (model.NoticeId != null)
            {
                if (model.PetitionerId != null && model.PetitionerId != Guid.Empty)
                {
                    var petitioner = PetitionerDataService.GetDetail(db).Where(p => p.PetitionerId == model.PetitionerId).FirstOrDefault();

                    petitioner.Name = model.Petitioners[i].Name;
                    petitioner.Address = model.Petitioners[i].Address;
                    petitioner.ContactNo = model.Petitioners[i].ContactNo;
                    petitioner.Email = model.Petitioners[i].Email;
                    petitioner.District = model.Petitioners[i].District;
                    petitioner.IDProof = IDProofType.AadharID;
                    petitioner.IDProofNumber = model.Petitioners[i].IDProofNumber;
                    petitioner.SalutationId = model.Petitioners[i].SalutationId;
                    petitioner.Age = model.Petitioners[i].Age;
                    petitioner.ParantageSalutationId = model.Petitioners[i].ParantageSalutationId;
                    petitioner.ParantageName = model.Petitioners[i].ParantageName;
                    petitioner.MobileNo = model.Petitioners[i].MobileNo;
                    petitioner.Pincode = model.Petitioners[i].Pincode;
                    petitioner.City = model.Petitioners[i].City;
                    SucessMessage = $"Notice Petitioner Updated successfully";
                }
                else
                {
                    Guid Petitioner_ID = Guid.NewGuid();
                    Petitioner Insert_Petitioner = new Petitioner
                    {

                        PetitionerId = Petitioner_ID,
                        NoticeId = model.NoticeId,
                        Name = model.Petitioners[i].Name,
                        Address = model.Petitioners[i].Address,
                        ContactNo = model.Petitioners[i].ContactNo,
                        Email = model.Petitioners[i].Email,
                        District = model.Petitioners[i].District,
                        IDProof = IDProofType.AadharID,
                        IDProofNumber = model.Petitioners[i].IDProofNumber,
                        SalutationId = model.Petitioners[i].SalutationId,
                        Age = model.Petitioners[i].Age,
                        ParantageSalutationId = model.Petitioners[i].ParantageSalutationId,
                        ParantageName = model.Petitioners[i].ParantageName,
                        MobileNo = model.Petitioners[i].MobileNo,
                        Pincode = model.Petitioners[i].Pincode,
                        City = model.Petitioners[i].City,
                        IsFirst = true,

                        ModifyBy = AppUser.UserName
                    };

                    db.Petitioners.Add(Insert_Petitioner);
                    SucessMessage = $"Notice Petitioner Saved successfully";
                }
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "pet";
                DisplayMessage(MessageType.Success, SucessMessage);
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        [HttpGet]
        public ActionResult GetPetitionerDetails(Guid? id)
        {
            var petitioner = PetitionerDataService.GetDetail(db).Where(p => p.PetitionerId == id).FirstOrDefault();
            return Json(new { data = petitioner }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeletePetitioner(Guid id, Guid NoticeId)
        {
            var dbdata = PetitionerDataService.GetDetail(db).Where(x => x.PetitionerId == id).FirstOrDefault();
            db.Petitioners.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "pet";
            DisplayMessage(MessageType.Error, $"Notice  Petitioner Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        #endregion

        #region Respondent
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SaveRespondent(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null)
            {
                if (model.NoticeRespondentId != null && model.NoticeRespondentId != Guid.Empty)
                {
                    var respondent = PartyAndRespondentDataService.GetDetail(db).Where(p => p.PartyAndRespondentId == model.NoticeRespondentId).FirstOrDefault();
                    respondent.PartyName = model.NoticeRespondents[i].PartyName;
                    respondent.ContactNo = model.NoticeRespondents[i].ContactNo;
                    respondent.Email = model.NoticeRespondents[i].Email;
                    respondent.Address = model.NoticeRespondents[i].Address;
                    respondent.District = model.NoticeRespondents[i].District;
                    respondent.PinCode = model.NoticeRespondents[i].Pincode;
                    if (!model.NoticeRespondents[i].IsPartyDepartment)
                    {
                        respondent.PartyType = PartyType.Individual;
                        respondent.IsPartyDepartment = false;
                    }
                    if (model.NoticeRespondents[i].IsPartyDepartment)
                    {
                        respondent.PartyType = PartyType.Department;
                        respondent.GovernmentDepartmentId = model.NoticeRespondents[i].GovernmentDepartmentId;
                        respondent.GovernmentDesignationId = model.NoticeRespondents[i].GovernmentDesignationId;
                        respondent.IsPartyDepartment = true;
                    }
                    SucessMessage = $"Notice Respondent Updated successfully";
                }
                else
                {

                    PartyAndRespondent InsertPartyAndRespondent = new PartyAndRespondent
                    {
                        PartyAndRespondentId = Guid.NewGuid(),
                        NoticeRespondentId = model.NoticeId,
                        NoticeOrCaseType = NoticeOrCaseType.Notice,
                        PartyAndRespondentType = PartyAndRespondentType.Respondent,
                        PartyName = model.NoticeRespondents[i].PartyName,
                        ContactNo = model.NoticeRespondents[i].ContactNo,
                        Email = model.NoticeRespondents[i].Email,
                        Address = model.NoticeRespondents[i].Address,
                        District = model.NoticeRespondents[i].District,
                        PinCode = model.NoticeRespondents[i].Pincode,
                        ModifyBy = AppUser.UserName,
                        IsFirst = false
                    };
                    if (!model.NoticeRespondents[i].IsPartyDepartment)
                    {
                        InsertPartyAndRespondent.PartyType = PartyType.Individual;
                        InsertPartyAndRespondent.IsPartyDepartment = false;
                    }
                    if (model.NoticeRespondents[i].IsPartyDepartment)
                    {
                        InsertPartyAndRespondent.PartyType = PartyType.Department;
                        InsertPartyAndRespondent.GovernmentDepartmentId = model.NoticeRespondents[i].GovernmentDepartmentId;
                        InsertPartyAndRespondent.GovernmentDesignationId = model.NoticeRespondents[i].GovernmentDesignationId;
                        InsertPartyAndRespondent.IsPartyDepartment = true;
                    }

                    db.PartyAndRespondents.Add(InsertPartyAndRespondent);
                    SucessMessage = $"Notice Respondent Saved successfully";
                }
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "respon";
                DisplayMessage(MessageType.Success, SucessMessage);
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        [HttpGet]
        public ActionResult GetRespondentDetails(Guid? id)
        {
            var Respondent = PartyAndRespondentDataService.GetDetail(db).Where(p => p.PartyAndRespondentId == id).FirstOrDefault();
            return Json(new { data = Respondent }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteRespondent(Guid id, Guid NoticeId)
        {
            var dbdata = PartyAndRespondentDataService.GetLite(db).Where(x => x.PartyAndRespondentId == id).FirstOrDefault();
            db.PartyAndRespondents.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "respon";
            DisplayMessage(MessageType.Error, $"Notice  Respondent Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        #endregion

        #region Act Details
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SaveActDetails(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null)
            {
                if (model.NoticeActTypeId != null && model.NoticeActTypeId != Guid.Empty)
                {
                    var noticeActType = NoticeActTypeDataService.GetDetail(db).Where(p => p.NoticeActTypeId == model.NoticeActTypeId).FirstOrDefault();
                    noticeActType.BelongsTo = model.NoticeActTypes[i].BelongsTo;
                    noticeActType.ActTitle = model.NoticeActTypes[i].ActTitle;
                    noticeActType.Section = model.NoticeActTypes[i].Section;
                    noticeActType.RuleTitle = model.NoticeActTypes[i].RuleTitle;
                    noticeActType.RuleNumber = model.NoticeActTypes[i].RuleNumber;
                    SucessMessage = $"Notice Act Details Updated successfully";
                }
                else
                {
                    Guid NoticeActTypeId = Guid.NewGuid();
                    NoticeActType Insert_NoticeActType = new NoticeActType
                    {
                        NoticeActTypeId = NoticeActTypeId,
                        NoticeId = model.NoticeId,
                        BelongsTo = model.NoticeActTypes[i].BelongsTo,
                        ActTitle = model.NoticeActTypes[i].ActTitle,
                        Section = model.NoticeActTypes[i].Section,
                        RuleTitle = model.NoticeActTypes[i].RuleTitle,
                        RuleNumber = model.NoticeActTypes[i].RuleNumber,
                        ModifyBy = AppUser.UserName
                    };
                    db.NoticeActTypes.Add(Insert_NoticeActType);
                    SucessMessage = $"Notice Act Details Saved successfully";
                }
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "act";
                DisplayMessage(MessageType.Success, SucessMessage);
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        [HttpGet]
        public ActionResult GetActDetails(Guid? id)
        {
            var NoticeActType = NoticeActTypeDataService.GetDetail(db).Where(p => p.NoticeActTypeId == id).FirstOrDefault();
            return Json(new { data = NoticeActType }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteActDetails(Guid id, Guid NoticeId)
        {
            var dbdata = NoticeActTypeDataService.GetLite(db).Where(x => x.NoticeActTypeId == id).FirstOrDefault();
            db.NoticeActTypes.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "act";
            DisplayMessage(MessageType.Error, $"Notice  Act Details Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        #endregion

        #region ImpungedST
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SaveImpungedST(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null)
            {
                if (model.ImpungedAndSTDetailID != null && model.ImpungedAndSTDetailID != Guid.Empty)
                {
                    var ImpungedST = ImpungedAndSTDetailDataService.GetDetail(db).Where(p => p.ImpungedAndSTDetailID == model.ImpungedAndSTDetailID).FirstOrDefault();

                    ImpungedST.CaseNo = model.ImpungedAndSTDetails[i].CaseNo;
                    ImpungedST.SubordinateCourtId = model.ImpungedAndSTDetails[i].SubordinateCourtId;
                    ImpungedST.CaseNoticeTypeId = model.ImpungedAndSTDetails[i].CaseNoticeTypeId;
                    ImpungedST.CaseYear = model.ImpungedAndSTDetails[i].CaseYear;
                    ImpungedST.CNRNo = model.ImpungedAndSTDetails[i].CNRNo;
                    ImpungedST.CourtType = model.ImpungedAndSTDetails[i].CourtType;
                    ImpungedST.DateOn = model.ImpungedAndSTDetails[i].DateOn;
                    ImpungedST.DetailsType = model.ImpungedAndSTDetails[i].DetailsType;
                    ImpungedST.District = model.ImpungedAndSTDetails[i].District;
                    ImpungedST.Judge = model.ImpungedAndSTDetails[i].Judge;
                    SucessMessage = $"Notice Impunged Or ST Details Updated successfully";
                }
                else
                {
                    Guid ImpungedAndStDetailId = Guid.NewGuid();
                    ImpungedAndSTDetail Insert_ImpungedAndSTDetail = new ImpungedAndSTDetail
                    {
                        ImpungedAndSTDetailID = ImpungedAndStDetailId,
                        NoticeId = model.NoticeId,
                        IsActive = true,
                        CaseNo = model.ImpungedAndSTDetails[i].CaseNo,
                        SubordinateCourtId = model.ImpungedAndSTDetails[i].SubordinateCourtId,
                        CaseNoticeTypeId = model.ImpungedAndSTDetails[i].CaseNoticeTypeId,
                        CaseYear = model.ImpungedAndSTDetails[i].CaseYear,
                        CNRNo = model.ImpungedAndSTDetails[i].CNRNo,
                        CourtType = model.ImpungedAndSTDetails[i].CourtType,
                        DateOn = model.ImpungedAndSTDetails[i].DateOn,
                        DetailsType = model.ImpungedAndSTDetails[i].DetailsType,
                        District = model.ImpungedAndSTDetails[i].District,
                        Judge = model.ImpungedAndSTDetails[i].Judge,
                        ModifyOn = DateTime.Now,

                        ModifyBy = AppUser.UserName
                    };

                    db.ImpungedAndSTDetails.Add(Insert_ImpungedAndSTDetail);
                    SucessMessage = $"Notice Impunged Or ST Details Saved successfully";
                }
                db.SaveChangesWithAudit(Request, AppUser.UserName);

                TempData["CurrentTab"] = "io";
                DisplayMessage(MessageType.Success, SucessMessage);
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        [HttpGet]
        public ActionResult GetImpungedST(Guid? id)
        {
            var ImpungedST = ImpungedAndSTDetailDataService.GetDetail(db).Where(p => p.ImpungedAndSTDetailID == id).FirstOrDefault();
            return Json(new { data = ImpungedST }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteImpungedST(Guid id, Guid NoticeId)
        {
            var dbdata = ImpungedAndSTDetailDataService.GetLite(db).Where(x => x.ImpungedAndSTDetailID == id).FirstOrDefault();
            db.ImpungedAndSTDetails.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "det";
            DisplayMessage(MessageType.Error, $"Notice  Petitioner Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        #endregion

        #region e-court
        [HttpPost]
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SaveECourtFee(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null && model.ECourtFees[i].Amount > 0)
            {
                ECourtFee InsertECourtFee = new ECourtFee
                {
                    eCourtFeeID = Guid.NewGuid(),
                    NoticeId = model.NoticeId,
                    ReceiptNo = model.ECourtFees[i].ReceiptNo,
                    Amount = model.ECourtFees[i].Amount,
                    EDate = model.ECourtFees[i].EDate,
                    InsertedBy = AppUser.UserName,
                    InsertedDate = DateTime.Now,
                    Status = true
                };

                db.ECourtFees.Add(InsertECourtFee);
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "e-court";
                DisplayMessage(MessageType.Success, $"Notice ECourtFee Saved successfully");
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        #endregion

        #region Upload Document

        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]

        public ActionResult SaveBasicIndex(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null)
            {
                NoticeBasicIndex Insert_NoticeBasicIndex = new NoticeBasicIndex
                {
                    NoticeBasicIndexId = Guid.NewGuid(),
                    NoticeId = model.NoticeId,
                    Particulars = model.NoticeBasicIndexs[i].Particulars,
                    Annexure = model.NoticeBasicIndexs[i].Annexure,
                    IndexDate = model.NoticeBasicIndexs[i].IndexDate
                };
                Insert_NoticeBasicIndex.IndexDate = model.NoticeBasicIndexs[i].IndexDate;
               
                if (model.NoticeBasicIndexs[i].BasicIndexFile.ContentLength > 0)
                {
                    Document Insert_Doc = new Document
                    {
                        DocumentId = Guid.NewGuid(),
                        NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId,
                        Name = Insert_NoticeBasicIndex.Annexure,
                        DocumentUrl = SaveDocument(model.NoticeBasicIndexs[i].BasicIndexFile),
                        FileType = DocumentFileType.Any,
                        EntityType = DocumentEntityType.CaseHearing,
                        Size = model.NoticeBasicIndexs[i].BasicIndexFile.ContentLength,
                        Status = DocumentStatus.Added,
                        CreatedBy = AppUser.UserId
                    };
                    Insert_NoticeBasicIndex.Documents.Add(Insert_Doc);
                }
                db.NoticeBasicIndexs.Add(Insert_NoticeBasicIndex);
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "upload";
                DisplayMessage(MessageType.Success, $"Notice Document Saved successfully");
                return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);               
                int FileSize = FileUpload.ContentLength;
                if (FileSize > 110000000)
                {
                    return "Maximum file size 100 mb will be uploaded";
                }
                else
                {
                    string fileName = Path.GetFileName(FileUpload.FileName);
                    Guid guid;
                    guid = Guid.NewGuid();

                    string UploadName = guid + "_" + fileName;
                    string FilePath = Path.Combine(Server.MapPath("~/Document/Notice/"), UploadName);
                    FileUpload.SaveAs(FilePath);
                    return UploadName;
                }
            }
            return "";
        }
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanDelete)]
        public ActionResult DeleteBasicIndex(Guid id, Guid NoticeId)
        {
            var dbdata = NoticeBasicIndexDataService.GetDetail(db).Where(x => x.NoticeBasicIndexId == id).FirstOrDefault();
            db.NoticeBasicIndexs.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "upload";
            DisplayMessage(MessageType.Error, $"Notice  Document Removed Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        #endregion

        #region Crime Details
        [PageRoleAuthorizarion(PageName.FreshCase, PagePermissionType.CanCreate)]
        public ActionResult SaveCrimeDetail(NoticeMvcModel model)
        {
            var i = 0;
            if (model.NoticeId != null)
            {
                if (model.BtnCommand == "UpdateCrimeDetail")
                {
                    var crimeDetail = CrimeDetailDataService.GetDetail(db).Where(p => p.NoticeId == model.NoticeId).FirstOrDefault();
                    if (crimeDetail != null)
                    {
                        crimeDetail.IsActive = true;
                        crimeDetail.CrimeNo = model.CrimeDetail.CrimeNo;
                        crimeDetail.CrimeYear = model.CrimeDetail.CrimeYear;
                        crimeDetail.District = model.CrimeDetail.District;
                        crimeDetail.PoliceStationID = model.CrimeDetail.PoliceStationID;
                    }
                    else
                    {
                        CrimeDetail detail = new CrimeDetail()
                        {
                            IsActive = true,
                            CrimeDetailID = Guid.NewGuid(),
                            CrimeNo = model.CrimeDetail.CrimeNo,
                            CrimeYear = model.CrimeDetail.CrimeYear,
                            District = model.CrimeDetail.District,
                            CreatedBy = AppUser.UserId,
                            CreatedOn = DateTime.Now,
                            NoticeId = model.NoticeId,
                            PoliceStationID = model.CrimeDetail.PoliceStationID,
                        };
                        db.CrimeDetails.Add(detail);
                    }
                    db.SaveChangesWithAudit(Request, AppUser.UserName);
                    TempData["CurrentTab"] = "case";
                    DisplayMessage(MessageType.Success, $"Crime Detail Saved successfully");
                    return RedirectToAction("Index", "FreshCases", new { id = model.NoticeId });
                }
                else if (model.BtnCommand == "FinalSubmit")
                {
                    var NoticeDetail = NoticeDataService.GetDetail(db).Where(p => p.NoticeId == model.NoticeId).FirstOrDefault();
                    NoticeDetail.FinalSubmit = NoticeFinalStatus.Submited;
                    db.SaveChangesWithAudit(Request, AppUser.UserName);
                    return RedirectToAction("List", "FreshCases");
                }
                else
                {
                    return RedirectToAction("List", "FreshCases");
                }
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }


        public ActionResult DeleteCrimeDetail(Guid id, Guid NoticeId)
        {
            var dbdata = PartyAndRespondentDataService.GetLite(db).Where(x => x.PartyAndRespondentId == id).FirstOrDefault();
            db.PartyAndRespondents.Remove(dbdata);
            db.SaveChanges();
            DisplayMessage(MessageType.Error, $"Notice  Respondent Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });
        }
        public ActionResult DeleteECourtFee(Guid id, Guid NoticeId)
        {
            var dbdata = ECourtFeeDataService.GetDetail(db).Where(x => x.eCourtFeeID == id).FirstOrDefault();
            db.ECourtFees.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "e-court";
            DisplayMessage(MessageType.Error, $"Application Document E Court Fee Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index", "FreshCases", new { id = NoticeId });

        }


        #endregion

        #region List
        public ActionResult List()
        {
            NoticeCaseIndexMvcModel model = new NoticeCaseIndexMvcModel() { };

            InitilizeCaseNoticeTypes(null, 0);

            var dbData = NoticeDataService.GetDetail(db, false)
                .Where(F => F.CreatedBy == AppUser.UserId && F.NoticeFrom == NoticeFrom.UserSystem)
                .OrderByDescending(i => i.NoticeDate).ToList();

            model.Notices = NoticeMvcModel.CopyFromEntityList(dbData, 0);

            return View(model);
        }
        public ActionResult ConfirmFinalSubmit(Guid id)
        {
            var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeId == id).FirstOrDefault();
            dbData.FinalSubmit = NoticeFinalStatus.Submited;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Notice Finally Submit successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Index", "FreshCases/List");
        }

        private List<SelectListItem> GetAllStatus()
        {
            var AllStatuses = CaseStatusDataService.GetLite(db).ToList();
            var CaseStatuses = new List<SelectListItem>();
            for (int i = 0; i < AllStatuses.Count; i++)
            {
                CaseStatuses.Add(new SelectListItem()
                {
                    Text = AllStatuses[i].Name,
                    Value = AllStatuses[i].CaseStatusId.ToString()
                });
            }
            return CaseStatuses;
        }
        #endregion
          

        #region Detail
        public ActionResult Detail(Guid id)
        {
            return null;
        }
        #endregion

        #region delete
        public ActionResult Delete(Guid id)
        {
            var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, $"Notice '{ dbData.NoticeNo }' Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("List", "FreshCases");
        }
        #endregion

        [HttpGet]
        public ActionResult GetDepartment(Guid? GovtDepartmentId)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == GovtDepartmentId).FirstOrDefault();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDepartmentDesignation(Guid? GovtDepartmentId)
        {
            var dbData = GovDesignationDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == GovtDepartmentId);
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPoliceStations(string d)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.District == d).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetNoticeTypes(int pid)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(i => (int)i.ParentNoticeType == pid).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSubjects(int pid)
        {
            var dbData = SubjectDataService.GetLite(db).Where(i => (int)i.ParentNoticeType == pid).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSubordinateCourts(string d)
        {
            var dbData = SubordinateCourtDataService.GetLite(db).Where(i => i.District == d).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CheckNoticeNo(string NoticeNo)
        {
            var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeNo == NoticeNo).FirstOrDefault();

            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        private void updateCrimeData(CrimeDetailMvcModel model, CrimeDetail ent)
        {
            ent.CaseNo = model.CaseNo;
            ent.CaseNo_Court = model.CaseNo_Court;
            ent.CrimeNo = model.CrimeNo;
            ent.CrimeYear = model.CrimeYear;
            ent.District = model.District;
            ent.TrailNo_Court = model.TrailNo_Court;
            ent.FIRDate = model.FIRDate;
            ent.FIRNO = model.FIRNO;
            ent.IsActive = true;
            ent.PoliceStationID = model.PoliceStationID;
            ent.TrailNo = model.TrailNo;
            ent.CreatedBy = AppUser.UserId;
        }

        private CrimeDetail CreateCrimeDetailWithData(CrimeDetailMvcModel model, Guid NoticeId)
        {
            CrimeDetail crimeDetail = new CrimeDetail()
            {
                CrimeDetailID = Guid.NewGuid(),
                CaseNo = model.CaseNo,
                CaseNo_Court = model.CaseNo_Court,
                CrimeNo = model.CrimeNo,
                CrimeYear = model.CrimeYear,
                District = model.District,
                TrailNo_Court = model.TrailNo_Court,
                FIRDate = model.FIRDate,
                FIRNO = model.FIRNO,
                IsActive = true,
                NoticeId = NoticeId,
                PoliceStationID = model.PoliceStationID,
                TrailNo = model.TrailNo,
                CreatedBy = AppUser.UserId
            };

            return crimeDetail;
        }

        private void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeGovDesignations(object selectedValue)
        {
            var dbData = GovDesignationDataService.GetLite(db).OrderBy(i => i.DesignationName).Take(10).ToList();
            var mvc = GovDesignationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovDesignations = new SelectList(mvc, nameof(GovDesignationMvcModel.GovernmentDesignationId)
                , nameof(GovDesignationMvcModel.DesignationName), selectedValue);
        }
        private void InitilizeCaseNoticeTypes(object selectedValue, int noticeType)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(n => noticeType == 0 || n.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var dbDataAll = NoticeTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            var mvcAll = CaseNoticeTypeMvcModel.CopyFromEntityList(dbDataAll, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);
            ViewBag.CaseNoticeTypesAll = new SelectList(mvcAll, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);
        }
        private void InitilizeRegions(object selectedValue)
        {

            if (AppUser.User.RegionId != null)
            {
                var dbData = RegionDataService.GetLite(db).Where(x => x.RegionId == AppUser.User.RegionId).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }
            else
            {
                var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }

        }
        private void InitilizeIPCSections(object selectedValue)
        {
            var dbData = IPCSectionDataService.GetLite(db).OrderBy(i => i.SectionName).ToList();
            var mvc = IPCSectionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.IPCSections = new SelectList(mvc, nameof(IPCSection.IPCSectionId), nameof(IPCSection.SectionName), selectedValue);
        }
        private void InitilizeDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
            ViewBag.Districts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }
        public void InitilizeSubject(object selectedValue, Guid? CaseNoticeType)
        {
            var dbData = SubjectDataService.GetLite(db).Where(s => s.CaseNoticeTypeId == CaseNoticeType).OrderBy(i => i.Name).ToList();
            var mvc = SubjectMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Subjects = new SelectList(dbData, nameof(Subject.Name), nameof(Subject.Name), selectedValue);

        }
        private void InitilizeSubject(object selectedValue, int noticeType)
        {
            var dbData = SubjectDataService.GetLite(db).Where(s => noticeType == 0 || s.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var mvc = SubjectMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Subjects = new SelectList(dbData, nameof(Subject.Name), nameof(Subject.Name), selectedValue);
        }
        private void InitilizePoliceStations(string district, object selectedValue)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(s => district != null && s.District == district).OrderBy(i => i.Locality).ToList();
            var mvc = PoliceStationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.PoliceStations = new SelectList(dbData, nameof(PoliceStation.PoliceStationId), nameof(PoliceStation.Locality), selectedValue);
        }
        private void InitialiseSalutations(object selected, object selectedParent)
        {
            var dbData = SalutationDataService.GetLite(db).OrderBy(s => s.Name).ToList();
            var mvc = SalutationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Salutations = new SelectList(dbData, nameof(Salutation.SalutationId), nameof(Salutation.Name), selected);
            ViewBag.ParentSalutations = new SelectList(dbData, nameof(Salutation.SalutationId), nameof(Salutation.Name), selectedParent);
        }

        private void InitialiseSpecialCategories(object selectedValue)
        {
            var dbData = SpecialcategoryDataService.GetLite(db).Where(x => x.Status == SpecialCategoryStatus.Active).ToList();
            var mvc = SpecialCategoryMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.SpecialCategories = new SelectList(dbData, nameof(SpecialCategory.SpecialCategoryId), nameof(SpecialCategory.Name), selectedValue);
        }
        private void InitialiseSubordinateCourts(object selectedValue)
        {
            var dbData = SubordinateCourtDataService.GetLite(db).ToList();
            var mvc = SubordinateCourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.SubordinateCourts = new SelectList(dbData, nameof(SubordinateCourt.SubordinateCourtID), nameof(SubordinateCourt.Name), selectedValue);
        }
        public void InitilizeActTypes(object selectedValue)
        {
            var dbData = ActTypeDataService.GetLite(db).Where(x => x.ParentActBelongType == ParentActBelongType.CriminalAct).OrderBy(i => i.ActBelongName).ToList();
            var mvc = ActTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.ActTypes = new SelectList(mvc, nameof(ActType.ActTypeID), nameof(ActType.ActBelongName), selectedValue);
        }
        private void InitilizeGroups(object selectedValue)
        {
            var dbData = GroupDataService.GetLite(db).OrderBy(i => i.GroupName).ToList();
            var mvc = GroupMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Groups = new SelectList(dbData, nameof(Group.GroupID), nameof(Group.GroupName), selectedValue);
        }
    }
}