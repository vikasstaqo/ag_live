﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class Advocate
    {
        public Advocate()
        {
           // Cases = new HashSet<Case>();
            AdvocateCourts = new HashSet<AdvocateCourt>();
        }
        public Guid AdvocateId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public AdvocateStatus Status { get; set; }
        public Guid? DesignationId { get; set; }       
        public string ModifyBy { get; set; } 
        public ICollection<AdvocateCourt> AdvocateCourts { get; set; }
        public Designation Designation { get; set; }       

      //  public ICollection<Case> Cases { get; set; }
    }
    public enum AdvocateStatus : byte
    {
        Active = 1,
        Inactive = 0
    }
}
