﻿using System;
using System.ComponentModel.DataAnnotations;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class ImpungedAndSTDetailMvcModel : BaseEntityModel<ImpungedAndSTDetail, ImpungedAndSTDetailMvcModel>
    {
        public Guid ImpungedAndSTDetailID { get; set; }
        public Guid NoticeId { get; set; }
        [Required]
        public DetailsType DetailsType { get; set; }
        [Required]
        public CourtType CourtType { get; set; }
        [Required]
        public string District { get; set; }
        [Required]
        public Guid SubordinateCourtId { get; set; }
        public SubordinateCourtMvcModel SubordinateCourt { get; set; }
        [Required]
        [Display(Name = "Case Type")]
        public Guid CaseNoticeTypeId { get; set; }
        public CaseNoticeTypeMvcModel CaseNoticeType { get; set; }
        [Required]
        [Display(Name = "Case Year")]
        public int CaseYear { get; set; }
        [Required]
        [Display(Name = "Case No.")]
        public int CaseNo { get; set; }
        [Required]
        [Display(Name = "Judge Name")]
        public string Judge { get; set; }
        [Required]
        [Display(Name = "CNR No.")]
        public string CNRNo { get; set; }
        [Required]
        [Display(Name = "Date"), DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOn { get; set; }
        public bool IsActive { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }

        public override ImpungedAndSTDetailMvcModel EntityToResponse(ImpungedAndSTDetail entity, ImpungedAndSTDetailMvcModel model)
        {
            model.CaseNoticeType = CaseNoticeTypeMvcModel.CopyFromEntity(entity.CaseNoticeType, model.RecursiveLevel);
            model.SubordinateCourt = SubordinateCourtMvcModel.CopyFromEntity(entity.SubordinateCourt, model.RecursiveLevel);
            return model;
        }
    }
}