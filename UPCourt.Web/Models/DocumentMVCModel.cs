﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DocumentMVCModel : BaseEntityModel<DocumentMaster, DocumentMVCModel>
    {

        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
        public string ModifyBy { get; set; }
        public bool IsNew { get; set; }
    }

}