﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CaseDetailsMvcModel : BaseEntityModel<Casedetail, CaseDetailsMvcModel>
    {
        public Guid CaseDetailId { get; set; }
        public Guid NoticeId { get; set; }
        public NoticeMvcModel Notice { get; set; }
        public string PetName { get; set; }
        public int BenchType { get; set; }
        public int Cs_subject { get; set; }
        public int C_subject { get; set; }
        public SubCategoryMvcModel SubCategory { get; set; }
        public CategoryMvcModel Category { get; set; }
        public int DispNature { get; set; }
        public int PurposeNext { get; set; }
        public string JudgeCode { get; set; }
        public DateTime DateOfDecision { get; set; }
        public string ResName { get; set; }
        public int CaseStateCode { get; set; }
        public int CaseDistCode { get; set; }
        public DistrictMvcModel District { get; set; }
        public string Case_no { get; set; }
        public int CauselistType { get; set; }
        public CauseListTypeMvcModel CauselistTypes { get; set; }
        public int BranchId { get; set; }
        public BenchTypeMvcModel Bench { get; set; }
        public int Status { get; set; }

        public int pet_adv_cd { get; set; }
        public ApiAdvocateMvcModel PetAdvocate { get; set; }
        public int res_adv_cd { get; set; }
        public ApiAdvocateMvcModel RespAdvocate { get; set; }

        public string fir_no { get; set; }
        public int? fir_year { get; set; }
        public string dist_name { get; set; }
        public string police_st_name { get; set; }
        public List<CaseDetailIAMvcModel> IA { get; set; }
        public List<CaseDetailPetMvcModel> Petitioner { get; set; }
        public List<CasedetailRespMvcModel> Respondent { get; set; }
        public override CaseDetailsMvcModel EntityToResponse(Casedetail entity, CaseDetailsMvcModel model)
        {
            if (entity.IAs != null)
                model.IA = CaseDetailIAMvcModel.CopyFromEntityList(entity.IAs.OrderBy(x => x.ia_regno), model.RecursiveLevel);

            if (entity.Petitioners != null)
                model.Petitioner = CaseDetailPetMvcModel.CopyFromEntityList(entity.Petitioners.OrderBy(x => x.party_no), model.RecursiveLevel);

            if (entity.Respondents != null)
                model.Respondent = CasedetailRespMvcModel.CopyFromEntityList(entity.Respondents.OrderBy(x => x.party_no), model.RecursiveLevel);

            model.Notice = NoticeMvcModel.CopyFromEntity(entity.Notice, model.RecursiveLevel);
            return model;
        }
    }


    public class SubCategoryMvcModel : BaseEntityModel<SubCategory, SubCategoryMvcModel>
    {
        public Guid SubCategoryId { get; set; }
        public int SubCategoryCode { get; set; }
        public string SubCategoryName { get; set; }
        public int CategoryCode { get; set; }
        public SubCategoryStatus Status { get; set; }
    }

    public class CategoryMvcModel : BaseEntityModel<Category, CategoryMvcModel>
    {
        public Guid CategoryId { get; set; }
        public int CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public CategoryStatus Status { get; set; }
    }

    public class CauseListTypeMvcModel : BaseEntityModel<CauseListType, CauseListTypeMvcModel>
    {
        public Guid CauseListTypeId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public CauseListTypeStatus Status { get; set; }
    }
    public class CaseDetailIAMvcModel : BaseEntityModel<CasedetailIa, CaseDetailIAMvcModel>
    {
        public Guid CaseDetailIAId { get; set; }
        public Guid CaseDetailId { get; set; }
        public Casedetail CaseDetail { get; set; }
        public int ia_regno { get; set; }
        public DateTime date_of_ia_registration { get; set; }
        public DateTime date_of_hearing { get; set; }
        public int court_no { get; set; }
        public string judge_code { get; set; }
        public int ia_regyear { get; set; }
        public DateTime date_of_order { get; set; }
        public int Status { get; set; }

    }

    public class CaseDetailPetMvcModel : BaseEntityModel<CasedetailPet, CaseDetailPetMvcModel>
    {
        public Guid CaseDetailPetId { get; set; }
        public Guid CaseDetailId { get; set; }
        public int party_no { get; set; }
        public string PetName { get; set; }
        public int Status { get; set; }
    }

    public class CasedetailRespMvcModel : BaseEntityModel<CasedetailResp, CasedetailRespMvcModel>
    {
        public Guid CaseDetailRespId { get; set; }
        public Guid CaseDetailId { get; set; }
        public int party_no { get; set; }
        public string RespName { get; set; }
        public int Status { get; set; }
    }


}
