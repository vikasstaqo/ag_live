﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class ChangePasswordController : BaseMVCController
    {
        // GET: ChangePassword
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult ChangePwd(ChangePasswordModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = UserDataService.GetDetail(db).Where(i => i.LoginId == model.username).FirstOrDefault();
            var passwordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.NewPassword, passwordSalt);
            dbData.PasswordSalt = passwordSalt;
            dbData.PasswordHash = passwordHash;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = "Password Changed successfully";
            return Json(new { code = "201", message = SucessMessage });
        }

        [HttpPost]
        public ActionResult BindUser()
        {
            if (AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.Admin || AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                var dbData = UserDataService.GetDetail(db).Select(i => new
                {
                    i.UserId,
                    i.LoginId
                }).Where(i => UserStatus.Active.ToBool() == true).ToArray();
                return Json(new { users = dbData });
            }
            else
            {
                var dbData = UserDataService.GetDetail(db).Select(i => new
                {
                    i.UserId,
                    i.LoginId
                }).Where(i => UserStatus.Active.ToBool() == true && i.UserId == AppUser.UserId).ToArray();
                return Json(new { users = dbData });
            }
        }

    }
}