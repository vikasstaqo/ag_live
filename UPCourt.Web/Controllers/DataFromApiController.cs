﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Helper;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;
using static UPCourt.Web.Models.ApiDataModel;

namespace UPCourt.Web.Controllers
{
    public class DataFromApiController : ApiController
    {
        public DatabaseContext db = new DatabaseContext();
        readonly DataFromApiHelper helper = new DataFromApiHelper();
        Guid SystemGuid;
        [HttpGet]
        public string Get(string id)
        {
            try
            {
                SystemGuid = UserDataService.GetDetail(db).Where(i => i.Name == "Admin").Select(u => u.UserId).FirstOrDefault();
                if (id == "BenchTypes")
                {
                    string benchTypeStatus = SyncBenchTypes();
                    return @" benchTypeStatus : " + benchTypeStatus;
                }
                else if (id == "Judges")
                {
                    string JudgeStatus = SyncJudges();
                    return @" JudgeStatus : " + JudgeStatus;
                }
                else if (id == "OIPCs")
                {
                    string OIPCStatus = SyncOIPC();
                    return @" OIPCStatus : " + OIPCStatus;
                }
                else if (id == "CaseTypes")
                {
                    string CaseTypeStatus = SyncCaseTypes();
                    return @" CaseTypeStatus : " + CaseTypeStatus;
                }
                else if (id == "Purpose")
                {
                    string PurposeStatus = SyncPurpose();
                    return @" PurposeStatus : " + PurposeStatus;
                }
                else if (id == "DisposalType")
                {
                    string DisposalTypeStatus = SyncDisposalType();
                    return @" DisposalTypeStatus : " + DisposalTypeStatus;
                }
                else if (id == "CauseListTypes")
                {
                    string CauseListTypesStatus = SyncCauseListType();
                    return @" CauseListTypesStatus : " + CauseListTypesStatus;
                }
                else if (id == "CauseList")
                {
                    string CauseListStatus = SyncCauseList();
                    return @" CauseList Status : " + CauseListStatus;
                }
                else if (id == "CategorySubCategory")
                {
                    string CategorySubCategoryStatus = SyncCategorySubCategory();
                    return @" CategorySubCategoryStatus : " + CategorySubCategoryStatus;
                }
                else if (id == "Benche")
                {
                    string BenchesStatus = SyncBenches();
                    return @" BenchesStatus : " + BenchesStatus;
                }
                else if (id == "NoticeNo")
                {
                    string NoticeNoStatus = SyncNoticeNo();
                    return @" NoticeNoStatus : " + NoticeNoStatus;
                }
                else if (id == "NoticeCases")
                {
                    string NoticeCasesStatus = SyncNoticeCases();
                    return @" NoticeCasesStatus : " + NoticeCasesStatus;
                }
                else
                {
                    return @" Parameter required ";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string SyncBenchTypes()
        {
            List<getBenchTypes> BenchTypes = new List<getBenchTypes>();
            var content = GetDataModelAsync("cismaster/getBenchTypes/");
            BenchTypes = JsonConvert.DeserializeObject<List<getBenchTypes>>(content.Result);

            foreach (var benchtype in BenchTypes)
            {
                var dbData = BenchTypeDataService.GetAll(db).Where(i => i.bench_type_code == benchtype.bench_type_code).FirstOrDefault();
                if (dbData == null)
                {
                    BenchType tempBenchType = new BenchType
                    {
                        BenchTypeID = Guid.NewGuid(),
                        DisplayOrder = MasterBusinessService.GetBenchTypeDisplayOrder(db),
                        Name = benchtype.bench_type_name,
                        bench_type_code = benchtype.bench_type_code,
                        IsActive = true,
                        Description = benchtype.bench_type_name,
                        NumberOfJudge = 0,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                    };
                    db.BenchType.Add(tempBenchType);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.Name != benchtype.bench_type_name)
                    {
                        dbData.Name = benchtype.bench_type_name;
                        dbData.Description = benchtype.bench_type_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncJudges()
        {
            List<getJudges> Judges = new List<getJudges>();
            var content = GetDataModelAsync("cismaster/getJudges/");
            Judges = JsonConvert.DeserializeObject<List<getJudges>>(content.Result);

            foreach (var Judge in Judges)
            {
                var dbData = JudgeDataService.GetAll(db).Where(i => i.judge_code == Judge.judge_code).FirstOrDefault();
                if (dbData == null)
                {
                    Judge tempData = new Judge()
                    {
                        JudgeId = Guid.NewGuid(),
                        Name = Judge.judge_name,
                        judge_code = Judge.judge_code,
                        Status = JudgeStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetJudgesDisplayOrder(db),
                        short_judge_name = Judge.short_judge_name,
                    };
                    db.Judges.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.Name != Judge.judge_name)
                    {
                        dbData.Name = Judge.judge_name;
                        dbData.short_judge_name = Judge.short_judge_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncOIPC()
        {
            List<getOIPC> OIPCS = new List<getOIPC>();
            var content = GetDataModelAsync("cismaster/getOIPC/");
            OIPCS = JsonConvert.DeserializeObject<List<getOIPC>>(content.Result);

            foreach (var OIPC in OIPCS)
            {
                var dbData = IPCSectionDataService.GetAll(db).Where(i => i.SectionName == OIPC.actcode.ToString()).FirstOrDefault();
                if (dbData == null)
                {
                    IPCSection tempData = new IPCSection()
                    {
                        IPCSectionId = Guid.NewGuid(),
                        SectionName = OIPC.actcode.ToString(),
                        Description = OIPC.actname,
                        Status = IPCSectionStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetIPSectionDisplayOrder(db)
                    };
                    db.IPCSections.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.Description != OIPC.actname)
                    {
                        dbData.Description = OIPC.actname;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncCaseTypes()
        {
            List<getCaseTypes> CaseTypes = new List<getCaseTypes>();
            var content = GetDataModelAsync("cismaster/getCaseTypes/");
            CaseTypes = JsonConvert.DeserializeObject<List<getCaseTypes>>(content.Result);

            foreach (var CaseType in CaseTypes)
            {
                var dbData = NoticeTypeDataService.GetAll(db).Where(i => i.case_type == CaseType.case_type).FirstOrDefault();
                if (dbData == null)
                {
                    CaseNoticeType tempCaseNoticeType = new CaseNoticeType
                    {
                        CaseNoticeTypeId = Guid.NewGuid(),
                        DisplayOrder = MasterBusinessService.GetCaseNoticeTypesDisplayOrder(db),
                        CreatedBy = SystemGuid,
                        case_type = CaseType.case_type,
                        Name = CaseType.full_form,
                        type_name = CaseType.type_name,
                        IsActive = true,
                        ParentNoticeType = (CaseType.type_flag == "1" ? ParentNoticeType.Civil : ParentNoticeType.Criminal)
                    };
                    db.CaseNoticeTypes.Add(tempCaseNoticeType);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.Name != CaseType.full_form)
                    {
                        dbData.Name = CaseType.full_form;
                        dbData.type_name = CaseType.type_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncPurpose()
        {
            List<getPurpose> Purposes = new List<getPurpose>();
            var content = GetDataModelAsync("cismaster/getPurpose/");
            Purposes = JsonConvert.DeserializeObject<List<getPurpose>>(content.Result);

            foreach (var Purpose in Purposes)
            {
                var dbData = PurposeDataService.GetAll(db).Where(i => i.Purposecode == Purpose.purpose_code).FirstOrDefault();
                if (dbData == null)
                {
                    Purpose tempData = new Purpose()
                    {
                        PurposeId = Guid.NewGuid(),
                        PurposeName = Purpose.purpose_name,
                        Purposecode = Purpose.purpose_code,
                        Status = PurposeStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetPurposeDisplayOrder(db)
                    };
                    db.Purposes.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.PurposeName != Purpose.purpose_name)
                    {
                        dbData.PurposeName = Purpose.purpose_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncDisposalType()
        {
            List<getDisposalType> DisposalTypes = new List<getDisposalType>();
            var content = GetDataModelAsync("cismaster/getDisposalTypes/");
            DisposalTypes = JsonConvert.DeserializeObject<List<getDisposalType>>(content.Result);

            foreach (var DisposalType in DisposalTypes)
            {
                var dbData = DisposalTypeDataService.GetAll(db).Where(i => i.DispType == DisposalType.disp_type).FirstOrDefault();
                if (dbData == null)
                {
                    DisposalType tempData = new DisposalType()
                    {
                        DisposalTypeId = Guid.NewGuid(),
                        DispTypeName = DisposalType.disp_name,
                        DispType = DisposalType.disp_type,
                        Status = DisposalTypeStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetDisposalTypeDisplayOrder(db)
                    };
                    db.DisposalTypes.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.DispTypeName != DisposalType.disp_name)
                    {
                        dbData.DispTypeName = DisposalType.disp_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }

        public string SyncCauseListType()
        {
            List<getCauseListType> CauseListTypes = new List<getCauseListType>();
            var content = GetDataModelAsync("cismaster/getCauseListTypes/");
            CauseListTypes = JsonConvert.DeserializeObject<List<getCauseListType>>(content.Result);

            foreach (var _CauseListType in CauseListTypes)
            {
                var dbData = CauseListTypeDataService.GetAll(db).Where(i => i.Id == _CauseListType.cause_list_type_id).FirstOrDefault();
                if (dbData == null)
                {
                    CauseListType tempData = new CauseListType()
                    {
                        CauseListTypeId = Guid.NewGuid(),
                        Name = _CauseListType.cause_list_type,
                        Id = _CauseListType.cause_list_type_id,
                        Status = CauseListTypeStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetCauseListTypeDisplayOrder(db)
                    };
                    db.CauseListTypes.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.Name != _CauseListType.cause_list_type)
                    {
                        dbData.Name = _CauseListType.cause_list_type;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }

        public string SyncCauseList()
        {
            List<getCauseListDateWise> CauseList_date = new List<getCauseListDateWise>();
           
            var content = GetDataModelAsync("cismaster/getCauseListDatewise/" + DateTime.Now.ToString("yyyy-MM-dd"));


            CauseList_date = JsonConvert.DeserializeObject<List<getCauseListDateWise>>(content.Result);
            var dbCauseListTypes = CauseListTypeDataService.GetAll(db).ToList();
            var dbCaseDetails = CasedetailDataService.GetAll(db).ToList();
            int cntr = 0;
            foreach (var _CauseList in CauseList_date)
            {
                cntr++;
                var dbCauseListType = dbCauseListTypes.Where(i => i.Id == _CauseList.causelist_type).FirstOrDefault();
                var dbCaseDetail = dbCaseDetails.Where(i => i.Case_no == _CauseList.case_no).FirstOrDefault();
                DateWiseCauseList tempData_datewise = new DateWiseCauseList()
                {
                    DateWiseCauseListId = Guid.NewGuid(),
                    CauseListTypeId = dbCauseListType.CauseListTypeId,
                    PetName = _CauseList.pet_name,
                    BenchId = _CauseList.bench_id,
                    cause_list_eliminate = _CauseList.cause_list_eliminate,
                    elimination = _CauseList.elimination,
                    CauseList_Id = _CauseList.causelist_id,
                    ResName = _CauseList.res_name,
                    CauseListStatus = _CauseList.cause_list_status,
                    causelist_sr_no = _CauseList.causelist_sr_no,
                    Case_no = _CauseList.case_no,
                    published = _CauseList.published,
                    CauseListDate = _CauseList.causelist_date,
                    CauseList_Type = _CauseList.causelist_type,
                    Court_no = _CauseList.court_no,
                    for_bench_id = _CauseList.for_bench_id,
                    Status = CauseList_DateWiseStatus.Active,
                    CreatedBy = SystemGuid,
                    CreatedOn = DateTime.Now,
                    DisplayOrder = MasterBusinessService.GetCauseListTypeDisplayOrder(db)
                };
                db.DateWiseCauseLists.Add(tempData_datewise);
                db.SaveChanges();

                if (dbCaseDetail != null)
                {
                    CauseList tempData = new CauseList()
                    {
                        CauseListId = Guid.NewGuid(),
                        CauseListTypeId = dbCauseListType.CauseListTypeId,
                        PetName = _CauseList.pet_name,
                        BenchId = _CauseList.bench_id,
                        cause_list_eliminate = _CauseList.cause_list_eliminate,
                        elimination = _CauseList.elimination,
                        CauseList_Id = _CauseList.causelist_id,
                        ResName = _CauseList.res_name,
                        CauseListStatus = _CauseList.cause_list_status,
                        causelist_sr_no = _CauseList.causelist_sr_no,
                        Case_no = _CauseList.case_no,
                        published = _CauseList.published,
                        CauseListDate = _CauseList.causelist_date,
                        CauseList_Type = _CauseList.causelist_type,
                        Court_no = _CauseList.court_no,
                        for_bench_id = _CauseList.for_bench_id,
                        Status = CauseListStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetCauseListTypeDisplayOrder(db)
                    };
                    db.CauseLists.Add(tempData);
                    db.SaveChanges();
                }
            }

            db.DateWiseCauseLists.RemoveRange(db.DateWiseCauseLists.Where(x => x.CauseListDate < DateTime.Now.AddDays(-7)));

            db.SaveChanges();

            if (cntr == 0 && CauseList_date.Count > 0)
                return "no records updated";
            else if (CauseList_date.Count == 0)
                return "no records found";
            else
                return cntr.ToString() + " records done" + ((CauseList_date.Count - cntr) > 0 ? ", " + (CauseList_date.Count - cntr).ToString() + " failed" : "");

        }

        private void UpdateCreateNewNotice()
        {
            Guid noticeGuid = Guid.NewGuid();
            var dbNotice = NoticeDataService.GetLite(db).Where(x => x.Cnr == "UPHC010940432022").FirstOrDefault();
          
            Notice notice = new Notice()
            {
                NoticeId = noticeGuid,
                NoticeType = dbNotice.NoticeType,
                SerialNo = dbNotice.SerialNo,
                NoticeNo = dbNotice.NoticeNo,
                NoticeDate = dbNotice.NoticeDate,
                NoticeYear = dbNotice.NoticeYear,
                Subject = dbNotice.Subject,
                IsContextOrder = dbNotice.IsContextOrder,
                RegionId = dbNotice.RegionId,
                Status = dbNotice.Status,
                CreatedBy = dbNotice.CreatedBy,
                CaseNoticeTypeId = dbNotice.CaseNoticeTypeId,
                NoticeFrom = dbNotice.NoticeFrom,
                IsDeleted = dbNotice.IsDeleted,
                NoticeCreateDate = dbNotice.NoticeCreateDate,
                CreatedOn = dbNotice.CreatedOn,
                ModifiedOn = dbNotice.ModifiedOn,
                RegCaseType = dbNotice.RegCaseType,
                RegNo = dbNotice.RegNo,
                Cnr = dbNotice.Cnr
            };

            db.Notices.Add(notice);
            db.SaveChanges();
        }

        public string SyncCategorySubCategory()
        {
            List<getCategorySubCategory> CategorySubCategorys = new List<getCategorySubCategory>();
            var content = GetDataModelAsync("cismaster/getSubject/");
            CategorySubCategorys = JsonConvert.DeserializeObject<List<getCategorySubCategory>>(content.Result);

            List<getCategory> Categorys = CategorySubCategorys.GroupBy(e => new { subject_name = e.subject_name, subject_code = e.subject_code })
                .Select(x => new getCategory { subject_name = x.Key.subject_name, subject_code = x.Key.subject_code }).ToList();
            List<getSubCategory> SubCategorys = CategorySubCategorys.GroupBy(e => new { subnature1_cd = e.subnature1_cd, subnature1_desc = e.subnature1_desc, subject_code = e.subject_code })
                .Select(x => new getSubCategory { subnature1_cd = x.Key.subnature1_cd, subnature1_desc = x.Key.subnature1_desc, subject_code = x.Key.subject_code }).Distinct().ToList();


            foreach (var Category in Categorys)
            {
                var dbData = CategoryDataService.GetAll(db).Where(i => i.CategoryCode == Category.subject_code).FirstOrDefault();
                if (dbData == null)
                {
                    Category tempData = new Category()
                    {
                        CategoryId = Guid.NewGuid(),
                        CategoryName = Category.subject_name,
                        CategoryCode = Category.subject_code,
                        Status = CategoryStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetCategoryDisplayOrder(db)
                    };
                    db.Categorys.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.CategoryName != Category.subject_name)
                    {
                        dbData.CategoryName = Category.subject_name;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }


            foreach (var SubCategory in SubCategorys)
            {
                var dbData = SubCategoryDataService.GetAll(db).Where(i => i.SubCategoryCode == SubCategory.subnature1_cd && i.CategoryCode == SubCategory.subject_code).FirstOrDefault();
                if (dbData == null)
                {
                    SubCategory tempData = new SubCategory()
                    {
                        SubCategoryId = Guid.NewGuid(),
                        SubCategoryName = SubCategory.subnature1_desc,
                        SubCategoryCode = SubCategory.subnature1_cd,
                        CategoryCode = SubCategory.subject_code,
                        Status = SubCategoryStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetSubCategoryDisplayOrder(db)
                    };
                    db.SubCategorys.Add(tempData);
                    db.SaveChanges();
                }
                else
                {
                    if (dbData.SubCategoryName != SubCategory.subnature1_desc)
                    {
                        dbData.SubCategoryName = SubCategory.subnature1_desc;
                        dbData.ModifiedBy = SystemGuid;
                        dbData.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }

            return "done";
        }
        public string SyncBenches()
        {
            List<getBenches> Benches = new List<getBenches>();
            var content = GetDataModelAsync("cismaster/getBenches/");
            Benches = JsonConvert.DeserializeObject<List<getBenches>>(content.Result);

            foreach (var Benche in Benches)
            {
                var dbData = BencheDataService.GetAll(db).Where(i => i.ForBenchId == Benche.for_bench_id).FirstOrDefault();
                if (dbData == null)
                {
                    Benche tempData = new Benche()
                    {
                        BencheId = Guid.NewGuid(),
                        ForBenchId = Benche.for_bench_id,
                        RoomNo = Benche.room_no,
                        CourtNo = Benche.court_no,
                        Status = BencheStatus.Active,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now,
                        DisplayOrder = MasterBusinessService.GetBencheDisplayOrder(db)
                    };
                    db.Benches.Add(tempData);
                    db.SaveChanges();
                    List<getJudges> Judges = Benche.judges;
                    foreach (var judge in Judges)
                    {
                        BencheJudge btempData = new BencheJudge()
                        {
                            BencheJudgeId = Guid.NewGuid(),
                            ForBenchId = Benche.for_bench_id,
                            JudgeName = judge.judge_name,
                            CreatedBy = SystemGuid,
                            CreatedOn = DateTime.Now,
                            DisplayOrder = MasterBusinessService.GetBencheJudgesDisplayOrder(db)
                        }; db.BencheJudges.Add(btempData);
                        db.SaveChanges();
                    }
                }
            }
            return "done";
        }
        public string SyncNoticeNo()
        {
            int noticeCaseCounts = helper.SyncNoticeNo();
            return noticeCaseCounts.ToString();
        }

        public string SyncNoticeCases()
        {
            int noticeCaseCounts = helper.SyncNoticeCases();
            return noticeCaseCounts.ToString();
        }

        private static Task<string> GetDataModelAsync(string url)
        {
            HttpClient client = new HttpClient() { BaseAddress = new Uri("https://efiling-alld.allahabadhighcourt.in") };
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "YWhjOmFoY0A1NDMyMQ==");
            client.Timeout = TimeSpan.FromMinutes(10);
            var result = client.GetAsync(url);
            result.Wait();
            var content = result.Result.Content.ReadAsStringAsync();
            return content;
        }


    }
}
