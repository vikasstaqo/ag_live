﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DashboardMvcModel : BaseEntityModel<Case, DashboardMvcModel>
    {
        [Display(Name = "Active Case")]
        public int UserPendingRequest { get; set; }

        [Display(Name = "Total Notices")]
        public int TotalNotices { get; set; }
        [Display(Name = "Pending Notices")]
        public int PendingNotices { get; set; }
        [Display(Name = "Total Assigned Cases")]
        public int TotalAssignedCases { get; set; }
        [Display(Name = "Total UnAssigned Cases")]
        public int TotalUnAssignedCases { get; set; }
        public int TodayNotice { get; set; }
        public int WeeklyNotice { get; set; }
        public int MonthNotice { get; set; }
        public int YearlyNotice { get; set; }

        public UserMvcModel User { get; set; }

        public List<UserCredentialRequest> UserRequest { get; set; }
        public List<CaseMvcModel> Cases { get; set; }
        public List<NoticeMvcModel> Notices { get; set; }
        public List<CaseMvcModel> UnAssignedCases { get; set; }
        public List<CaseMvcModel> CaseHearing { get; set; }
        public List<UserOfficials> UserOfficials { get; set; }
        public List<UserOfficials> Availablecouncil { get; set; }
        public List<DashboardCase> DashboardCase { get; set; }
        public List<DistrictWiseCase> DistrictWiseCase { get; set; }
        public List<NoticeTypeWiseCase> NoticeTypeWiseCase { get; set; }
        public List<GovernmentDepartmentWiseCase> GovernmentDepartmentWiseCase { get; set; }
        public override DashboardMvcModel EntityToResponse(Case entity, DashboardMvcModel model)
        {
            return model;
        }
        public List<MonthGraphData> ActiveCasesCounts { get; set; }
        public List<MonthGraphData> DisposedCasesCounts { get; set; }

        public List<MonthGraphData> ContextOrdersCounts { get; set; }
        public List<DayGraphData> PendingNoticesCounts { get; set; }
        public List<CaseMvcModel> DisposedCases { get; set; }
        public string DocumentRequestedCasesCount { get; set; }
        public string DocumentReceivedCasesCount { get; set; }
        public string TimeBoundComplianceCount { get; set; }
        public int OICUserCount { get; internal set; }
        public int DesignationUserCount { get; set; }

    }
    public class UserOfficials
    {
        public string OfficialName { get; set; }
        public int UserCount { get; set; }

    }
    public class DashboardCase : CaseStatus
    {
        public int Count { get; set; }

    }
    public class DistrictWiseCase
    {
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int CaseCount { get; set; }
    }
    public class NoticeTypeWiseCase
    {
        public Guid NoticeTypeId { get; set; }
        public string NoticeTypeName { get; set; }
        public int CaseCount { get; set; }
    }

    public class MonthGraphData
    {
        public string MonthName { get; set; }
        public int CaseCount { get; set; }
    }
    public class DayGraphData
    {
        public string DayName { get; set; }
        public int CaseCount { get; set; }
    }
    public class GovernmentDepartmentWiseCase
    {
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int CaseCount { get; set; }
    }
}