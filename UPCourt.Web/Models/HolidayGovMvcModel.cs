﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class HolidayGovMvcModel : BaseEntityModel<HolidayGov, HolidayGovMvcModel>
    {
        public Guid GovernmentDepartmentDCId { get; set; }


        //[MaxLength(50)]
        //[Display(Name = "Department Short Name (Max 50 chars)")]
        //public string DepartmentShortName { get; set; }
        //[MaxLength(200)]
        //[Display(Name = "Department Full Name (Max 200 chars)")]
        //public string DepartmentFullName { get; set; }
        //[MaxLength(200)]
        //[Display(Name = "Address Line 1 (Max 200 chars)")]
        //public string Address1 { get; set; }
        ////[MaxLength(200)]
        //[Display(Name = "Address Line 2 (Max 200 chars)")]
        //public string Address2 { get; set; }
        //[MaxLength(100)]
        //[Display(Name = "City/Village (Max 100 chars)")]

        //public string Holidayg { get; set; }
        //public string District { get; set; }
        //[Display(Name = "Other District (Max 200 chars)")]
        //[MaxLength(200)]



        // public Guid GovernmentDepartmentDCId { get; set; }
        // public GovernmentDeparmentType DepartmentType { get; set; }
        //public string DepartmentShortName { get; set; }
        //public string DepartmentFullName { get; set; }
        //public string Address1 { get; set; }
        //public string Address2 { get; set; }

        //public string District { get; set; }
        public Guid HolidayId { get; set; }
        public string Holidayg { get; set; }




        // public string Tehsil { get; set; }

        //public string GovBlock { get; set; }
        //[Display(Name = "Block Name")]
        //public string GovBlk { get; set; }
        // public List<GovernmentDepartmentDCContactMvcModel> GovernmentDepartmentDCContacts { get; set; }
        //public override HolidayGovMvcModel EntityToResponse(HolidayGov entity, HolidayGovMvcModel model)
        //{
        //    model.GovernmentDepartmentDCContacts = GovernmentDepartmentDCContactMvcModel.CopyFromEntityList(entity.GovernmentDepartmentDCContacts, model.RecursiveLevel);
        //    return model;
        //}
    }
}