﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifiedBy))]
    public class Court:BaseMasterEntity
    {
        public Court()
        {
            AdvocateCourts = new HashSet<AdvocateCourt>();
            CourtOpenCloseLogs = new HashSet<CourtOpenCloseLog>();
            Cases = new HashSet<Case>();
            this.CaseTransfers = new HashSet<CaseTransfer>();            
        }
        public Guid CourtId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public Guid BenchTypeID { get; set; }
        public CourtStatus Status { get; set; }
        public bool IsClose { get; set; }
      
        public ICollection<AdvocateCourt> AdvocateCourts { get; set; }
        public ICollection<CourtOpenCloseLog> CourtOpenCloseLogs { get; set; }
        public BenchType BenchType { get; set; }
        public ICollection<Case> Cases { get; set; }
        public ICollection<CaseTransfer> CaseTransfers { get; set; }
    }
    
    public enum CourtStatus
    {
        Inactive = 0,
        Active = 1
    }
}
