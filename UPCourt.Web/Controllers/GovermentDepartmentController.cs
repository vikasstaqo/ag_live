﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class GovermentDepartmentController : BaseMVCController
    {
        // GET: Department
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(GovermentDepartmentMvcModel.CopyFromEntityList(GovermentDepartmentDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        // GET: Department/Create
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            InitilizeRegions(null);
            var model = new GovermentDepartmentMvcModel
            {
                GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddressMvcModel> { new GovernmentDepartmentAddressMvcModel() { } },
                GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNoMvcModel> { new GovernmentDepartmentContactNoMvcModel() { } },
                GovDesignations = new List<GovDesignationMvcModel> { new GovDesignationMvcModel() { Status = GovernmentDesignationStatus.Active } },
            };
            model.Status = GovtDeparmentStatus.Active;
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(GovermentDepartmentMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            GovernmentDepartment tempData = new GovernmentDepartment
            {
                GovernmentDepartmentId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Status = model.Status,
                IsDefault = model.IsDefault,
                POCName = model.POCName,
                POCContact = model.POCContact,
                ModifiedBy = AppUser.UserId,
                HeadOffice = model.HeadOffice,
                POCFaxNo = model.POCFaxNo,
                POCMobileNo = model.POCMobileNo,
                IsAssignUser = true,
                DisplayOrder = MasterBusinessService.GetGovernmentDepartmentsDisplayOrder(db),
                GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddress>(),
                GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNo>()
            };
            foreach (var item in model.GovernmentDepartmentContactNos.Where(x => x.bActive == true))
            {
                GovernmentDepartmentContactNo governmentDepartmentContactNo = new GovernmentDepartmentContactNo();
                governmentDepartmentContactNo.GovernmentDepartmentContactNoId = Guid.NewGuid();
                governmentDepartmentContactNo.GovernmentDepartmentId = tempData.GovernmentDepartmentId;
                governmentDepartmentContactNo.ContactNo = item.ContactNo;
                governmentDepartmentContactNo.ModifyBy = AppUser.UserName;
                db.GovernmentDepartmentContactNos.Add(governmentDepartmentContactNo);
            }
            foreach (var item in model.GovernmentDepartmentAddresses.Where(x => x.bActive == true))
            {
                GovernmentDepartmentAddress tempGovernmentDepartmentAddress = new GovernmentDepartmentAddress();
                tempGovernmentDepartmentAddress.GovernmentDepartmentAddressId = Guid.NewGuid();
                tempGovernmentDepartmentAddress.GovernmentDepartmentId = tempData.GovernmentDepartmentId;
                tempGovernmentDepartmentAddress.Address = item.Address;
                tempGovernmentDepartmentAddress.IsHeadOfficeAddress = item.IsHeadOfficeAddress;
                tempGovernmentDepartmentAddress.ModifyBy = AppUser.UserName;
                db.GovernmentDepartmentAddresses.Add(tempGovernmentDepartmentAddress);
            }
            if (model.GovDesignations != null)
            {
                foreach (var item in model.GovDesignations.Where(x => x.bActive == true))
                {
                    GovernmentDesignation tempGovernmentDesignation = new GovernmentDesignation();
                    tempGovernmentDesignation.GovernmentDesignationId = Guid.NewGuid();
                    tempGovernmentDesignation.GovernmentDepartmentId = tempData.GovernmentDepartmentId;
                    tempGovernmentDesignation.DesignationName = item.DesignationName;
                    tempGovernmentDesignation.POCName = item.POCName;
                    tempGovernmentDesignation.POCContact = item.POCContact;
                    tempGovernmentDesignation.POCFaxNo = item.POCFaxNo;
                    tempGovernmentDesignation.POCMobileNo = item.POCMobileNo;
                    tempGovernmentDesignation.Status = item.Status;
                    tempGovernmentDesignation.DisplayOrder = MasterBusinessService.GetDesignationsDisplayOrder(db);
                    tempGovernmentDesignation.ModifiedBy = AppUser.UserId;
                    db.GovernmentDesignations.Add(tempGovernmentDesignation);
                }
            }


            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            var userRoleID = UserRoleDataService.GetDetail(db).Where(x => x.UserSystemRoleId == UserSystemRole.GovDeptHead).FirstOrDefault().UserRoleId;

            User tmpUser = new User
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                LoginId = model.Email,
                IsDefault = model.IsDefault,
                Status = (UserStatus)model.Status,
                Phone = model.GovernmentDepartmentContactNos[0].ContactNo,
                Address = model.GovernmentDepartmentAddresses[0].Address,
                UserType = UserType.GovernmentDepartment,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                AGDepartmentId = null,
                GovernmentDepartmentId = tempData.GovernmentDepartmentId,
                UserRoleId = userRoleID,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetUserDisplayOrder(db)
            };
            db.Users.Add(tmpUser);
            db.GovernmentDepartments.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
                        DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            var model = GovermentDepartmentMvcModel.CopyFromEntity(dbData, 0);
            if (model.GovDesignations.Count == 0)
            {
                model.GovDesignations = new List<GovDesignationMvcModel> { new GovDesignationMvcModel() { Status = GovernmentDesignationStatus.Active } };
            }
            if (model.GovernmentDepartmentAddresses.Count == 0)
            {
                model.GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddressMvcModel> { new GovernmentDepartmentAddressMvcModel() { } };
            }
            if (model.GovernmentDepartmentContactNos.Count == 0)
            {
                model.GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNoMvcModel> { new GovernmentDepartmentContactNoMvcModel() { } };
            }
            return View(model);
        }
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(GovermentDepartmentMvcModel model)
        {

            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }


            dbData.Name = model.Name;
            dbData.Email = model.Email;
            dbData.Status = model.Status;
            dbData.IsDefault = model.IsDefault;
            dbData.POCName = model.POCName;
            dbData.POCContact = model.POCContact;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.HeadOffice = model.HeadOffice;
            dbData.POCFaxNo = model.POCFaxNo;
            dbData.POCMobileNo = model.POCMobileNo;

            foreach (var Deleted_DepartmentAddressesModel in model.GovernmentDepartmentAddresses.Where(x => x.bActive == false))
            {
                if (Deleted_DepartmentAddressesModel.GovernmentDepartmentAddressId != null)
                {
                    GovernmentDepartmentAddress GDAdd = dbData.GovernmentDepartmentAddresses.Where(p => p.GovernmentDepartmentAddressId == Deleted_DepartmentAddressesModel.GovernmentDepartmentAddressId).FirstOrDefault();
                    dbData.GovernmentDepartmentAddresses.Remove(GDAdd);
                }
            }
            foreach (var Item_DepartmentAddressesModel in model.GovernmentDepartmentAddresses.Where(x => x.bActive == true))
            {
                bool newAddress = false;
                GovernmentDepartmentAddress GDAdd = dbData.GovernmentDepartmentAddresses.Where(p => p.GovernmentDepartmentAddressId == Item_DepartmentAddressesModel.GovernmentDepartmentAddressId).FirstOrDefault();
                if (GDAdd == null)
                {
                    newAddress = true;
                    GDAdd = new GovernmentDepartmentAddress();
                    GDAdd.GovernmentDepartmentId = dbData.GovernmentDepartmentId;
                    GDAdd.GovernmentDepartmentAddressId = Guid.NewGuid();
                }
                if (GDAdd.GovernmentDepartmentAddressId == null || GDAdd.GovernmentDepartmentAddressId == Guid.Empty)
                    GDAdd.GovernmentDepartmentAddressId = Item_DepartmentAddressesModel.GovernmentDepartmentAddressId;

                GDAdd.Address = Item_DepartmentAddressesModel.Address;
                GDAdd.IsHeadOfficeAddress = Item_DepartmentAddressesModel.IsHeadOfficeAddress;
                GDAdd.ModifyBy = AppUser.UserName;

                if (newAddress)
                    dbData.GovernmentDepartmentAddresses.Add(GDAdd);
            }
                        foreach (var Deleted_DepartmentContactNosModel in model.GovernmentDepartmentContactNos.Where(x => x.bActive == false))
            {
                if (Deleted_DepartmentContactNosModel.GovernmentDepartmentContactNoId != null)
                {
                    GovernmentDepartmentContactNo GDContact = dbData.GovernmentDepartmentContactNos.Where(p => p.GovernmentDepartmentContactNoId == Deleted_DepartmentContactNosModel.GovernmentDepartmentContactNoId).FirstOrDefault();
                    dbData.GovernmentDepartmentContactNos.Remove(GDContact);
                }
            }
            foreach (var Item_DepartmentContactNosModel in model.GovernmentDepartmentContactNos.Where(x => x.bActive == true))
            {
                bool newContact = false;
                GovernmentDepartmentContactNo GDContact = dbData.GovernmentDepartmentContactNos.Where(p => p.GovernmentDepartmentContactNoId == Item_DepartmentContactNosModel.GovernmentDepartmentContactNoId).FirstOrDefault();
                if (GDContact == null)
                {
                    newContact = true;
                    GDContact = new GovernmentDepartmentContactNo();
                    GDContact.GovernmentDepartmentId = dbData.GovernmentDepartmentId;
                    GDContact.GovernmentDepartmentContactNoId = Guid.NewGuid();
                }
                if (GDContact.GovernmentDepartmentContactNoId == null || GDContact.GovernmentDepartmentContactNoId == Guid.Empty)
                    GDContact.GovernmentDepartmentContactNoId = Item_DepartmentContactNosModel.GovernmentDepartmentContactNoId;

                GDContact.ContactNo = Item_DepartmentContactNosModel.ContactNo;
                GDContact.ModifyBy = AppUser.UserName;

                if (newContact)
                    dbData.GovernmentDepartmentContactNos.Add(GDContact);
            }

            var GovDeptUser = UserDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId
            && i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead).FirstOrDefault();

            if (GovDeptUser != null)
            {
                GovDeptUser.Status = model.Status == GovtDeparmentStatus.Active ? UserStatus.Active : UserStatus.Inactive;
               
                if (!model.Password.IsNullOrEmpty())
                {
                    var passwordSalt = PasswordHelper.GetPasswordSalt();
                    var passwordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt);
                    GovDeptUser.PasswordSalt = passwordSalt;
                    GovDeptUser.PasswordHash = passwordHash;
                }
            }
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData != null)
            {
                dbData.IsDeleted = true;
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                DisplayMessage(MessageType.Success, DeletedMessage);
            }
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(GovermentDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }
        public void InitilizeGovermentDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovernmentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private bool CommonForInsertUpdate(GovermentDepartmentMvcModel model)
        {

            var duplicateCheck = GovermentDepartmentDataService.GetLite(db).FirstOrDefault(i => i.GovernmentDepartmentId != model.GovernmentDepartmentId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }

            var emaaailCheck = GovermentDepartmentDataService.GetLite(db).FirstOrDefault(i => i.GovernmentDepartmentId != model.GovernmentDepartmentId && i.Email == model.Email);
            if (emaaailCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), $"{model.Email} already exists");
                return false;
            }
            return true;
        }

        public void UpdateAssignUser(GovermentDepartmentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();

                dbData.Name = model.Name;
                dbData.Email = model.Email;
                dbData.Status = model.Status;
                dbData.IsDefault = model.IsDefault;
                dbData.POCName = model.POCName;
                dbData.IsAssignUser = model.IsAssignUser;
                dbData.POCContact = model.POCContact;
                dbData.ModifiedBy = AppUser.UserId;
                dbData.HeadOffice = model.HeadOffice;
                db.SaveChangesWithAudit(Request, AppUser.UserName);
            }

        }

        [HttpGet]
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanCreate)]
        public ActionResult CreateUser(Guid? GDI)
        {

            if (!IsValidPermission(rModel, PageName.GovermentDepartment, PagePermissionType.CanCreate))
            {
                return RedirectToAction("Index", "Home");
            }
            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(GDI);
            var model = new UserMvcModel();
            model.UserType = UserType.GovernmentDepartment;
            model.RegionId = AppUser.TempRegionId;
            InitilizeUserType(model, 4);
            return PartialView("CreateUser", model);

        }
        [HttpPost]
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanCreate)]
        public ActionResult CreateUser(UserMvcModel model)
        {
            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);

            if (!ModelState.IsValid) { return PartialView("CreateUser", model); }
            if (!CommonForInsertUpdate(model)) { return PartialView("CreateUser", model); }
            if (model.UserType == UserType.GovernmentDepartment)
            {
                if (model.GovernmentDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is required");
                    return ReBindOnError(model);
                }

            }
            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            User tempData = new User()
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Address = model.Address,
                Phone = model.Phone,
                LoginId = model.Email,
                Status = model.Status,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = model.IsDefault,
                UserType = model.UserType,
                AGDepartmentId = model.AGDepartmentId,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                UserRoleId = model.UserRoleId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };

            db.Users.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            var GDMModel = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();
            GDMModel.IsAssignUser = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"User {tempData.Name} created successfully";
            return RedirectToAction("Index");

            ActionResult ReBindOnError(UserMvcModel model1)
            {
                InitilizeUserRoles(model1.UserRoleId);
                InitilizeAGDepartments(model1.AGDepartmentId);
                InitilizeGovermentDepartments(model1.GovernmentDepartmentId);
                model1.UserType = UserType.GovernmentDepartment;
                InitilizeUserType(model1, 4);
                return PartialView("CreateUser", model1);
            }
        }
        public void InitilizeRegions(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(mvc, nameof(RegionMvcModel.RegionId)
                , nameof(RegionMvcModel.Name), selectedValue);
        }
        public void InitilizeUserRoles(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRoleMvcModel.Name), selectedValue);
        }
        public void InitilizeAGDepartments(object selectedValue)
        {
            var dbData = AGDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = AGDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AGDepartments = new SelectList(mvc, nameof(AGDepartmentMvcModel.AGDepartmentId)
                , nameof(AGDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeUserType(UserMvcModel model, int? Id)
        {
            model.UserTypeList = Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(F => new SelectListItem()
            {
                Text = F.GetDescription(),
                Value = F.ToInt().ToString(),
                Selected = Id.HasValue ? Id.Value == F.ToInt() : false
            }).ToList();
        }
        private bool CommonForInsertUpdate(UserMvcModel model)
        {
            var emailForCheck = model.Email.TrimX();
            var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.UserId != model.UserId && i.Email == emailForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), "Email already exists");
                return false;
            }
            return true;
        }
    }
}
