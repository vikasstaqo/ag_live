﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Service.BusinessService
{
    public class TimelineBusinessService : BaseBusinessService
    {
        public static void SaveTimeLine(DatabaseContext db, Guid? noticeId, Guid? caseId, EventType eventType, Guid userId, string description, Guid? parentTimelineId)
        {
            Timeline tempTimeline = new Timeline
            {
                TimelineId = Guid.NewGuid(),
                NoticeId = noticeId,
                CaseId = caseId,
                EventType = eventType,
                UserId = userId,
                Description = description,
                ParentTimelineId = parentTimelineId,
                TimeOfEvent = DateTime.Now
            };
            db.Timelines.Add(tempTimeline);
            db.SaveChanges();
        }

        public static string TimeLineDate(DateTime date)
        {
            return date.ToString("dd MMM. yyyy", CultureInfo.InvariantCulture);
        }
        public static string TimeLineTime(DateTime date)
        {
            return date.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
        }
        public static IQueryable<Timeline> GetCaseTimeline(DatabaseContext db, Guid caseId)
        {
            var Noticeid = CaseDataService.GetLite(db).Where(x => x.CaseId == caseId).FirstOrDefault().Notice.NoticeId;

            var data = TimelineDataService.GetDetail(db).Where(F => F.CaseId == caseId || F.NoticeId == Noticeid);
            return data;

        }


    }
}