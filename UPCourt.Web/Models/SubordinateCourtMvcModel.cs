﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class SubordinateCourtMvcModel : BaseEntityModel<SubordinateCourt, SubordinateCourtMvcModel>
    {
        public Guid SubordinateCourtID { get; set; }
        [Required]
        public string Name { get; set; }
        public string District { get; set; }
        public bool IsActive { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }

    }
}