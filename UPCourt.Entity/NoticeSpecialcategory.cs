﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeId), UserNameProp: nameof(CreatedBy))]
    public class NoticeSpecialcategory : BaseTransEntity
    {
        public NoticeSpecialcategory()
        {
        }
        public Guid NoticeSpecialcategoryId { get; set; }
        public Guid NoticeId { get; set; }
        public Guid SpecialCategoryId { get; set; }
        public SpecialCategory SpecialCategory { get; set; }

    }
}
