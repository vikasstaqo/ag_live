﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;
using UPCourt.Web.Service.BusinessService;

namespace UPCourt.Web.Controllers
{
    public class NoticeCaseController : BaseMVCController
    {

        // GET: NoticeCase    

        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanView)]
        public ActionResult Index(NoticeCaseIndexMvcModel model)
        {
            var dbData = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.Status == NoticeStatus.Pending).OrderByDescending(i => i.NoticeCreateDate).ToList();

            if (model.NoticeNo != null)
                dbData = dbData.Where(x => x.NoticeNo.Trim().Contains(model.NoticeNo.Trim())).ToList();

            if (model.NoticeFromDate.HasValue)
                dbData = dbData.Where(x => x.NoticeDate.Value.Date >= model.NoticeFromDate.Value.Date).ToList();

            if (model.NoticeToDate.HasValue)
                dbData = dbData.Where(x => x.NoticeDate.Value.Date <= model.NoticeToDate.Value.Date).ToList();

            if (model.ParentNoticeType != 0)
                dbData = dbData.Where(x => x.NoticeType == model.ParentNoticeType).ToList();

            if (model.NoticeTypeId != null && model.NoticeTypeId != Guid.Empty)
                dbData = dbData.Where(x => x.CaseNoticeTypeId == model.NoticeTypeId).ToList();

            if (model.Petitioner != null && model.Petitioner.Trim().Length > 0)
                dbData = dbData.Where(x => x.Petitioners.Where(p => p.Name.Trim().ToLower().Contains(model.Petitioner.Trim().ToLower())).ToList().Count > 0).ToList();

            if (model.Counselor != null && model.Counselor.Trim().Length > 0)
                dbData = dbData.Where(x => x.Counselors.Where(c => c.Name.Trim().ToLower().Contains(model.Counselor.Trim().ToLower())).ToList().Count > 0).ToList();

            if (model.Respondent != null && model.Respondent.Trim().Length > 0)
                dbData = dbData.Where(x => x.Respondents.Where(r => (r.PartyName != null && r.PartyName.Trim().ToLower().Contains(model.Respondent.Trim().ToLower()))
                     || r.GovernmentDepartment != null && (r.GovernmentDepartment.Name.Trim().ToLower().Contains(model.Respondent.Trim().ToLower())
                     || r.GovernmentDepartment.POCName.ToLower().Contains(model.Respondent.Trim().ToLower()))
                 ).ToList().Count > 0).ToList();

            if (model.CrimeNo != null && model.CrimeNo.Trim().Length > 0)
                dbData = dbData.Where(x => x.CrimeDetail != null && x.CrimeDetail.CrimeNo.Trim() != null && x.CrimeDetail.CrimeNo.Trim().ToLower().Contains(model.CrimeNo.Trim().ToLower())).ToList();

            if (model.FIRNO != null && model.FIRNO.Trim().Length > 0)
                dbData = dbData.Where(x => x.CrimeDetail != null && x.CrimeDetail.FIRNO.Trim() != null && x.CrimeDetail.FIRNO.Trim().ToLower().Contains(model.FIRNO.Trim().ToLower())).ToList();

            if (model.GovermentDepartmentId != null && model.GovermentDepartmentId != Guid.Empty)
                dbData = dbData.Where(x => x.Respondents.Where(y => y.GovernmentDepartmentId == model.GovermentDepartmentId).Count() > 0).ToList();

            if (model.CreatedBy != null && model.CreatedBy != Guid.Empty)
                dbData = dbData.Where(x => x.CreatedBy == model.CreatedBy).ToList();

            if (model.NoticeFrom != 0)
            {
                dbData = dbData.Where(x => x.NoticeFrom == model.NoticeFrom).ToList();
                if (model.NoticeFrom == NoticeFrom.UserSystem)
                    dbData = dbData.Where(x => x.FinalSubmit == NoticeFinalStatus.Submited).ToList();

            }

            InitilizeNoticeType(model.NoticeTypeId, Convert.ToInt32(model.ParentNoticeType));
            InitilizeDepartment(model.GovermentDepartmentId);
            InitilizeUser(model.CreatedBy);
            model.CaseStatuses = GetAllStatus();
            model.Notices = NoticeMvcModel.CopyFromEntityList(dbData, 0);

            return View(model);
        }

        [HttpPost]
        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanCreate)]
        public ActionResult CreateCase(NoticeCaseIndexMvcModel model)
        {

            var Notice = NoticeDataService.GetDetail(db).FirstOrDefault(F => F.NoticeId == model.NoticeId);
            if (Notice.IsNull()) return RedirectToAction("Index");
            var TotalCases = CaseDataService.GetLite(db).ToList().Count;
            Case NewCase = new Case()
            {
                CaseId = Guid.NewGuid(),
                NoticeId = model.NoticeId,
                OrdinalNo = TotalCases + 1,
                PetitionNo = model.CaseNo.Trim(),
                CaseStatusId = model.CaseStatusId,
                Type = Notice.NoticeType,
                RegionId = Notice.RegionId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            Notice.BasicIndexs.ToList().ForEach(F => F.CaseId = NewCase.CaseId);

            Notice.Status = NoticeStatus.Accept;

            db.Cases.Add(NewCase);
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateNewCaseNotification(NewCase);

            string description = $"Notice '{ Notice.NoticeNo }' Converted to Case '{ NewCase.PetitionNo }' successfully";
            TimelineBusinessService.SaveTimeLine(db, NewCase.NoticeId, NewCase.CaseId, EventType.NoticeToCase, AppUser.UserId, description, null);

            DisplayMessage(MessageType.Success, description, "Case No Assigned", "");

            return RedirectToAction("Index", "Case", new { id = NewCase.CaseId });
        }

        // GET: NoticeCase/Create
        public ActionResult CaseView(Guid id)
        {

            var dbData = CaseDataService.GetDetail(db).Where(x => x.CaseId == id).FirstOrDefault();
            CaseMvcModel CaseMobelobj = CaseMvcModel.CopyFromEntity(dbData, 0);
            CaseMobelobj.Timelines = TimelineMvcModel.CopyFromEntityList(TimelineBusinessService.GetCaseTimeline(db, id), 0);
            DocumentMvcModel obj = new DocumentMvcModel();
            CaseMobelobj.NewCaseHearing = new NewCaseHearingMvcModelList();

            CaseMobelobj.NewCaseHearing.DocumentMvcModel = new List<DocumentMvcModel>();
            CaseMobelobj.NewCaseHearing.DocumentMvcModel.Add(new DocumentMvcModel { Name = "" });
            return View(CaseMobelobj);
        }

        [HttpPost]
        [ValidateInput(false)]
        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanEdit)]
        public ActionResult CaseView(CaseMvcModel DocumentMVCModel)
        {

            CaseHearing ChObj = new CaseHearing();
            ChObj.CaseHearingId = Guid.NewGuid();
            ChObj.CaseId = DocumentMVCModel.CaseId;
            ChObj.HearingDate = DocumentMVCModel.NewCaseHearing.Hearing.HearingDate;
            ChObj.NextHearingDate = DocumentMVCModel.NewCaseHearing.Hearing.NextHearingDate;
            ChObj.Comments = DocumentMVCModel.NewCaseHearing.Hearing.Comments;
            int OrderNo = 1;
            foreach (var Doc in DocumentMVCModel.NewCaseHearing.DocumentMvcModel)
            {
                Document DocNewObj = new Document();
                DocNewObj.DocumentId = Guid.NewGuid();
                DocNewObj.CaseHearingId = ChObj.CaseHearingId;
                DocNewObj.Name = Doc.Name;
                DocNewObj.DocumentUrl = SaveDocument(Doc.UploadFile);
                DocNewObj.FileType = DocumentFileType.Any;
                DocNewObj.EntityType = DocumentEntityType.CaseHearing;
                DocNewObj.Ordinal = OrderNo;
                DocNewObj.Size = Doc.UploadFile.ContentLength;
                DocNewObj.Status = DocumentStatus.Added;
                OrderNo++;
                db.Documents.Add(DocNewObj);
            }
            db.CaseHearings.Add(ChObj);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Case Hearing Added successfully";
            return RedirectToAction("CaseView", DocumentMVCModel.CaseId);
        }
        string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                if (ext.ToUpper() == ".DOC" || ext.ToUpper() == ".DOCX" || ext.ToUpper() == ".PDF" || ext.ToUpper() == ".XLS" || ext.ToUpper() == ".JPEG" || ext.ToUpper() == ".JPG")
                {
                    int FileSize = FileUpload.ContentLength;
                    if (FileSize > 5048576)
                    {
                        return ".jpeg, .jpg, .doc, .docx, .xls and .pdf  with Maximum file size 5 mb will be uploaded";
                    }
                    else
                    {
                        string fileName = "", UploadName = "", FilePath = "";

                        fileName = Path.GetFileName(FileUpload.FileName);
                        Guid guid;
                        guid = Guid.NewGuid();

                        UploadName = guid + "_" + fileName;
                        FilePath = Path.Combine(Server.MapPath("~/Document/"), UploadName);
                        FileUpload.SaveAs(FilePath);
                        return UploadName;
                    }
                }
            }

            return "";
        }

        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanView)]
        public ActionResult CaseHearingDOC(Guid CaseHearID)
        {
            var dbData = CaseDataService.GetDetail(db).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault();

            return PartialView("CaseHearingDOC", CaseMvcModel.CopyFromEntity(dbData, 0));
        }
        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanView)]
        public ActionResult CaseHearingComment(Guid CaseHearID)
        {
            var obj = CaseDataService.GetDetail(db).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault();
            var Commenst = obj.CaseHearings.Where(m => m.CaseHearingId == CaseHearID).Select(x => x.Comments).FirstOrDefault();
            return PartialView("CaseHearingComment", Commenst);
        }

        [PageRoleAuthorizarion(PageName.NoticeCase, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = NoticeDataService.GetLite(db).Where(i => i.NoticeId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, $"Notice '{ dbData.NoticeNo }' Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index");
        }
        public ActionResult View(Guid id)
        {
            InitilizeGovermentDepartments(null);
            var dbData = NoticeDataService.GetDetail(db).Where(F => F.NoticeId == id).FirstOrDefault();
            NoticeMvcModel model = NoticeMvcModel.CopyFromEntity(dbData, 0);
            if (Request["nid"] != null)
            {
                Guid n_id = Guid.Empty;
                if (Request["s"] == "1" && Guid.TryParse(Request["nid"], out n_id))
                {

                    AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(n_id, NotificationStatusEnum.IsRead);
                }
            }
            if (Request.QueryString["nid"] != null && Request.QueryString["nid"].Length > 0 && Request.QueryString["s"] != null && Request.QueryString["s"] == "1")
            {
                AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(Guid.Parse(Request.QueryString["nid"]), NotificationStatusEnum.IsRead);
            }
            return View(model);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        List<SelectListItem> GetAllStatus()
        {
            var AllStatuses = CaseStatusDataService.GetLite(db).Where(x => x.Name.ToLower().Contains("fresh case")).ToList();
            var CaseStatuses = new List<SelectListItem>();
            for (int i = 0; i < AllStatuses.Count; i++)
            {
                CaseStatuses.Add(new SelectListItem()
                {
                    Text = AllStatuses[i].Name,
                    Value = AllStatuses[i].CaseStatusId.ToString(),
                });
            }
            return CaseStatuses;
        }
        [HttpGet]
        public ActionResult CheckCaseNo(string CaseNo)
        {
            var dbData = CaseDataService.GetLite(db).Where(i => i.PetitionNo == CaseNo).FirstOrDefault();

            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        public void InitilizeNoticeType(object selectedValue, int noticeType)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(n => n.ParentNoticeType == (ParentNoticeType)noticeType).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);

        }

        public void InitilizeDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Department = new SelectList(mvc, "GovernmentDepartmentId", "Name", selectedValue);
        }

        public void InitilizeUser(object selectedValue)
        {
            var dbData = UserDataService.GetLite(db).Where(x => x.RegionId == AppUser.TempRegionId).ToList();
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);

            ViewBag.User = new SelectList(mvc, nameof(Entity.User.UserId), nameof(Entity.User.Name), selectedValue);
        }

        [HttpGet]
        public ActionResult SyncNotices()
        {

            Helper.DataFromApiHelper helper = new Helper.DataFromApiHelper();

            int noticeCounts = helper.SyncNoticeNo();
            int noticeCaseCounts = helper.SyncNoticeCases();
            string description = noticeCounts.ToString() + " NoticeNos and " + noticeCaseCounts.ToString() + " CaseDetails for Notices synced successfully";
            DisplayMessage(MessageType.Success, description, "Notices Synced", "");

            return RedirectToAction("", "NoticeCase");
        }
    }
}
