﻿function ConfirmAppDelete(ApplicationDocumentID) {
 
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete Application Document !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/ApplicationDocument/Delete/" + ApplicationDocumentID);
        }
    });
}

function ConfirmSumit(ApplicationDocumentID) {
 
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to Final Submit Application Document !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace("/ApplicationDocument/ConfirmFinalSubmit/" + ApplicationDocumentID);
        }
    });
}