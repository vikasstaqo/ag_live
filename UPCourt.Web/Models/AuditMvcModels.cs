﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UPCourt.Web.Models
{
    public class AuditMvcModels
    {
        [Required]
        [Display(Name = "Audit Type")]
        public string Audit_Type { get; set; }
        [Display(Name = "From")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime from { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "To")]
        public DateTime to { get; set; }
        [Required]
        [Display(Name = "Action")]
        public string Action { get; set; }
        [Required]
        [Display(Name = "Action By")]
        public string Action_by { get; set; }
    }
}