﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UPCourt.Entity.Migrations
{
    public partial class CourtUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsClose",
                table: "Courts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsClose",
                table: "Courts");
        }
    }
}
