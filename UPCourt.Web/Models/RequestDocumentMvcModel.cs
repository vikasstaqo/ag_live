﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;


namespace UPCourt.Web.Models
{
    public class RequestDocumentMvcModel : BaseEntityModel<RequestDocument, RequestDocumentMvcModel>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string DocumentName { get; set; }
        public string CommentRequestor { get; set; }
        public string GovDeptComment { get; set; }
        [Display(Name = "Document Name")]
        public string Document_Name { get; set; }
        public string DocumentStatus { get; set; }
        public string DocumentPath { get; set; }
        [Display(Name = "Requested At")]
        public DateTime? RequestedAt { get; set; }
        [Display(Name = "Submited At")]
        public DateTime? GivenAt { get; set; }
        [Display(Name = "Last Submit Date")]
        public DateTime? LastSubmitDate { get; set; }
        public string ModifyBy { get; set; }
        public Guid? CaseId { get; set; }
        public string CaseNo { get; set; }
        public Guid? DocumentId { get; set; }
        public List<Guid> SharedBy { get; set; }
        public HttpPostedFileBase UploadDocument { get; set; }

    }
}