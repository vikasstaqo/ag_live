﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CaseStatusMvcModel : BaseEntityModel<CaseStatus, CaseStatusMvcModel>
    {
        public Guid CaseStatusId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Hex Color Code")]
        public string HexColorCode { get; set; }
        public CaseStatusType Type { get; set; }
        public bool ShowOnDashboard { get; set; }
    }

    public class CaseTypeMvcModel
    {
        public Guid CaseTypeId { get; set; }
        public string type_name { get; set; }
        public string full_form { get; set; }
        public string case_type { get; set; }
        public string type_flag { get; set; }
    }

    public class SearchCaseStatusMvcModel
    {
        public List<CaseTypeNoticeModel> Notices { get; set; }
        public List<NoticeMvcModel> NoticesD { get; set; }
        public CaseNoticeTypeMvcModel caseNoticeType { get; set; }
        public Guid CaseNoticeTypeId { get; set; }
        public CaseTypeMvcModel CaseType { get; set; }
        public string case_number { get; set; }
        public string case_year { get; set; }
    }

    public class SearchCauseListMvcModel
    {

    }
    public class CaseTypeNoticeModel
    {
        public int reg_year { get; set; }
        public int regcase_type { get; set; }
        public int reg_no { get; set; }
        public string cnr { get; set; }
        public List<CaseDetailMvcModel> CaseDetails { get; set; }
    }

    public class CaseDetailMvcModel : BaseEntityModel<Casedetail, CaseDetailMvcModel>
    {
        public string pet_email { get; set; }
        public string pet_adv { get; set; }
        public string pet_adv_cd { get; set; }
        public string pet_name { get; set; }
        public string res_mobile { get; set; }
        public string pet_age { get; set; }
        public string res_age { get; set; }
        public string res_sex { get; set; }
        public string res_adv_cd { get; set; }
        public string pet_father_name { get; set; }
        public string res_name { get; set; }
        public string pet_mobile { get; set; }
        public string pet_sex { get; set; }
        public string res_email { get; set; }
        public string res_father_name { get; set; }
        public string case_no { get; set; }
        public string cino { get; set; }
        public string res_adv { get; set; }
        public CaseTypeMvcModel CaseType { get; internal set; }
    }

    public class CaseStatusBenchType
    {
        public int bench_type_code { get; set; }
        public string bench_type_name { get; set; }
    }

    public class CaseStatusAppDetails
    {
        public DateTime? date_of_order { get; set; }
        public int ia_regyear { get; set; }
        public string judge_code { get; set; }
        public int court_no { get; set; }
        public DateTime? date_of_hearing { get; set; }
        public DateTime? date_of_ia_registration { get; set; }
        public int ia_regno { get; set; }
    }

    public class AdvocateDetails
    {
        public string adv_phone1 { get; set; }
        public int adv_code { get; set; }
        public string off_add { get; set; }
        public string adv_reg { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string adv_full_name { get; set; }
        public string adv_mobile { get; set; }
        public string adv_name { get; set; }
        public string adv_phone { get; set; }
    }

    public class CaseStatus_Respondents
    {
        public string name { get; set; }
        public int party_no { get; set; }
    }

    public class CaseStatus_Petitioners
    {
        public string name { get; set; }
        public int party_no { get; set; }
    }

    public class CaseStatus_IA
    {
        public DateTime? date_of_order { get; set; }
        public int ia_regyear { get; set; }
        public string judge_code { get; set; }
        public int court_no { get; set; }
        public DateTime? date_of_hearing { get; set; }
        public DateTime? date_of_ia_registration { get; set; }
        public int ia_regno { get; set; }
    }

    public class CaseStatusDetails
    {
        public DateTime? date_first_list { get; set; }
        public int case_dist_code { get; set; }
        public string pet_name { get; set; }
        public int bench_type { get; set; }
        public CaseStatusBenchType benchType { get; set; }
        public List<CaseStatus_Petitioners> Petitioners { get; set; }
        public int cs_subject { get; set; }
        public int disp_nature { get; set; }
        public int purpose_next { get; set; }
        public string judge_code { get; set; }
        public DateTime? date_of_decision { get; set; }
        public string res_name { get; set; }
        public int case_state_code { get; set; }
        public List<CaseStatus_Respondents> Respondents { get; set; }
        public int c_subject { get; set; }
        public List<CaseStatus_IA> IA { get; set; }
        public string case_no { get; set; }
        public string short_order { get; set; }
        public int causelist_type { get; set; }
        public int branch_id { get; set; }
        public CaseDetailMvcModel CaseDetail { get; set; }
        public CaseStatusSubject Subject { get; set; }
    }

    public class CaseStatusSubject
    {
        public string subnature1_desc { get; set; }
        public string subject_name { get; set; }
        public int subnature1_cd { get; set; }
        public int subject_code { get; set; }
    }
}