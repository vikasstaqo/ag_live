﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: null, UserNameProp: nameof(ModifiedBy))]
    public class UserRolePagePermission
    {
        public Guid UserRolePagePermissionId { get; set; }
        public Guid UserRoleId { get; set; }
        public Guid PagePermissionId { get; set; }
        public IEnumerable<PagePermissionType> PermittedPermissionTypes { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public UserRole UserRole { get; set; }
        public PagePermission PagePermission { get; set; }

    }
}