﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class UserLogInLogDataService
    {
        public static IQueryable<UserLogInLog> GetLite(DatabaseContext db)
        {
            return db.UserLogInLogs;
        }
    }
}