﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class PoliceStationController : BaseMVCController
    {
        // GET: PoliceStation
        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanView)]
        public ActionResult Index(PoliceStationFilterMvcModel model)
        {
            InitilizeZone(model.RangeID);
            InitilizeRange(null, model.ZoneID);
            var dbData = PoliceStationDataService.GetDetail(db).OrderByDescending(x => x.DisplayOrder).ToList();
            if (model.ZoneID != null)
            {
                dbData = PoliceStationDataService.GetDetail(db).Where(x => x.ZoneID == model.ZoneID).OrderByDescending(x => x.DisplayOrder).ToList();
            }
            if (model.RangeID != null)
            {
                dbData = PoliceStationDataService.GetDetail(db).Where(x => x.RangeID == model.RangeID).OrderByDescending(x => x.DisplayOrder).ToList();
            }
            if (model.Locality != null && model.Locality.Trim().Length > 0)
            {
                dbData = dbData.Where(x => x.Locality.ToLower().Contains(model.Locality.Trim().ToLower())).ToList();
            }
            model.PoliceStation = PoliceStationMvcModel.CopyFromEntityList(dbData, 0);
            return View(model);
        }

        [HttpGet]
        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new PoliceStationMvcModel { IsActive = true };
            InitilizeDistrict(null);
            InitilizeZone(model.ZoneID);
            InitilizeRange(model.RangeID, model.ZoneID);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(PoliceStationMvcModel model)
        {
            InitilizeDistrict(model.District);
            InitilizeZone(model.ZoneID);
            InitilizeRange(model.RangeID, model.ZoneID);
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            PoliceStation tempData = new PoliceStation()
            {
                PoliceStationId = Guid.NewGuid(),
                District = model.District,
                ZoneID = model.ZoneID,
                RangeID = model.RangeID,
                Locality = model.Locality,
                POC = model.POC,
                EmailID = model.EmailID,
                Phone = model.Phone,
                Fax = model.Fax,
                Mobile = model.Mobile,
                Address = model.Address,
                PinCode = model.PinCode,
                IsActive = model.IsActive,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetPoliceStationsDisplayOrder(db)
            };
            db.PoliceStations.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        private void InitilizeDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
            ViewBag.Districts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }
        public void InitilizeZone(object selectedValue)
        {
            var dbData = ZoneDataService.GetLite(db).OrderBy(i => i.ZoneName).ToList();
            var mvc = ZoneMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Zone = new SelectList(mvc, nameof(Zone.ZoneID), nameof(Zone.ZoneName), selectedValue);
        }
        public void InitilizeRange(object selectedValue, Guid? ZoneID)
        {
            var dbData = RangeDataService.GetLite(db).Where(x => x.ZoneID == (ZoneID == null || ZoneID == Guid.Empty ? x.ZoneID : ZoneID)).OrderBy(i => i.RangeName).ToList();
            var mvc = RangeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Range = new SelectList(mvc, nameof(Range.RangeID), nameof(Range.RangeName), selectedValue);
        }

        [HttpGet]
        public ActionResult GetZoneByDistrict(string District)
        {
            var dbData = ZoneDataService.GetLite(db).Where(i => i.DistrictName == District).OrderBy(i => i.ZoneName).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetRangeByZone(Guid Zone)
        {
            var dbData = RangeDataService.GetLite(db).Where(i => i.ZoneID == Zone).OrderBy(i => i.RangeName).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }

        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.PoliceStationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeDistrict(dbData.District);
            InitilizeZone(dbData.ZoneID);
            InitilizeRange(dbData.RangeID, dbData.ZoneID);
            PoliceStationMvcModel model = PoliceStationMvcModel.CopyFromEntity(dbData, 0);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(PoliceStationMvcModel model)
        {
            InitilizeDistrict(model.District);
            InitilizeZone(model.ZoneID);
            InitilizeRange(model.RangeID, model.ZoneID);
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.PoliceStationId == model.PoliceStationId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }

            dbData.District = model.District;
            dbData.ZoneID = model.ZoneID;
            dbData.RangeID = model.RangeID;
            dbData.Locality = model.Locality;
            dbData.POC = model.POC;
            dbData.EmailID = model.EmailID;
            dbData.IsActive = model.IsActive;
            dbData.Phone = model.Phone;
            dbData.Fax = model.Fax;
            dbData.Mobile = model.Mobile;
            dbData.Address = model.Address;
            dbData.PinCode = model.PinCode;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.PoliceStationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeDistrict(dbData.District);
            InitilizeZone(dbData.ZoneID);
            InitilizeRange(dbData.RangeID, dbData.ZoneID);
            return View(PoliceStationMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(PoliceStationMvcModel model)
        {
            var duplicateCheck = PoliceStationDataService.GetLite(db).FirstOrDefault(i => i.PoliceStationId != model.PoliceStationId && i.Locality.ToLower() == model.Locality.ToLower() && i.RangeID == model.RangeID && i.ZoneID == model.ZoneID && i.District.ToLower() == model.District.ToLower());
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Locality), $"{model.Locality}  Locality already exists with Same District,Zone,Range");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.PoliceStation, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = PoliceStationDataService.GetLite(db).Where(i => i.PoliceStationId == id).FirstOrDefault();
            if (dbData != null)
            {
                dbData.IsDeleted = true;
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                DisplayMessage(MessageType.Success, DeletedMessage);
            }
            return RedirectToAction("Index");
        }
    }
}