﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class Salutation : BaseMasterEntity
    {
        public Salutation() { }
        public Guid SalutationId { get; set; }
        public string Name { get; set; }
        public SalutationStatus Status { get; set; }

    }
    public enum SalutationStatus
    {
        Active = 1,
        Inactive = 0
    }
}
