﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PetitionNo), UserNameProp: nameof(CreatedBy))]
    public class Case : BaseTransEntity
    {
        public Case()
        {
            this.CaseHearings = new HashSet<CaseHearing>();
            this.Timelines = new HashSet<Timeline>();
            this.UserCases = new HashSet<UserCase>();
            this.CaseTransfers = new HashSet<CaseTransfer>();
        }
        public Guid CaseId { get; set; }
        public Guid NoticeId { get; set; }
        public int OrdinalNo { get; set; }
        public string PetitionNo { get; set; }
        public Guid? CaseStatusId { get; set; }
        public CaseDisposeType? DisposeType { get; set; }
        public Guid? AdvocateId { get; set; }

        public Guid? DocsPreprationId { get; set; }
        public Guid? CourtId { get; set; }
        public Guid? RegionId { get; set; }
        public Guid? ParentCaseId { get; set; }
        public CaseMovementStatus? Status2 { get; set; }
        public ParentNoticeType Type { get; set; }
        public DateTime? NextHearingDate { get; set; }
        public int HearingStatus { get; set; }
        public CaseStatus CaseStatus { get; set; }
        public User Advocate { get; set; }
        public User DocsPrepration { get; set; }
        public Court Court { get; set; }
        public Region Region { get; set; }
        public Notice Notice { get; set; }
        public ICollection<UserCase> UserCases { get; set; }
        public ICollection<NoticeBasicIndex> BasicIndexs { get; set; }
        public ICollection<CaseHearing> CaseHearings { get; set; }
        public ICollection<Timeline> Timelines { get; set; }
        public ICollection<CaseTransfer> CaseTransfers { get; set; }
        public ICollection<RequestDocument> RequestDocument { get; set; }
    }
    public enum CaseMovementStatus
    {
        [Description("Review Petiton")]
        ReviewPetiton = 0,
        [Description("Moved To Supreme Court")]
        MovedToSupremeCourt = 1,
        [Description("Send Back To High Court")]
        SendBackToHighCourt = 2
    }

    [Description("Dispose Type")]
    public enum CaseDisposeType
    {
        [Description("Dispose Type 1")]
        Dispose1 = 1,
        [Description("Dispose Type 2")]
        Dispose2 = 2,
        [Description("Dispose Type 3")]
        Dispose3 = 3
    }
}
