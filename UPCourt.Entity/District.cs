﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DistrictName), UserNameProp: nameof(CreatedBy))]
    public class District : BaseMasterEntity
    {
        public District() { }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int DistCode { get; set; }
        public DistrictStatus Status { get; set; }
    }

    public enum DistrictStatus
    {
        Active = 1,
        Inactive = 0
    }

}
