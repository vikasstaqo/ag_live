﻿$(function () {
 
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartCanvas2 = $('#stackedBarChart2').get(0).getContext('2d')
    var stackedBarChartCanvas3 = $('#stackedBarChart3').get(0).getContext('2d')
    var stackedBarChartCanvas4 = $('#stackedBarChart4').get(0).getContext('2d')

    var stackedBarChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        }
    }

    new Chart(stackedBarChartCanvas, {
        type: 'bar',
        options: stackedBarChartOptions
    })
    new Chart(stackedBarChartCanvas2, {
        type: 'bar',
        options: stackedBarChartOptions
    })
    new Chart(stackedBarChartCanvas3, {
        type: 'bar',
        options: stackedBarChartOptions
    })
    new Chart(stackedBarChartCanvas4, {
        type: 'bar',
        options: stackedBarChartOptions
    })
});