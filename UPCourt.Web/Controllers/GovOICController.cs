﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class GovOICController : BaseMVCController
    {
        // GET: GovOIC

        public ActionResult Index()
        {
            return View(GovOICMvcModel.CopyFromEntityList(GovOICDataService.GetFiltered(db, AppUser.User).OrderByDescending(i => i.DisplayOrder), 0));
        }

        public ActionResult Create()
        {
            InitilizeGovermentDepartments(AppUser.User.GovernmentDepartmentId);
            InitilizeGovermentDesignations(AppUser.User.DesignationId, AppUser.User.GovernmentDepartmentId);
            var model = new GovOICMvcModel
            {
                Status = GovernmentDepartmentOicStatus.Active,
                GovernmentDepartmentId = (Guid)(AppUser.User.GovernmentDepartmentId != null ? AppUser.User.GovernmentDepartmentId : Guid.Empty),
                GovernmentDesignationId = (Guid)(AppUser.User.DesignationId != null ? AppUser.User.DesignationId : Guid.Empty)
            };
            return View(model);
        }

        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetFiltered(db, AppUser.User).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId), nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }

        public void InitilizeGovermentDesignations(object selectedValue, Guid? GovernmentDepartmentId)
        {
            var dbData = GovDesignationDataService.GetFiltered(db, AppUser.User, "DB").Where(y => y.GovernmentDepartmentId == GovernmentDepartmentId).OrderBy(i => i.DesignationName).ToList();
            var mvc = GovDesignationMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDesignations = new SelectList(mvc, nameof(GovDesignationMvcModel.GovernmentDesignationId), nameof(GovDesignationMvcModel.DesignationName), selectedValue);
        }

        [HttpPost]
        public ActionResult Create(GovOICMvcModel model)
        {
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            InitilizeGovermentDesignations(model.GovernmentDesignationId, model.GovernmentDepartmentId);
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Guid GovernmentdepartmentOICID = Guid.NewGuid();
            GovernmentDepartmentOic tempData = new GovernmentDepartmentOic()
            {
                GovernmentdepartmentOICID = GovernmentdepartmentOICID,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                GovernmentDesignationId = model.GovernmentDesignationId,
                OICName = model.OICName,
                POCName = model.POCName,
                POCEmail = model.POCEmail,
                POCMobileNo = model.POCMobileNo,
                POCContact = model.POCContact,
                POCFaxNo = model.POCFaxNo,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetGovernmentDepartmentOICsDisplayOrder(db)
            };
            db.GovernmentDepartmentOics.Add(tempData);
            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passWordSalt);
            var UserRoleId = UserRoleDataService.GetLite(db).Where(x => x.UserSystemRoleId == UserSystemRole.GovDeptOIC).FirstOrDefault().UserRoleId;
            User tempUser = new User()
            {
                UserId = Guid.NewGuid(),
                Name = model.OICName,
                Email = model.POCEmail,
                Phone = model.POCMobileNo,
                LoginId = model.POCEmail,
                Status = UserStatus.Active,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = true,
                UserType = UserType.GovernmentDepartment,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                DesignationId = model.GovernmentDesignationId,
                GovermentDepartmentOICID = GovernmentdepartmentOICID,
                UserRoleId = UserRoleId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            db.Users.Add(tempUser);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var dbData = GovOICDataService.GetDetail(db).Where(i => i.GovernmentdepartmentOICID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            InitilizeGovermentDesignations(dbData.GovernmentDesignationId, dbData.GovernmentDepartmentId);
            return View(GovOICMvcModel.CopyFromEntity(dbData, 0));
        }
        [HttpPost]

        public ActionResult Edit(GovOICMvcModel model)
        {
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            InitilizeGovermentDesignations(model.GovernmentDesignationId, model.GovernmentDepartmentId);
            if (!ModelState.IsValid) { return View(model); }
            var dbData = GovOICDataService.GetDetail(db).Where(i => i.GovernmentdepartmentOICID == model.GovernmentdepartmentOICID).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            dbData.OICName = model.OICName;
            dbData.GovernmentDepartmentId = model.GovernmentDepartmentId;
            dbData.GovernmentDesignationId = model.GovernmentDesignationId;
            dbData.Status = model.Status;
            dbData.POCName = model.POCName;
            dbData.POCMobileNo = model.POCMobileNo;
            dbData.POCContact = model.POCContact;
            dbData.POCFaxNo = model.POCFaxNo;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;

            var OICuser = UserDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId
            && i.GovermentDepartmentOICID == model.GovernmentdepartmentOICID
            && i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptOIC).FirstOrDefault();

            if (OICuser == null)
            {
                User tempData = new User()
                {
                    UserId = Guid.NewGuid(),
                    Name = model.OICName,
                    Email = model.POCEmail,
                    Phone = model.POCMobileNo,
                    LoginId = model.POCEmail,
                    Status = model.Status == GovernmentDepartmentOicStatus.Active ? UserStatus.Active : UserStatus.Inactive,
                    IsDefault = true,
                    UserType = UserType.GovernmentDepartment,
                    GovernmentDepartmentId = model.GovernmentDepartmentId,
                    GovermentDepartmentOICID = model.GovernmentdepartmentOICID,
                    DesignationId = model.GovernmentDesignationId,
                    UserRoleId = UserRoleDataService.GetLite(db).Where(x => x.UserSystemRoleId == UserSystemRole.GovDeptOIC).FirstOrDefault().UserRoleId,
                    CreatedBy = AppUser.UserId,
                    CreatedOn = DateTime.Now
                };
                //If password changed
                if (!model.POCPassword.IsNullOrEmpty())
                {
                    var passwordSalt = PasswordHelper.GetPasswordSalt();
                    var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passwordSalt);
                    tempData.PasswordSalt = passwordSalt;
                    tempData.PasswordHash = passwordHash;
                }

                db.Users.Add(tempData);
                dbData.IsAssignUser = true;
            }
            else
            {
                OICuser.Status = model.Status == GovernmentDepartmentOicStatus.Active ? UserStatus.Active : UserStatus.Inactive;
                //If password changed
                if (!model.POCPassword.IsNullOrEmpty())
                {
                    var passwordSalt = PasswordHelper.GetPasswordSalt();
                    var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passwordSalt);
                    OICuser.PasswordSalt = passwordSalt;
                    OICuser.PasswordHash = passwordHash;
                }
            }

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }


        public ActionResult Details(Guid id)
        {
            var dbData = GovOICDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(GovOICMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(GovOICMvcModel model)
        {
            var duplicateCheck = GovOICDataService.GetLite(db).FirstOrDefault(i => i.GovernmentdepartmentOICID != model.GovernmentdepartmentOICID && i.GovernmentDepartmentId == model.GovernmentDepartmentId && i.OICName == model.OICName);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.OICName), $"{ model.OICName } already exists with Same Deparment ");
                return false;
            }
            var duplicateEmailCheck = GovOICDataService.GetLite(db).FirstOrDefault(i => i.GovernmentdepartmentOICID != model.GovernmentdepartmentOICID && i.POCEmail == model.POCEmail);
            if (duplicateEmailCheck != null)
            {
                ModelState.AddModelError(nameof(model.POCEmail), "Email already exists");
                return false;
            }

            var duplicateEmailUserCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.GovermentDepartmentOICID != model.GovernmentdepartmentOICID && i.Email == model.POCEmail);
            if (duplicateEmailUserCheck != null)
            {
                ModelState.AddModelError(nameof(model.POCEmail), "Email already exists in user master.");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.GovDesignation, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = GovOICDataService.GetLite(db).Where(i => i.GovernmentdepartmentOICID == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
