﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DivisionMvcModel:BaseEntityModel<Division, DivisionMvcModel>
    {
        public Guid DivisionId { get; set; }
        public string DivisionName { get; set; }
        public DivisionStatus Status { get; set; }
    }
}