﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class NoticeTypeDataService
    {
        public static IQueryable<CaseNoticeType> GetAll(DatabaseContext db)
        {
            return db.CaseNoticeTypes;
        }
        public static IQueryable<CaseNoticeType> GetLite(DatabaseContext db)
        {
            return db.CaseNoticeTypes.Where(x => x.IsDeleted == false);
        }

        public static IQueryable<CaseNoticeType> GetActive(DatabaseContext db)
        {
            return db.CaseNoticeTypes.Where(x => x.IsDeleted == false && x.IsActive == true);
        }
        public static IQueryable<CaseNoticeType> GetDetail(DatabaseContext db)
        {
            return GetLite(db);
        }

    }
}