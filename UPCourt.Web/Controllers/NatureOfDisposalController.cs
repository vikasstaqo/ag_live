﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NatureOfDisposalController : BaseMVCController
    {
        // GET: Court
        public ActionResult Index()
        {
            var dbData = NatureOfDisposalDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder).ToList();
            return View(NatureOfDisposalMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = NatureOfDisposalDataService.GetLite(db).Where(i => i.NatureOfDisposalId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(NatureOfDisposalMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Court/Create
        public ActionResult Create()
        {
            NatureOfDisposalMvcModel model = new NatureOfDisposalMvcModel();
            return View(model);
        }

        // POST: Court/Create
        [HttpPost]
        public ActionResult Create(NatureOfDisposalMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NatureOfDisposal tempNOD = new NatureOfDisposal();
            tempNOD.NatureOfDisposalId = Guid.NewGuid();
            tempNOD.DisplayOrder = MasterBusinessService.GetNatureOfDisposalsDisplayOrder(db);
            InitializeData(tempNOD, model, true);
            tempNOD.CreatedBy = AppUser.UserId;
            tempNOD.CreatedOn = DateTime.Now;
            db.NatureOfDisposals.Add(tempNOD);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            //db.SaveChanges();
            SucessMessage = $"Court {tempNOD.Name} created successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create");
        }

        // GET: AGDepartment/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = NatureOfDisposalDataService.GetLite(db).Where(i => i.NatureOfDisposalId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(NatureOfDisposalMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: CaseStatus/Edit/5
        [HttpPost]
        public ActionResult Edit(NatureOfDisposalMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = NatureOfDisposalDataService.GetLite(db).Where(i => i.NatureOfDisposalId == model.NatureOfDisposalId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitializeData(dbData, model, false);
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            //db.SaveChanges();
            SucessMessage = $"Court {dbData.Name} update successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Index");
        }

        private void InitializeData(NatureOfDisposal tempNOD, NatureOfDisposalMvcModel model, bool IsNew)
        {
            tempNOD.Name = model.Name;
            tempNOD.IsActive = model.IsActive;
            if (!IsNew)
            {
                tempNOD.NatureOfDisposalId = model.NatureOfDisposalId;
            }
        }

        public ActionResult Delete(Guid id)
        {
            var dbData = NatureOfDisposalDataService.GetLite(db).Where(i => i.NatureOfDisposalId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, $"Nature Of Disposal '{ dbData.Name }' Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index");
        }
    }
}
