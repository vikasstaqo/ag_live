﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
   
    public class CourtJudgeMappingModel : BaseEntityModel<CourtJudgeMapping, CourtJudgeMappingModel>
    {
       
        public Guid CourtJudgeMappingID { get; set; }
        public Guid CourtId { get; set; }

        public string CourtName { get; set; }
        public Guid BenchTypeID { get; set; }
        public Guid JudgeId { get; set; }
        public string ModifyBy { get; set; }

    }
   

}