﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class NatureOfDisposalDataService
    {
        public static IQueryable<NatureOfDisposal> GetLite(DatabaseContext db)
        {
            return db.NatureOfDisposals.Where(x => x.IsDeleted == false);
        }
      
    }
}