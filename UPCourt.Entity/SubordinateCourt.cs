﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]    public class SubordinateCourt
    {   
        public Guid SubordinateCourtID { get; set; }
        public string Name { get; set; }
        public string District { get; set; }
        public bool IsActive { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }
    }
    
}
