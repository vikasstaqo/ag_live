﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class HStatusDataService
    {
        public static IQueryable<HStatus> GetLite(DatabaseContext db)
        {
            return db.HStatus.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<HStatus> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}