﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;

namespace UPCourt.Web.Controllers
{
    public class BasicIndexController : BaseMVCController
    {
        // GET: BasicIndex
        [AllowAnonymous]
        [HttpPost]
        public ActionResult UploadFiles()
        {
            var uploadDocs = FileUploadCommon(rModel, Enumerable.Empty<Document>(), Enumerable.Empty<DocumentMvcModel>(), 1, 10, DocumentFileType.Any);

            if (!rModel.IsSuccess)
            {
                return Json(rModel, JsonRequestBehavior.AllowGet);
            }
            var finalDocs = DocumentMvcModel.CopyFromEntityList(uploadDocs, 0);
            foreach (var doc in finalDocs)
            {
                doc.PageCount = NoticeBusinessService.GetPages_PDF(FileUploadWebApiHelper.GetServerFilePathFromUrl(doc.DocumentUrl));
            }
            rModel.Data = finalDocs;
            return Json(rModel, JsonRequestBehavior.AllowGet);
        }
    }
}