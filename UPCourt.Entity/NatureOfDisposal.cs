﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NatureOfDisposal), UserNameProp: nameof(ModifiedBy))]
    public class NatureOfDisposal : BaseMasterEntity
    {
        public Guid NatureOfDisposalId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
      
    }
}
