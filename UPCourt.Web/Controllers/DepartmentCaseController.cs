﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;
using static UPCourt.Web.Models.ResponseModel;

namespace UPCourt.Web.Controllers
{
    public class DepartmentCaseController : BaseMVCCaseController
    {

        // GET: DepartmentCase
        [PageRoleAuthorizarion(PageName.DepartmentCase, PagePermissionType.CanView)]
        public ActionResult Index(CaseFilterMvcModel caseFilterMvcModel, bool? TBCase, bool? DocRequested, bool? DocRecived)
        {
            InitilizeCaseStatus(caseFilterMvcModel.CaseStatusActive);
            InitilizeCaseType(caseFilterMvcModel.ParentNoticeTypeActive, AppUser.User.UserRole.UserSystemRoleId);
            InitializeOICs(null);

            List<Case> ActiveCases = new List<Case>();
            ActiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderBy(i => i.Notice.NoticeDate).ToList();

            if (TBCase != null && (bool)TBCase)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.IsTimeBoundCompliance == TimeBoundCompliance.Yes).Count() > 0).ToList();

            if (DocRequested != null && (bool)DocRequested)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingRespondents.Count() > 0).Count() > 0).ToList();

            if (DocRecived != null && (bool)DocRecived)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingResponses.Count() > 0).Count() > 0).ToList();

            if (caseFilterMvcModel.CaseDateFrom != null)
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeDate >= caseFilterMvcModel.CaseDateFrom).ToList();

            if (caseFilterMvcModel.CaseDateTo != null)
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeDate <= caseFilterMvcModel.CaseDateTo).ToList();

            if (caseFilterMvcModel.CaseStatusActive != null && caseFilterMvcModel.CaseStatusActive.ToString() != "")
                ActiveCases = ActiveCases.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusActive).ToList();

            if (caseFilterMvcModel.ParentNoticeTypeActive != null && caseFilterMvcModel.ParentNoticeTypeActive != "99" && caseFilterMvcModel.ParentNoticeTypeActive != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.ParentNoticeTypeActive).ToList();

            if (caseFilterMvcModel.CaseNoActive != null && caseFilterMvcModel.CaseNoActive != "")
                ActiveCases = ActiveCases.Where(x => x.PetitionNo.ToLower().Contains(caseFilterMvcModel.CaseNoActive.ToLower())).ToList();

            if (caseFilterMvcModel.CasePetitionerNameActive != null && caseFilterMvcModel.CasePetitionerNameActive != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.Petitioners.Where(y => y.Name.ToLower().Contains(caseFilterMvcModel.CasePetitionerNameActive.ToLower())).Count() > 0).ToList();

            caseFilterMvcModel.AcctiveCases = CaseMvcModel.CopyFromEntityList(ActiveCases, 0);
            return View(caseFilterMvcModel);
        }

        [HttpPost]
        public ActionResult AssignOIC(Guid CaseGuid, Guid GovOIC)
        {

            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "").Where(x => x.CaseId == CaseGuid).FirstOrDefault();

            var newGovOIC = GovOICDataService.GetFiltered(db, AppUser.User, "DB").Where(x => x.GovernmentdepartmentOICID == GovOIC).FirstOrDefault();

            PartyAndRespondent dbRespondent = dbcase.Notice.Respondents.Where(r => r.GovernmentDepartmentId == newGovOIC.GovernmentDepartmentId).FirstOrDefault();
            if (dbRespondent == null)
            {
                SucessMessage = $" The case \"{ dbcase.PetitionNo }\" does not have any Party/Respondent of the Government Department \"{ newGovOIC.GovernmentDepartment.Name }\" to which OIC \"{ newGovOIC.OICName }\" belongs to. Could not assign OIC";
                return RedirectToAction("Index", new { CurrentTab = "AcctiveCases" });
            }

            var oldOIC = dbRespondent.GovernmentDepartmentOic;
            var oldOICName = oldOIC == null ? "" : oldOIC.OICName;

            dbRespondent.GovernmentdepartmentOICID = GovOIC;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string newOICname = GovOICDataService.GetFiltered(db, AppUser.User, "DB").Where(x => x.GovernmentdepartmentOICID == GovOIC).FirstOrDefault().OICName;
            if (oldOICName != newOICname)
            {
                if (oldOICName != null && oldOICName != "")
                {
                    string description = $" Government Department OIC is changed from '{ oldOICName }' to '{ newOICname }' successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepOICChange, AppUser.UserId, description, null);
                }
                else
                {
                    string description = $"New Government Department OIC '{ newOICname }' is assigned successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepOICAssignment, AppUser.UserId, description, null);
                }
            }

            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateGovOICAssignmentNotification(dbcase, (oldOIC == null ? Guid.Empty : oldOIC.GovernmentdepartmentOICID), dbRespondent);

            SucessMessage = $" OIC assigned to case :- { dbcase.PetitionNo } successfully";

            return RedirectToAction("Index", new { CurrentTab = "AcctiveCases" });
        }

        [HttpGet]
        public ActionResult GetOIC(Guid? OICId)
        {
            var dbData = GovOICDataService.GetFiltered(db, AppUser.User, "DB").ToList();
            if (OICId != Guid.Empty)
                dbData = dbData.Where(x => x.GovernmentdepartmentOICID != OICId).ToList();

            var mvc = GovOICMvcModel.CopyFromEntityList(dbData, 0).Select(x => new GovOICMvcModel() { GovernmentdepartmentOICID = x.GovernmentdepartmentOICID, OICName = x.OICName });

            return Json(JsonConvert.SerializeObject(mvc), JsonRequestBehavior.AllowGet);
        }

        private void InitializeOICs(object selectedValue)
        {
            var dbData = GovOICDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == AppUser.User.GovernmentDepartmentId);
            var mvc = GovOICMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovOIC = new SelectList(mvc, nameof(GovOICMvcModel.GovernmentdepartmentOICID), nameof(GovOICMvcModel.OICName), selectedValue);
        }

        public void InitilizeCouncil(object selectedValue)
        {
            var dbData = AdvocateDataService.GetLite(db, AppUser).ToList();
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Advocate = new SelectList(mvc, nameof(UserMvcModel.UserId), nameof(UserMvcModel.Name), selectedValue);
        }

        [HttpPost]
        public ActionResult GetRequestData(Guid id)
        {
            var dbdata = RequestDocDataService.GetDetail(db).Where(x => x.CaseId == id).OrderBy(i => i.DocumentId).ToList();
            var ep = (from gd in dbdata
                      join pr in db.GovernmentDepartments on gd.SharedBy equals pr.GovernmentDepartmentId
                      join c in db.DocumentMaster on gd.DocumentId equals c.DocumentId
                      select new
                      {
                          pr.Name,
                          c.DocumentName,
                          gd.CommentRequestor,
                          gd.GovDeptComment,
                          gd.Document_Name,
                          gd.DocumentStatus,
                          gd.DocumentPath,
                          gd.CaseId
                      });
            return Json(ep);

        }
        public void InitilizeRegion(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var allRegions = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(allRegions, "RegionId", "Name", selectedValue);
        }

        public void InitilizeCourt(object selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AllCourt = new SelectList(AllCourt, "CourtId", "Name", selectedValue);


            dbData = dbData.Where(x => x.IsClose == false && x.Status == CourtStatus.Active).ToList();
            var OpenedCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.OpenedCourt = new SelectList(OpenedCourt, "CourtId", "Name", selectedValue);
        }

        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }

        public void InitilizeCaseType(object selectedValue, UserSystemRole userSystemRole)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Criminal", Value = "Criminal" });
            items.Add(new SelectListItem { Text = "Civil", Value = "Civil" });
            items.Add(new SelectListItem { Text = "All", Value = "All" });
            ViewBag.CaseType = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitilizeDocument(object selectedValue)
        {
            var dbData = CaseDataService.GetDocument(db).ToList();
            var mvc = DocumentMVCModel.CopyFromEntityList(dbData, 0);
            ViewBag.Document = new SelectList(mvc, nameof(DocumentMVCModel.DocumentId), nameof(DocumentMVCModel.DocumentName), selectedValue);
        }
        public void InitilizeDepartment(object selectedValue, Guid? caseid)
        {
            var dbData = CaseDataService.GetDepartment(db, caseid).ToList();
            ViewBag.Departmemt = new SelectList(dbData, "GovernmentDepartmentId", "Name", selectedValue);
        }

        public void InitilizeCouncil(object selectedValue, ParentNoticeType? CaseType)
        {
            var dbData = AdvocateDataService.GetLite(db, AppUser).ToList();

            if (CaseType != null)
            {
                if (CaseType == ParentNoticeType.Civil)
                    dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("civil")).ToList();
                if (CaseType == ParentNoticeType.Criminal)
                    dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("criminal")).ToList();
            }
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Advocate = new SelectList(mvc, nameof(UserMvcModel.UserId), nameof(UserMvcModel.Name), selectedValue);
        }

        public ActionResult CaseView(Guid id)
        {

            InitilizeDocument(null);
            InitilizeDepartment(null, id);

            CaseMvcModel CaseMobelobj = GetCaseMvcModel(id);
            InitilizeCaseStatus(CaseMobelobj.CaseStatusId);
            InitilizeCouncil(CaseMobelobj.AdvocateId, CaseMobelobj.Notice.NoticeType);
            InitilizeCourt(CaseMobelobj.CourtId);
            InitilizeRegion(CaseMobelobj.RegionId);

            CaseMobelobj.IsCurrentAdvocate = CaseMobelobj.AdvocateId == AppUser.UserId;

            if (Request["nid"] != null)
            {
                Guid n_id = Guid.Empty;
                if (Request["s"] == "1" && Guid.TryParse(Request["nid"], out n_id))
                {
                    AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(n_id, NotificationStatusEnum.IsRead);
                }
            }
            return View(CaseMobelobj);
        }

        [HttpPost]
        public ActionResult SaveHearingDetail(Guid CaseHearingId, Guid CaseId, HttpPostedFileBase UploadDocument, string Comments)
        {
            Guid DocumentId = Guid.NewGuid();
            int HasDoc = 0;
            if (UploadDocument != null && UploadDocument.ContentLength > 0)
            {
                string DocumentUrl = SaveDocument(UploadDocument);
                if (DocumentUrl != null && DocumentUrl != "")
                {
                    Document DocNewObj = new Document
                    {
                        DocumentId = DocumentId,
                        CaseHearingId = CaseHearingId,
                        Name = "Response Doc ",
                        DocumentUrl = DocumentUrl,
                        FileType = DocumentFileType.Any,
                        EntityType = DocumentEntityType.CaseHearing,
                        Type = DocumentType.CHResponse,
                        Ordinal = 1,
                        Size = UploadDocument.ContentLength,
                        Status = DocumentStatus.Added,
                        CreatedBy = AppUser.UserId,
                        CreatedOn = DateTime.Now
                    };
                    db.Documents.Add(DocNewObj);
                    HasDoc = 1;
                }
            }
            CaseHearingResponse chR = new CaseHearingResponse
            {
                CaseHearingResponseId = Guid.NewGuid(),
                CaseHearingId = CaseHearingId,
                DocumentId = HasDoc == 1 ? DocumentId : Guid.Empty,
                Comments = Comments,
                GovDepartmentId = (Guid)UserDataService.GetLite(db).Where(x => x.UserId == AppUser.UserId).FirstOrDefault().GovernmentDepartmentId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            db.CaseHearingResponses.Add(chR);
            int iresult = db.SaveChangesWithAudit(Request);


            if (iresult > 0)
            {
                string CaseName = CaseDataService.GetLite(db).Where(x => x.CaseId == CaseId).FirstOrDefault().PetitionNo;
                string description = $"Case Hearing Response saved successfully for Case No { CaseName } ";
                TimelineBusinessService.SaveTimeLine(db, null, CaseId, EventType.GovCHResponse, AppUser.UserId, description, null);
            }
            return RedirectToAction("CaseView/" + CaseId);
        }
    }
}