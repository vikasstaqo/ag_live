﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class HolidayListDataService
    {
        public static IQueryable<Holiday> GetLite(DatabaseContext db)
        {
            return db.Holiday.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Holiday> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }

    }

    
}