﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CourtController : BaseMVCController
    {
        // GET: Court
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(CourtMvcModel.CopyFromEntityList(CourtDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        // GET: CaseStatus/Details/5
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeBench(dbData.BenchTypeID);
            InitilizeJudge(id);
            return View(CourtMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Court/Create
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            CourtMvcModel model = new CourtMvcModel();
            InitilizeBench(null);
            InitilizeJudge(null);
            return View(model);
        }

        public void InitilizeBench(Guid? selectedValue)
        {
            var dbData = BenchTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = BenchTypeMvcModel.CopyFromEntityList(dbData, 0);
            var benchDetails = new List<BenchDetails>();
            foreach (var item in mvc)
            {
                benchDetails.Add(new BenchDetails
                {
                    BenchTypeID = item.BenchTypeID,
                    Name = item.Name,
                    NumberOfJudge = item.NumberOfJudge,
                    selectedValue = selectedValue != null && item.BenchTypeID == selectedValue.Value ? selectedValue.Value.ToString() : ""
                });
            }
            ViewBag.Bench = benchDetails;
        }

        public void InitilizeJudge(Guid? selectedValue)
        {
            List<JudgeResponseModel> mvc;
            var dbData = JudgeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            mvc = JudgeResponseModel.CopyFromEntityList(dbData, 0);

            if (selectedValue != null && selectedValue != new Guid())
            {
                var dbJudgeData = JudgeDataService.GetSelectdJudgeDetails(db, selectedValue.Value).ToList();
                mvc = JudgeResponseModel.CopyFromEntityList(dbJudgeData, 0);
            }
            ViewBag.Judge = new SelectList(mvc, "JudgeId", "Name", selectedValue);
        }

        public void InitilizeJudgeForEdit(Guid? selectedValue, out List<Guid> selectedIds)
        {
            selectedIds = new List<Guid>();
            List<JudgeResponseModel> mvc;
            var dbData = JudgeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            mvc = JudgeResponseModel.CopyFromEntityList(dbData, 0);

            if (selectedValue != null && selectedValue != new Guid())
            {
                var dbJudgeData = JudgeDataService.GetSelectdJudgeDetails(db, selectedValue.Value).ToList();
                foreach (var data in dbJudgeData)
                    selectedIds.Add(data.JudgeId);
            }
            ViewBag.Judge = new SelectList(mvc, "JudgeId", "Name", selectedIds);
        }
        // POST: Court/Create
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(CourtMvcModel model)
        {

            if (!ModelState.IsValid)
            {
                InitilizeBench(null);
                InitilizeJudge(null);
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                InitilizeBench(null);
                InitilizeJudge(null);
                return View(model);
            }
            Court tempCourt = new Court();
            tempCourt.CourtId = Guid.NewGuid();
            tempCourt.DisplayOrder = MasterBusinessService.GetCourtsDisplayOrder(db);
            InitializeData(tempCourt, model, true);
            tempCourt.CreatedBy = AppUser.UserId;
            tempCourt.CreatedOn = DateTime.Now;
            db.Courts.Add(tempCourt);
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            foreach (var item in model.SelectedJudgeIds)
            {
                var courtJudgeMapping = new CourtJudgeMapping
                {
                    BenchTypeID = model.BenchTypeID,
                    CourtId = tempCourt.CourtId,
                    CourtJudgeMappingID = Guid.NewGuid(),
                    JudgeId = item,
                    Name = model.Name,
                    CreatedBy = AppUser.UserId,
                    IsActive = true
                };
                db.CourtJudgeMappings.Add(courtJudgeMapping);
                db.SaveChangesWithAudit(Request, AppUser.UserName);
            }
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Create");
        }

        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = CourtDataService.GetLite(db).Where(i => i.CourtId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
        // GET: AGDepartment/Edit/5
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeBench(dbData.BenchTypeID);
            List<Guid> selectedIds;
            InitilizeJudgeForEdit(id, out selectedIds);
            var model = CourtMvcModel.CopyFromEntity(dbData, 0);
            model.SelectedJudgeIds = selectedIds.ToArray();
            return View(model);
        }
        // POST: CaseStatus/Edit/5
        [PageRoleAuthorizarion(PageName.Court, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(CourtMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == model.CourtId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                InitilizeBench(model.BenchTypeID);
                return View(model);
            }
            InitializeData(dbData, model, false);

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            var courtJudgeMappings = CourtJudgeMappingDataService.GetLite(db).ToList();

            var deletedJudgeMapping = courtJudgeMappings.Where(t => !model.SelectedJudgeIds.Contains(t.JudgeId) && t.CourtId == model.CourtId).ToList();
            if (deletedJudgeMapping.Count > 0)
            {

                foreach (var item in deletedJudgeMapping)
                {
                    item.IsActive = false;
                }
                db.SaveChangesWithAudit(Request, AppUser.UserName);
            }
            foreach (var item in model.SelectedJudgeIds)
            {
                var juddgeMappingExist = courtJudgeMappings.Where(t => t.JudgeId == item && t.CourtId == model.CourtId).FirstOrDefault();
                if (juddgeMappingExist != null)
                {
                    juddgeMappingExist.IsActive = true;
                    juddgeMappingExist.Name = model.Name;
                    juddgeMappingExist.BenchTypeID = model.BenchTypeID;
                    juddgeMappingExist.CourtId = model.CourtId;
                    juddgeMappingExist.CreatedBy = AppUser.UserId;
                    juddgeMappingExist.CreatedOn = DateTime.Now;
                    db.SaveChangesWithAudit(Request, AppUser.UserName);
                }
                else
                {
                    var courtJudgeMapping = new CourtJudgeMapping
                    {
                        BenchTypeID = model.BenchTypeID,
                        CourtId = model.CourtId,
                        CourtJudgeMappingID = Guid.NewGuid(),
                        JudgeId = item,
                        Name = model.Name,
                        CreatedBy = AppUser.UserId,
                        CreatedOn = DateTime.Now,
                        IsActive = true
                    };
                    db.CourtJudgeMappings.Add(courtJudgeMapping);
                    db.SaveChangesWithAudit(Request, AppUser.UserName);
                }
            }
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        private void InitializeData(Court tempCourt, CourtMvcModel model, bool IsNew)
        {
            if (IsNew && model.IsClose)
            {
                CourtOpenCloseLog tempCourtOpenCloseLog = new CourtOpenCloseLog();
                tempCourtOpenCloseLog.CourtOpenCloseLogId = Guid.NewGuid();
                tempCourtOpenCloseLog.CourtId = tempCourt.CourtId;
                tempCourtOpenCloseLog.CloseFromDate = model.CloseFromDate;
                tempCourtOpenCloseLog.CloseToDate = model.CloseToDate;
                tempCourtOpenCloseLog.ModifiedBy = AppUser.UserId;
                tempCourt.CourtOpenCloseLogs.Add(tempCourtOpenCloseLog);
            }
            if (!IsNew)
            {
                //Update
                if (tempCourt.IsClose == true && model.IsClose == true)
                {
                    var tempLastClosingDate = tempCourt.CourtOpenCloseLogs.OrderBy(i => i.CloseToDate).FirstOrDefault();
                    tempLastClosingDate.CloseFromDate = model.CloseFromDate;
                    tempLastClosingDate.CloseToDate = model.CloseToDate;
                }
                //Create
                if (tempCourt.IsClose == false && model.IsClose == true)
                {
                    CourtOpenCloseLog tempCourtOpenCloseLog = new CourtOpenCloseLog();
                    tempCourtOpenCloseLog.CourtOpenCloseLogId = Guid.NewGuid();
                    tempCourtOpenCloseLog.CourtId = tempCourt.CourtId;
                    tempCourtOpenCloseLog.CloseFromDate = model.CloseFromDate;
                    tempCourtOpenCloseLog.CloseToDate = model.CloseToDate;
                    tempCourtOpenCloseLog.ModifiedBy = AppUser.UserId;
                    tempCourt.CourtOpenCloseLogs.Add(tempCourtOpenCloseLog);
                }
            }
            tempCourt.Name = model.Name;
            tempCourt.Number = model.Number;
            tempCourt.Status = model.Status;
            tempCourt.BenchTypeID = model.BenchTypeID;
            tempCourt.IsClose = model.IsClose;

        }
        private bool CommonForInsertUpdate(CourtMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var CourtNoforcheck = model.Number.TrimX();
            var duplicateCheck = CourtDataService.GetLite(db).FirstOrDefault(i => i.CourtId != model.CourtId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Court name already exists");
                return false;
            }

            var duplicateCheckForNumber = CourtDataService.GetLite(db).FirstOrDefault(i => i.CourtId != model.CourtId && i.Number == CourtNoforcheck);
            if (duplicateCheckForNumber != null)
            {
                ModelState.AddModelError(nameof(model.Number), "Number already exists");
                return false;
            }

            return true;
        }
    }
}
