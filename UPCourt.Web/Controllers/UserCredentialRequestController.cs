﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class UserCredentialRequestController : BaseMVCController
    {
        // GET: UserCredentialRequest
        public ActionResult Index()
        {
            var dbData = UserCredentialRequestDataService.GetDetail(db).OrderBy(i => (i.CreateDate)).ToList();
            return View(UserCredentialRequestModel.CopyFromEntityList(dbData, 0));
        }

        // GET: UserCredentialRequest/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            InitilizeRegions(null);
            var model = new UserCredentialRequestModel();
            return View(model);
        }

        // POST: UserCredentialRequest/Create
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Create(UserCredentialRequestModel model)
        {
            InitilizeRegions(model.RegionName);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbUser = UserCredentialRequestDataService.GetLite(db).FirstOrDefault(i => i.Email== model.Email);
            if (dbUser != null)
            {
                ModelState.AddModelError(nameof(UserCredentialRequestModel.Email), "User already exist");
                return View(model);
            }
            UserCredentialRequest tempUserCredentialRequest = new UserCredentialRequest();
            tempUserCredentialRequest.UserCredentialRequestId = Guid.NewGuid();
            tempUserCredentialRequest.CreateDate = Extended.CurrentIndianTime.ToString();
            tempUserCredentialRequest.Status = UserCredentialRequestStatus.Pending;
            InitializeData(tempUserCredentialRequest, model);

            db.UserCredentialRequests.Add(tempUserCredentialRequest);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
           
            SucessMessage = $"User {tempUserCredentialRequest.Name} requested successfully";

            ViewBag.insertresult = SucessMessage;
        
            return View(model);
        }
        private bool CommonForInsertUpdate(UserCredentialRequestModel model)
        {

            var duplicateCheck = UserCredentialRequestDataService.GetLite(db).FirstOrDefault(i => i.UserCredentialRequestId != model.UserCredentialRequestId && i.Name == model.Name && i.Email == model.Email && i.MobileNo == model.MobileNo);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), $"{model.Email} already exists");
               
                return false;
            }
            return true;
        }

        // GET: UserCredentialRequest/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeRegions(dbData.Name);
            return View(UserCredentialRequestModel.CopyFromEntity(dbData, 0));
        }

        public ActionResult Action (Guid id)
        {
            var dbData = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeRegions(dbData.RegionName);
           
            return View(UserCredentialRequestModel.CopyFromEntity(dbData, 0));
        }

        [HttpPost]
        public ActionResult Action(UserCredentialRequestModel usercredentialmodel, UserMvcModel Usermodel)
        {
            if (!ModelState.IsValid)
            {
                return View(usercredentialmodel);
            }
            
            var GetRole = UserRoleDataService.GetDetail(db).Where(i => i.Name == "Guest").ToList();

           

             var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(usercredentialmodel.Password, passWordSalt);
            User tempData = new User();
            tempData.UserId = Guid.NewGuid();
            tempData.Name = usercredentialmodel.Name;
            tempData.Email = usercredentialmodel.Email;
            tempData.Address = usercredentialmodel.Address;
            tempData.Phone = usercredentialmodel.MobileNo;
            tempData.LoginId = usercredentialmodel.Email;
            tempData.Status = UserStatus.Active ;
            tempData.PasswordSalt = passWordSalt;
            tempData.PasswordHash = passwordHash;
            tempData.UserType = UserType.UserCrendial;
            tempData.ModifiedBy= AppUser.UserId;
            tempData.UserRoleId = GetRole[0].UserRoleId;
            
            
            

            db.Users.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);




            var dbDataUserCredential = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == usercredentialmodel.UserCredentialRequestId).FirstOrDefault();
            if (dbDataUserCredential == null)
            {
                return RedirectToAction("Index");
            }

            dbDataUserCredential.Status = UserCredentialRequestStatus.Accepted;
            dbDataUserCredential.ModifyBy = AppUser.UserName;
           
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"User credential request {dbDataUserCredential.Name} update successfully";

            return RedirectToAction("Index");
        }
       

        // POST: UserCredentialRequest/Edit/5
        [HttpPost]
        public ActionResult Edit(UserCredentialRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == model.UserCredentialRequestId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            dbData.Status = model.Status;
            dbData.ModifyBy = AppUser.UserName;
            
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"User credential request {dbData.Name} update successfully";

            return RedirectToAction("Index");
        }
        private void InitializeData(UserCredentialRequest tempUserCredentialRequest, UserCredentialRequestModel model)
        {
            tempUserCredentialRequest.Name = model.Name;
            tempUserCredentialRequest.Address = model.Address;
            tempUserCredentialRequest.Email = model.Email;
            tempUserCredentialRequest.MobileNo = model.MobileNo;
            tempUserCredentialRequest.PetitionNo = model.PetitionNo;
            tempUserCredentialRequest.RegionId = model.RegionId;
            tempUserCredentialRequest.ModifyBy = model.Name;
            tempUserCredentialRequest.RegionName = model.RegionName;
        }
        public void InitilizeRegions(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(mvc, nameof(RegionMvcModel.Name)
                , nameof(RegionMvcModel.Name), selectedValue);
        }

     
        [HttpPost]
        public ActionResult GeneratePassword()
        {
            int lowercase = 3; int uppercase = 3; int numerics = 3;
            string lowers = "abcdefghijklmnopqrstuvwxyz";
            string uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string number = "0123456789";

            Random random = new Random();

            string generated = "!";
            for (int i = 1; i <= lowercase; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    lowers[random.Next(lowers.Length - 1)].ToString()
                );

            for (int i = 1; i <= uppercase; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    uppers[random.Next(uppers.Length - 1)].ToString()
                );

            for (int i = 1; i <= numerics; i++)
                generated = generated.Insert(
                    random.Next(generated.Length),
                    number[random.Next(number.Length - 1)].ToString()
                );

            return Json(new { password = generated.Replace("!", string.Empty) });
        }



    }
}
