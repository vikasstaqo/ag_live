﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class NotificationTypeModel : BaseEntityModel<Entity.NotificationsType, NotificationTypeModel>
    {
        public int NotificationTypeId { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}