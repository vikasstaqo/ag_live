﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CaseStage), UserNameProp: nameof(ModifiedBy))]
    public class CaseStage:BaseMasterEntity
    {
        public Guid CaseStageId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        
    }
}
