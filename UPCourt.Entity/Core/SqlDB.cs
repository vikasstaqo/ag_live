﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UPCourt.Entity.Core
{
    public class SqlDB : IDisposable
    {
        private string ConnectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }
                disposedValue = true;
            }
        }


        void IDisposable.Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public string DatabaseName
        {
            get
            {
                using (var con = new MySqlConnection(ConnectionString))
                {
                    return con.Database.Trim();
                }
            }
        }

        public DataTable GetDataTableAsync(string SqlString)
        {

            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SqlString, con))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }
        public string ExecuteNonQuery(string SqlString)
        {
            try
            {

                return Convert.ToString(MySqlHelper.ExecuteNonQuery(ConnectionString, SqlString));
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public DataTable GetDataTable(string SqlString)
        {

            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SqlString, con))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        public DataSet GetDataSetAsync(string SqlString)
        {

            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SqlString, con))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        sda.SelectCommand = cmd;
                        using (DataSet dt = new DataSet())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        public DataSet GetDataSet(string SqlString)
        {

            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SqlString, con))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        sda.SelectCommand = cmd;
                        using (DataSet dt = new DataSet())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        public DataSet GetDataSet(string Proc, params MySqlParameter[] parameters)
        {

            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(Proc, con))
                {
                    cmd.CommandType = CommandType.Text;
                    if (parameters != null) cmd.Parameters.AddRange(parameters);
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        sda.SelectCommand = cmd;
                        using (DataSet dt = new DataSet())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        public DataTable GetDataTableAsync(string Proc, params MySqlParameter[] parameters)
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(Proc, con))
                {
                    cmd.CommandType = CommandType.Text;
                    if (parameters != null) cmd.Parameters.AddRange(parameters);
                    //con.Open();
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        //cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            return dt;
                        }
                    }
                }
            }
        }

        public int ExecuteAsync(string sConnectionString, string sSQL, params MySqlParameter[] parameters)
        {
            using (var newConnection = new MySqlConnection(sConnectionString))
            using (var newCommand = new MySqlCommand(sSQL, newConnection))
            {
                newCommand.CommandType = CommandType.Text;
                if (parameters != null) newCommand.Parameters.AddRange(parameters);
                newConnection.Open();
                return newCommand.ExecuteNonQuery();
            }
        }
        public string ExecuteScalarStringAsync(string SqlString, params MySqlParameter[] parameters)
        {
            try
            {

                return Convert.ToString(MySqlHelper.ExecuteScalarAsync(ConnectionString, SqlString, parameters));
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ExecuteScalarString(string SqlString)
        {
            try
            {

                return Convert.ToString(MySqlHelper.ExecuteScalar(ConnectionString, SqlString));
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public string BulkUploadCSV(string strFile, string tableName)
        {
            string InsRows = "0";
            try
            {
                //connectionstrng = allowLoadLocalInfile=true;                

                using (MySqlConnection con = new MySqlConnection(ConnectionString))
                {
                    con.Open();
                    MySqlBulkLoader bcp1 = new MySqlBulkLoader(con);
                    bcp1.TableName = tableName;
                    bcp1.Local = true;
                    bcp1.Priority = MySqlBulkLoaderPriority.Low;
                    bcp1.FieldTerminator = ",";
                    bcp1.LineTerminator = "\r\n";
                    bcp1.NumberOfLinesToSkip = 1;
                    bcp1.FileName = strFile;
                    InsRows = bcp1.Load().ToString();
                }
                return InsRows;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
                //return ex.ToString();
            }
        }

        public void ValidateCSV(string filePath, out int SuccessCount, out int RejectCount, out int TotalCount)
        {
            SuccessCount = 0;
            RejectCount = 0;
            TotalCount = 0;
            try
            {
                string pattern = @"^[0-9]+$";
                Regex rg = new Regex(pattern);
                TotalCount = File.ReadAllLines(filePath).Length;
                if (TotalCount > 100000)
                {
                    return;
                }
                int i = 0;
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        if (i > 0)
                        {
                            if (rg.IsMatch(line) && line.Length >= 10)
                            {
                                SuccessCount++;
                            }
                            else
                                RejectCount++;

                        }
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
