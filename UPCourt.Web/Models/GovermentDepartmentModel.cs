﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class GovermentDepartmentMvcModel : BaseEntityModel<GovernmentDepartment, GovermentDepartmentMvcModel>
    {

        public Guid GovernmentDepartmentId { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Post")]
        public string HeadOffice { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        [Display(Name = "POC Name")]
        public string POCName { get; set; }
        [Required]
        [Display(Name = "POC Contact")]
        public string POCContact { get; set; }
      
        [Display(Name = "POC Fax No")]
        public string POCFaxNo { get; set; }
        [Display(Name = "POC Mobile No")]
     
        public string POCMobileNo { get; set; }

        public bool IsDefault { get; set; }
        public bool IsAssignUser { get; set; }
        public GovtDeparmentStatus Status { get; set; }
        [FilterUIHint("GovernmentDepartmentContactNoMvcModel")]
        public List<GovernmentDepartmentContactNoMvcModel> GovernmentDepartmentContactNos { get; set; }
        [FilterUIHint("GovernmentDepartmentAddressMvcModel")]
        public List<GovernmentDepartmentAddressMvcModel> GovernmentDepartmentAddresses { get; set; }
        [FilterUIHint("GovDesignationMvcModel")]
        public List<GovDesignationMvcModel> GovDesignations { get; set; }
        public override GovermentDepartmentMvcModel EntityToResponse(GovernmentDepartment entity, GovermentDepartmentMvcModel model)
        {
            model.GovernmentDepartmentContactNos = GovernmentDepartmentContactNoMvcModel.CopyFromEntityList(entity.GovernmentDepartmentContactNos, model.RecursiveLevel);
            model.GovernmentDepartmentAddresses = GovernmentDepartmentAddressMvcModel.CopyFromEntityList(entity.GovernmentDepartmentAddresses, model.RecursiveLevel);
            model.GovDesignations = GovDesignationMvcModel.CopyFromEntityList(entity.GovernmentDesignations, model.RecursiveLevel);
            return model;
        }
        [RegularExpression(@"^[a-zA-Z0-9'@&#.\s]{7,10}$", ErrorMessage = "Password length must be between 7 to 10 characters which contain at least one numeric digit and a special character")]
        public string Password { get; set; }

    }

    public class GovernmentDepartmentContactNoMvcModel : BaseEntityModel<GovernmentDepartmentContactNo, GovernmentDepartmentContactNoMvcModel>
    {
        public Guid GovernmentDepartmentContactNoId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        [RegularExpression(RegularExpressionHelper.PhoneNoValidationExpression, ErrorMessage = "Entered phone format is not valid.")]
        public string ContactNo { get; set; }
        public bool bActive { get; set; }
        public bool DeletePhone { get; set; }
    }

    public class GovernmentDepartmentAddressMvcModel : BaseEntityModel<GovernmentDepartmentAddress, GovernmentDepartmentAddressMvcModel>
    {
        public Guid GovernmentDepartmentAddressId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string Address { get; set; }
        public bool IsHeadOfficeAddress { get; set; }
        public bool bActive { get; set; }
        public bool DeleteAddress { get; set; }
    }

    public class GovDesignationMvcModel : BaseEntityModel<GovernmentDesignation, GovDesignationMvcModel>
    {
        public Guid GovernmentDesignationId { get; set; }
        [Display(Name = "Government DepartmentId")]
        public Guid GovernmentDepartmentId { get; set; }
        [Display(Name = "Designation Name")]
        public string DesignationName { get; set; }
        public GovernmentDesignationStatus Status { get; set; }
        public DateTime ModifyOn { get; set; }
        public bool bActive { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        [Display(Name = "POC Name")]
        public string POCName { get; set; }

        [Display(Name = "POC Email (User Name)"), DataType(DataType.EmailAddress)]
        public string POCEmail { get; set; }
        [Required]
        [Display(Name = "POC Contact")]
        public string POCContact { get; set; }

        [Display(Name = "POC Fax No")]
        public string POCFaxNo { get; set; }

        [Display(Name = "POC Mobile No")]
        [DataType(DataType.PhoneNumber)]
        
        public string POCMobileNo { get; set; }

        [Display(Name = "POC Password"), DataType(DataType.Password)]
        public string POCPassword { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }
        public override GovDesignationMvcModel EntityToResponse(GovernmentDesignation entity, GovDesignationMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            return model;
        }
    }


    public class GovOICMvcModel : BaseEntityModel<GovernmentDepartmentOic, GovOICMvcModel>
    {
        public Guid GovernmentdepartmentOICID { get; set; }
        [Display(Name = "Government Department")]
        public Guid GovernmentDepartmentId { get; set; }
        [Display(Name = "Government Designation")]
        public Guid GovernmentDesignationId { get; set; }
        [Display(Name = "OIC Name")]
        public string OICName { get; set; }
        public GovernmentDepartmentOicStatus Status { get; set; }
        public DateTime ModifyOn { get; set; }
        public bool bActive { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z\. ]*$", ErrorMessage = "Please use alphabets only.")]
        [Display(Name = "POC Name")]
        public string POCName { get; set; }

        [Display(Name = "POC Email (User Name)"), DataType(DataType.EmailAddress)]
        public string POCEmail { get; set; }
        [Required]
        [Display(Name = "POC Contact")]
        public string POCContact { get; set; }

        [Display(Name = "POC Fax No")]
        public string POCFaxNo { get; set; }

        [Display(Name = "POC Mobile No")]
        [DataType(DataType.PhoneNumber)]
      
        public string POCMobileNo { get; set; }

        [Display(Name = "POC Password"), DataType(DataType.Password)]
        public string POCPassword { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }
        public GovDesignationMvcModel GovermentDesignation { get; set; }
        public override GovOICMvcModel EntityToResponse(GovernmentDepartmentOic entity, GovOICMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            model.GovermentDesignation = GovDesignationMvcModel.CopyFromEntity(entity.GovernmentDesignation, model.RecursiveLevel);
            return model;
        }
    }
}