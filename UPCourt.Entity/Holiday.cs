﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(HolidayName), UserNameProp: nameof(CreatedBy))]
    public class Holiday : BaseMasterEntity
    {
        public Holiday() { }
        public Guid HolidayId { get; set; }
        public string HolidayName { get; set; }
        public string Hdate { get; set; }

        public string HStatus{get;set;}
        public string Hyear { get; set; }
    }
   
}
