﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CreatedBy), UserNameProp: nameof(CreatedBy))]
    public class CaseHearing : BaseTransEntity
    {
        public CaseHearing()
        {
            Documents = new HashSet<Document>();
            CaseHearingRespondents = new HashSet<CaseHearingRespondent>();
            CaseHearingGovAdvocates = new HashSet<CaseHearingGovAdvocate>();
        }
        public Guid CaseHearingId { get; set; }
        public Guid CaseId { get; set; }
        public DateTime? HearingDate { get; set; }
        public DateTime? NextHearingDate { get; set; }
        public string Comments { get; set; }
        public Case Case { get; set; }
        public ICollection<Document> Documents { get; set; }
        public CaseHearingType HearingType { get; set; }
        public Guid JudgeId { get; set; }
        public Judge Judge { get; set; }
        public string CauseListSrNo { get; set; }
        public string CauseListReason { get; set; }
        public string CourtInstructions { get; set; }
        public CaseHearingStatus HearingStatus { get; set; }
        public CertifiedCopyReceived IsCertifiedCopyReceived { get; set; }
        public string CertifiedCopyFileName { get; set; }
        public Guid CertifiedCopyDocumentId { get; set; }

        public TimeBoundCompliance IsTimeBoundCompliance { get; set; }
        public DateTime? TimeBoundComplianceDate { get; set; }
        public ICollection<CaseHearingRespondent> CaseHearingRespondents { get; set; }
        public ICollection<CaseHearingGovAdvocate> CaseHearingGovAdvocates { get; set; }
        public ICollection<CaseHearingResponse> CaseHearingResponses { get; set; }
    }

    public enum CertifiedCopyReceived
    {
        Yes = 1,
        No = 0
    }

    public enum TimeBoundCompliance
    {
        Yes = 1,
        No = 0
    }
    public enum CaseHearingStatus
    {
        Pending = 1,
        Disposed = 0
    }

    public enum CaseHearingType
    {
        [Display(Name = "Admission")]
        Admission = 1,
        [Display(Name = "Motion Hearing")]
        MotionHearing = 2,
        [Display(Name = "Final Order")]
        FinalOrder = 3
    }

    public class CaseHearingRespondent : BaseTransEntity
    {
        public Guid CaseHearingRespondentId { get; set; }
        public Guid CaseHearingId { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public Guid DocumentType { get; set; }
        public DateTime LastComplianceDate { get; set; }
        public Priority Priority { get; set; }
        public string Comments { get; set; }
    }

    public enum Priority
    {
        High = 1,
        Medium = 2,
        Low = 3
    }

    public class CaseHearingGovAdvocate : BaseTransEntity
    {
        public Guid CaseHearingGovAdvocateId { get; set; }
        public Guid CaseHearingId { get; set; }
        public Guid AdvocateId { get; set; }
        public User Advocate { get; set; }
    }

    public class CaseHearingResponse : BaseTransEntity
    {
        public Guid CaseHearingResponseId { get; set; }
        public Guid CaseHearingId { get; set; }
        public Guid? GovDepartmentId { get; set; }
        public Guid? DocumentId { get; set; }
        public Guid? AdvocateId { get; set; }
        public string Comments { get; set; }

    }

}
