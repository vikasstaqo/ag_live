﻿using Microsoft.EntityFrameworkCore;
using UPCourt.Entity;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using UPCourt.Web.Models;
using NLog;

namespace UPCourt.Web.Service.DataService
{
    public class CaseDataService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static IQueryable<Case> GetLite(DatabaseContext db)
        {
            return db.Cases.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Case> GetDetail(DatabaseContext db)
        {
            return GetDetail(GetLite(db));
        }

        public static IQueryable<Case> GetDetail(IQueryable<Case> cases)
        {
            return cases
                .Include(F => F.Court)
                .Include(F => F.Region)
                .Include(F => F.Timelines)
                .Include(F => F.Notice).ThenInclude(F => F.CaseNoticeType)
                .Include(F => F.Notice).ThenInclude(F => F.BasicIndexs)
                .Include(F => F.Notice).ThenInclude(F => F.BasicIndexs).ThenInclude(F => F.Documents)
                .Include(F => F.Notice).ThenInclude(F => F.Petitioners)
                .Include(F => F.Notice).ThenInclude(F => F.Counselors)
                .Include(F => F.Notice).ThenInclude(F => F.IPCs).ThenInclude(F => F.IPCSection)
                .Include(F => F.Notice).ThenInclude(F => F.CrimeDetail).ThenInclude(F => F.PoliceStation)
                .Include(F => F.Notice).ThenInclude(F => F.CaseDetail).ThenInclude(F => F.Petitioners)
                .Include(F => F.Notice).ThenInclude(F => F.CaseDetail).ThenInclude(F => F.Respondents)
                .Include(F => F.Notice).ThenInclude(F => F.CaseDetail).ThenInclude(F => F.IAs)
                .Include(F => F.Notice).ThenInclude(F => F.Respondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.Notice).ThenInclude(F => F.Respondents).ThenInclude(F => F.GovernmentDepartmentOic)
                .Include(F => F.UserCases)
                .Include(F => F.Advocate)
                .Include(F => F.DocsPrepration)
                .Include(F => F.CaseHearings).ThenInclude(F => F.Documents)
                .Include(F => F.CaseHearings).ThenInclude(F => F.Judge)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingGovAdvocates).ThenInclude(F => F.Advocate)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingRespondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingResponses)
                .Include(F => F.CaseStatus)
                .Include(F => F.CaseTransfers)
                .Include(F => F.RequestDocument);
        }


        public static IQueryable<Case> GetFiltered(DatabaseContext db, Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, Guid? CaseId, string HitFrom = "")
        {
            IQueryable<Case> CaseList ;
            if (CaseId == null || CaseId == Guid.Empty)
                CaseList = GetFiltered(db, UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, HitFrom);
            else
            {
                if (HitFrom == "DB")
                    CaseList = GetDBDetail(GetLite(db).Where(X => X.CaseId == CaseId));
                else
                    CaseList = GetDetail(GetLite(db).Where(X => X.CaseId == CaseId));
            }

            return CheckCases(UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, CaseList);

        }

        public static IQueryable<Case> GetFiltered(DatabaseContext db, Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, string HitFrom = "")
        {
            IQueryable<Case> CaseList;
            if (HitFrom == "DB")
            {
                logger.Info("Data from Database Start");
                CaseList = GetDBDetail(db);
                logger.Info("Data from Database End");
            }
            else
                CaseList = GetDetail(db);

            return CheckCases(UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, CaseList);
        }

        private static IQueryable<Case> CheckCases(Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId,  Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, IQueryable<Case> CaseList)
        {
            if ((UserSystemRoleId == UserSystemRole.GovDeptHead || UserSystemRoleId == UserSystemRole.GovDeptDegUser ||
               UserSystemRoleId == UserSystemRole.GovDeptOIC) && GovernmentDepartmentId != null)
            {
                CaseList = CaseList.Where(x => x.Notice.Respondents.Where(y => y.GovernmentDepartmentId == GovernmentDepartmentId).Count() > 0); ;
                if (UserSystemRoleId == UserSystemRole.GovDeptDegUser)
                    CaseList = CaseList.Where(x => x.Notice.Respondents.Where(y => y.GovernmentDesignationId == DesignationId).Count() > 0);
                if (UserSystemRoleId == UserSystemRole.GovDeptOIC)
                    CaseList = CaseList.Where(x => x.Notice.Respondents.Where(y => y.GovernmentdepartmentOICID == GovermentDepartmentOICID).Count() > 0);
            }
            else
            {
                CaseList = CaseList.Where(x => x.RegionId == TempRegionId || UserSystemRoleId == UserSystemRole.Admin);
                if (UserSystemRoleId == UserSystemRole.CSC || UserSystemRoleId == UserSystemRole.AdvCivil)
                    CaseList = CaseList.Where(x => x.Notice.NoticeType == ParentNoticeType.Civil);
                else if (UserSystemRoleId == UserSystemRole.CGA || UserSystemRoleId == UserSystemRole.AdvCriminal)
                    CaseList = CaseList.Where(x => x.Notice.NoticeType == ParentNoticeType.Criminal);
                if (UserSystemRoleId == UserSystemRole.AdvCriminal || UserSystemRoleId == UserSystemRole.AdvCivil)
                    CaseList = CaseList.Where(x => x.AdvocateId == UserId);
            }
            return CaseList.AsQueryable();
        }

        public static IQueryable<Case> GetDBDetail(DatabaseContext db)
        {
            return GetDBDetail(GetLite(db));
        }

        public static IQueryable<Case> GetDBDetail(IQueryable<Case> cases)
        {
            return cases
                .Include(F => F.CaseStatus)
                .Include(F => F.Notice).ThenInclude(F => F.CaseNoticeType)
                .Include(F => F.Region)
                .Include(F => F.Notice).ThenInclude(F => F.Petitioners)
                .Include(F => F.Notice).ThenInclude(F => F.Counselors)
                .Include(F => F.Notice).ThenInclude(F => F.Respondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.Notice).ThenInclude(F => F.Respondents).ThenInclude(F => F.GovernmentDepartmentOic)
                .Include(F => F.Advocate)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingGovAdvocates)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingRespondents)
                .Include(F => F.CaseHearings).ThenInclude(F => F.CaseHearingResponses);
        }


        public static IQueryable<DocumentMaster> GetDocument(DatabaseContext db)
        {
            return db.DocumentMaster;
        }
        public static IQueryable<GovernmentDepartment> GetDepartment(DatabaseContext db, System.Guid? Case)
        {
            if (Case != null && Case != Guid.Empty)
            {
                var ep = (from gd in db.GovernmentDepartments
                          join pr in db.PartyAndRespondents on gd.GovernmentDepartmentId equals pr.GovernmentDepartmentId
                          join c in db.Cases on pr.NoticeRespondentId equals c.NoticeId
                          where c.CaseId == Case && pr.PartyType == PartyType.Department
                          select gd);
                return ep;
            }
            else
            {
                var ep = (from gd in db.GovernmentDepartments select gd);
                return ep;
            }
        }


        public static List<CaseTypeMvcModel> GetCaseTypeAsync()
        {
            List<UPCourt.Web.Models.CaseTypeMvcModel> caseTypes = new List<Models.CaseTypeMvcModel>();
            var content = GetDataModelAsync("cismaster/getCaseTypes/");
            caseTypes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UPCourt.Web.Models.CaseTypeMvcModel>>(content.Result);
            return caseTypes;
        }

        public static CaseDetailMvcModel GetCaseDetail(string caseTypeNo)
        {
            List<CaseTypeNoticeModel> notice = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseTypeNoticeModel>>(GetDataModelAsync("cismaster/getNoticeNo/" + caseTypeNo).Result);

            List<CaseDetailMvcModel> caseDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseDetailMvcModel>>(GetDataModelAsync("cismaster/getCaseDetails/" + notice[0].cnr).Result);
            return caseDetail[0];
        }

        public static SearchCaseStatusMvcModel GetSearchCaseStatus(string caseTypeNo)
        {
            List<CaseTypeNoticeModel> notices = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseTypeNoticeModel>>(GetDataModelAsync("cismaster/getNoticeNo/" + caseTypeNo).Result);
            SearchCaseStatusMvcModel searchCaseStatus = new SearchCaseStatusMvcModel();
            searchCaseStatus.Notices = notices;
       
            foreach (var notice in notices)
                notice.CaseDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseDetailMvcModel>>(GetDataModelAsync("cismaster/getCaseDetails/" + notice.cnr).Result);
            return searchCaseStatus;
        }

        internal static List<CaseStatusSubject> GetCaseSubjects()
        {
            List<UPCourt.Web.Models.CaseStatusSubject> caseTypes = new List<Models.CaseStatusSubject>();
            var content = GetDataModelAsync("cismaster/getSubject/");
            caseTypes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UPCourt.Web.Models.CaseStatusSubject>>(content.Result);
            return caseTypes;
        }

        internal static List<CaseStatusBenchType> GetBenchType()
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseStatusBenchType>>(GetDataModelAsync("cismaster/getBenchTypes/").Result);
        }
        internal static CaseStatusDetails GetCaseStatusDetails(string cino)
        {
            List<CaseStatusDetails> caseStatuses = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseStatusDetails>>(GetDataModelAsync("cismaster/getCaseStatus/" + cino).Result);

            if (caseStatuses.Count > 0)
                return caseStatuses[0];
            else
                return null;
        }
        private static Task<string> GetDataModelAsync(string url)
        {
            HttpClient client = new HttpClient() { BaseAddress = new Uri("https://efiling-alld.allahabadhighcourt.in") };
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "YWhjOmFoY0A1NDMyMQ==");
            var result = client.GetAsync(url);
            result.Wait();
            var content = result.Result.Content.ReadAsStringAsync();
            return content;
        }

        public static Guid GetCaseStatus(CaseStatusType cst)
        {
            DatabaseContext db = new DatabaseContext();
            return CaseStatusDataService.GetLite(db).Where(x => x.Type == cst).FirstOrDefault().CaseStatusId;
        }
    }

    public class DocumentDataService
    {
        public static IQueryable<Document> GetLite(DatabaseContext db)
        {
            return db.Documents;
        }

        public static Document GetDetail(Guid? docid)
        {
            DatabaseContext db = new DatabaseContext();
            return GetLite(db).Where(x => x.DocumentId == docid).FirstOrDefault();
        }
    }

}