﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class ApplicationDocumentMvcModel : BaseEntityModel<ApplicationDocument, ApplicationDocumentMvcModel>
    {
        public Guid ApplicationDocumentID { get; set; }

        [Display(Name = "Application Type")]
        public Guid ApplicationTypeID { get; set; }
        public ApplicationType ApplicationType { get; set; }

        [Display(Name = "Applicant Name")]
        public string ApplicantName { get; set; }

        [Display(Name = "Registered Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime InsertedOn { get; set; }

        public string InsertedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        public bool IsActive { get; set; }

        public ApplicationStatus FinalStatus { get; set; }

        public List<ApplicationDocumentMvcModel> ApplicationDocuments { get; set; }

        public List<ApplicationTypeMvcModel> ApplicationTypes { get; set; }

        public List<ECourtFeeMvcModel> ECourtFees { get; set; }

        public List<NoticeBasicIndexMvcModel> NoticeBasicIndexs { get; set; }

        public override ApplicationDocumentMvcModel EntityToResponse(ApplicationDocument entity, ApplicationDocumentMvcModel model)
        {
         
            model.ECourtFees = ECourtFeeMvcModel.CopyFromEntityList(entity.ECourtFees, model.RecursiveLevel);
            model.NoticeBasicIndexs = NoticeBasicIndexMvcModel.CopyFromEntityList(entity.BasicIndexs, model.RecursiveLevel);
            return model;
        }


    }

    public class ApplicationTypeMvcModel : BaseEntityModel<ApplicationType, ApplicationTypeMvcModel>
    {
        public Guid ApplicationTypeID { get; set; }
        public string ATName { get; set; }
        public bool IsActive { get; set; }

        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }
    }

    public class ApplicationDocumentIndexMvcModel
    {
        public Guid ApplicationDocumentID { get; set; }

        [Display(Name = "Registered Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? InsertedOn { get; set; }


        [Display(Name = "Application Type")]
        public Guid ApplicationTypeID { get; set; }

        [Display(Name = "Applicant Name")]
        public string ApplicantName { get; set; }

        [Display(Name = "Stage")]
        public string FinalStatus { get; set; }
        public List<ApplicationDocumentMvcModel> ApplicationDocuments { get; set; }
    }


}