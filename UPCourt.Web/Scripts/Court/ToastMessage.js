﻿var Toast = Swal.mixin({ toast: true, position: 'top-end', showConfirmButton: false, timer: 5000 });
function ToastSuccess(Message) { Toast.fire({ icon: 'success', title: Message }) }
function ToastInformation(Message) { Toast.fire({ icon: 'info', title: Message }) }
function ToastWarning(Message) { Toast.fire({ icon: 'warning', title: Message }) }
function ToastError(Message) { Toast.fire({ icon: 'error', title: Message }) }

function DisplayMessage(objMsg) {
    debugger;
    setTimeout(function () {
        $(objMsg).each(function () {
            $(document).Toasts('create', {
                class: this.Type,
                title: this.Title,
                subtitle: this.SubTitle,
                autohide: true,
                delay: 5000,
                position: 'topRight',
                body: this.Message
            });
        });
    }, 1000);
}