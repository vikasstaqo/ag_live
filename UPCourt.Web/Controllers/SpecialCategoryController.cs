﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class SpecialCategoryController : BaseMVCController
    {
        // GET: Special Category
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(SpecialCategoryMvcModel.CopyFromEntityList(SpecialcategoryDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new SpecialCategoryMvcModel();
            model.Status = SpecialCategoryStatus.Active;
            return View(model);
        }
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(SpecialCategoryMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            SpecialCategory tempData = new SpecialCategory()
            {
                SpecialCategoryId = Guid.NewGuid(),
                Name = model.Name,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetSpecialCategorysDisplayOrder(db)

            };
            db.SpecialCategorys.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = SpecialcategoryDataService.GetDetail(db).Where(i => i.SpecialCategoryId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(SpecialCategoryMvcModel.CopyFromEntity(dbData, 0));
        }
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(SpecialCategoryMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = SpecialcategoryDataService.GetDetail(db).Where(i => i.SpecialCategoryId == model.SpecialCategoryId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Status = model.Status;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        private bool CommonForInsertUpdate(SpecialCategoryMvcModel model)
        {
            var duplicateCheck = SpecialcategoryDataService.GetLite(db).FirstOrDefault(i => i.SpecialCategoryId != model.SpecialCategoryId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }
            return true;
        }
        [PageRoleAuthorizarion(PageName.SpecialCategory, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = SpecialcategoryDataService.GetLite(db).Where(i => i.SpecialCategoryId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
