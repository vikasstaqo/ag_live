﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class HomeController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }

        public HomeController() : this(new CustomUserManager())
        {
        }

        public HomeController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public void InitilizeCaseStatus(object selectedValue)
        {

            var dbData = CaseStatusDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }

    }
}