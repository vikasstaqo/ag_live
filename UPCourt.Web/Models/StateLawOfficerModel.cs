﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
        public class StateLawOfficerModel : BaseEntityModel<StateLawOfficer, StateLawOfficerModel>
    {
        
        public Guid AdvId { get; set; }
        public string AdvName { get; set; }
        
        public string AdvLocation { get; set; }
        public string AdvPost { get; set; }
        public string AdvEnrl { get; set; }
        public string AdvAor { get; set; }
    }
    public class StateLawOfficerIndexMvcModel
    {
        public Guid AdvId { get; set; }
        public string AdvName { get; set; }

        public string AdvLocation { get; set; }
        public string AdvPost { get; set; }
        public string AdvEnrl { get; set; }
        public string AdvAor { get; set; }
        public List<StateLawOfficerModel>StateLawOfficersDocs{ get; set; }
        public List<StateLawOfficerModel> StateLawOfficersDocsGA { get; set; }

        public List<StateLawOfficerModel> StateLawOfficersDocsCSE { get; set; }
        public List<StateLawOfficerModel> StateLawOfficersDocsACSE { get; set; }


    }
}