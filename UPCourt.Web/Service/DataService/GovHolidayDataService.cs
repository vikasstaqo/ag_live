﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovHolidayDataService
    {
        public static IQueryable<GovHoliday> GetLite(DatabaseContext db)
        {
            return db.GovHoliday.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<GovHoliday> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}