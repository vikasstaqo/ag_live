﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;
using UPCourt.Web.Service.BusinessService;
using Newtonsoft.Json;
using System.IO.Compression;
using System.Collections.Specialized;

namespace UPCourt.Web.Controllers
{
    public class CaseController : BaseMVCCaseController
    {
        [PageRoleAuthorizarion(PageName.Case, PagePermissionType.CanView)]
        // GET: Case
        public ActionResult Index(CaseFilterMvcModel caseFilterMvcModel, string CurrentTab, string AdvStatus, bool? TBCase, bool? DocRequested, bool? DocRecived)
        {
            InitilizeCouncil(null, null);
            InitilizeCourt(null);
            InitilizeCourtBench(null);
            InitilizeContextOrder(caseFilterMvcModel.IsContextOrder);
            InitilizeDepartment(caseFilterMvcModel.GovDepartmentIdActive, null);
            if (CurrentTab == null)
                CurrentTab = "AcctiveCases";
            ViewBag.CurrentTab = CurrentTab;
            ViewBag.UserSystemRoleId = AppUser.User.UserRole.UserSystemRoleId;

            List<Case> ActiveCases = GetCasesSearch(caseFilterMvcModel, CurrentTab, AdvStatus, false, TBCase, DocRequested, DocRecived);

            if (CurrentTab == "AcctiveCases")
            {
                if (caseFilterMvcModel.CaseStatusActive != null)
                    InitilizeCaseStatus(caseFilterMvcModel.CaseStatusActive);
                else
                    InitilizeCaseStatus(null);

                if (caseFilterMvcModel.ParentNoticeTypeActive != null)
                    InitilizeCaseType(caseFilterMvcModel.ParentNoticeTypeActive, AppUser.User.UserRole.UserSystemRoleId);
                else
                    InitilizeCaseType(null, AppUser.User.UserRole.UserSystemRoleId);

                if (caseFilterMvcModel.CaseHearingStatusActive != null)
                    InitilizeCaseHearingStatus(caseFilterMvcModel.CaseHearingStatusActive);
                else
                    InitilizeCaseHearingStatus(null);

                InitilizeContextOrder(caseFilterMvcModel.IsContextOrderActive);

            }
            else
            {
                InitilizeCaseStatus(null);
                InitilizeCaseType(null, AppUser.User.UserRole.UserSystemRoleId);
                InitilizeCaseHearingStatus(null);
                InitilizeContextOrder(null);
            }

            caseFilterMvcModel.AcctiveCases = CaseMvcModel.CopyFromEntityList(ActiveCases, 0);

            return View(caseFilterMvcModel);
        }

        private List<Case> GetCasesSearch(CaseFilterMvcModel caseFilterMvcModel, string CurrentTab, string AdvStatus, bool isFinalOrder, bool? TBCase, bool? DocRequested, bool? DocRecived)
        {
            List<Case> ActiveCases = new List<Case>();
            ActiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderByDescending(i => i.CreatedOn).ToList();
            if (isFinalOrder)
                ActiveCases = ActiveCases.SelectMany(x => x.CaseHearings).Where(c => c.HearingType == CaseHearingType.FinalOrder).Select(ch => ch.Case).Distinct().ToList();

            if (AdvStatus != null && AdvStatus != "")
            {
                if (AdvStatus.ToLower() == "assigned")
                    ActiveCases = ActiveCases.Where(x => x.AdvocateId != null && x.AdvocateId != Guid.Empty).ToList();
                if (AdvStatus.ToLower() == "unassigned")
                    ActiveCases = ActiveCases.Where(x => x.AdvocateId == null || x.AdvocateId == Guid.Empty).ToList();
            }
            if (TBCase != null && (bool)TBCase)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.IsTimeBoundCompliance == TimeBoundCompliance.Yes).Count() > 0).ToList();

            if (DocRequested != null && (bool)DocRequested)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingRespondents.Count() > 0).Count() > 0).ToList();

            if (DocRecived != null && (bool)DocRecived)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingResponses.Count() > 0).Count() > 0).ToList();


            if (AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.Admin || AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                if (caseFilterMvcModel.IsContextOrderActive == 2)
                    ActiveCases = ActiveCases.Where(x => x.Notice.IsContextOrder == false).ToList();
                if (caseFilterMvcModel.IsContextOrderActive == 1)
                    ActiveCases = ActiveCases.Where(x => x.Notice.IsContextOrder == true).ToList();
            }

            if (caseFilterMvcModel.CaseStatusActive != null && caseFilterMvcModel.CaseStatusActive.ToString() != "")
                ActiveCases = ActiveCases.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusActive).ToList();
            if (caseFilterMvcModel.ParentNoticeTypeActive != null && caseFilterMvcModel.ParentNoticeTypeActive != "All" && caseFilterMvcModel.ParentNoticeTypeActive != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.ParentNoticeTypeActive).ToList();
            if (caseFilterMvcModel.CaseHearingStatusActive != null && caseFilterMvcModel.CaseHearingStatusActive.ToString() != "")
                ActiveCases = ActiveCases.Where(x => x.HearingStatus == Convert.ToInt32(caseFilterMvcModel.CaseHearingStatusActive)).ToList();
            if (caseFilterMvcModel.GovDepartmentIdActive != null && caseFilterMvcModel.GovDepartmentIdActive.ToString() != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.Respondents.Where(y => y.GovernmentDepartmentId == caseFilterMvcModel.GovDepartmentIdActive).Count() > 0).ToList();
            if (caseFilterMvcModel.CaseDateFrom.HasValue)
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeDate >= caseFilterMvcModel.CaseDateFrom).ToList();
            if (caseFilterMvcModel.CaseDateTo.HasValue)
                ActiveCases = ActiveCases.Where(x => x.Notice.NoticeDate <= caseFilterMvcModel.CaseDateTo).ToList();
            if (caseFilterMvcModel.CaseNoActive != null && caseFilterMvcModel.CaseNoActive != "")
                ActiveCases = ActiveCases.Where(x => x.PetitionNo.Trim().ToLower().Contains(caseFilterMvcModel.CaseNoActive.Trim().ToLower())).ToList();
            if (caseFilterMvcModel.CasePetitionerNameActive != null && caseFilterMvcModel.CasePetitionerNameActive != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.Petitioners.Where(y => y.Name.Trim().ToLower().Contains(caseFilterMvcModel.CasePetitionerNameActive.Trim().ToLower())).Count() > 0).ToList();

            return ActiveCases;
        }

        [HttpPost]
        public ActionResult AssignDuty(Guid CaseGuid, Guid Advocate, Guid DocsPrepration)
        {
            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == CaseGuid).FirstOrDefault();
            var OldAdvocatename = UserDataService.GetLite(db).Where(x => x.UserId == dbcase.AdvocateId).FirstOrDefault()?.Name;
            var OldDocsPreprationename = UserDataService.GetLite(db).Where(x => x.UserId == dbcase.DocsPreprationId).FirstOrDefault()?.Name;
            var oldAdvocateId = dbcase.AdvocateId;

            dbcase.AdvocateId = Advocate;
            dbcase.DocsPreprationId = DocsPrepration;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string newAdvocatename = UserDataService.GetLite(db).Where(x => x.UserId == Advocate).FirstOrDefault().Name;
            string newDocsPreprationname = UserDataService.GetLite(db).Where(x => x.UserId == DocsPrepration).FirstOrDefault().Name;

            if (OldAdvocatename != newAdvocatename)
            {
                if (OldAdvocatename != null && OldAdvocatename != "")
                {
                    string description = $" Advocate is changed from '{ OldAdvocatename }' to '{ newAdvocatename }' successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.AdvocateChange, AppUser.UserId, description, null);
                }
                else
                {
                    string description = $"New Advocate '{ newAdvocatename }' is assigned successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.AdvocateAssignment, AppUser.UserId, description, null);
                }
            }
            if (OldDocsPreprationename != newDocsPreprationname)
            {
                if (OldDocsPreprationename != null && OldDocsPreprationename != "")
                {
                    string description = $" Document Prepration is changed from '{ OldDocsPreprationename }' to '{ newDocsPreprationname }' successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.DocsPreprationChange, AppUser.UserId, description, null);
                }
                else
                {
                    string description = $"New Document Prepration '{ newDocsPreprationname }' is assigned successfully";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.DocsPreprationAssignment, AppUser.UserId, description, null);
                }
            }
            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateAdvocateAssignmentNotification(dbcase, oldAdvocateId);

            SucessMessage = $" Advocate assigned to case :- { dbcase.PetitionNo } successfully";

            return RedirectToAction("Index", new { CurrentTab = "AcctiveCases" });
        }

        [HttpPost]
        public ActionResult ReOpenCase(Guid ReCaseGuid)
        {
            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == ReCaseGuid).FirstOrDefault();
            var CaseOldStatus = CaseStatusDataService.GetLite(db).Where(x => x.CaseStatusId == dbcase.CaseStatusId).FirstOrDefault().Name;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Re-Opened").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string description = $" Case status changed from '{ CaseOldStatus }' to 'Re-opened' successfully";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Reopen, AppUser.UserId, description, null);

            SucessMessage = $"Case :- {dbcase.PetitionNo} Re-Opened successfully";

            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        public ActionResult GetCourtsByBench(Guid BenchId)
        {
            var dbData = CourtDataService.GetLite(db).Where(c => c.BenchTypeID == BenchId).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            SelectList courts = new SelectList(AllCourt, "CourtId", "Name", null);
            return Json(new { data = courts.ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdvocates(Guid? caseId, Guid? regionId)
        {
            var dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == caseId).FirstOrDefault();
            var dbData = UserDataService.GetLite(db).Where(x => x.UserRole.Name.Contains("advocate") && x.Name.ToLower().Contains(dbcase.Notice.NoticeType.ToString().ToLower()) && x.RegionId == regionId)?.ToList();
            List<UserMvcModel> model = UserMvcModel.CopyFromEntityList(dbData, 0);
            return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReAppealCase(UpdateDisposedCaseMvcModal model)
        {
            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == model.CaseGuid).FirstOrDefault();
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Re-Appeal").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            addNewCase(dbcase, model);
            SucessMessage = $"Case :- {dbcase.PetitionNo} Re-Appeal done successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        public ActionResult MoveToSupremeCourt(UpdateDisposedCaseMvcModal model)
        {
            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == model.CaseGuid).FirstOrDefault();
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Moved to Supreme Court").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            addNewCase(dbcase, model);
            SucessMessage = $"Case :- {dbcase.PetitionNo} Moved to Supreme Court successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        private void addNewCase(Case dbcase, UpdateDisposedCaseMvcModal model)
        {
            Case newCase = getNewCase(dbcase, model);
            db.Cases.Add(newCase);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
        }

        private Case getNewCase(Case model, UpdateDisposedCaseMvcModal uModel)
        {
            var TotalCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).ToList().Count;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Fresh Case").FirstOrDefault();
            Case NewCase = new Case()
            {
                CaseId = Guid.NewGuid(),
                NoticeId = model.NoticeId,
                OrdinalNo = TotalCases + 1,
                Type = model.Type,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                CaseStatusId = dbstatus.CaseStatusId,
                AdvocateId = model.AdvocateId,
                Status2 = model.Status2,
                PetitionNo = uModel.CaseNo,
                ParentCaseId = uModel.CaseGuid,
                CourtId = uModel.CourtId
            };

            return NewCase;
        }

        [HttpPost]
        public ActionResult DisposeCase(Guid DisposeCaseGuid, CaseDisposeType disposeType)
        {

            Case dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == DisposeCaseGuid).FirstOrDefault();
            var CaseOldStatus = CaseStatusDataService.GetLite(db).Where(x => x.CaseStatusId == dbcase.CaseStatusId).FirstOrDefault().Name;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Disposed").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;

            dbcase.DisposeType = disposeType;

            db.SaveChangesWithAudit(Request, AppUser.UserName);



            string description = $" Case status changed from '{ CaseOldStatus }' to 'Disposed' successfully";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Disposed, AppUser.UserId, description, null);

            SucessMessage = $"Case :- {dbcase.PetitionNo} disposed successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });

        }

        [HttpPost]
        public ActionResult MoveToOtherCourt(string CaseTransferIds, Guid NewCourt, string Description)
        {
            string[] caseids = CaseTransferIds.Split(',');
            foreach (string cas in caseids)
            {
                if (cas != "" && cas != null)
                {
                    var dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == Guid.Parse(cas)).FirstOrDefault();
                    var CaseOldCourt = CourtDataService.GetLite(db).Where(x => x.CourtId == dbcase.CourtId).FirstOrDefault()?.Name;
                    CaseTransfer caseTransfer = new CaseTransfer()
                    {
                        CaseTransferId = Guid.NewGuid(),
                        CaseId = Guid.Parse(cas),
                        CourtId = NewCourt,
                        OldCourtId = dbcase.CourtId,
                        TransferDate = DateTime.Now,
                        Description = Description,
                        CreatedBy = AppUser.UserId
                    };

                    dbcase.CaseTransfers.Add(caseTransfer);
                    dbcase.CourtId = NewCourt;
                    db.SaveChangesWithAudit(Request, AppUser.UserName);

                    var CaseNewCourt = CourtDataService.GetLite(db).Where(x => x.CourtId == NewCourt).FirstOrDefault().Name;
                    if (CaseOldCourt != null && CaseOldCourt != "")
                    {
                        string description = $" Court is changed from '{ CaseOldCourt }' to '{ CaseNewCourt }' successfully";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.CourtChange, AppUser.UserId, description, null);
                    }
                    else
                    {
                        string description = $"New Court '{ CaseNewCourt }' is assigned successfully";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.CourtAssignment, AppUser.UserId, description, null);
                    }
                }
            }

            SucessMessage = $"Case transfered successfully";
            return RedirectToAction("Index", new { CurrentTab = "CaseTransfer" });
        }
        public void InitilizeCouncil(object selectedValue, ParentNoticeType? CaseType)
        {
            var dbData = AdvocateDataService.GetLite(db, AppUser).ToList();

            if (CaseType != null)
            {
                if (CaseType == ParentNoticeType.Civil)
                    dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("civil")).ToList();
                if (CaseType == ParentNoticeType.Criminal)
                    dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("criminal")).ToList();
            }
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Advocate = new SelectList(mvc, nameof(UserMvcModel.UserId), nameof(UserMvcModel.Name), selectedValue);
        }

        public void InitilizeRegion(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var allRegions = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(allRegions, "RegionId", "Name", selectedValue);
        }

        [HttpGet]
        public ActionResult GetAdvocate(Guid? AdvocateId, Guid? DocsPreprationId, ParentNoticeType CaseType)
        {
            var dbData = AdvocateDataService.GetLite(db, AppUser).ToList();
            if (CaseType == ParentNoticeType.Civil)
                dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("civil")).ToList();
            if (CaseType == ParentNoticeType.Criminal)
                dbData = dbData.Where(x => x.UserRole.Name.ToLower().Contains("criminal")).ToList();

            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0).Select(x => new UserMvcModel() { UserId = x.UserId, Name = x.Name });

            return Json(JsonConvert.SerializeObject(mvc), JsonRequestBehavior.AllowGet);
        }
        public void InitilizeCourt(object selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AllCourt = new SelectList(AllCourt, "CourtId", "Name", selectedValue);


            dbData = dbData.Where(x => x.IsClose == false && x.Status == CourtStatus.Active).ToList();
            var OpenedCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.OpenedCourt = new SelectList(OpenedCourt, "CourtId", "Name", selectedValue);
        }
        public void InitilizeCourtBench(object selectedValue)
        {
            var dbData = BenchTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var benchs = BenchTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Benchs = new SelectList(benchs, "BenchTypeID", "Name", selectedValue);
        }
        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }

        public void InitilizeCaseHearingStatus(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Admission", Value = "1" });
            items.Add(new SelectListItem { Text = "Motion Hearing", Value = "2" });
            items.Add(new SelectListItem { Text = "Final Order", Value = "3" });
            ViewBag.CaseHearingStatus = new SelectList(items, "Value", "Text", selectedValue);
        }
        public void InitilizeCaseType(object selectedValue, UserSystemRole userSystemRole)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (!(userSystemRole == UserSystemRole.CSC || userSystemRole == UserSystemRole.AdvCivil))
                items.Add(new SelectListItem { Text = "Criminal", Value = "Criminal" });

            if (!(userSystemRole == UserSystemRole.CGA || userSystemRole == UserSystemRole.AdvCriminal))
                items.Add(new SelectListItem { Text = "Civil", Value = "Civil" });

            if (userSystemRole == UserSystemRole.Admin || userSystemRole == UserSystemRole.AGAdmin)
                items.Add(new SelectListItem { Text = "All", Value = "All" });

            ViewBag.CaseType = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitilizeContextOrder(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Yes", Value = "1" });
            items.Add(new SelectListItem { Text = "No", Value = "2" });
            ViewBag.ContextOrder = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitializeAdvocates(Guid CaseId, Guid? regionId, IEnumerable<Guid> selectedValues = null)
        {
            var dbData = UserDataService.GetDetail(db).Where(x => (x.UserRole.UserSystemRoleId == UserSystemRole.AdvCivil || x.UserRole.UserSystemRoleId == UserSystemRole.AdvCriminal) && (x.RegionId == regionId || regionId == null || regionId == Guid.Empty))?.ToList();
            List<UserMvcModel> model = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovAdvocates = new MultiSelectList(model, "UserId", "Name", selectedValues);
        }
        public void InitilizeJudge(object selectedValue)
        {
            var dbData = JudgeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var Judges = JudgeResponseModel.CopyFromEntityList(dbData, 0);

            ViewBag.Judges = new SelectList(Judges, "JudgeId", "Name", selectedValue);
        }

        public ActionResult CaseHearing(Guid id)
        {
            var cs = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, id).FirstOrDefault();
            InitializeAdvocates(id, AppUser.RegionId);
            InitilizeJudge(null);
            InitializeCaseGovDepartments(id, null, cs);
            InitializeExceptCaseGovDepartments(id, null, cs);
            InitializeExceptCaseGovDepartmentDesignations(id, null, cs);
            InitializeRemoveCaseGovDepartments(id, null, cs);
            InitilizeDocument(id);
            var model = new CaseHearingMvcModel()
            {
                CaseId = id,
                CaseHearingRespondents = new List<CaseHearingRespondentMvcModel> { new CaseHearingRespondentMvcModel() },
                AddRespondents = new List<NoticeRespondentMvcModel> { new NoticeRespondentMvcModel() }
            };
            model.Case = GetCaseMvcModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult CaseHearing(CaseHearingMvcModel _caseHearingMvcModel)
        {
            var cs = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, _caseHearingMvcModel.CaseId).FirstOrDefault();
            InitializeAdvocates(_caseHearingMvcModel.CaseId, AppUser.RegionId);
            InitilizeJudge(null);
            InitializeCaseGovDepartments(_caseHearingMvcModel.CaseId, null, cs);
            InitializeExceptCaseGovDepartments(_caseHearingMvcModel.CaseId, null, cs);
            InitializeExceptCaseGovDepartmentDesignations(_caseHearingMvcModel.CaseId, null, cs);
            InitializeRemoveCaseGovDepartments(_caseHearingMvcModel.CaseId, null, cs);

            InitilizeDocument(_caseHearingMvcModel.CaseId);

            var dbcase = CaseDataService.GetLite(db).Where(x => x.CaseId == _caseHearingMvcModel.CaseId).FirstOrDefault();

            Guid CaseHearingId = Guid.NewGuid();
            CaseHearing ChObj = new CaseHearing();
            ChObj.CaseHearingId = CaseHearingId;
            ChObj.CaseId = _caseHearingMvcModel.CaseId;
            ChObj.HearingType = _caseHearingMvcModel.HearingType;
            ChObj.HearingDate = _caseHearingMvcModel.HearingDate;
            ChObj.JudgeId = _caseHearingMvcModel.JudgeId;
            ChObj.CauseListSrNo = _caseHearingMvcModel.CauseListSrNo;
            ChObj.CauseListReason = _caseHearingMvcModel.CauseListReason;
            ChObj.CourtInstructions = _caseHearingMvcModel.CourtInstructions;
            ChObj.IsCertifiedCopyReceived = _caseHearingMvcModel.IsCertifiedCopyReceived;
            ChObj.HearingStatus = _caseHearingMvcModel.HearingStatus;
            ChObj.NextHearingDate = _caseHearingMvcModel.NextHearingDate;
            ChObj.IsTimeBoundCompliance = _caseHearingMvcModel.IsTimeBoundCompliance;
            ChObj.TimeBoundComplianceDate = _caseHearingMvcModel.TimeBoundComplianceDate;

            ChObj.CreatedBy = AppUser.UserId;
            ChObj.CreatedOn = DateTime.Now;

            dbcase.HearingStatus = Convert.ToInt32(_caseHearingMvcModel.HearingType);
            db.CaseHearings.Add(ChObj);

            foreach (var ga in _caseHearingMvcModel.AdvocateIds)
            {
                CaseHearingGovAdvocate CHGA = new CaseHearingGovAdvocate();
                CHGA.CaseHearingGovAdvocateId = Guid.NewGuid();
                CHGA.CaseHearingId = CaseHearingId;
                CHGA.AdvocateId = ga;
                CHGA.CreatedBy = AppUser.UserId;
                CHGA.CreatedOn = DateTime.Now;
                db.CaseHearingGovAdvocates.Add(CHGA);
            }

            int OrderNo = 1;
            if (_caseHearingMvcModel.SupportingDocuments.MultiUploadFiles != null && _caseHearingMvcModel.SupportingDocuments.MultiUploadFiles.Count() > 0)
            {
                foreach (var Doc in _caseHearingMvcModel.SupportingDocuments.MultiUploadFiles)
                {

                    if (Doc.ContentLength > 0)
                    {
                        string DocumentUrl = SaveDocument(Doc);
                        if (DocumentUrl != null && DocumentUrl != "")
                        {
                            Document DocNewObj = new Document();
                            DocNewObj.DocumentId = Guid.NewGuid();
                            DocNewObj.CaseHearingId = CaseHearingId;
                            DocNewObj.Name = "Supporting Doc " + OrderNo.ToString();
                            DocNewObj.DocumentUrl = DocumentUrl;
                            DocNewObj.FileType = DocumentFileType.Any;
                            DocNewObj.EntityType = DocumentEntityType.CaseHearing;
                            DocNewObj.Type = DocumentType.CHSupportingDoc;
                            DocNewObj.Ordinal = OrderNo;
                            DocNewObj.Size = Doc.ContentLength;
                            DocNewObj.Status = DocumentStatus.Added;
                            DocNewObj.CreatedBy = AppUser.UserId;
                            DocNewObj.CreatedOn = DateTime.Now;
                            OrderNo++;
                            db.Documents.Add(DocNewObj);
                        }
                    }
                }
            }
            OrderNo = 1;
            if (_caseHearingMvcModel.CertifiedCopy.UploadFile != null && _caseHearingMvcModel.CertifiedCopy.UploadFile.ContentLength > 0)
            {
                string DocumentUrl = SaveDocument(_caseHearingMvcModel.CertifiedCopy.UploadFile);
                if (DocumentUrl != null && DocumentUrl != "")
                {
                    Document DocCertifiedCopy = new Document();
                    DocCertifiedCopy.DocumentId = Guid.NewGuid();
                    DocCertifiedCopy.CaseHearingId = CaseHearingId;
                    DocCertifiedCopy.Name = "Certified Copy " + OrderNo.ToString();
                    DocCertifiedCopy.DocumentUrl = DocumentUrl;
                    DocCertifiedCopy.FileType = DocumentFileType.Any;
                    DocCertifiedCopy.EntityType = DocumentEntityType.CaseHearing;
                    DocCertifiedCopy.Type = DocumentType.CHCertifiedCopy;
                    DocCertifiedCopy.Ordinal = OrderNo;
                    DocCertifiedCopy.Size = _caseHearingMvcModel.CertifiedCopy.UploadFile.ContentLength;
                    DocCertifiedCopy.Status = DocumentStatus.Added;
                    DocCertifiedCopy.CreatedBy = AppUser.UserId;
                    DocCertifiedCopy.CreatedOn = DateTime.Now;
                    OrderNo++;
                    db.Documents.Add(DocCertifiedCopy);
                }
            }

            string description = $" Case Hearing saved for Case '{ dbcase.PetitionNo }' ";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.HearingEntry, AppUser.UserId, description, null);

            foreach (var CHR in _caseHearingMvcModel.CaseHearingRespondents)
            {
                if (CHR.bActive == true && CHR.GovernmentDepartmentId != null && CHR.GovernmentDepartmentId != Guid.Empty)
                {
                    CaseHearingRespondent CHRNewObj = new CaseHearingRespondent();
                    CHRNewObj.CaseHearingRespondentId = Guid.NewGuid();
                    CHRNewObj.CaseHearingId = CaseHearingId;
                    CHRNewObj.DocumentType = CHR.DocumentType;
                    CHRNewObj.LastComplianceDate = CHR.LastComplianceDate;
                    CHRNewObj.GovernmentDepartmentId = CHR.GovernmentDepartmentId;
                    CHRNewObj.Priority = CHR.Priority;
                    CHRNewObj.Comments = CHR.Comments;
                    CHRNewObj.CreatedBy = AppUser.UserId;
                    CHRNewObj.CreatedOn = DateTime.Now;
                    OrderNo++;
                    db.CaseHearingRespondents.Add(CHRNewObj);

                                      string depAdddescription = $" Doc '{DocumentMasterDataService.GetName(CHR.DocumentType) }' is requested from Department '{ GovermentDepartmentDataService.GetName(CHR.GovernmentDepartmentId) }' ";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.DocumentRequest, AppUser.UserId, depAdddescription, null);
                                        try
                    {
                        var modelNotify = new AppNotifications
                        {
                            Id = Guid.NewGuid(),
                            Source = (int)NotificationSourceEntityType.Case,
                            TargetUserId = UserDataService.GetLite(db).Where(x => x.GovernmentDepartmentId == CHR.GovernmentDepartmentId).FirstOrDefault().UserId,
                            NotificationType = 1,
                            Status = 1,
                            Message = "Request for '" + DocumentMasterDataService.GetName(CHR.DocumentType) + "' document of case: '" + dbcase.PetitionNo + "' ",
                            CreatedBy = AppUser.UserId,
                            CreatedOn = Extended.CurrentIndianTime,
                            RouteValue = "/Case/CaseView/" + _caseHearingMvcModel.CaseId,
                            HashId = CaseHearingId
                        };
                        var iResult = AppNotifications.Create(modelNotify);
                    }
                    catch
                    {

                    }
                }
            }

            foreach (var AR in _caseHearingMvcModel.AddRespondents)
            {
                if (AR.bActive == true && AR.GovernmentDepartmentId != null && AR.GovernmentDepartmentId != Guid.Empty)
                {
                    Guid NoticeId = dbcase.NoticeId;
                    PartyAndRespondent InsertPartyAndRespondent = new PartyAndRespondent();
                    InsertPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    InsertPartyAndRespondent.NoticeRespondentId = NoticeId;
                    InsertPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    InsertPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    InsertPartyAndRespondent.ModifyBy = AppUser.UserName;
                    InsertPartyAndRespondent.IsFirst = false;
                    InsertPartyAndRespondent.PartyType = PartyType.Department;
                    InsertPartyAndRespondent.GovernmentDepartmentId = AR.GovernmentDepartmentId;
                    InsertPartyAndRespondent.GovernmentDesignationId = AR.GovernmentDesignationId;
                    InsertPartyAndRespondent.SNo = PartyAndRespondentDataService.GetLite(db).Where(x => x.NoticeRespondentId == NoticeId).OrderBy(x => x.SNo).LastOrDefault().SNo + 1;
                    db.PartyAndRespondents.Add(InsertPartyAndRespondent);

                    string depAdddescription = $" New Department '{ GovermentDepartmentDataService.GetName(AR.GovernmentDepartmentId) }' added for Case '{ dbcase.PetitionNo }' ";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepAddedtocase, AppUser.UserId, depAdddescription, null);
                                    }
            }

            if (_caseHearingMvcModel.RemoveRespondents != null && _caseHearingMvcModel.RemoveRespondents.Count > 0)
            {
                foreach (var RR in _caseHearingMvcModel.RemoveRespondents)
                {
                    var resp = NoticeRespondentMvcModel.CopyFromEntity(PartyAndRespondentDataService.GetDetail(db).Where(r => r.GovernmentDepartmentId == RR).FirstOrDefault(), 0);
                    if (resp.GovernmentDepartmentId != null && resp.GovernmentDepartmentId != Guid.Empty)
                    {
                        Guid NoticeId = CaseDataService.GetLite(db).Where(x => x.CaseId == _caseHearingMvcModel.CaseId).FirstOrDefault().NoticeId;
                                                var removedate = PartyAndRespondentDataService.GetLite(db).Where(x => x.NoticeRespondentId == NoticeId && x.GovernmentDepartmentId == resp.GovernmentDepartmentId).ToList();
                        foreach (var rec in removedate)
                        {
                            db.PartyAndRespondents.Remove(rec);
                        }
                        string depAdddescription = $" Department '{ GovermentDepartmentDataService.GetName(resp.GovernmentDepartmentId) }' is removed from Case '{ dbcase.PetitionNo }' ";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepRemovedtocase, AppUser.UserId, depAdddescription, null);
                                            }
                }
            }

            if (ChObj.HearingStatus == CaseHearingStatus.Disposed)
            {
                var CaseStatusId = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Dispose).FirstOrDefault();
                dbcase.CaseStatus = CaseStatusId;
                string depAdddescription = $" Case  '{ dbcase.PetitionNo }' is disposed ";
                TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Disposed, AppUser.UserId, depAdddescription, null);
            }
            else if (ChObj.HearingStatus == CaseHearingStatus.Pending)
            {
                var oldstatus = dbcase.CaseStatus;
                var CaseStatusId = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Pending).FirstOrDefault();
                dbcase.CaseStatus = CaseStatusId;
                if (oldstatus != CaseStatusId)
                {
                    string depAdddescription = $" Case  '{ dbcase.PetitionNo }' is Pending ";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Pending, AppUser.UserId, depAdddescription, null);
                }
            }
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateCaseHearingChangesNotifications(ChObj, false);
            if (TempData.ContainsKey("CaseViewModel"))
                TempData.Remove("CaseViewModel");

            return RedirectToAction("CaseView", new { id = _caseHearingMvcModel.CaseId });
        }

        [HttpGet]
        public ActionResult EditHearing(Guid id)
        {
            var dbData = CaseHearingDataService.GetDetail(db, id);
            dbData.CaseHearingGovAdvocates = dbData.CaseHearingGovAdvocates.Where(x => x.IsDeleted == false).ToList();

            InitializeAdvocates(dbData.CaseId, AppUser.RegionId, dbData.CaseHearingGovAdvocates.Select(x => x.Advocate.UserId));

            InitilizeJudge(dbData.JudgeId);
            InitializeCaseGovDepartments(dbData.CaseId, null, dbData.Case);
            InitializeExceptCaseGovDepartments(dbData.CaseId, null, dbData.Case);
            InitializeExceptCaseGovDepartmentDesignations(dbData.CaseId, null, dbData.Case);
            InitializeRemoveCaseGovDepartments(dbData.CaseId, null, dbData.Case);
            InitilizeDocument(dbData.CaseId);

            var model = CaseHearingMvcModel.CopyFromEntity(dbData, 0);
            if (model.CaseHearingRespondents.Count == 0)
                model.CaseHearingRespondents = new List<CaseHearingRespondentMvcModel> { new CaseHearingRespondentMvcModel() };
            model.ExistingSupportingDocs = model.Documents.Where(d => d.EntityType == DocumentEntityType.CaseHearing && d.Type == DocumentType.CHSupportingDoc).ToList();
            model.AddRespondents = new List<NoticeRespondentMvcModel> { new NoticeRespondentMvcModel() };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditHearing(CaseHearingMvcModel model)
        {

            var dbcase = CaseDataService.GetLite(db).Where(x => x.CaseId == model.CaseId).FirstOrDefault();

            var dbCaseHearing = CaseHearingDataService.GetDetail(db, model.CaseHearingId);

            CaseHearing ChObj = CaseHearingDataService.GetLite(db).Where(x => x.CaseHearingId == model.CaseHearingId).FirstOrDefault();
            if ((int)model.HearingType != 0)
                ChObj.HearingType = model.HearingType;
            ChObj.HearingDate = model.HearingDate;
            ChObj.JudgeId = model.JudgeId;
            ChObj.CauseListSrNo = model.CauseListSrNo;
            ChObj.CauseListReason = model.CauseListReason;
            ChObj.CourtInstructions = model.CourtInstructions;
            ChObj.IsCertifiedCopyReceived = model.IsCertifiedCopyReceived;
            ChObj.HearingStatus = model.HearingStatus;
            ChObj.NextHearingDate = model.NextHearingDate;
            ChObj.IsTimeBoundCompliance = model.IsTimeBoundCompliance;
            ChObj.TimeBoundComplianceDate = model.TimeBoundComplianceDate;

            ChObj.CreatedBy = AppUser.UserId;
            ChObj.CreatedOn = DateTime.Now;

            bool noAdv = false;
            var advs = db.CaseHearingGovAdvocates.Where(x => x.CaseHearingId == model.CaseHearingId && x.IsDeleted == false).ToList();
            foreach (var adv in advs)
            {
                adv.IsDeleted = true;
            }
            foreach (var ga in model.AdvocateIds)
            {
                CaseHearingGovAdvocate CHGA = advs.Where(x => x.AdvocateId == ga).FirstOrDefault();
                if (CHGA != null)
                    CHGA.IsDeleted = false;
                else
                {
                    CHGA = new CaseHearingGovAdvocate();
                    CHGA.CaseHearingGovAdvocateId = Guid.NewGuid();
                    CHGA.CaseHearingId = model.CaseHearingId;
                    CHGA.AdvocateId = ga;
                    CHGA.CreatedBy = AppUser.UserId;
                    CHGA.CreatedOn = DateTime.Now;
                    db.CaseHearingGovAdvocates.Add(CHGA);
                }
            }
            noAdv = false;
            int OrderNo = 1;
            if (model.ExistingSupportingDocs != null)
            {
                OrderNo = model.ExistingSupportingDocs.Count;
                foreach (var dc in model.ExistingSupportingDocs)
                {
                    var doc = DocumentDataService.GetLite(db).Where(x => x.DocumentId == dc.DocumentId).FirstOrDefault();
                    doc.IsDeleted = dc.IsDeleted;
                }
            }
            db.SaveChanges();
            if (model.SupportingDocuments.MultiUploadFiles != null && model.SupportingDocuments.MultiUploadFiles.Count() > 0)
            {
                foreach (var Doc in model.SupportingDocuments.MultiUploadFiles)
                {
                    if (Doc != null && Doc.ContentLength > 0)
                    {
                        string DocumentUrl = SaveDocument(Doc);
                        if (DocumentUrl != null && DocumentUrl != "")
                        {
                            Document DocNewObj = new Document();
                            DocNewObj.DocumentId = Guid.NewGuid();
                            DocNewObj.CaseHearingId = ChObj.CaseHearingId;
                            DocNewObj.Name = "Supporting Doc " + OrderNo.ToString();
                            DocNewObj.DocumentUrl = DocumentUrl;
                            DocNewObj.FileType = DocumentFileType.Any;
                            DocNewObj.EntityType = DocumentEntityType.CaseHearing;
                            DocNewObj.Type = DocumentType.CHSupportingDoc;
                            DocNewObj.Ordinal = OrderNo;
                            DocNewObj.Size = Doc.ContentLength;
                            DocNewObj.Status = DocumentStatus.Added;
                            DocNewObj.CreatedBy = AppUser.UserId;
                            DocNewObj.CreatedOn = DateTime.Now;
                            OrderNo++;
                            db.Documents.Add(DocNewObj);
                        }
                    }
                }
            }
            OrderNo = 1;
            if (model.CertifiedCopy.UploadFile != null && model.CertifiedCopy.UploadFile.ContentLength > 0)
            {
                string DocumentUrl = SaveDocument(model.CertifiedCopy.UploadFile);
                if (DocumentUrl != null && DocumentUrl != "")
                {
                    Document DocCertifiedCopy = new Document();
                    DocCertifiedCopy.DocumentId = Guid.NewGuid();
                    DocCertifiedCopy.CaseHearingId = ChObj.CaseHearingId;
                    DocCertifiedCopy.Name = "Certified Copy " + OrderNo.ToString();
                    DocCertifiedCopy.DocumentUrl = DocumentUrl;
                    DocCertifiedCopy.FileType = DocumentFileType.Any;
                    DocCertifiedCopy.EntityType = DocumentEntityType.CaseHearing;
                    DocCertifiedCopy.Type = DocumentType.CHCertifiedCopy;
                    DocCertifiedCopy.Ordinal = OrderNo;
                    DocCertifiedCopy.Size = model.CertifiedCopy.UploadFile.ContentLength;
                    DocCertifiedCopy.Status = DocumentStatus.Added;
                    DocCertifiedCopy.CreatedBy = AppUser.UserId;
                    DocCertifiedCopy.CreatedOn = DateTime.Now;
                    OrderNo++;
                    db.Documents.Add(DocCertifiedCopy);
                }
            }

            string description = $" Case Hearing updated for Case '{ dbcase.PetitionNo }' ";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.HearingEntry, AppUser.UserId, description, null);

            foreach (var CHR in model.CaseHearingRespondents)
            {
                if (CHR.bActive == true && CHR.GovernmentDepartmentId != null && CHR.GovernmentDepartmentId != Guid.Empty)
                {
                    var earlierRespondent = dbCaseHearing.CaseHearingRespondents.Where(r => r.CaseHearingRespondentId == CHR.CaseHearingRespondentId).FirstOrDefault();
                    string depAdddescription = $" Doc '{DocumentMasterDataService.GetName(CHR.DocumentType) }' is requested from Department '{ GovermentDepartmentDataService.GetName(CHR.GovernmentDepartmentId) }' ";
                    string notifMsg = "Request for '" + DocumentMasterDataService.GetName(CHR.DocumentType) + "' document of case: '" + dbcase.PetitionNo + "' ";
                    if (earlierRespondent != null && CHR.DocumentType != earlierRespondent.DocumentType)
                    {
                        depAdddescription = $" Doc requested is updated from '{DocumentMasterDataService.GetName(earlierRespondent.DocumentType) }' to '{DocumentMasterDataService.GetName(CHR.DocumentType) }' from Department '{ GovermentDepartmentDataService.GetName(CHR.GovernmentDepartmentId) }' ";
                        notifMsg = "Doc Request updated to '" + DocumentMasterDataService.GetName(CHR.DocumentType) + "' from '" + DocumentMasterDataService.GetName(earlierRespondent.DocumentType) + "' case: '" + dbcase.PetitionNo + "' for CaseHearing '" + dbCaseHearing.HearingType.GetEnumDescription() + "' ";
                    }

                    var CHRNewObj = db.CaseHearingRespondents.Where(x => x.CaseHearingRespondentId == CHR.CaseHearingRespondentId).FirstOrDefault();
                    if (CHRNewObj == null || CHRNewObj.CaseHearingRespondentId == null)
                    {
                        noAdv = true;
                        CHRNewObj = new CaseHearingRespondent();
                        CHRNewObj.CaseHearingRespondentId = Guid.NewGuid();
                    }
                    CHRNewObj.CaseHearingId = model.CaseHearingId;
                    CHRNewObj.DocumentType = CHR.DocumentType;
                    CHRNewObj.LastComplianceDate = CHR.LastComplianceDate;
                    CHRNewObj.GovernmentDepartmentId = CHR.GovernmentDepartmentId;
                    CHRNewObj.Priority = CHR.Priority;
                    CHRNewObj.Comments = CHR.Comments;
                    CHRNewObj.CreatedBy = AppUser.UserId;
                    CHRNewObj.CreatedOn = DateTime.Now;
                    OrderNo++;
                    if (noAdv)
                    {
                        noAdv = false;
                        db.CaseHearingRespondents.Add(CHRNewObj);
                    }

                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.DocumentRequest, AppUser.UserId, depAdddescription, null);


                    try
                    {
                        var modelNotify = new AppNotifications
                        {
                            Id = Guid.NewGuid(),
                            Source = (int)NotificationSourceEntityType.Case,
                            TargetUserId = UserDataService.GetLite(db).Where(x => x.GovernmentDepartmentId == CHR.GovernmentDepartmentId).FirstOrDefault().UserId,
                            NotificationType = 1,
                            Status = 1,
                            Message = notifMsg,
                            CreatedBy = AppUser.UserId,
                            CreatedOn = Extended.CurrentIndianTime,
                            RouteValue = "/Case/CaseView/" + model.CaseId,
                            HashId = model.CaseHearingId
                        };
                        var iResult = AppNotifications.Create(modelNotify);
                    }
                    catch
                    {

                    }
                }
            }
            noAdv = false;
            foreach (var AR in model.AddRespondents)
            {
                if (AR.bActive == true && AR.GovernmentDepartmentId != null && AR.GovernmentDepartmentId != Guid.Empty)
                {
                    Guid NoticeId = dbcase.NoticeId;
                    PartyAndRespondent InsertPartyAndRespondent = new PartyAndRespondent();
                    InsertPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    InsertPartyAndRespondent.NoticeRespondentId = NoticeId;
                    InsertPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    InsertPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    InsertPartyAndRespondent.ModifyBy = AppUser.UserName;
                    InsertPartyAndRespondent.IsFirst = false;
                    InsertPartyAndRespondent.PartyType = PartyType.Department;
                    InsertPartyAndRespondent.GovernmentDepartmentId = AR.GovernmentDepartmentId;
                    InsertPartyAndRespondent.GovernmentDesignationId = AR.GovernmentDesignationId;
                    InsertPartyAndRespondent.SNo = PartyAndRespondentDataService.GetLite(db).Where(x => x.NoticeRespondentId == NoticeId).OrderBy(x => x.SNo).LastOrDefault().SNo + 1;
                    db.PartyAndRespondents.Add(InsertPartyAndRespondent);

                    string depAdddescription = $" New Department '{ GovermentDepartmentDataService.GetName(AR.GovernmentDepartmentId) }' added for Case '{ dbcase.PetitionNo }' ";
                    TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepAddedtocase, AppUser.UserId, depAdddescription, null);

                }
            }

            if (model.RemoveRespondents != null && model.RemoveRespondents.Count > 0)
            {
                foreach (var RR in model.RemoveRespondents)
                {
                    var resp = NoticeRespondentMvcModel.CopyFromEntity(PartyAndRespondentDataService.GetDetail(db).Where(r => r.GovernmentDepartmentId == RR).FirstOrDefault(), 0);
                    if (resp.bActive == true && resp.GovernmentDepartmentId != null && resp.GovernmentDepartmentId != Guid.Empty)
                    {
                        Guid NoticeId = CaseDataService.GetLite(db).Where(x => x.CaseId == model.CaseId).FirstOrDefault().NoticeId;

                        var removedate = PartyAndRespondentDataService.GetLite(db).Where(x => x.NoticeRespondentId == NoticeId && x.GovernmentDepartmentId == resp.GovernmentDepartmentId).ToList();
                        foreach (var rec in removedate)
                        {
                            db.PartyAndRespondents.Remove(rec);
                        }
                        string depAdddescription = $" Department '{ GovermentDepartmentDataService.GetName(resp.GovernmentDepartmentId) }' is removed from Case '{ dbcase.PetitionNo }' ";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.GovDepRemovedtocase, AppUser.UserId, depAdddescription, null);

                    }
                }
            }
                       
            db.SaveChangesWithAudit(Request, AppUser.UserName);
                        AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateCaseHearingChangesNotifications(ChObj, true);

            if (TempData.ContainsKey("CaseViewModel"))
                TempData.Remove("CaseViewModel");

            return RedirectToAction("CaseView", new { id = model.CaseId });
        }

        [HttpPost]
        public ActionResult EditHearingDeleteDoc(Guid CaseHearingId, Guid documentId)
        {
            var doc = DocumentDataService.GetLite(db).Where(x => x.DocumentId == documentId).FirstOrDefault();
            doc.IsDeleted = true;
            db.SaveChanges();

            var casehearing = CaseHearingDataService.GetDetail(db, CaseHearingId);

            Dictionary<string, bool> ret = new Dictionary<string, bool>();
            ret.Add("success", true);
            if (casehearing.Documents == null || casehearing.Documents.Count == 0)
                ret.Add("hasMore", false);
            else
                ret.Add("hasMore", true);
            return Json(JsonConvert.SerializeObject(ret), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHearingDetail(Guid CaseHearingId, Guid CaseId, HttpPostedFileBase UploadDocument, string Comments)
        {

            Guid DocumentId = Guid.NewGuid();
            int HasDoc = 0;
            if (UploadDocument != null && UploadDocument.ContentLength > 0)
            {
                string DocumentUrl = SaveDocument(UploadDocument);
                if (DocumentUrl != null && DocumentUrl != "")
                {
                    Document DocNewObj = new Document
                    {
                        DocumentId = DocumentId,
                        CaseHearingId = CaseHearingId,
                        Name = "Response Doc ",
                        DocumentUrl = DocumentUrl,
                        FileType = DocumentFileType.Any,
                        EntityType = DocumentEntityType.CaseHearing,
                        Type = DocumentType.CHResponse,
                        Ordinal = 1,
                        Size = UploadDocument.ContentLength,
                        Status = DocumentStatus.Added,
                        CreatedBy = AppUser.UserId,
                        CreatedOn = DateTime.Now
                    };
                    db.Documents.Add(DocNewObj);
                    HasDoc = 1;
                }
            }

            CaseHearingResponse chR = new CaseHearingResponse
            {
                CaseHearingResponseId = Guid.NewGuid(),
                CaseHearingId = CaseHearingId,
                DocumentId = HasDoc == 1 ? DocumentId : Guid.Empty,
                Comments = Comments,
                AdvocateId = AppUser.UserId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            db.CaseHearingResponses.Add(chR);
            int iresult = db.SaveChangesWithAudit(Request);


            if (iresult > 0)
            {
                string CaseName = CaseDataService.GetLite(db).Where(x => x.CaseId == CaseId).FirstOrDefault().PetitionNo;
                string description = $"Case Hearing Response saved successfully for Case No { CaseName } ";
                TimelineBusinessService.SaveTimeLine(db, null, CaseId, EventType.AdvCHResponse, AppUser.UserId, description, null);
            }
            return RedirectToAction("CaseView/" + CaseId);
        }



        private void InitializeCaseGovDepartments(Guid caseId, object selectedValue, Case cs = null)
        {
            var dbcase = cs;
            if (dbcase == null || dbcase.Notice == null)
                dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, caseId).FirstOrDefault();
            var GovDepts = dbcase.Notice.Respondents.Where(x => x.GovernmentDepartmentId != null).ToList();
            ViewBag.CaseGovDepartments = new SelectList(GovDepts, "GovernmentDepartmentId", "GovernmentDepartment.Name", selectedValue);
        }
        private void InitializeRemoveCaseGovDepartments(Guid caseId, object selectedValue, Case cs = null)
        {
            var dbcase = cs;
            if (dbcase == null || dbcase.Notice == null)
                dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, caseId).FirstOrDefault();
            var GovDepts = dbcase.Notice.Respondents.Where(x => x.IsFirst == false).ToList();
            ViewBag.RemoveCaseGovDepartments = new SelectList(GovDepts, "GovernmentDepartmentId", "GovernmentDepartment.Name", selectedValue);
        }

        private void InitializeExceptCaseGovDepartments(Guid caseId, object selectedValue, Case cs = null)
        {
            var dbcase = cs;
            if (dbcase == null || dbcase.Notice == null)
                dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, caseId).FirstOrDefault();
            var mappedgov = dbcase.Notice.Respondents.ToList();
            var GovDepts = GovermentDepartmentDataService.GetLite(db).ToList()
                    .GroupJoin(
                        inner: mappedgov,
                        outerKeySelector: g => g.GovernmentDepartmentId,
                        innerKeySelector: m => m.GovernmentDepartmentId,
                        resultSelector: (g, mrows) => new { g, mrows = mrows.DefaultIfEmpty() })
                    .SelectMany(a => a.mrows.Select(m => new { t1 = a.g, t2 = m }))
                    .ToList().Where(x => x.t2 == null).Select(x => x.t1).ToList();
            ViewBag.ExceptCaseGovDepartments = new SelectList(GovDepts, "GovernmentDepartmentId", "Name", selectedValue);
        }
        private void InitializeExceptCaseGovDepartmentDesignations(Guid caseId, object selectedValue, Case cs = null)
        {
            var dbcase = cs;
            if (dbcase == null || dbcase.Notice == null)
                dbcase = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, caseId).FirstOrDefault();
            var GovDepts = GovDesignationDataService.GetLite(db).Where(x => x.GovernmentDepartmentId != null).Distinct().ToList();
            ViewBag.ExceptCaseGovDepartmentDesignation = new SelectList(GovDepts, "GovernmentDesignationId", "DesignationName", selectedValue);
        }

        public ActionResult GetDesignationsForDepartment(Guid? govDepartmentId)
        {
            var GovDepts = new List<GovernmentDesignation>();
            if (govDepartmentId != null && govDepartmentId != Guid.Empty)
                GovDepts = GovDesignationDataService.GetLite(db).Where(x => x.GovernmentDepartmentId == govDepartmentId).Distinct().ToList();
            SelectList designations = new SelectList(GovDepts, "GovernmentDesignationId", "DesignationName", null);
            return Json(new { data = designations.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadDocument(Guid? id)
        {
            var dbData = DocumentDataService.GetLite(db).Where(x => x.DocumentId == id).FirstOrDefault();
            var FilePath = Path.Combine(Server.MapPath("~/Document/"), dbData.DocumentUrl);
            if (System.IO.File.Exists(FilePath))
            {
                var mimeType = MimeMapping.MimeUtility.GetMimeMapping(dbData.DocumentUrl);
                return File(FilePath, mimeType);
            }
            return Json(new { msg = "File not found" }, JsonRequestBehavior.AllowGet);
        }

        [PageRoleAuthorizarion(PageName.Case, PagePermissionType.CanView)]
        public ActionResult CaseView(Guid id)
        {
            InitilizeDocument(null);
            InitilizeDepartment(null, id);

            CaseMvcModel CaseMobelobj = GetCaseMvcModel(id);
            InitilizeCaseStatus(CaseMobelobj.CaseStatusId);
            InitilizeCouncil(CaseMobelobj.AdvocateId, CaseMobelobj.Notice.NoticeType);
            InitilizeCourt(CaseMobelobj.CourtId);
            InitilizeRegion(CaseMobelobj.RegionId);

            if (Request["nid"] != null)
            {
                Guid n_id = Guid.Empty;
                if (Request["s"] == "1" && Guid.TryParse(Request["nid"], out n_id))
                {
                    AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(n_id, NotificationStatusEnum.IsRead);
                }
            }
            if (Request.QueryString["nid"] != null && Request.QueryString["nid"].Length > 0 && Request.QueryString["s"] != null && Request.QueryString["s"] == "1")
            {
                AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(Guid.Parse(Request.QueryString["nid"]), NotificationStatusEnum.IsRead);
            }

            return View(CaseMobelobj);
        }

        [HttpGet]
        public ActionResult CaseDocuments(Guid id)
        {
            var CaseMobelobj = GetCaseMvcModel(id);

            return View(CaseMobelobj);
        }

        [HttpGet]
        public ActionResult DownloadAllCaseDocuments(Guid id)
        {
            var CaseMobelobj = GetCaseMvcModel(id);
            int fileCounts = 0;
            //GET ALL DOCUMENTS IN A ZIP FILE AND DOWNLOAD
            using (var memoryStream = new MemoryStream())
            {
                using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    if (CaseMobelobj.Notice.NoticeBasicIndexs != null)
                    {
                        foreach (var BasicIndex in CaseMobelobj.Notice.NoticeBasicIndexs)
                        {
                            if (BasicIndex.Documents != null)
                            {
                                foreach (var document in BasicIndex.Documents)
                                {
                                    fileCounts = AddDocumentToArchive(document, ziparchive, fileCounts, "Notice");
                                }
                            }
                        }
                    }
                    if (CaseMobelobj.CaseHearings != null)
                    {
                        foreach (var caseHearing in CaseMobelobj.CaseHearings)
                        {
                            if (caseHearing.Documents != null)
                            {
                                foreach (var document in caseHearing.Documents)
                                {
                                    fileCounts = AddDocumentToArchive(document, ziparchive, fileCounts);
                                }
                            }
                            if (caseHearing.CaseHearingResponses != null)
                            {
                                foreach (var hearingResponse in caseHearing.CaseHearingResponses.OrderBy(x => x.CreatedOn))
                                {
                                    if (hearingResponse.DocumentId != null)
                                    {
                                        var doc = DocumentDataService.GetDetail(hearingResponse.DocumentId);
                                        if (doc != null)
                                        {
                                            var document = DocumentMvcModel.CopyFromEntity(doc, 0);
                                            fileCounts = AddDocumentToArchive(document, ziparchive, fileCounts);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ziparchive.Dispose();
                    if (fileCounts > 0)
                        return File(memoryStream.ToArray(), "application/zip", "CaseDocuments.zip");
                }
            }
            return null;
        }

        private int AddDocumentToArchive(DocumentMvcModel document, ZipArchive ziparchive, int fileCounts, string innerPath = "")
        {
            string filePath = Server.MapPath("~/Document/" + innerPath), tempPath = "";
            if (document.DocumentUrl != null)
            {
                tempPath = filePath + "\\" + document.DocumentUrl;
                FileInfo info = new FileInfo(tempPath);
                if (info.Exists)
                {
                    fileCounts++;
                    var zEntry = ziparchive.CreateEntry(document.DocumentUrl);
                    FileStream stream = info.OpenRead();
                    stream.Seek(0, SeekOrigin.Begin);
                    byte[] fileBytes = stream.ToByteArray();

                    using (var zEntryStream = zEntry.Open())
                        zEntryStream.Write(fileBytes, 0, fileBytes.Length);
                    stream.Close();
                }
            }
            return fileCounts;
        }
        /* Moved the method to Base Controller BaseMVCCaseController
        private CaseMvcModel GetCaseMvcModel(Guid id)
        {
            CaseMvcModel CaseMobelobj;
            if (TempData.ContainsKey("CaseViewModel"))
            {
                CaseMobelobj = (CaseMvcModel)TempData["CaseViewModel"];
                if (CaseMobelobj != null && CaseMobelobj.CaseId == id)
                {
                    TempData["CaseViewModel"] = CaseMobelobj;
                    TempData.Keep();
                    return CaseMobelobj;
                }
            }

            var dbData = CaseDataService.GetFiltered(db, AppUser.User, id).Where(x => x.CaseId == id).FirstOrDefault();
            CaseMobelobj = CaseMvcModel.CopyFromEntity(dbData, 1);
            if (CaseMobelobj.Notice.CrimeDetail != null)
                if (CaseMobelobj.Notice.CrimeDetail.PoliceStation == null && CaseMobelobj.Notice.CrimeDetail.PoliceStationID != null)
                {
                    CaseMobelobj.Notice.CrimeDetail.PoliceStation = PoliceStationMvcModel.CopyFromEntity(PoliceStationDataService.GetLite(db).Where(x => x.PoliceStationId == CaseMobelobj.Notice.CrimeDetail.PoliceStationID).FirstOrDefault(), 0);
                }
            var dbUsers = UserDataService.GetLite(db).ToList();
            foreach (var caseHearing in CaseMobelobj.CaseHearings)
                caseHearing.CreatedByUser = UserMvcModel.CopyFromEntity(dbUsers.Where(x => x.UserId == (Guid)caseHearing.CreatedBy).FirstOrDefault(), 0);

            CaseMobelobj.Timelines = TimelineMvcModel.CopyFromEntityList(TimelineBusinessService.GetCaseTimeline(db, id), 0);
            DocumentMvcModel obj = new DocumentMvcModel();
            CaseMobelobj.NewCaseHearing = new NewCaseHearingMvcModelList();

            CaseMobelobj.NewCaseHearing.DocumentMvcModel = new List<DocumentMvcModel>();
            CaseMobelobj.NewCaseHearing.DocumentMvcModel.Add(new DocumentMvcModel { Name = "" });

            if (CaseMobelobj.Notice.CaseDetail != null)
            {
                CaseMobelobj.Notice.CaseDetail.Bench = BenchTypeMvcModel.CopyFromEntity(db.BenchType.Where(x => x.bench_type_code == CaseMobelobj.Notice.CaseDetail.BenchType).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.Category = CategoryMvcModel.CopyFromEntity(db.Categorys.Where(x => x.CategoryCode == CaseMobelobj.Notice.CaseDetail.C_subject).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.SubCategory = SubCategoryMvcModel.CopyFromEntity(db.SubCategorys.Where(x => x.CategoryCode == CaseMobelobj.Notice.CaseDetail.C_subject && x.SubCategoryCode == CaseMobelobj.Notice.CaseDetail.Cs_subject).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.CauselistTypes = CauseListTypeMvcModel.CopyFromEntity(db.CauseListTypes.Where(x => x.Id == CaseMobelobj.Notice.CaseDetail.CauselistType).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.District = DistrictMvcModel.CopyFromEntity(db.Districts.Where(x => x.DistCode == CaseMobelobj.Notice.CaseDetail.CaseDistCode).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.PetAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == CaseMobelobj.Notice.CaseDetail.pet_adv_cd).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.RespAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == CaseMobelobj.Notice.CaseDetail.res_adv_cd).FirstOrDefault(), 0);
            }
            CaseMobelobj.IsCurrentAdvocate = CaseMobelobj.AdvocateId == AppUser.UserId;

            TempData["CaseViewModel"] = CaseMobelobj;
            TempData.Keep();

            return CaseMobelobj;
        }*/


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveBasicIndex(CaseMvcModel DocumentMVCModel)
        {
            if (!IsValidPermission(rModel, PageName.Case, PagePermissionType.CanView))
            {
                return RedirectToAction("Index", "Home");
            }



            var dbData = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseId == DocumentMVCModel.CaseId).FirstOrDefault();
            var oldAdvocate = dbData.AdvocateId;
            dbData.AdvocateId = DocumentMVCModel.AdvocateId == null ? dbData.AdvocateId : DocumentMVCModel.AdvocateId;
            dbData.CaseStatusId = DocumentMVCModel.CaseStatus.CaseStatusId;
            dbData.CourtId = DocumentMVCModel.Court == null || DocumentMVCModel.Court.CourtId == null ? dbData.CourtId : DocumentMVCModel.Court.CourtId;
            dbData.Status2 = DocumentMVCModel.Status2 == null ? dbData.Status2 : DocumentMVCModel.Status2;
            dbData.RegionId = DocumentMVCModel.Region == null || DocumentMVCModel.Region.RegionId == null ? dbData.RegionId : DocumentMVCModel.Region.RegionId;

            db.Cases.Update(dbData);


            db.SaveChangesWithAudit(Request, AppUser.UserName);

            if (DocumentMVCModel.AdvocateId != null && oldAdvocate != DocumentMVCModel.AdvocateId)
                AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateAdvocateAssignmentNotification(dbData, oldAdvocate);

            SucessMessage = $"Basic Index Saved successfully";

            return RedirectToAction("CaseView", new { id = DocumentMVCModel.CaseId });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CaseView(CaseMvcModel caseMvcModel)
        {
            if (!IsValidPermission(rModel, PageName.Case, PagePermissionType.CanView))
            {
                return RedirectToAction("Index", "Home");
            }
            CaseHearing ChObj = new CaseHearing();
            ChObj.CaseHearingId = Guid.NewGuid();
            ChObj.CaseId = caseMvcModel.CaseId;
            ChObj.HearingDate = caseMvcModel.NewCaseHearing.Hearing.HearingDate;
            ChObj.NextHearingDate = caseMvcModel.NewCaseHearing.Hearing.NextHearingDate;
            ChObj.Comments = caseMvcModel.NewCaseHearing.Hearing.Comments;
            int OrderNo = 1;
            foreach (var Doc in caseMvcModel.NewCaseHearing.DocumentMvcModel)
            {
                if (Doc.UploadFile != null)
                {
                    string DocumentUrl = SaveDocument(Doc.UploadFile);
                    if (DocumentUrl != null && DocumentUrl != "")
                    {
                        Document DocNewObj = new Document();
                        DocNewObj.DocumentId = Guid.NewGuid();
                        DocNewObj.CaseHearingId = ChObj.CaseHearingId;
                        DocNewObj.Name = Doc.Name;
                        DocNewObj.DocumentUrl = DocumentUrl;
                        DocNewObj.FileType = DocumentFileType.Any;
                        DocNewObj.EntityType = DocumentEntityType.CaseHearing;
                        DocNewObj.Ordinal = OrderNo;
                        DocNewObj.Size = Doc.UploadFile.ContentLength;
                        DocNewObj.Status = DocumentStatus.Added;
                        OrderNo++;
                        db.Documents.Add(DocNewObj);
                    }
                }
            }
            db.CaseHearings.Add(ChObj);
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            AppNotificationHelper.CreateAppNotificationHelperObject(this).CreateCaseHearingChangesNotifications(ChObj);
                        SucessMessage = $"Case Hearing Added successfully";

            return RedirectToAction("CaseView", caseMvcModel.CaseId);
        }
        public ActionResult CaseHearingDOC(Guid CaseHearID, Guid? CaseId)
        {
            CaseMvcModel model = new CaseMvcModel();

            if (CaseId != null)
            {
                model = GetCaseMvcModel((Guid)CaseId);
                if (model.CaseHearings.Where(x => x.CaseHearingId == CaseHearID).Count() == 0)
                    model = new CaseMvcModel();
            }
            else
                model = CaseMvcModel.CopyFromEntity(CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault(), 0);

            return PartialView("CaseHearingDOC", model);
        }
        public ActionResult CaseHearingComment(Guid CaseHearID, Guid? CaseId)
        {
            CaseMvcModel model = new CaseMvcModel();
                        if (CaseId != null)
            {
                model = GetCaseMvcModel((Guid)CaseId);
                if (model.CaseHearings.Where(x => x.CaseHearingId == CaseHearID).Count() == 0)
                    model = new CaseMvcModel();
            }
            else
                model = CaseMvcModel.CopyFromEntity(CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault(), 0);

            var Comments = model.CaseHearings.Where(m => m.CaseHearingId == CaseHearID).Select(x => x.Comments).FirstOrDefault();
            return PartialView("CaseHearingComment", Comments);
        }

        [HttpPost]
        public ActionResult GetDocument(DocumentMaster model)
        {
            var dbData = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID).ToList();
            return Json(new { ddllist = dbData });
        }

        [HttpPost]
        public ActionResult GetRequestData(Guid id)
        {
            var dbdata = RequestDocDataService.GetDetail(db).Where(x => x.CaseId == id).OrderBy(i => i.DocumentId).ToList();
            var ep = (from gd in dbdata
                      join pr in db.GovernmentDepartments on gd.SharedBy equals pr.GovernmentDepartmentId
                      join c in db.DocumentMaster on gd.DocumentId equals c.DocumentId
                      select new
                      {
                          pr.Name,
                          c.DocumentName,
                          gd.CommentRequestor,
                          gd.GovDeptComment,
                          gd.Document_Name,
                          gd.DocumentStatus,
                          gd.DocumentPath,
                          gd.CaseId
                      });
            return Json(ep);

        }

        [HttpPost]
        public ActionResult AddDocumentRequest(RequestDocumentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            foreach (var DepartmentId in model.SharedBy)
            {
                RequestDocument tempData = new RequestDocument()
                {
                    Id = Guid.NewGuid(),
                    DocumentId = model.DocumentId,
                    SharedBy = DepartmentId,
                    DocumentStatus = 0,
                    CommentRequestor = model.CommentRequestor,
                    RequestedBy = AppUser.UserId,
                    RequestedAt = DateTime.Now,
                    ModifyBy = AppUser.UserName,
                    CaseId = model.CaseId,
                    LastSubmitDate = model.LastSubmitDate
                };
                db.RequestDocument.Add(tempData);
                var modelNotify = new AppNotifications
                {
                    Id = Guid.NewGuid(),
                    Source = (int)NotificationSourceEntityType.Case,
                    TargetUserId = UserDataService.GetLite(db).Where(x => x.GovernmentDepartmentId == DepartmentId).FirstOrDefault().UserId,
                    NotificationType = 1,
                    Status = 1,
                    Message = "Request for new document of case: '" + CaseDataService.GetLite(db).Where(x => x.CaseId == model.CaseId).FirstOrDefault().PetitionNo + "' ",
                    CreatedBy = AppUser.UserId,
                    CreatedOn = Extended.CurrentIndianTime,
                    RouteValue = ""
                };
                var iResult = AppNotifications.Create(modelNotify);

                string DocumentTypeName = DocumentMasterDataService.GetLite(db).Where(x => x.DocumentId == model.DocumentId).FirstOrDefault().DocumentName;
                string description = $"Request raise for '{ DocumentTypeName }' successfully";
                TimelineBusinessService.SaveTimeLine(db, null, model.CaseId, EventType.DocumentRequest, AppUser.UserId, description, null);

            }

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"RequestDocuments Created successfully";

            return RedirectToAction("CaseView/" + model.CaseId);
        }

        public void InitilizeDocument(object selectedValue)
        {
            var dbData = CaseDataService.GetDocument(db).ToList();
            var mvc = DocumentMVCModel.CopyFromEntityList(dbData, 0);
            ViewBag.Document = new SelectList(mvc, nameof(DocumentMVCModel.DocumentId)
                , nameof(DocumentMVCModel.DocumentName), selectedValue);
        }
        public void InitilizeDepartment(object selectedValue, Guid? caseid)
        {
            var dbData = CaseDataService.GetDepartment(db, caseid).ToList();
            ViewBag.Departmemt = new SelectList(dbData, "GovernmentDepartmentId", "Name", selectedValue);
        }

        [HttpGet]
        public ActionResult SearchCauseList()
        {
            ViewBag.AdvancedSearch = false;
            return View();
        }

        [HttpPost]
        public ActionResult SearchCauseList(CauseListSearchMvcModel model)
        {
            if (model.searchLevel == 1)
            {
                return SearchCauseListAdv(model);
            }
            ViewBag.AdvancedSearch = true;
            ViewBag.Dates = getDates();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchCauseListAdv(CauseListSearchMvcModel model)
        {
            ViewBag.AdvancedSearch = true;

            List<DateWiseCauseListMvcModel> lstDateWiseCauseLists = new List<DateWiseCauseListMvcModel>();

            if (model.CauseListSearch == CauseListSearchTypes.CombinedCauseList)
            {
                if (model.SearchMode == CauseListSearchMode.CourtWise)
                {
                    var dbCauseLists = CauseListDataService.GetAllDateWise(db).Where(x => x.CauseListDate == model.ListingDate && x.Court_no == (int)model.CourtNumber);
                    lstDateWiseCauseLists = DateWiseCauseListMvcModel.CopyFromEntityList(dbCauseLists, 0);
                    foreach (var _causeList in lstDateWiseCauseLists)
                    {
                        _causeList.Notice = NoticeMvcModel.CopyFromEntity(NoticeDataService.GetAll(db).Where(x => x.CaseDetail.Case_no == _causeList.Case_no && x.CaseDetail.CauselistType == _causeList.CauseList_Type).FirstOrDefault(), 0);
                    }
                }
                else if (model.SearchMode == CauseListSearchMode.CouncilWise)
                {
                    var lsNotices = NoticeMvcModel.CopyFromEntityList(NoticeDataService.GetAll(db), 0).Where(x => x.CaseDetail.PetAdvocate.Name.ToLower() == model.CouncilName.ToLower());
                    foreach (var _Notice in lsNotices)
                    {
                        var _causeList = DateWiseCauseListMvcModel.CopyFromEntity(CauseListDataService.GetAllDateWise(db).Where(x => x.Case_no == _Notice.CaseDetail.Case_no && x.CauseList_Type == _Notice.CaseDetail.CauselistType).FirstOrDefault(), 0);
                        if (_causeList.CauseListDate == model.ListingDate)
                        {
                            _causeList.Notice = _Notice;
                            lstDateWiseCauseLists.Add(_causeList);
                        }
                    }
                }
                model.DateWiseCauseLists = lstDateWiseCauseLists;
            }

            ViewBag.Dates = getDates();
            return View(model);
        }

        private IEnumerable<SelectListItem> getDates()
        {
            var list = new List<SelectListItem>();
                       DateTime currDate = DateTime.Now.AddDays(1);

            int maxDays = 10;
            int dayCtr = 0;

            while (dayCtr < maxDays)
            {
                if (dayCtr > 0)
                    currDate = currDate.AddDays(-1);
                while (currDate.DayOfWeek == DayOfWeek.Sunday || currDate.DayOfWeek == DayOfWeek.Saturday)
                    currDate = currDate.AddDays(-1);
                string strVal = currDate.ToString("dd/MM/yyyy");
                list.Add(new SelectListItem { Text = strVal, Value = strVal });
                dayCtr++;
            }

            return list;
        }

        [HttpGet]
        public ActionResult SearchCase()
        {
            var dbData = NoticeTypeDataService.GetActive(db).OrderBy(x => x.type_name).Select(y => new { CaseNoticeTypeId = y.CaseNoticeTypeId, TypeName = string.Concat(y.type_name, " - ", y.Name) }).ToList();

            ViewBag.NoticeTypes = new SelectList(dbData, "CaseNoticeTypeId", "TypeName");
            return View();
        }

        [HttpPost]
        public ActionResult SearchCase(SearchCaseStatusMvcModel model)
        {
            var dbNoticeTypes = NoticeTypeDataService.GetActive(db).OrderBy(x => x.type_name).Select(y => new { CaseNoticeTypeId = y.CaseNoticeTypeId, TypeName = string.Concat(y.type_name, " - ", y.Name) }).ToList();
            ViewBag.NoticeTypes = new SelectList(dbNoticeTypes, "CaseNoticeTypeId", "TypeName");

            var dbData = NoticeDataService.GetDetail(db).Where(x => x.CaseNoticeType.CaseNoticeTypeId == model.caseNoticeType.CaseNoticeTypeId
                                                                    && x.RegNo != null && x.RegNo.ToString() == model.case_number
                                                                    && x.NoticeYear.ToString() == model.case_year
                                                                    && x.Cnr != null).ToList();
            model.NoticesD = NoticeMvcModel.CopyFromEntityList(dbData, 0);

            TempData["CD"] = model;
            TempData.Keep();

            return View(model);
        }

        [HttpGet]
        public ActionResult CaseStatus(Guid id)
        {
            var dbData = NoticeDataService.GetDetail(db).Where(x => x.NoticeId == id).FirstOrDefault();

            SearchCaseStatusMvcModel model = (SearchCaseStatusMvcModel)TempData["CD"];
            TempData.Keep();
            NoticeMvcModel noticeModel = NoticeMvcModel.CopyFromEntity(dbData, 0);
            noticeModel.CaseDetail.Bench = BenchTypeMvcModel.CopyFromEntity(db.BenchType.Where(x => x.bench_type_code == noticeModel.CaseDetail.BenchType).FirstOrDefault(), 0);
            noticeModel.CaseDetail.Category = CategoryMvcModel.CopyFromEntity(db.Categorys.Where(x => x.CategoryCode == noticeModel.CaseDetail.C_subject).FirstOrDefault(), 0);
            noticeModel.CaseDetail.SubCategory = SubCategoryMvcModel.CopyFromEntity(db.SubCategorys.Where(x => x.CategoryCode == noticeModel.CaseDetail.C_subject && x.SubCategoryCode == noticeModel.CaseDetail.Cs_subject).FirstOrDefault(), 0);
            noticeModel.CaseDetail.CauselistTypes = CauseListTypeMvcModel.CopyFromEntity(db.CauseListTypes.Where(x => x.Id == noticeModel.CaseDetail.CauselistType).FirstOrDefault(), 0);
            noticeModel.CaseDetail.District = DistrictMvcModel.CopyFromEntity(db.Districts.Where(x => x.DistCode == noticeModel.CaseDetail.CaseDistCode).FirstOrDefault(), 0);
            noticeModel.CaseDetail.PetAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == noticeModel.CaseDetail.pet_adv_cd).FirstOrDefault(), 0);
            noticeModel.CaseDetail.RespAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == noticeModel.CaseDetail.res_adv_cd).FirstOrDefault(), 0);
            return View(noticeModel);
        }

        [HttpGet]
        public ActionResult SearchCaseStatus()
        {
            var data = CaseDataService.GetCaseTypeAsync();

            //ViewBag.CaseTypes = data.ToEnum<CaseTypeMvcModel>();
            ViewBag.CaseTypes = new SelectList(data as IEnumerable<CaseTypeMvcModel>, nameof(CaseTypeMvcModel.case_type), nameof(CaseTypeMvcModel.full_form), null);
            return View();
        }

        [HttpPost]
        public ActionResult SearchCaseStatus(string case_type, string case_number, string case_year)
        {
            var data = CaseDataService.GetCaseTypeAsync();

            //ViewBag.CaseTypes = data.ToEnum<CaseTypeMvcModel>();
            ViewBag.CaseTypes = new SelectList(data as IEnumerable<CaseTypeMvcModel>, nameof(CaseTypeMvcModel.case_type), nameof(CaseTypeMvcModel.full_form), null);
            var casetype = data.Where(x => x.case_type == case_type).FirstOrDefault();
            var caseTypeNo = case_year + "-" + case_number + "&" + casetype.case_type;
            //Case Detail
            SearchCaseStatusMvcModel searchCaseStatus = CaseDataService.GetSearchCaseStatus(caseTypeNo);
            //CaseDetailMvcModel caseDetail = CaseDataService.GetCaseDetail(caseTypeNo);
            //caseDetail.CaseType = casetype;
            //ViewBag.CaseNoView = casetype.type_name + "/" + case_number + "/" + case_year;
            //ViewBag.PetRes = caseDetail.pet_name + " vs " + caseDetail.res_name;
            //ViewBag.cino = caseDetail.cino;
            //TempData["CD"] = caseDetail;
            searchCaseStatus.case_number = case_number;
            searchCaseStatus.case_year = case_year;
            searchCaseStatus.CaseType = casetype;
            TempData["CD"] = searchCaseStatus;
            TempData.Keep();
            return View(searchCaseStatus);
        }

        public ActionResult CaseStatusDetails(string id)
        {
            SearchCaseStatusMvcModel searchCaseStatus = (SearchCaseStatusMvcModel)TempData["CD"];
            TempData.Keep();
            CaseStatusDetails caseStatusDetails = CaseDataService.GetCaseStatusDetails(id);
            caseStatusDetails.Subject = CaseDataService.GetCaseSubjects().Where(x => x.subject_code == caseStatusDetails.c_subject && x.subnature1_cd == caseStatusDetails.cs_subject).FirstOrDefault();
            caseStatusDetails.CaseDetail = searchCaseStatus.Notices.Where(x => x.cnr == id).FirstOrDefault().CaseDetails.Where(x => x.cino == id).FirstOrDefault();
            caseStatusDetails.CaseDetail.CaseType = searchCaseStatus.CaseType;
            caseStatusDetails.benchType = CaseDataService.GetBenchType().Where(x => x.bench_type_code == caseStatusDetails.bench_type).FirstOrDefault();
            caseStatusDetails.IA = caseStatusDetails.IA.OrderBy(x => x.ia_regno).ToList();
            ViewBag.case_year = searchCaseStatus.case_year;
            ViewBag.case_number = searchCaseStatus.case_number;
            return View(caseStatusDetails);
        }
    }
}
