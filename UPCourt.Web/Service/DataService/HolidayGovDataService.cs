﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class HolidayGovDataService
    {
        public static IQueryable<HolidayGov> GetLite(DatabaseContext db)
        {
            return db.HolidayGov;
        }
        public static IQueryable<HolidayGov> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
        //public static IQueryable<GovernmentDepartmentDC> GetDetail(DatabaseContext db)
        //{
        //    return GetLite(db)
        //        //.Include(i => i.GovernmentDepartmentDCContacts);

        //}
    }
}