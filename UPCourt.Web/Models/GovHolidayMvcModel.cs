﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class GovHolidayMvcModel : BaseEntityModel<GovHoliday, GovHolidayMvcModel>
    {
        public Guid HolidayId { get; set; }

        public string HolidayName { get; set; }
        public DateTime HolidayDate { get; set; }
    }
}