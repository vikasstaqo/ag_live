﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class DashBoardController : BaseMVCController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // GET: DashBoard
        public ActionResult Index()
        {
            UserSystemRole UserSystemRoleId = AppUser.User.UserRole.UserSystemRoleId;

            if (UserSystemRoleId == UserSystemRole.Admin || UserSystemRoleId == UserSystemRole.AGAdmin
               || UserSystemRoleId == UserSystemRole.CSC || UserSystemRoleId == UserSystemRole.CGA
               || UserSystemRoleId == UserSystemRole.AdvCivil || UserSystemRoleId == UserSystemRole.AdvCriminal)
            {
                ViewBag.ActionName = "Dashboard";
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptHead)
            {
                ViewBag.ActionName = "GovDeptDashboard";
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptDegUser)
            {
                ViewBag.ActionName = "GovDeptDegDashboard";
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptOIC)
            {
                ViewBag.ActionName = "GovDeptOICDashboard";
            }
            else if (UserSystemRoleId == UserSystemRole.DataEntryOperator)
            {
                ViewBag.ActionName = "DataEntryOperatorDashboard";
            }

            return View();
        }
        public ActionResult Dashboard()
        {

            logger.Info("Dashboard Function Start");
            DashboardMvcModel DMM = new DashboardMvcModel();
            getDashboardCaseCounts(DMM);
            GetGraphDetail(DMM);
            DMM.PendingNotices = DMM.Notices.Where(i => i.Status == NoticeStatus.Pending).Count();

            logger.Info("UserDataService.GetFiltered Start");
            DMM.UserOfficials = UserDataService.GetFiltered(db, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, "DB").GroupBy(x => x.UserRole.Name).Select(group => new UserOfficials { OfficialName = group.Key, UserCount = group.Count() }).ToList();
            logger.Info("UserDataService.GetFiltered End");

            logger.Info("Dashboard Function End");
            return PartialView(DMM);

        }

        private void GetGraphDetail(DashboardMvcModel DMM)
        {
            logger.Info("GetGraphDetail Function Start");
            var activeCases = DMM.Cases.Where(x => x.CaseStatus.Type == CaseStatusType.Normal && x.Notice.NoticeDate > DateTime.Now.AddMonths(-12)).OrderBy(x => x.Notice.NoticeDate);
            var disposedCases = DMM.Cases.Where(x => x.CaseStatus.Type == CaseStatusType.Dispose && x.Notice.NoticeDate > DateTime.Now.AddMonths(-12)).ToList();
            var contextCases = DMM.Cases.Where(x => x.Notice.IsContextOrder && x.Notice.NoticeDate > DateTime.Now.AddMonths(-12)).ToList();
            var PendingNotices = DMM.Notices.Where(x => x.Status == NoticeStatus.Pending && x.NoticeDate > DateTime.Now.AddDays(-12)).OrderByDescending(i => i.NoticeDate).ToList();
            logger.Info("Collection done");


            var today = DateTime.Now;

            List<Days> days = new List<Days>();
            for (int i = 7; i >= 0; i--)
            {
                today = DateTime.Now.AddDays(-i);
                days.Add(new Days() { DayName = today.ToString("dd MMM"), Year = today.Year, Month = today.Month, Day = today.Day, count = 0 });
            }
            List<Months> months = new List<Months>();
            for (int i = 11; i >= 0; i--)
            {
                today = DateTime.Now.AddMonths(-i);
                months.Add(new Months() { MonthName = today.ToString("MMM"), Month = today.Month, Year = today.Year, count = 0 });
            }

            DMM.ActiveCasesCounts = (from mn in months
                                     join ac in (from p in activeCases
                                                 group p by new { year = p.Notice.NoticeDate?.Year, month = p.Notice.NoticeDate?.Month } into d
                                                 select new { d.Key.year, d.Key.month, Reccount = d.Count() }
                                                 ) on mn.Month.ToString() + mn.Year.ToString() equals ac.month.ToString() + ac.year.ToString() into tmpAc
                                     from ac in tmpAc.DefaultIfEmpty()
                                     select new MonthGraphData { CaseCount = ac != null ? ac.Reccount : 0, MonthName = mn.MonthName }).ToList();


            DMM.DisposedCasesCounts = (from mn in months
                                       join ac in (from p in disposedCases
                                                   group p by new { year = p.Notice.NoticeDate?.Year, month = p.Notice.NoticeDate?.Month } into d
                                                   select new { year = d.Key.year, month = d.Key.month, Reccount = d.Count() }
                                                   ) on mn.Month.ToString() + mn.Year.ToString() equals ac.month.ToString() + ac.year.ToString() into tmpAc
                                       from ac in tmpAc.DefaultIfEmpty()
                                       select new MonthGraphData { CaseCount = ac != null ? ac.Reccount : 0, MonthName = mn.MonthName }).ToList();

            DMM.ContextOrdersCounts = (from mn in months
                                       join ac in (from p in contextCases
                                                   group p by new { year = p.Notice.NoticeDate?.Year, month = p.Notice.NoticeDate?.Month } into d
                                                   select new { year = d.Key.year, month = d.Key.month, Reccount = d.Count() }
                                                   ) on mn.Month.ToString() + mn.Year.ToString() equals ac.month.ToString() + ac.year.ToString() into tmpAc
                                       from ac in tmpAc.DefaultIfEmpty()
                                       select new MonthGraphData { CaseCount = ac != null ? ac.Reccount : 0, MonthName = mn.MonthName }).ToList();

            DMM.PendingNoticesCounts = (from mn in days
                                        join ac in (from n in PendingNotices
                                                    group n by new { year = n.NoticeDate?.Year, month = n.NoticeDate?.Month, day = n.NoticeDate?.Day } into d
                                                    select new { year = d.Key.year, month = d.Key.month, day = d.Key.day, Reccount = d.Count() }
                                                    ) on mn.Day.ToString() + mn.Month.ToString() + mn.Year.ToString() equals ac.day.ToString() + ac.month.ToString() + ac.year.ToString() into tmpAc
                                        from ac in tmpAc.DefaultIfEmpty()
                                        select new DayGraphData { CaseCount = ac != null ? ac.Reccount : 0, DayName = mn.DayName }).ToList();

            logger.Info("GetGraphDetail Function End");

        }

        public ActionResult GovDeptDashboard()
        {
            DashboardMvcModel DMM = new DashboardMvcModel();
            getDashboardCaseCounts(DMM);
            DMM.TotalNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Count();
            DMM.PendingNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(i => i.Status == NoticeStatus.Pending).Count();
            var Users = UserDataService.GetFiltered(db, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, "DB");
            DMM.DesignationUserCount = Users.Where(i => i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptDegUser).Count();
            DMM.OICUserCount = Users.Where(i => i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptOIC).Count();
            return PartialView(DMM);
        }
        public ActionResult GovDeptDegDashboard()
        {
            DashboardMvcModel DMM = new DashboardMvcModel();
            getDashboardCaseCounts(DMM);
            DMM.TotalNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Count();
            DMM.PendingNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(i => i.Status == NoticeStatus.Pending).Count();
            var Users = UserDataService.GetFiltered(db, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, "DB");
            DMM.OICUserCount = Users.Where(i => i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptOIC).Count();

            return PartialView(DMM);
        }

        public ActionResult GovDeptOICDashboard()
        {
            DashboardMvcModel DMM = new DashboardMvcModel();
            getDashboardCaseCounts(DMM);
            DMM.TotalNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Count();
            DMM.PendingNotices = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(i => i.Status == NoticeStatus.Pending).Count();
            return PartialView(DMM);
        }

        private void getDashboardCaseCounts(DashboardMvcModel DMM)
        {
            var dbNotices = DashboardDataService.GetFilteredNotices(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB");
            DMM.Notices = NoticeMvcModel.CopyFromEntityList(dbNotices, 0);

            DMM.Cases = DMM.Notices.Where(x => x.Cases != null).SelectMany(x => x.Cases).ToList();

            DMM.DashboardCase = DMM.Cases.GroupBy(x => new { x.CaseStatus.CaseStatusId, x.CaseStatus.Name, x.CaseStatus.HexColorCode, x.CaseStatus.ShowOnDashboard })
                .Select(group => new DashboardCase { CaseStatusId = group.Key.CaseStatusId, Name = group.Key.Name, Count = group.Count(), HexColorCode = group.Key.HexColorCode, ShowOnDashboard = group.Key.ShowOnDashboard })
                .Where(x => x.ShowOnDashboard).ToList();

            DMM.TotalAssignedCases = DMM.Cases.Where(x => x.AdvocateId != null).Count();
            DMM.TotalUnAssignedCases = DMM.Cases.Where(x => x.AdvocateId == null).Count();
            DMM.UnAssignedCases = DMM.Cases.Where(x => x.AdvocateId == null).ToList();
            DMM.DisposedCases = DMM.Cases.Where(x => x.CaseStatus.Type == CaseStatusType.Dispose).ToList();

            var hearingCases = DMM.Cases.Where(x => x.CaseHearings.Count > 0).SelectMany(x => x.CaseHearings).ToList();
            DMM.DocumentRequestedCasesCount = hearingCases.Where(x => x.CaseHearingRespondents.Count > 0).Select(x => x.CaseId).Distinct().Count().ToString();
            DMM.DocumentReceivedCasesCount = hearingCases.Where(x => x.CaseHearingResponses.Count > 0).Select(x => x.CaseId).Distinct().Count().ToString();
            DMM.TimeBoundComplianceCount = hearingCases.Where(x => x.IsTimeBoundCompliance == TimeBoundCompliance.Yes).Select(x => x.CaseId).Distinct().Count().ToString();

            DMM.DistrictWiseCase = DMM.Cases.Where(x => x.Notice.Petitioners.Where(y => y.IsFirst).FirstOrDefault().District != null)
                .GroupBy(x => new { x.Notice.Petitioners.Where(y => y.IsFirst).FirstOrDefault().District })
                .OrderByDescending(g => g.Count())
                .Select(group => new DistrictWiseCase
                {
                    DistrictName = group.Key.District,
                    CaseCount = group.Count()
                }).ToList();
            logger.Info("DMM.DistrictWiseCase End   DMM.NoticeTypeWiseCase  Start");

            DMM.NoticeTypeWiseCase = DMM.Cases.Where(a => a.Notice.CaseNoticeTypeId != null)
                .GroupBy(x => new { x.Notice.CaseNoticeTypeId, x.Notice.CaseNoticeType.Name })
                .OrderByDescending(g => g.Count())
                .Select(group => new NoticeTypeWiseCase
                {
                    NoticeTypeId = (Guid)group.Key.CaseNoticeTypeId,
                    NoticeTypeName = group.Key.Name,
                    CaseCount = group.Count()
                }).ToList();
            logger.Info("DMM.NoticeTypeWiseCase End DMM.GovernmentDepartmentWiseCase Start");

            DMM.GovernmentDepartmentWiseCase = DMM.Cases.Where(a => a.Notice.NoticeRespondents.Where(y => y.GovernmentDepartmentId != null).Count() > 0)
                .GroupBy(x => new
                {
                    x.Notice.NoticeRespondents.Where(a => a.GovernmentDepartmentId != null).FirstOrDefault().GovernmentDepartmentId,
                    x.Notice.NoticeRespondents.Where(a => a.GovernmentDepartmentId != null).FirstOrDefault().GovermentDepartment.Name
                })
                .OrderByDescending(g => g.Count())
               .Select(group => new GovernmentDepartmentWiseCase
               {
                   DepartmentId = (Guid)group.Key.GovernmentDepartmentId,
                   DepartmentName = group.Key.Name,
                   CaseCount = group.Count()
               }).ToList();
            DMM.CaseHearing = DMM.Cases.Where(x => x.CaseHearings.Where(a => a.NextHearingDate.HasValue && a.NextHearingDate >= DateTime.Now && a.NextHearingDate <= DateTime.Now.AddDays(30)).Count() > 0).ToList();
        }

        private struct Months
        {
            internal int Year { get; set; }
            internal string MonthName { get; set; }
            internal int Month { get; set; }
            internal int count { get; set; }
        }
        private struct Days
        {
            internal string DayName { get; set; }
            internal int Year { get; set; }
            internal int Month { get; set; }
            internal int Day { get; set; }
            internal int count { get; set; }
        }

        public ActionResult DataEntryOperatorDashboard()
        {
            DashboardMvcModel DMM = new DashboardMvcModel();
            DateTime baseDate = DateTime.Today;
            DateTime thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
            DateTime thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
            DMM.TodayNotice = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(x => x.NoticeCreateDate == DateTime.Now).Count();
            DMM.WeeklyNotice = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(x => x.NoticeCreateDate >= thisWeekStart && x.NoticeCreateDate <= thisWeekEnd).Count();
            DMM.MonthNotice = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(x => x.NoticeCreateDate.Month == DateTime.Now.Month && x.NoticeCreateDate.Year == DateTime.Now.Year).Count();
            DMM.YearlyNotice = NoticeDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").Where(x => x.NoticeCreateDate.Year == DateTime.Now.Year).Count();
            return PartialView(DMM);
        }

        public ActionResult UnuthorizedAccess()
        {
            ViewBag.Message = "You Don't Have Permission To Perform This Opearation";
            return View();
        }
    }
}