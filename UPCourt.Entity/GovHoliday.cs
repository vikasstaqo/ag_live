﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(HolidayName), UserNameProp: nameof(CreatedBy))]
   public class GovHoliday:BaseMasterEntity
    {
        
        public Guid HolidayId { get; set; }
        
        public string HolidayName { get; set; }
        public DateTime HolidayDate { get; set; }
        
        //public DateTime CreatedOn { get; set; }
       
        // public string Block { get; set; }
        //public ICollection<GovernmentDepartmentDCContact> GovernmentDepartmentDCContacts { get; set; }
    }
}
