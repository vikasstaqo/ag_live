﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity.Core.Converter
{
    public class EnumerableToStringValueConverter<E> : ValueConverter<IEnumerable<E>, string>
    {
        public EnumerableToStringValueConverter() : base(le => ListToString(le), (s => StringToList(s)))
        {

        }

        public static string ListToString(IEnumerable<E> value)
        {
            if (value.IsEmptyCollection())
            {
                return null;
            }

            return value.Join(',');
        }

        public static List<E> StringToList(string value)
        {
            List<E> list = new List<E>();

            if (value.IsNullOrEmpty())
            {
                return list;
            }

            list = value.Split(',').Select(i => (E)Extended.CConvert(i, typeof(E))).ToList();

            return list;
        }
    }
}
