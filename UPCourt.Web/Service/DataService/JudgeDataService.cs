﻿namespace UPCourt.Web.Service.DataService
{
    using System;
    using System.Linq;
    using UPCourt.Entity;

    public class JudgeDataService
    {
        public static IQueryable<Judge> GetAll(DatabaseContext db)
        {
            return db.Judges;
        }
        public static IQueryable<Judge> GetLite(DatabaseContext db)
        {
            return db.Judges.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Judge> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
        public static IQueryable<Judge> GetSelectdJudgeDetails(DatabaseContext db, Guid courtId)
        {
            var ep = (from jd in db.Judges
                      join cr in db.CourtJudgeMappings on jd.JudgeId equals cr.JudgeId
                      where jd.IsDeleted == false && jd.JudgeId == cr.JudgeId && cr.CourtId == courtId && cr.IsActive == true
                      select jd);
            return ep;
        }
    }
}