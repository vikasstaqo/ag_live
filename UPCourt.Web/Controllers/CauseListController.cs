﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CauseListController : BaseMVCController
    {
        // GET: CauseList
        public ActionResult Index(CauseListFilterMVCModel model)
        {
            var dbData = CauseListDataService.GetDetail(db).ToList();
            model.CauseLists = CauseListMvcModel.CopyFromEntityList(dbData, 0);

            return View(model);
        }
    }
}