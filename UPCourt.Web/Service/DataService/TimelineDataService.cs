﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class TimelineDataService
    {
        public static IQueryable<Timeline> GetLite(DatabaseContext db)
        {
            return db.Timelines;
        }
        public static IQueryable<Timeline> GetDetail(DatabaseContext db)
        {
            return db.Timelines.Include(i => i.User);
        }
    }
}