﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class DivisionDataService
    {
        public static IQueryable<Division> GetLite(DatabaseContext db)
        {
            return db.Division.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Division> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}