﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ReceiptNo), UserNameProp: nameof(ModifyBy))]
    public class ECourtFee
    {
        public Guid eCourtFeeID { get; set; }

        public Guid? NoticeId { get; set; }
        public string ReceiptNo { get; set; }

        public decimal Amount { get; set; }

        public DateTime? EDate { get; set; }

        public DateTime InsertedDate { get; set; }

        public string InsertedBy { get; set; }

        public string ModifyBy { get; set; }

        public DateTime ModifyDate { get; set; }

        public bool Status { get; set; }
        public Guid? ApplicationDocumentID { get; set; }
    }

}
