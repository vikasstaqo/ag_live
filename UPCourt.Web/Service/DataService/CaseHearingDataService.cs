﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CaseHearingDataService
    {
        public static IQueryable<CaseHearing> GetAll(DatabaseContext db)
        {
            return db.CaseHearings;
        }
        public static IQueryable<CaseHearing> GetLite(DatabaseContext db)
        {
            return db.CaseHearings.Where(x => x.IsDeleted == false);
        }
        
        public static CaseHearing GetDetail(DatabaseContext db, Guid hearingId)
        {
            var ret = GetLite(db).Where(h => h.CaseHearingId == hearingId)
                .Include(F => F.Case)
                .Include(F => F.CaseHearingGovAdvocates)
                .Include(F => F.CaseHearingRespondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.CaseHearingResponses)
                .Include(F => F.Documents)
                .Include(F => F.Judge).FirstOrDefault();
            ret.CaseHearingGovAdvocates = ret.CaseHearingGovAdvocates.Where(x => x.IsDeleted == false).ToList();
            ret.Documents = ret.Documents.Where(d => d.IsDeleted == false).ToList();
            return ret;
        }

        public static IQueryable<CaseHearing> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(F => F.Case) 
                .Include(F => F.CaseHearingGovAdvocates)
                .Include(F => F.CaseHearingRespondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.CaseHearingResponses)
                .Include(F => F.Documents)
                .Include(F => F.Judge);
        }

    }
}