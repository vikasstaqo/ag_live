﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeId), UserNameProp: nameof(ModifyBy))]
    public class NoticeIPC
    {
        public NoticeIPC()
        {
        }
        public Guid NoticeIpcId { get; set; }
        public Guid NoticeId { get; set; }
        public Guid IPCSectionId { get; set; }
        public IPCSection IPCSection { get; set; }
        public Guid CrimeDetailId { get; set; }
        public string ModifyBy { get; set; }
        public int? SNo { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeId), UserNameProp: nameof(ModifyBy))]
    public class NoticeActType
    {
        public NoticeActType()
        {
        }
        public Guid NoticeActTypeId { get; set; }
        public Guid NoticeId { get; set; }
        public string BelongsTo { get; set; }
        public string ActTitle { get; set; }
        public string Section { get; set; }
        public string RuleTitle { get; set; }
        public string RuleNumber { get; set; }
        public string ModifyBy { get; set; }
        public int? SNo { get; set; }
    }
}
