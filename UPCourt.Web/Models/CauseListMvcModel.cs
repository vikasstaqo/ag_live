﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DateWiseCauseListMvcModel : BaseEntityModel<DateWiseCauseList, DateWiseCauseListMvcModel>
    {
        public Guid DateWiseCauseListId { get; set; }
        public Guid CauseListTypeId { get; set; }
        public string PetName { get; set; }
        public int BenchId { get; set; }
        public string cause_list_eliminate { get; set; }
        public string elimination { get; set; }
        public int CauseList_Id { get; set; }
        public string ResName { get; set; }
        public string CauseListStatus { get; set; }
        public int causelist_sr_no { get; set; }
        public string Case_no { get; set; }
        public string published { get; set; }
        public DateTime CauseListDate { get; set; }
        public int CauseList_Type { get; set; }
        public int Court_no { get; set; }
        public int for_bench_id { get; set; }
        public CauseList_DateWiseStatus Status { get; set; }
        public NoticeMvcModel Notice { get; set; }
    }

    public class CauseListMvcModel : BaseEntityModel<CauseList, CauseListMvcModel>
    {
        public Guid CauseListId { get; set; }
        public Guid CauseListTypeId { get; set; }
        public CauseListTypeMvcModel CauseListType { get; set; }
        public string PetName { get; set; }
        public int BenchId { get; set; }
        public string cause_list_eliminate { get; set; }
        public string elimination { get; set; }
        public int CauseList_Id { get; set; }
        public string ResName { get; set; }
        public string CauseListStatus { get; set; }
        public int causelist_sr_no { get; set; }
        public string Case_no { get; set; }
        public string published { get; set; }
        public DateTime CauseListDate { get; set; }
        public int CauseList_Type { get; set; }
        public int Court_no { get; set; }
        public int for_bench_id { get; set; }
        public CauseListStatus Status { get; set; }
        public NoticeMvcModel Notice { get; set; }
    }

    public class CauseListSearchMvcModel
    {
        [Display(Name = "CAUSE LIST *")]
        public CauseListSearchTypes CauseListSearch { get; set; }

        [Display(Name = "Listing Date *")]

        public DateTime ListingDate { get; set; }

        public CauseListSearchMode SearchMode { get; set; }
        [Display(Name = "Court No *")]
        public AllahbadCourtNumbers CourtNumber { get; set; }
        [Display(Name = "Counsel Name: Mr./Ms./Dr. *")]
        public string CouncilName { get; set; }

        public List<DateWiseCauseListMvcModel> DateWiseCauseLists { get; set; }

        public List<CauseListMvcModel> CauseLists { get; set; }

        public int searchLevel { get; set; }

    }

    public enum AllahbadCourtNumbers
    {
        [Display(Name = "Entire List")]
        EntireList = -1,
        [Display(Name = "COURT NO.4")]
        CourtNo4 = 4,
        [Display(Name = "COURT NO.5")]
        CourtNo5 = 5,
        [Display(Name = "COURT NO.5")]
        CourtNO6 = 6,
        [Display(Name = "COURT NO.21")]
        CourtNo21 = 21,
        [Display(Name = "COURT NO.32")]
        CourtNo32 = 32,
        [Display(Name = "COURT NO.34")]
        CourtNo34 = 34,
        [Display(Name = "COURT NO.42")]
        CourtNo42 = 42,
        [Display(Name = "COURT NO.43")]
        CourtNo43 = 43,
        [Display(Name = "COURT NO.47")]
        CourtNo47 = 47
    }

    public enum CauseListSearchMode
    {
        [Display(Name = "Court Wise")]
        CourtWise,
        [Display(Name = "Council Wise (only for next working day)")]
        CouncilWise
    }

    public enum CauseListSearchTypes
    {
        [Display(Name = "Combined Cause List")]
        CombinedCauseList,
        [Display(Name = "Daily Cause List")]
        DailyCauseList,
        [Display(Name = "Fresh Cases")]
        FreshCases,
        [Display(Name = "Additional Cause List")]
        AdditionalCauseList,
        [Display(Name = "Backlog Fresh Cases")]
        BacklogFreshCases,
        [Display(Name = "Supplementary Fresh Cases")]
        SupplementaryFreshCases,
        [Display(Name = "Appliaction (IA)")]
        AppliactionIA,
        [Display(Name = "Appliaction (Correction)")]
        AppliactionCorrection,
        [Display(Name = "Weekly List")]
        WeeklyList,
        [Display(Name = "National Lok Adalat")]
        NationalLokAdalat

    }
}