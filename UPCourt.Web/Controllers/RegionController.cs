﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class RegionController : BaseMVCController
    {
        // GET: Region
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(RegionMvcModel.CopyFromEntityList(RegionDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        // GET: Region/Details/5
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(RegionMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Region/Create
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new RegionMvcModel();
            model.Status = RegionStatus.Active;
            return View(model);
        }

        // POST: Region/Create
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(RegionMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            Region tempRegion = new Region();
            tempRegion.RegionId = Guid.NewGuid();
            tempRegion.DisplayOrder = MasterBusinessService.GetRegionsDisplayOrder(db);
            InitializeData(tempRegion, model);

            db.Regions.Add(tempRegion);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Create");
        }

        // GET: Region/Edit/5
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(RegionMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: Region/Edit/5
        [HttpPost]
        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanEdit)]
        public ActionResult Edit(RegionMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == model.RegionId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            InitializeData(dbData, model);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        private void InitializeData(Region tempRegion, RegionMvcModel model)
        {
            tempRegion.Name = model.Name;
            tempRegion.Description = model.Description;
            tempRegion.Status = model.Status;
            tempRegion.IsDefault = model.IsDefault;
            tempRegion.ModifiedBy = AppUser.UserId;
        }
        private bool CommonForInsertUpdate(RegionMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = RegionDataService.GetLite(db).FirstOrDefault(i => i.RegionId != model.RegionId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Region name already exists");
                return false;
            }

            if (model.IsDefault == true)
            {
                var DefaultCheck = RegionDataService.GetLite(db).FirstOrDefault(i => i.RegionId != model.RegionId && i.IsDefault == true);
                if (DefaultCheck != null)
                {
                    ModelState.AddModelError(nameof(model.IsDefault), "Default Region already selected.");
                    return false;
                }
            }

            return true;
        }

        [PageRoleAuthorizarion(PageName.Region, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
