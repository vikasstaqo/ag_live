﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifiedBy))]
    public class Region : BaseMasterEntity
    {
        public Region()
        {
            Cases = new HashSet<Case>();
            UserCredentialRequests = new HashSet<UserCredentialRequest>();
        }
        public Guid RegionId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string Description { get; set; }
        public RegionStatus Status { get; set; }
        public ICollection<Case> Cases { get; set; }
        public ICollection<UserCredentialRequest> UserCredentialRequests { get; set; }
    }
    public enum RegionStatus
    {
        Active = 1,
        Inactive = 0
    }
}
