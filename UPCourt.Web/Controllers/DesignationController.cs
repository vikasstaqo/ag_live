﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class DesignationController : BaseMVCController
    {
        // GET: Designation
        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(DesignationResponseModel.CopyFromEntityList(DesignationDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        // GET: Designation/Create
        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            InitilizeDesignation(null);
            var model = new DesignationResponseModel();
            model.Status = DesignationStatus.Active;
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(DesignationResponseModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Designation tempData = new Designation()
            {
                DesignationId = Guid.NewGuid(),
                Name = model.Name,
                Description = model.Description,
                Status = model.Status,
                IsDefault = model.IsDefault,
                ModifiedBy = AppUser.UserId,
                DisplayOrder = MasterBusinessService.GetDesignationsDisplayOrder(db)
            };
            db.Designations.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = DesignationDataService.GetDetail(db).Where(i => i.DesignationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeDesignation(dbData.DesignationId);
            return View(DesignationResponseModel.CopyFromEntity(dbData, 0));
        }
        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(DesignationResponseModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = DesignationDataService.GetDetail(db).Where(i => i.DesignationId == model.DesignationId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Description = model.Description;
            dbData.Status = model.Status;
            dbData.IsDefault = model.IsDefault;
            dbData.ModifiedBy = AppUser.UserId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = DesignationDataService.GetLite(db).Where(i => i.DesignationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(DesignationResponseModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(DesignationResponseModel model)
        {
            var duplicateCheck = DesignationDataService.GetLite(db).FirstOrDefault(i => i.DesignationId != model.DesignationId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }
            return true;
        }
        public void InitilizeDesignation(object selectedValue)
        {
            var dbData = DesignationDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = DesignationResponseModel.CopyFromEntityList(dbData, 0);
            ViewBag.Designations = new SelectList(mvc, nameof(DesignationResponseModel.DesignationId)
                , nameof(DesignationResponseModel.Name), selectedValue);
        }

        [PageRoleAuthorizarion(PageName.Designation, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = DesignationDataService.GetLite(db).Where(i => i.DesignationId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
