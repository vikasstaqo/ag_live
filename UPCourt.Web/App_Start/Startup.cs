﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPCourt.Web
{
    public class Startup
    {
        public const string CokkieName = "UPCourt";
        public void Configuration(IAppBuilder app)
        {

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = Startup.CokkieName,
                LoginPath = new PathString("/Account/Login")
            });
        }
    }
}