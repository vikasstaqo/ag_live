﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class ApplicationTypeDataService
    {
        public static IQueryable<ApplicationType> GetLite(DatabaseContext db)
        {
            return db.ApplicationTypes;
        }
        public static IQueryable<ApplicationType> GetDetail(DatabaseContext db)
        {
            return db.ApplicationTypes;
        }
    }

}