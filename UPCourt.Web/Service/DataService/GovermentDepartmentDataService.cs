﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovermentDepartmentDataService
    {
        public static string GetName(Guid? govid)
        {
            DatabaseContext db = new DatabaseContext();
            var data = GetLite(db).Where(x => x.GovernmentDepartmentId == govid).FirstOrDefault();
            if (data != null)
                return data.Name;
            else
                return "";
        }
        public static IQueryable<GovernmentDepartment> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartments.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<GovernmentDepartment> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.GovernmentDepartmentAddresses)
                .Include(i => i.GovernmentDepartmentContactNos)
                .Include(i => i.GovernmentDesignations);

        }
        public static GovernmentDepartment GetGovernmentDepartmentById(DatabaseContext db, System.Guid id, bool detail = false)
        {
            if (detail)
                return GetDetail(db).Where(x => x.GovernmentDepartmentId == id).FirstOrDefault();
            else
                return GetLite(db).Where(x => x.GovernmentDepartmentId == id).FirstOrDefault();
        }
        public static IQueryable<GovernmentDepartment> GetFiltered(DatabaseContext db, User LoginUser)
        {
            List<GovernmentDepartment> GovDeptList = GetLite(db).ToList();
            if (LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead && LoginUser.GovernmentDepartmentId != null)
            {
                GovDeptList = GovDeptList.Where(x => x.GovernmentDepartmentId == LoginUser.GovernmentDepartmentId).ToList();
            }
            return GovDeptList.AsQueryable();
        }
    }

    public class GovermentDepartmentDCDataService
    {
        public static IQueryable<GovernmentDepartmentDC> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentDC;
        }
        public static IQueryable<GovernmentDepartmentDC> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.GovernmentDepartmentDCContacts);

        }
    }

    public class GovermentDepartmentDCContactDataService
    {
        public static IQueryable<GovernmentDepartmentDCContact> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentDCContacts;
        }
    }

    public class GovDesignationDataService
    {
        public static IQueryable<GovernmentDesignation> GetLite(DatabaseContext db)
        {
            return db.GovernmentDesignations.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<GovernmentDesignation> GetDetail(DatabaseContext db)
        {
            return GetLite(db).Include(i => i.GovernmentDepartment);
        }
        public static IQueryable<GovernmentDesignation> GetFiltered(DatabaseContext db, User LoginUser, string HitFrom = "")
        {
            List<GovernmentDesignation> GDList = new List<GovernmentDesignation>();
            if (HitFrom == "DB")
                GDList = GetLite(db).ToList();
            else
                GDList = GetDetail(db).ToList();
            if (LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead && LoginUser.GovernmentDepartmentId != null)
            {
                GDList = GDList.Where(x => x.GovernmentDepartmentId == LoginUser.GovernmentDepartmentId).ToList();
            }
            return GDList.AsQueryable();
        }
    }

    public class GovOICDataService
    {
        public static IQueryable<GovernmentDepartmentOic> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentOics.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<GovernmentDepartmentOic> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.GovernmentDepartment)
                .Include(i => i.GovernmentDesignation);


        }
        public static IQueryable<GovernmentDepartmentOic> GetFiltered(DatabaseContext db, User LoginUser, string HitFrom = "")
        {
            List<GovernmentDepartmentOic> GDOICList = new List<GovernmentDepartmentOic>();
            if (HitFrom == "DB")
                GDOICList = GetLite(db).ToList();
            else
                GDOICList = GetDetail(db).ToList();
            if (LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead && LoginUser.GovernmentDepartmentId != null)
            {
                GDOICList = GDOICList.Where(x => x.GovernmentDepartmentId == LoginUser.GovernmentDepartmentId).ToList();
            }
            return GDOICList.AsQueryable();
        }
    }
    public class GovernmentDepartmentAddressDataService
    {
        public static IQueryable<GovernmentDepartmentAddress> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentAddresses;
        }

    }
    public class GovernmentDepartmentContactNoDataService
    {
        public static IQueryable<GovernmentDepartmentContactNo> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentContactNos;
        }
    }
}