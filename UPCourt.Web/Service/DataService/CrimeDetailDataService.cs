﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CrimeDetailDataService
    {
        public static IQueryable<CrimeDetail> GetLite(DatabaseContext db)
        {
            return db.CrimeDetails;
        }

        public static IQueryable<CrimeDetail> GetDetail(DatabaseContext db)
        {
            return db.CrimeDetails
                .Include(i => i.PoliceStation);
        }
    }
}