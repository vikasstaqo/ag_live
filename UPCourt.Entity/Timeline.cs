﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Description), UserNameProp: nameof(UserId))]
    public class Timeline
    {
        public Guid TimelineId { get; set; }
        public Guid? CaseId { get; set; }
        public Guid? NoticeId { get; set; }
        public EventType EventType { get; set; }
        public Guid UserId { get; set; }
        public DateTime TimeOfEvent { get; set; }
        public string Description { get; set; }
        public Guid? ParentTimelineId { get; set; }

        public Timeline ParentTimeline { get; set; }
        public User User { get; set; }
    }

    public enum EventType
    {
        [EventTypeAttribute("Notice entry into the system", "fas fa-envelope bg-blue")]
        NoticeCreated = 1,
        [EventTypeAttribute("Case no recived for notice no", "fas fa-user bg-green")]
        NoticeToCase = 2,
        [EventTypeAttribute("Court Assignment", "fas fa-comments bg-yellow")]
        CourtAssignment = 3,
        [EventTypeAttribute("Advocate Assignment", "fas fa-user bg-green")]
        AdvocateAssignment = 4,
        [EventTypeAttribute("Document Request", "fas fa-comments bg-yellow")]
        DocumentRequest = 5,
        [EventTypeAttribute("Hearing Entry", "fas fa-user bg-green")]
        HearingEntry = 6,
        [EventTypeAttribute("Advocate Change", "fas fa-comments bg-yellow")]
        AdvocateChange = 7,
        [EventTypeAttribute("Change in court", "fas fa-comments bg-yellow")]
        CourtChange = 8,
        [EventTypeAttribute("Disposed", "fas fa-user bg-green")]
        Disposed = 9,
        [EventTypeAttribute("Re-open", "fas fa-envelope bg-blue")]
        Reopen = 10,
        [EventTypeAttribute("Re-appeal", "fas fa-comments bg-yellow")]
        Reappeal = 11,
        [EventTypeAttribute("Move to Supreme Court", "fas fa-user bg-green")]
        MovetoSupremeCourt = 12,
        [EventTypeAttribute("Moved back to High court", "fas fa-comments bg-yellow")]
        MovedbacktoHighcourt = 13,
        [EventTypeAttribute("Application Document entry into the system", "fas fa-envelope bg-blue")]
        ApplicationDocumentCreated = 14,
        [EventTypeAttribute("Request Document Uploaded ", "fas fa-upload bg-yellow")]
        RequestDocumentUploaded = 15,
        [EventTypeAttribute("Advocate Assignment", "fas fa-user bg-green")]
        DocsPreprationAssignment = 16,
        [EventTypeAttribute("Advocate Change", "fas fa-comments bg-yellow")]
        DocsPreprationChange = 17,
        [EventTypeAttribute("Government Department OIC Assignment", "fas fa-user bg-green")]
        GovDepOICAssignment = 18,
        [EventTypeAttribute("Government Department OIC Change", "fas fa-comments bg-yellow")]
        GovDepOICChange = 19,
        [EventTypeAttribute("New Government Department Added", "fas fa-plus bg-green")]
        GovDepAddedtocase = 20,
        [EventTypeAttribute("Government Department Removed", "fas fa-minus bg-red")]
        GovDepRemovedtocase = 21,
        [EventTypeAttribute("Case Hearing Response by Dept", "fas fa-upload bg-red")]
        GovCHResponse = 22,
        [EventTypeAttribute("Case Hearing Response by Advocate", "fas fa-upload bg-red")]
        AdvCHResponse = 23,
        [EventTypeAttribute("Pending", "fas fa-user bg-green")]
        Pending = 24,
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class EventTypeAttribute : Attribute
    {
        public EventTypeAttribute(string DisplayText, string IconType)
        {
            _DisplayText = DisplayText;
            _IconType = IconType;
        }
        string _DisplayText;
        string _IconType;
        public string DisplayText => _DisplayText;
        public string IconType => _IconType;

    }

}
