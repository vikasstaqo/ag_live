﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(SourceName), UserNameProp: nameof(SourceName))]
    public class NotificationSource
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public bool IsActive { get; set; }
    }
}
