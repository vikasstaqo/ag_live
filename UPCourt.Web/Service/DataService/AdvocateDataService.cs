﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Models;

namespace UPCourt.Web.Service.DataService
{
    public class AdvocateDataService
    {
        public static IQueryable<User> GetLite(DatabaseContext db, ApplicationUser LoginUser)
        {
            List<User> AdvocateList = UserDataService.GetDetail(db).Where(x => x.UserRole.Name.ToLower().Contains("advocate")).ToList();

            if (LoginUser.User.UserRole.Name.ToLower() == "department user" && LoginUser.User.GovernmentDepartmentId != null)
            {
            }
            else
            {
                AdvocateList = AdvocateList.Where(x => x.RegionId == LoginUser.TempRegionId || LoginUser.User.Name == "System Administrator").ToList();
                if (LoginUser.User.UserRole.UserSystemRoleId == UserSystemRole.AdvCivil)
                {
                    AdvocateList = AdvocateList.Where(i => i.UserRole.UserSystemRoleId == UserSystemRole.AdvCivil).ToList();
                }
                else if (LoginUser.User.UserRole.UserSystemRoleId == UserSystemRole.AdvCriminal)
                {
                    AdvocateList = AdvocateList.Where(i => i.UserRole.UserSystemRoleId == UserSystemRole.AdvCriminal).ToList();
                }
            }
            return AdvocateList.AsQueryable();
        }
        public static IQueryable<User> GetDetail(DatabaseContext db, ApplicationUser LoginUser)
        {
            return GetLite(db, LoginUser).Include(i => i.UserRole);
        }

    }
}