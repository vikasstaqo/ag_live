﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Core
{
    public class AppNotificationHelper
    {
        #region properties & variables
        protected NotificationSourceEnum Source { get; set; }
        protected NotificationTypeEnum Type { get; set; }
        protected NotificationStatusEnum Status { get; set; }
        protected string RoutingValue { get; set; }
        protected Guid? HashId { get; set; }
        private List<Guid> targetUserIds { get; set; }
        private BaseMVCController Controller;

        //SMS
        private string sms_api_base_url = "";
        private string sms_api_url = "";
        private string sms_api_user = "";
        private string sms_api_pass = "";
        private string sms_api_sender = "";
        private string sms_api_bearer_token = "";
        #endregion

        #region public

        #region static methods
        /// <summary>
        /// Static method to create the object of this class
        /// </summary>
        /// <param name="controller">BaseMvcController</param>
        /// <returns>Object of this class</returns>
        public static AppNotificationHelper CreateAppNotificationHelperObject(BaseMVCController controller)
        {
            try
            {
                if (controller == null)
                    controller = new BaseMVCController();

                return new AppNotificationHelper(controller);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        #endregion

        #region object public methods

        #region SMS & OTP

        private Task<string> SendSMSAsync(HttpContent content)
        {
            HttpClient client = new HttpClient() { BaseAddress = new Uri(sms_api_base_url) };
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", sms_api_bearer_token);
            var result = client.PostAsync(sms_api_url, content);
            result.Wait();
            var ret = result.Result.Content.ReadAsStringAsync();
            return ret;
        }

        private Task<string> GetTokenAsync(HttpContent content)
        {
            HttpClient client = new HttpClient() { BaseAddress = new Uri(sms_api_base_url) };

            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Base64Encode(sms_api_user + ":" + sms_api_pass));

            var result = client.PostAsync(sms_api_url + "/token?action=generate", content);
            result.Wait();
            var ret = result.Result.Content.ReadAsStringAsync();
       
            return ret;
        }

        public void sendOTP(Dictionary<string, string> dataSet)
        {
            try
            {
                var mobileNo = dataSet["mobile"];
                var sms_text = dataSet["message"];
                var sha = "";
                // Replace special charactor
                string[] frm = { "#", "$", "&", "*", "<| ", ">| ", "?", "@", "[", "\\", "]", "{", "|", "}", "~", "\n", "\"" };
                string[] to = { "&#35;", "&#36;", "&#38;", "&#42;", "&#60;", "&#62;", "&#63;", "&#64;", "&#91;", "&#92;", "&#93;", "&#123;", "&#124;", "&#125;", "&#126;", "&#10;", "&#34;" };

                // For English encode the URL
                for (int i = 0; i < frm.Length; i++)
                    sms_text = sms_text.Replace(frm[i], to[i]);


                if (dataSet.ContainsKey("sha"))
                {
                    sha = dataSet["sha"];
                    sha = sha.Replace("+", "%2B").Replace("-", "%2D");
                    sms_text += sha;
                }


                List<KeyValuePair<string, string>> content = new List<KeyValuePair<string, string>>();
                content.Add(new KeyValuePair<string, string>("to", mobileNo));
                content.Add(new KeyValuePair<string, string>("from", sms_api_sender));
                content.Add(new KeyValuePair<string, string>("text", sms_text));
                content.Add(new KeyValuePair<string, string>("dlr-mask", "19"));
                content.Add(new KeyValuePair<string, string>("dlr-url", ""));

                HttpContent postContent = new FormUrlEncodedContent(content);
                //var postData = "to=" + mobileNo + "&from=" + sms_api_sender + "&text=" + sms_text + "&dlr-mask=19&dlr-url";

                var token = GetTokenAsync(null);
                sms_api_bearer_token = ((Newtonsoft.Json.Linq.JContainer)Newtonsoft.Json.JsonConvert.DeserializeObject(token.Result)).First.First.ToString();
                var sendResult = SendSMSAsync(postContent);
            }
            catch (Exception ex) { }
           
        }

        #endregion



        /// <summary>
        /// Creates Notification Created Notifications and is sent to all respondents Government departmentss
        /// </summary>
        /// <param name="notice">Object of Entity Notice</param>
        /// <returns></returns>
        public bool CreateNoticeCreateNotification(Notice notice)
        {
            try
            {
                //Get all GovernmentDepartment connected users to add notifications to all of them
                targetUserIds = GetGovernmentDepartmentUsers(notice);
                Guid? targetUser = (UserDataService.GetDetail(Controller.db).Where(x => x.UserRole.UserSystemRoleId == (notice.NoticeType == ParentNoticeType.Civil ? UserSystemRole.AdvCivil : UserSystemRole.AdvCriminal) && (x.RegionId != null && x.RegionId == notice.RegionId))?.FirstOrDefault())?.UserId;
                if (targetUser != null)
                {
                    string messageSummary = "New Notice with Notice No " + notice.NoticeNo + " is created.";
                    string messageDetail = "A new Notice with Notice no <a href=\"/NoticeCase/View/" + notice.NoticeId.ToString() + "\" target=\"_blank\" >" + notice.NoticeNo + "</a> is created .";
                    return CreateNotification(targetUser, messageSummary, messageDetail, "/NoticeCase/View/" + notice.NoticeId.ToString());
                }
                //Log reason for returning false
                return false;
            }
            catch (Exception ex)
            {
                //Write log for error.
                return false;
            }
        }

        /// <summary>
        /// Create Notice when the Case Hearing is added or updated
        /// </summary>
        /// <param name="nCase"></param>
        /// <returns></returns>
        public bool CreateCaseHearingChangesNotifications(CaseHearing hearing, bool updated = false)
        {
            try
            {
                var nCase = CaseDataService.GetLite(Controller.db).Where(x => x.CaseId == hearing.CaseId).FirstOrDefault();
                var notice = NoticeDataService.GetDetail(Controller.db).Where(x => x.NoticeId == hearing.Case.NoticeId).FirstOrDefault();
                Guid? targetUser = (UserDataService.GetDetail(Controller.db).Where(x => x.UserId == nCase.AdvocateId)?.FirstOrDefault())?.UserId;
                if (targetUser != null)
                {
                    //Get all GovernmentDepartment connected users to add notifications to all of them
                    targetUserIds = GetGovernmentDepartmentUsers(notice);
                    string messageSummary = "", messageDetail = "";
                    if (!updated)
                    {
                        messageSummary = "New Case Hearing for Case No " + nCase.PetitionNo + " created.";
                        messageDetail = "A new case hearing with Case No <a href=\"/Case/CaseView/" + nCase.CaseId.ToString() + "\" target=\"_blank\" >" + nCase.PetitionNo + "</a> is created .";
                    }
                    else
                    {
                        messageSummary = "Case Hearing for Case No " + nCase.PetitionNo + " is changed .";
                        messageDetail = "Case Hearing for Case No <a href=\"/Case/CaseView/" + nCase.CaseId.ToString() + "\" target=\"_blank\" >" + nCase.PetitionNo + "</a> is changed .";
                    }

                    HashId = hearing.CaseHearingId;
                    return CreateNotification(targetUser, messageSummary, messageDetail, "/Case/CaseView/" + hearing.CaseId.ToString());
                }
                //Log reason for returning false
                return false;
            }
            catch (Exception ex)
            {
                //Write log for error.
                return false;
            }
        }

        /// <summary>
        /// Create Notification when a new case is created
        /// </summary>
        /// <param name="newCase">Object of Entity type Case</param>
        /// <see cref="Case"/>
        /// <returns>success or failure of method call</returns>
        public bool CreateNewCaseNotification(Case newCase)
        {
            return CreateNewCaseNotification(newCase, Controller.db);
        }

        /// <summary>
        /// Create Notification when a new case is created
        /// </summary>
        /// <param name="newCase">Object of Entity type Case</param>
        /// <param name="db">DatabaseContext onbject</param>
        /// <see cref="Case"/>
        /// <returns>success or failure of method call</returns>
        public bool CreateNewCaseNotification(Case newCase, DatabaseContext db)
        {
            try
            {
                var notice = NoticeDataService.GetDetail(db).Where(x => x.NoticeId == newCase.NoticeId).FirstOrDefault();
                Guid? targetUser = (UserDataService.GetDetail(db).Where(x => x.UserRole.UserSystemRoleId == (notice.NoticeType == ParentNoticeType.Civil ? UserSystemRole.AdvCivil : UserSystemRole.AdvCriminal) && (x.RegionId != null && x.RegionId == notice.RegionId))?.FirstOrDefault())?.UserId;
                if (targetUser != null)
                {
                    //Get all GovernmentDepartment connected users to add notifications to all of them
                    targetUserIds = GetGovernmentDepartmentUsers(notice);
                    string messageSummary = "New Case Created for Notice No " + notice.NoticeNo + " ";
                    string messageDetail = "A new case with case no <a href=\"/Case/CaseView/" + newCase.CaseId.ToString() + "\" target=\"_blank\" >" + newCase.PetitionNo + "</a> is created for Notice No: " + notice.NoticeNo + " .";
                    return CreateNotification(targetUser, messageSummary, messageDetail, "/Case/CaseView/" + newCase.CaseId.ToString());
                }
                //Log reason for returning false
                return false;
            }
            catch (Exception ex)
            {
                //Write log for error.
                return false;
            }
        }

        /// <summary>
        /// Create notification for new advocate assigned to a case or advocate is changed
        /// </summary>
        /// <param name="assignedCase">The case to which Advocate is assigned</param>
        /// <param name="oldAdvocateId">Optional. Only if advocate is changed, old advocate id to send notification for old advocate also</param>
        /// <see cref="Case"/>
        /// <seealso cref="Advocate.AdvocateId"/>
        /// <returns>success or failure of execution</returns>
        public bool CreateAdvocateAssignmentNotification(Case assignedCase, Guid? oldAdvocateId)
        {
            try
            {
                var notice = NoticeDataService.GetDetail(Controller.db).Where(x => x.NoticeId == assignedCase.NoticeId).FirstOrDefault();
                targetUserIds = GetGovernmentDepartmentUsers(notice);
                if (assignedCase.AdvocateId != null)
                {
                    HashId = assignedCase.AdvocateId;
                    string messageSummary = "", messageDetail = "";
                    if (oldAdvocateId != null)
                    {
                        //Create notification for Advocate changed
                        messageSummary = "Advocate changed for Case No " + assignedCase.PetitionNo + " ";
                        messageDetail = "Case No <a href=\"/Case/CaseView/" + assignedCase.CaseId.ToString() + "\" target=\"_blank\" >" + assignedCase.PetitionNo + "</a> for Notice No: " + notice.NoticeNo + " is assigned a new Advocate .";
                        CreateNotification(oldAdvocateId, messageSummary, messageDetail, "/Case/CaseView/" + assignedCase.CaseId.ToString());
                    }
                    //Create notification for new Advocate assigned
                    messageSummary = "Case No " + assignedCase.PetitionNo + " is assigned ";
                    messageDetail = "Case No <a href=\"/Case/CaseView/" + assignedCase.CaseId.ToString() + "\" target=\"_blank\" >" + assignedCase.PetitionNo + "</a> for Notice No: " + notice.NoticeNo + " is assigned .";

                    CreateNotification(assignedCase.AdvocateId, messageSummary, messageDetail, "/Case/CaseView/" + assignedCase.CaseId.ToString());
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }

        /// <summary>
        /// Create notification for new Government Department OIC assigned to a case or Government Department OIC is changed
        /// </summary>
        /// <param name="assignedCase">The case to which Government Department OIC is assigned</param>
        /// <param name="oldGovOICId">Optional. Only if advocate is changed, old advocate id to send notification for old advocate also</param>
        /// <param name="resp">The Respondent where the new Government Department OIC is to be assigned</param>
        /// <see cref="Case"/>
        /// <seealso cref="GovernmentDepartmentOIC.GovernmentdepartmentOICID"/>
        /// <returns>success or failure of execution</returns>
        public bool CreateGovOICAssignmentNotification(Case assignedCase, Guid? oldGovOICId, PartyAndRespondent resp)
        {
            try
            {
                var notice = NoticeDataService.GetDetail(Controller.db).Where(x => x.NoticeId == assignedCase.NoticeId).FirstOrDefault();
                targetUserIds = GetGovernmentDepartmentUsers(notice);

                if (resp.GovernmentdepartmentOICID != null)
                {
                    HashId = resp.GovernmentdepartmentOICID;
                    string messageSummary = "", messageDetail = "";
                    if (oldGovOICId != null)
                    {
                        //Create notification for Advocate changed
                        messageSummary = "Government Department OIC changed for Case No " + assignedCase.PetitionNo + " ";
                        messageDetail = "Case No <a href=\"/Case/CaseView/" + assignedCase.CaseId.ToString() + "\" target=\"_blank\" >" + assignedCase.PetitionNo + "</a> for Notice No: " + notice.NoticeNo + " is assigned a new Advocate .";
                        CreateNotification(oldGovOICId, messageSummary, messageDetail, "/Case/CaseView/" + assignedCase.CaseId.ToString());
                    }
                    //Create notification for new Advocate assigned
                    messageSummary = "Case No " + assignedCase.PetitionNo + " is assigned ";
                    messageDetail = "Case No <a href=\"/Case/CaseView/" + assignedCase.CaseId.ToString() + "\" target=\"_blank\" >" + assignedCase.PetitionNo + "</a> for Notice No: " + notice.NoticeNo + " is assigned .";

                    CreateNotification(resp.GovernmentdepartmentOICID, messageSummary, messageDetail, "/Case/CaseView/" + assignedCase.CaseId.ToString());
                    return true;
                }
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }

        /// <summary>
        /// Create Notification when a new case is created for a targeted user
        /// </summary>
        /// <param name="targetUser">The Guid of the user for whom the notification is to be created for</param>
        /// <param name="messageSummary">The summarised message that displays by default</param>
        /// <param name="messageDetail">The details if any for the message to display</param>
        /// <param name="routingValue">The routing value if any, or default value</param>
        /// <returns>success or failure of method call</returns>
        public bool CreateNotification(Guid? targetUser, string messageSummary, string messageDetail = "", string routingValue = "/UserNotifications/")
        {
            try
            {
                Source = NotificationSourceEnum.Case;
                Type = NotificationTypeEnum.System;
                Status = NotificationStatusEnum.IsCreated;
                this.RoutingValue = routingValue;
                //Create private method createNotification
                return createNotification(targetUser, messageSummary, messageDetail);
            }
            catch (Exception ex)
            {
                //Write log
                return false;
            }
        }

        /// <summary>
        /// Creates and saves new Notification as per given values
        /// </summary>
        /// <param name="targetUserId">This is required field. It represents the user for whom this notification is created</param>
        /// <param name="messageSummary">The message summary. This is required field</param>
        /// <param name="nsource">This is optional field and will take the source value as General</param>
        /// <param name="ntype">This is optional field and will take System by default</param>
        /// <param name="nstatus">This is optional field ans will take Created Status</param>
        /// <param name="messageDetails">This is optional field. Here you can add message in detail</param>
        /// <returns>Success result</returns>
        public bool CreateNotification(Guid? targetUserId, string messageSummary, NotificationSourceEnum nsource = NotificationSourceEnum.General, NotificationTypeEnum ntype = NotificationTypeEnum.System, NotificationStatusEnum nstatus = NotificationStatusEnum.IsCreated, string messageDetails = "", string RoutingValue = "/UserNotifications/")
        {
            try
            {
                if (messageSummary.Trim().Length == 0 || targetUserId == null)
                    return false;
                Source = nsource;
                Type = ntype;
                Status = nstatus;
                this.RoutingValue = RoutingValue;
                return CreateNotification(targetUserId, messageSummary, messageDetails);
            }
            catch (Exception ex)
            {
                //Write logs
                return false;
            }
        }

        /// <summary>
        /// This updated only the Status of the App Notification
        /// </summary>
        /// <param name="appNotificationId">The Guid for the AppNOtification to update. Required</param>
        /// <param name="newStatus">The new Status to update to</param>
        /// <returns>List of Notifications for the user</returns>
        public List<Models.AppNotificationsMvcModel> UpdateNotificationStatus(Guid? appNotificationId, NotificationStatusEnum newStatus)
        {
            return _UpdateNotificationStatus(appNotificationId, newStatus);
        }

        /// <summary>
        /// Method updates all data for the Notification
        /// </summary>
        /// <param name="appNotificationId">The Guid representing the Notification to update</param>
        /// <param name="newSource">Updates new Notificationn Source</param>
        /// <param name="newType">Updates new Notification Type</param>
        /// <param name="newStatus">Updates new Notification Status</param>
        /// <param name="messageSummary">Updates the message summary</param>
        /// <param name="messageDetail">Updates message details</param>
        /// <param name="targetUser">Updates the target user</param>
        /// <param name="RouteValue">Updates the Routing value if the Notification is to be targeted to a value</param>
        /// <returns></returns>
        public List<Models.AppNotificationsMvcModel> UpdateAppNotification(Guid? appNotificationId, NotificationSourceEnum newSource, NotificationTypeEnum newType, NotificationStatusEnum newStatus, string messageSummary, string messageDetail, Guid? targetUser, string RouteValue)
        {
            return _UpdateNotification(appNotificationId, newSource, newType, newStatus, messageSummary, messageDetail, targetUser, RouteValue);
        }

        #endregion

        #endregion

        #region private

        #region private constructors

        /// <summary>
        /// Private Constructor so no object can be directly created
        /// </summary>
        /// <param name="controller">The BaseController Type</param>
        private AppNotificationHelper(BaseMVCController controller)
        {
            Source = NotificationSourceEnum.General;
            Type = NotificationTypeEnum.System;
            Status = NotificationStatusEnum.IsCreated;
            RoutingValue = "/UserNotifications/";
            this.Controller = controller;
            this.sms_api_base_url = ConfigurationManager.AppSettings["SMSAPIBASEURL"];
            this.sms_api_url = ConfigurationManager.AppSettings["SMSAPIURL"];
            this.sms_api_user = ConfigurationManager.AppSettings["SMSAPIUSER"];
            this.sms_api_pass = ConfigurationManager.AppSettings["SMSAPIPASS"];
            this.sms_api_sender = ConfigurationManager.AppSettings["SMSAPISENDERID"];
        }

        /// <summary>
        /// Private Constructor so no object can be directly created
        /// </summary>
        private AppNotificationHelper() { }

        #endregion

        #region private methods

        private List<Guid> GetGovernmentDepartmentUsers(Notice notice)
        {
            //return UserDat9aService.GetLite(Controller.db).ToList()
            //                    .Join(notice.Respondents
            //                        , u => u.GovernmentDepartmentId, r => r.GovernmentDepartmentId
            //                        , (u, r) => u.UserId).ToList();

            List<Guid> DepUsers = new List<Guid>();
            var noticgovid = notice.Respondents.Select(x => x.GovernmentDepartmentId);
            foreach (var Loc in noticgovid)
            {
                var LocDepUsers = UserDataService.GetLite(Controller.db).Where(x => x.GovernmentDepartmentId == Loc)?.Select(x => x.UserId).ToList();
                DepUsers.AddRange(LocDepUsers);
            }
            return DepUsers;
        }
        private bool createNotification(Guid? targetUserId, string messageSummary, string messageDetails, bool sendAll = true)
        {
            try
            {
                //Add main notification to target user
                Controller.db.AppNotifications.Add(CreateNewAppNotification(targetUserId, messageSummary, messageDetails));

                //if sendAll then send all Respondent DepartmentId users.                  
                if (sendAll && targetUserIds != null && targetUserIds.Count > 0)
                {
                    foreach (Guid usrId in targetUserIds)
                    {
                        Controller.db.AppNotifications.Add(CreateNewAppNotification(usrId, messageSummary, messageDetails));
                    }
                }
                Controller.db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }

        private AppNotifications CreateNewAppNotification(Guid? targetUserId, string messageSummary, string messageDetails)
        {
            Guid UserId = new Guid();
            if (Controller.AppUser != null && Controller.AppUser.UserId != null)
            {
                UserId = Controller.AppUser.UserId;
            }
            else
            {
                UserId = UserDataService.GetDetail(Controller.db).Where(i => i.Name == "Admin").Select(u => u.UserId).FirstOrDefault();

            }
            return new AppNotifications()
            {
                Id = Guid.NewGuid(),
                Source = (int)Source,
                TargetUserId = targetUserId,
                NotificationType = (int)Type,
                Status = (int)Status,
                Message = messageSummary,
                Details = messageDetails,
                CreatedBy = UserId,
                CreatedOn = Extended.CurrentIndianTime,
                RouteValue = RoutingValue,
                HashId = this.HashId,
            };
        }

        private List<Models.AppNotificationsMvcModel> _UpdateNotificationStatus(Guid? appNotificationId, NotificationStatusEnum newStatus)
        {
            var data = GetItems();
            var model = Models.AppNotificationsMvcModel.CopyFromEntityList(data, 0);
            try
            {
                if (appNotificationId != null)
                {
                    var notification = data.Where(x => x.Id == appNotificationId).FirstOrDefault();
                    if (notification.Status != (int)newStatus)
                    {
                        notification.Status = (int)newStatus;
                        notification.ModifiedDate = Extended.CurrentIndianTime;
                        notification.CreatedBy = Controller.AppUser.UserId;
                        Controller.db.SaveChangesWithAudit(Controller.Request, Controller.AppUser.UserName);
                    }
                }
                model.Where(m => m.Id == appNotificationId).FirstOrDefault().ShowDefaultNotification = true;
            }
            catch (Exception ex)
            {
                
            }
            //Returning the model even after the exception so all other notifications can be displayed.
            return model;
        }
        private List<Models.AppNotificationsMvcModel> _UpdateNotification(Guid? appNotificationId, NotificationSourceEnum newSource, NotificationTypeEnum newType, NotificationStatusEnum newStatus, string messageSummary, string messageDetail, Guid? targetUser, string newRouteValue = "")
        {
            var data = GetItems();
            var model = Models.AppNotificationsMvcModel.CopyFromEntityList(data, 0);
            try
            {
                if (appNotificationId != null)
                {
                    var notification = data.Where(x => x.Id == appNotificationId).FirstOrDefault();
                    notification.Status = (int)newStatus;
                    notification.Source = (int)newSource;
                    notification.NotificationType = (int)newType;
                    notification.Message = messageSummary;
                    notification.Details = messageDetail;
                    notification.TargetUserId = targetUser;
                    notification.ModifiedDate = Extended.CurrentIndianTime;
                    notification.CreatedBy = Controller.AppUser.UserId;
                    if (newRouteValue.Trim().Length > 0)
                        notification.RouteValue = newRouteValue;
                    Controller.db.SaveChangesWithAudit(Controller.Request, Controller.AppUser.UserName);
                }
                model.Where(m => m.Id == appNotificationId).FirstOrDefault().ShowDefaultNotification = true;
            }
            catch (Exception ex)
            {
                
            }
            //returns the model for any notification to display
            return model;
        }
        private List<AppNotifications> GetItems()
        {
            return AppNotificationsDataService.GetLite(Controller.db)
                .Where(x => x.TargetUserId == Controller.AppUser.UserId)
                .OrderBy(o => o.Status)
                    .ThenByDescending(o => o.CreatedOn).ToList();
        }
        #endregion

        #endregion
    }

    public enum NotificationSourceEnum
    {
        General = 1,
        Case = 2,
        Notice = 3
    }

    public enum NotificationTypeEnum
    {
        System = 1,
        Email = 2,
        SMS = 3
    }

    public enum NotificationStatusEnum
    {
        IsCreated = 1,
        IsDelivered = 2,
        IsRead = 3
    }
}