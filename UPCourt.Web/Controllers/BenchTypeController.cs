﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class BenchTypeController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanView)]
        // GET: BenchType
        public ActionResult Index()
        {
            return View(BenchTypeMvcModel.CopyFromEntityList(BenchTypeDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanView)]
        // GET: BenchType/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = BenchTypeDataService.GetLite(db).Where(i => i.BenchTypeID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(BenchTypeMvcModel.CopyFromEntity(dbData, 0));
        }

        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanCreate)]
        // GET: BenchType/Create
        public ActionResult Create()
        {
            BenchTypeMvcModel model = new BenchTypeMvcModel();
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanCreate)]
        // POST: BenchType/Create
        [HttpPost]
        public ActionResult Create(BenchTypeMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var count = BenchTypeDataService.GetLite(db).Where(i => i.Name == model.Name).FirstOrDefault();
            if (count != null)
            {
                ModelState.AddModelError(nameof(model.Name), "This Name Already Exist");
                return View(model);
            }
            BenchType tempBenchType = new BenchType();
            tempBenchType.BenchTypeID = Guid.NewGuid();
            tempBenchType.DisplayOrder = MasterBusinessService.GetBenchTypeDisplayOrder(db);
            InitializeData(tempBenchType, model, true);

            db.BenchType.Add(tempBenchType);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanEdit)]
        // GET: BenchType/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = BenchTypeDataService.GetLite(db).Where(i => i.BenchTypeID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(BenchTypeMvcModel.CopyFromEntity(dbData, 0));
        }

        [PageRoleAuthorizarion(PageName.BenchType, PagePermissionType.CanEdit)]
        // POST: Bench/Edit/5
        [HttpPost]
        public ActionResult Edit(BenchTypeMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = BenchTypeDataService.GetLite(db).Where(i => i.BenchTypeID == model.BenchTypeID).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitializeData(dbData, model, false);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(Guid id)
        {
            var dbData = BenchTypeDataService.GetLite(db).Where(i => i.BenchTypeID == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
        private void InitializeData(BenchType tempBenchType, BenchTypeMvcModel model, bool IsNew)
        {
            tempBenchType.Name = model.Name;
            tempBenchType.IsActive = model.IsActive;
            tempBenchType.Description = model.Description;
            tempBenchType.NumberOfJudge = model.NumberOfJudge;
            tempBenchType.ModifiedBy = AppUser.UserId;
            if (!IsNew)
            {
                tempBenchType.BenchTypeID = model.BenchTypeID;
            }
        }
    }
}
