﻿using System;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class AGDepartment : BaseMasterEntity
    {
        public Guid AGDepartmentId { get; set; }
        public string Name { get; set; }
        public string DepartmentHeadName { get; set; }
        public DepartmentStatus Status { get; set; }
        public bool IsDefault { get; set; }
        public User user { get; set; }
    }
    public enum DepartmentStatus
    {
        Active = 1,
        Inactive = 0
    }
}
