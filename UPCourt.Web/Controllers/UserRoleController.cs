﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class UserRoleController : BaseMVCController
    {
        // GET: Role
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(UserRoleMvcModel.CopyFromEntityList(UserRoleDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        // GET: Role/Create
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new UserRolePermissionMvcModel() { Status = UserRoleStatus.Active };
            var allPages = PagePermissionDataService.GetLite(db).ToList().OrderBy(F => F.PageName.GetCustomAttribute<PagePermissionAttribute>().ParentMenue.GetEnumDisplayAttribute().Order).ThenBy(G => G.PageName.GetCustomAttribute<PagePermissionAttribute>().DisplayIndex);
            model.PagePermissions = PagePermissionMvcModel.CopyFromEntityList(allPages, 0);
            model.Status = UserRoleStatus.Active;
            return View(model);
        }

        // POST: UserRole/Create
        [HttpPost]
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanCreate)]
        public ActionResult Create(UserRolePermissionMvcModel model)
        {

            if (!ModelState.IsValid) { return View(model); }
            UserRole tempData = new UserRole()
            {
                UserRoleId = Guid.NewGuid(),
                Name = model.Name,
                Status = model.Status,
                IsSytemCreated = false,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetUserRolesDisplayOrder(db)
            };

            if (!CommonValidationInsertUpdate(model, null)) { return View(model); }

            if (!CommonSaveChanges(tempData, model)) { return View(model); }

            db.UserRoles.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Create");
        }

        // GET: Country1/Edit/5
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {

            var dbData = UserRoleDataService.GetDetail(db).Where(i => i.UserRoleId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            var UserRole = UserRoleMvcModel.CopyFromEntity(dbData, 0);
            UserRolePermissionMvcModel model = Extended.CopyX<UserRoleMvcModel, UserRolePermissionMvcModel>(UserRole);

            var allPages = PagePermissionDataService.GetLite(db).ToList().OrderBy(F => F.PageName.GetCustomAttribute<PagePermissionAttribute>().ParentMenue.GetEnumDisplayAttribute().Order).ThenBy(G => G.PageName.GetCustomAttribute<PagePermissionAttribute>().DisplayIndex);
            var pagePer = PagePermissionMvcModel.CopyFromEntityList(allPages, 0);
            foreach (var page in pagePer)
            {
                var userPage = model.UserRolePagePermissions?.FirstOrDefault(i => i.PagePermissionId == page.PagePermissionId);
                foreach (var permisson in page.PermissionTypes.ToList())
                {
                    var AllowedPermission = userPage?.PermittedPermissionTypes ?? new List<PagePermissionType>();
                    foreach (var userPer in AllowedPermission.ToList())
                    {
                        if (permisson == userPer)
                        {
                            var current = page.PermissionTypesDestructed.ToList().Find(F => F.Value == userPer.ToString());
                            current.IsSelected = true;
                        }
                    }
                }
            }
            model.PagePermissions = pagePer;

            return View(model);
        }

        // POST: Country1/Edit/5
        [HttpPost]
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanEdit)]
        public ActionResult Edit(UserRolePermissionMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = UserRoleDataService.GetDetail(db).Where(i => i.UserRoleId == model.UserRoleId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonValidationInsertUpdate(model, model.UserRoleId)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Status = model.Status;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            if (!CommonSaveChanges(dbData, model)) { return View(model); }
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = UserRoleDataService.GetLite(db).Where(i => i.UserRoleId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }

        // GET: UserRole/Details/5
        [PageRoleAuthorizarion(PageName.UserRole, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = UserRoleDataService.GetDetail(db).Where(i => i.UserRoleId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            var UserRole = UserRoleMvcModel.CopyFromEntity(dbData, 0);
            UserRolePermissionMvcModel model = Extended.CopyX<UserRoleMvcModel, UserRolePermissionMvcModel>(UserRole);

            var allPages = PagePermissionDataService.GetLite(db).ToList().OrderBy(F => F.PageName.GetCustomAttribute<PagePermissionAttribute>().ParentMenue.GetEnumDisplayAttribute().Order).ThenBy(G => G.PageName.GetCustomAttribute<PagePermissionAttribute>().DisplayIndex);
            var pagePer = PagePermissionMvcModel.CopyFromEntityList(allPages, 0);
            foreach (var page in pagePer)
            {
                var userPage = model.UserRolePagePermissions?.FirstOrDefault(i => i.PagePermissionId == page.PagePermissionId);
                foreach (var permisson in page.PermissionTypes.ToList())
                {
                    var AllowedPermission = userPage?.PermittedPermissionTypes ?? new List<PagePermissionType>();
                    foreach (var userPer in AllowedPermission.ToList())
                    {
                        if (permisson == userPer)
                        {
                            var current = page.PermissionTypesDestructed.ToList().Find(F => F.Value == userPer.ToString());
                            current.IsSelected = true;
                        }
                    }
                }
            }
            model.PagePermissions = pagePer;

            return View(model);
        }

        private bool CommonValidationInsertUpdate(UserRolePermissionMvcModel model, Guid? UserRoleId)
        {
            var tempUserRole = db.UserRoles.FirstOrDefault(i => i.Name == model.Name && (!UserRoleId.HasValue || i.UserRoleId != UserRoleId.Value));
            if (tempUserRole != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Duplicate Role");
                rModel.Message = "UserRole name already exists";
                return false;
            }
            var dbPagePermissions = PagePermissionDataService.GetLite(db).ToList();
            for (int index = 0; index < model.PagePermissions.Count; index++)
            {
                var tempPage = model.PagePermissions[index];
                var dbPage = dbPagePermissions.FirstOrDefault(i => i.PagePermissionId == tempPage.PagePermissionId);
 if (dbPage == null)
                {
                    model.PagePermissions.RemoveAt(index);
                    continue;
                }

                foreach (var item in tempPage.PermissionTypesDestructed)
                {
                    var tempPermisionEnum = item.Value.ToEnum<PagePermissionType>();
                    if (!dbPage.PermissionTypes.Any(i => i == tempPermisionEnum))
                    {
                        ModelState.AddModelError(nameof(model.PagePermissions), $"Ivalid permission '{item.Name}' found in page '{tempPage.PageNameDisplay}");
                        rModel.Message = $"Ivalid permission '{item.Name}' found in page '{tempPage.PageNameDisplay}";
                        return false;
                    }
                }
            }
            return true;
        }
        private bool CommonSaveChanges(UserRole tempEntity, UserRolePermissionMvcModel model)
        {
            foreach (var item in model.PagePermissions)
            {
                var dbPermission = tempEntity.UserRolePagePermissions.FirstOrDefault(i => i.PagePermissionId == item.PagePermissionId);

                if (dbPermission == null)
                {
                    dbPermission = new UserRolePagePermission();
                    dbPermission.UserRolePagePermissionId = Guid.NewGuid();
                    dbPermission.UserRoleId = tempEntity.UserRoleId;
                    dbPermission.PagePermissionId = item.PagePermissionId;

                    tempEntity.UserRolePagePermissions.Add(dbPermission);
                }


                dbPermission.PermittedPermissionTypes = item.PermissionTypesDestructed
                                                        .Where(i => i.IsSelected)
                                                        .Select(i => i.Value.ToEnum<PagePermissionType>());

            }
            return true;
        }
    }
}
