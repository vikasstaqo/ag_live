﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class PagePermissionDataService
    {
        public static IQueryable<PagePermission> GetLite(DatabaseContext db)
        {
            return db.PagePermissions;
        }

        public static IQueryable<PagePermission> GetDetail(DatabaseContext db)
        {
            return db.PagePermissions
                .Include(i => i.UserRolePagePermissions)
                .Include(i => i.PermissionTypes);
        }
    }
}