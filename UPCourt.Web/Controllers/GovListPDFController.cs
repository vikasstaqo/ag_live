﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPCourt.Web.Controllers
{
    public class GovListPDFController : Controller
    {
        // GET: GovListPDF
        public ActionResult Index()
        {
            return View();
        }
        public FileResult OpenPDF()
        {
            return File(Server.MapPath("/PDF/Aptitude Formulas.pdf"), "application/pdf");
        }
    }
}