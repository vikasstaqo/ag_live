﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class AppNotificationsMvcModel : BaseEntityModel<Entity.AppNotifications, AppNotificationsMvcModel>
    {
        public Guid Id { get; set; }
     
        public int Source { get; set; }
        public Guid TargetUserId { get; set; }
        [Display(Name = "User Type")]
        public Guid UserType { get; set; }
        /// <summary>
        /// System = 1, Email = 2, SMS = 3
        /// </summary>
        [Display(Name = "Notification Type")]
        public int NotificationType { get; set; }
        [Display(Name = "Status")]
        public int Status { get; set; }
        [Display(Name = "Content")]
        public string Message { get; set; }
        public string Details { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Display(Name = "Route Value")]
        public string RouteValue { get; set; }
        [Display(Name = "Target Users")]
        public List<Guid> AssignedUsers { get; set; }
        [Display(Name = "Notification Type")]
        public bool ShowDefaultNotification { get; set; }
        public ICollection<Guid> NotificationTypeList { get; set; }
        public Guid? HashId { get; set; }

        public override AppNotificationsMvcModel EntityToResponse(Entity.AppNotifications entity, AppNotificationsMvcModel model)
        {
            return model;
        }
    }
}