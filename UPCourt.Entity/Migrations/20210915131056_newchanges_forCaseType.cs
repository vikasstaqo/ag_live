﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UPCourt.Entity.Migrations
{
    public partial class newchanges_forCaseType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Advocates_AdvocateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_NoticeCounselors_NoticeCounselorId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Petitioners_PetitionerId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Notices_NoticeCounselors_NoticeCounselorId",
                table: "Notices");

            migrationBuilder.DropForeignKey(
                name: "FK_Notices_Petitioners_PetitionerId",
                table: "Notices");

            migrationBuilder.DropForeignKey(
                name: "FK_PartyAndRespondents_Cases_CasePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropForeignKey(
                name: "FK_PartyAndRespondents_Cases_CaseRespondentId",
                table: "PartyAndRespondents");

            migrationBuilder.DropForeignKey(
                name: "FK_PartyAndRespondents_Notices_NoticePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropIndex(
                name: "IX_PartyAndRespondents_CasePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropIndex(
                name: "IX_PartyAndRespondents_CaseRespondentId",
                table: "PartyAndRespondents");

            migrationBuilder.DropIndex(
                name: "IX_PartyAndRespondents_NoticePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropIndex(
                name: "IX_Notices_NoticeCounselorId",
                table: "Notices");

            migrationBuilder.DropIndex(
                name: "IX_Notices_PetitionerId",
                table: "Notices");

            migrationBuilder.DropIndex(
                name: "IX_Cases_NoticeCounselorId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_NoticeId",
                table: "Cases");

            migrationBuilder.DropIndex(
                name: "IX_Cases_PetitionerId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "CasePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropColumn(
                name: "CaseRespondentId",
                table: "PartyAndRespondents");

            migrationBuilder.DropColumn(
                name: "NoticePartyId",
                table: "PartyAndRespondents");

            migrationBuilder.DropColumn(
                name: "NoticeBasicIndexId",
                table: "Notices");

            migrationBuilder.DropColumn(
                name: "NoticeCounselorId",
                table: "Notices");

            migrationBuilder.DropColumn(
                name: "PetitionerId",
                table: "Notices");

            migrationBuilder.DropColumn(
                name: "CourtBench",
                table: "Courts");

            migrationBuilder.DropColumn(
                name: "NoticeCounselorId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PetitionerId",
                table: "Cases");

            migrationBuilder.AddColumn<byte[]>(
                name: "DesignationId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "RegionId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFirst",
                table: "Petitioners",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "NoticeId",
                table: "Petitioners",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AddColumn<bool>(
                name: "IsFirst",
                table: "PartyAndRespondents",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<byte[]>(
                name: "TargetEntityType",
                table: "Notifications",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "CreationDate",
                table: "Notifications",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotificationType",
                table: "Notifications",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<byte[]>(
                name: "RegionId",
                table: "Notices",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AddColumn<byte[]>(
                name: "PetitionerId",
                table: "NoticeCounselors",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AddColumn<bool>(
                name: "IsAssignUser",
                table: "GovernmentDepartments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "BenchTypeID",
                table: "Courts",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AddColumn<byte[]>(
                name: "OldCourtId",
                table: "CaseTransfer",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "ParentCaseId",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Comments",
                table: "CaseHearings",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppNotifications",
                columns: table => new
                {
                    Id = table.Column<byte[]>(nullable: false),
                    Source = table.Column<int>(nullable: false),
                    TargetUserId = table.Column<byte[]>(nullable: true),
                    NotificationType = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true),
                    CreationDate = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<string>(nullable: true),
                    RouteValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppNotifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BenchType",
                columns: table => new
                {
                    BenchTypeID = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BenchType", x => x.BenchTypeID);
                });

            migrationBuilder.CreateTable(
                name: "CaseNoticeTypes",
                columns: table => new
                {
                    CaseNoticeTypeId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentCaseNoticeTypeId = table.Column<byte[]>(nullable: true),
                    IsParent = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifyDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseNoticeTypes", x => x.CaseNoticeTypeId);
                });

            migrationBuilder.CreateTable(
                name: "CaseStages",
                columns: table => new
                {
                    CaseStageId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifyDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseStages", x => x.CaseStageId);
                });

            migrationBuilder.CreateTable(
                name: "Judges",
                columns: table => new
                {
                    JudgeId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Judges", x => x.JudgeId);
                });

            migrationBuilder.CreateTable(
                name: "NatureOfDisposals",
                columns: table => new
                {
                    NatureOfDisposalId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifyDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NatureOfDisposals", x => x.NatureOfDisposalId);
                });

            migrationBuilder.CreateTable(
                name: "NotificationSource",
                columns: table => new
                {
                    SourceId = table.Column<int>(nullable: false),
                    SourceName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationSource", x => x.SourceId);
                });

            migrationBuilder.CreateTable(
                name: "NotificationsType",
                columns: table => new
                {
                    NotificationTypeId = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationsType", x => x.NotificationTypeId);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTargetUsers",
                columns: table => new
                {
                    EntityId = table.Column<int>(nullable: false),
                    EntityName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTargetUsers", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "RequestDocument",
                columns: table => new
                {
                    Id = table.Column<byte[]>(nullable: false),
                    CaseId = table.Column<byte[]>(nullable: true),
                    DocumentId = table.Column<byte[]>(nullable: true),
                    RequestedAt = table.Column<DateTime>(nullable: false),
                    RequestedBy = table.Column<byte[]>(nullable: true),
                    GivenAt = table.Column<DateTime>(nullable: true),
                    SharedBy = table.Column<byte[]>(nullable: true),
                    CommentRequestor = table.Column<string>(nullable: true),
                    GovDeptComment = table.Column<string>(nullable: true),
                    DocumentStatus = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true),
                    Document_Name = table.Column<string>(nullable: true),
                    DocumentPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestDocument", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestDocument_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Timelines",
                columns: table => new
                {
                    TimelineId = table.Column<byte[]>(nullable: false),
                    CaseId = table.Column<byte[]>(nullable: true),
                    NoticeId = table.Column<byte[]>(nullable: true),
                    EventType = table.Column<int>(nullable: false),
                    UserId = table.Column<byte[]>(nullable: false),
                    TimeOfEvent = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ParentTimelineId = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timelines", x => x.TimelineId);
                    table.ForeignKey(
                        name: "FK_Timelines_Timelines_ParentTimelineId",
                        column: x => x.ParentTimelineId,
                        principalTable: "Timelines",
                        principalColumn: "TimelineId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Timelines_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DocumentMaster",
                columns: table => new
                {
                    DocumentId = table.Column<byte[]>(nullable: false),
                    DocumentName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true),
                    RequestDocumentId = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentMaster", x => x.DocumentId);
                    table.ForeignKey(
                        name: "FK_DocumentMaster_RequestDocument_RequestDocumentId",
                        column: x => x.RequestDocumentId,
                        principalTable: "RequestDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_DesignationId",
                table: "Users",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RegionId",
                table: "Users",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Petitioners_NoticeId",
                table: "Petitioners",
                column: "NoticeId");

            migrationBuilder.CreateIndex(
                name: "IX_Notices_RegionId",
                table: "Notices",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeCounselors_PetitionerId",
                table: "NoticeCounselors",
                column: "PetitionerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Courts_BenchTypeID",
                table: "Courts",
                column: "BenchTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_NoticeId",
                table: "Cases",
                column: "NoticeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentMaster_RequestDocumentId",
                table: "DocumentMaster",
                column: "RequestDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestDocument_CaseId",
                table: "RequestDocument",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Timelines_ParentTimelineId",
                table: "Timelines",
                column: "ParentTimelineId");

            migrationBuilder.CreateIndex(
                name: "IX_Timelines_UserId",
                table: "Timelines",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Users_AdvocateId",
                table: "Cases",
                column: "AdvocateId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Courts_BenchType_BenchTypeID",
                table: "Courts",
                column: "BenchTypeID",
                principalTable: "BenchType",
                principalColumn: "BenchTypeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NoticeCounselors_Petitioners_PetitionerId",
                table: "NoticeCounselors",
                column: "PetitionerId",
                principalTable: "Petitioners",
                principalColumn: "PetitionerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notices_Regions_RegionId",
                table: "Notices",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "RegionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Petitioners_Notices_NoticeId",
                table: "Petitioners",
                column: "NoticeId",
                principalTable: "Notices",
                principalColumn: "NoticeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Designations_DesignationId",
                table: "Users",
                column: "DesignationId",
                principalTable: "Designations",
                principalColumn: "DesignationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Regions_RegionId",
                table: "Users",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "RegionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cases_Users_AdvocateId",
                table: "Cases");

            migrationBuilder.DropForeignKey(
                name: "FK_Courts_BenchType_BenchTypeID",
                table: "Courts");

            migrationBuilder.DropForeignKey(
                name: "FK_NoticeCounselors_Petitioners_PetitionerId",
                table: "NoticeCounselors");

            migrationBuilder.DropForeignKey(
                name: "FK_Notices_Regions_RegionId",
                table: "Notices");

            migrationBuilder.DropForeignKey(
                name: "FK_Petitioners_Notices_NoticeId",
                table: "Petitioners");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Designations_DesignationId",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Regions_RegionId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "AppNotifications");

            migrationBuilder.DropTable(
                name: "BenchType");

            migrationBuilder.DropTable(
                name: "CaseNoticeTypes");

            migrationBuilder.DropTable(
                name: "CaseStages");

            migrationBuilder.DropTable(
                name: "DocumentMaster");

            migrationBuilder.DropTable(
                name: "Judges");

            migrationBuilder.DropTable(
                name: "NatureOfDisposals");

            migrationBuilder.DropTable(
                name: "NotificationSource");

            migrationBuilder.DropTable(
                name: "NotificationsType");

            migrationBuilder.DropTable(
                name: "NotificationTargetUsers");

            migrationBuilder.DropTable(
                name: "Timelines");

            migrationBuilder.DropTable(
                name: "RequestDocument");

            migrationBuilder.DropIndex(
                name: "IX_Users_DesignationId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_RegionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Petitioners_NoticeId",
                table: "Petitioners");

            migrationBuilder.DropIndex(
                name: "IX_Notices_RegionId",
                table: "Notices");

            migrationBuilder.DropIndex(
                name: "IX_NoticeCounselors_PetitionerId",
                table: "NoticeCounselors");

            migrationBuilder.DropIndex(
                name: "IX_Courts_BenchTypeID",
                table: "Courts");

            migrationBuilder.DropIndex(
                name: "IX_Cases_NoticeId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "DesignationId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsFirst",
                table: "Petitioners");

            migrationBuilder.DropColumn(
                name: "NoticeId",
                table: "Petitioners");

            migrationBuilder.DropColumn(
                name: "IsFirst",
                table: "PartyAndRespondents");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "NotificationType",
                table: "Notifications");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Notices");

            migrationBuilder.DropColumn(
                name: "PetitionerId",
                table: "NoticeCounselors");

            migrationBuilder.DropColumn(
                name: "IsAssignUser",
                table: "GovernmentDepartments");

            migrationBuilder.DropColumn(
                name: "BenchTypeID",
                table: "Courts");

            migrationBuilder.DropColumn(
                name: "OldCourtId",
                table: "CaseTransfer");

            migrationBuilder.DropColumn(
                name: "ParentCaseId",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "Comments",
                table: "CaseHearings");

            migrationBuilder.AddColumn<byte[]>(
                name: "CasePartyId",
                table: "PartyAndRespondents",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "CaseRespondentId",
                table: "PartyAndRespondents",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "NoticePartyId",
                table: "PartyAndRespondents",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TargetEntityType",
                table: "Notifications",
                type: "int",
                nullable: false,
                oldClrType: typeof(byte[]));

            migrationBuilder.AddColumn<byte[]>(
                name: "NoticeBasicIndexId",
                table: "Notices",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "NoticeCounselorId",
                table: "Notices",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "PetitionerId",
                table: "Notices",
                type: "varbinary(16)",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.AddColumn<int>(
                name: "CourtBench",
                table: "Courts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<byte[]>(
                name: "NoticeCounselorId",
                table: "Cases",
                type: "varbinary(16)",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "PetitionerId",
                table: "Cases",
                type: "varbinary(16)",
                nullable: false,
                defaultValue: new byte[] {  });

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_CasePartyId",
                table: "PartyAndRespondents",
                column: "CasePartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_CaseRespondentId",
                table: "PartyAndRespondents",
                column: "CaseRespondentId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_NoticePartyId",
                table: "PartyAndRespondents",
                column: "NoticePartyId");

            migrationBuilder.CreateIndex(
                name: "IX_Notices_NoticeCounselorId",
                table: "Notices",
                column: "NoticeCounselorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notices_PetitionerId",
                table: "Notices",
                column: "PetitionerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_NoticeCounselorId",
                table: "Cases",
                column: "NoticeCounselorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_NoticeId",
                table: "Cases",
                column: "NoticeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_PetitionerId",
                table: "Cases",
                column: "PetitionerId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Advocates_AdvocateId",
                table: "Cases",
                column: "AdvocateId",
                principalTable: "Advocates",
                principalColumn: "AdvocateId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_NoticeCounselors_NoticeCounselorId",
                table: "Cases",
                column: "NoticeCounselorId",
                principalTable: "NoticeCounselors",
                principalColumn: "NoticeCounselorId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cases_Petitioners_PetitionerId",
                table: "Cases",
                column: "PetitionerId",
                principalTable: "Petitioners",
                principalColumn: "PetitionerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notices_NoticeCounselors_NoticeCounselorId",
                table: "Notices",
                column: "NoticeCounselorId",
                principalTable: "NoticeCounselors",
                principalColumn: "NoticeCounselorId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Notices_Petitioners_PetitionerId",
                table: "Notices",
                column: "PetitionerId",
                principalTable: "Petitioners",
                principalColumn: "PetitionerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PartyAndRespondents_Cases_CasePartyId",
                table: "PartyAndRespondents",
                column: "CasePartyId",
                principalTable: "Cases",
                principalColumn: "CaseId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PartyAndRespondents_Cases_CaseRespondentId",
                table: "PartyAndRespondents",
                column: "CaseRespondentId",
                principalTable: "Cases",
                principalColumn: "CaseId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PartyAndRespondents_Notices_NoticePartyId",
                table: "PartyAndRespondents",
                column: "NoticePartyId",
                principalTable: "Notices",
                principalColumn: "NoticeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
