﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class SubjectDataService
    {
        public static IQueryable<Subject> GetLite(DatabaseContext db)
        {
            return db.Subjects.Where(x => x.IsDeleted == false);
        }

        public static IQueryable<Subject> GetDetail(DatabaseContext db)
        {
            return GetLite(db).Include(i => i.CaseNoticeType);
        }
    }
}