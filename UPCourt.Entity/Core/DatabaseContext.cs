﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using UPCourt.Entity.Core;
using UPCourt.Entity.Core.Converter;

namespace UPCourt.Entity
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Advocate> Advocates { get; set; }
        public virtual DbSet<EntityChangeTrakerLog> EntityChangeTrakerLogs { get; set; }
        public virtual DbSet<Designation> Designations { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<AdvocateCourt> AdvocateCourts { get; set; }
        public virtual DbSet<Court> Courts { get; set; }
        public virtual DbSet<BenchType> BenchType { get; set; }
        public virtual DbSet<CourtOpenCloseLog> CourtOpenCloseLogs { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<AGDepartment> AGDepartments { get; set; }
        public virtual DbSet<GovernmentDepartment> GovernmentDepartments { get; set; }

        public virtual DbSet<GovernmentDepartmentAddress> GovernmentDepartmentAddresses { get; set; }
        public virtual DbSet<GovernmentDepartmentContactNo> GovernmentDepartmentContactNos { get; set; }
        public virtual DbSet<PagePermission> PagePermissions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserRolePagePermission> UserRolePagePermissions { get; set; }
        public virtual DbSet<UserLogInLog> UserLogInLogs { get; set; }
        public virtual DbSet<UserCase> UserCases { get; set; }
        public virtual DbSet<Notice> Notices { get; set; }
        public virtual DbSet<CaseNoticeType> CaseNoticeTypes { get; set; }
        public virtual DbSet<CaseStage> CaseStages { get; set; }
        public virtual DbSet<NatureOfDisposal> NatureOfDisposals { get; set; }
        public virtual DbSet<NoticeBasicIndex> NoticeBasicIndexs { get; set; }
        public virtual DbSet<PartyAndRespondent> PartyAndRespondents { get; set; }
        public virtual DbSet<NoticeCounselor> NoticeCounselors { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<Petitioner> Petitioners { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Case> Cases { get; set; }
        public virtual DbSet<CaseHearing> CaseHearings { get; set; }
        public virtual DbSet<CaseStatus> CaseStatuses { get; set; }
        public virtual DbSet<UserCredentialRequest> UserCredentialRequests { get; set; }
        public virtual DbSet<StateLawOfficer> StateLawOfficer { get; set; }
        public virtual DbSet<Judge> Judges { get; set; }
        public virtual DbSet<Timeline> Timelines { get; set; }
        public virtual DbSet<RequestDocument> RequestDocument { get; set; }
        public virtual DbSet<DocumentMaster> DocumentMaster { get; set; }
        public virtual DbSet<AppNotifications> AppNotifications { get; set; }
        public virtual DbSet<NotificationsType> NotificationsType { get; set; }
        public virtual DbSet<NotificationSource> NotificationSources { get; set; }
        public virtual DbSet<NotificationTargetUsers> NotificationTargetUsers { get; set; }
        public virtual DbSet<District> Districts { get; set; }

        public virtual DbSet<HStatus> HStatus { get; set; }

        public virtual DbSet<Division> Division { get; set; }

        public virtual DbSet<Tehsil> Tehsil { get; set; }

        public virtual DbSet<Holiday> Holiday { get; set; }

        //public virtual DbSet<Holidays> Holidays { get; set; }

        // public virtual DbSet<HolidayGov> HolidayGov { get; set; }

        public virtual DbSet<IPCSection> IPCSections { get; set; }
        public virtual DbSet<NoticeIPC> NoticeIPCs { get; set; }
        public virtual DbSet<PoliceStation> PoliceStations { get; set; }
        public virtual DbSet<GovernmentDesignation> GovernmentDesignations { get; set; }
        public virtual DbSet<GovernmentDepartmentOic> GovernmentDepartmentOics { get; set; }
        public virtual DbSet<CrimeDetail> CrimeDetails { get; set; }

        public virtual DbSet<CourtJudgeMapping> CourtJudgeMappings { get; set; }
        public virtual DbSet<SpecialCategory> SpecialCategorys { get; set; }
        public virtual DbSet<NoticeSpecialcategory> NoticeSpecialcategorys { get; set; }
        public virtual DbSet<Salutation> Salutations { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }
        public virtual DbSet<ImpungedAndSTDetail> ImpungedAndSTDetails { get; set; }
        public virtual DbSet<SubordinateCourt> SubordinateCourts { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Range> Ranges { get; set; }
        public virtual DbSet<ECourtFee> ECourtFees { get; set; }
        public virtual DbSet<ActType> ActTypes { get; set; }
        public virtual DbSet<NoticeActType> NoticeActTypes { get; set; }
        public virtual DbSet<ApplicationDocument> ApplicationDocuments { get; set; }

        //public virtual DbSet<StHoliday> StHoliday { get; set; }
        public virtual DbSet<ApplicationType> ApplicationTypes { get; set; }

        //  public virtual DbSet<HolidayList> HolidayList { get; set; }
        public virtual DbSet<CaseHearingRespondent> CaseHearingRespondents { get; set; }
        public virtual DbSet<CaseHearingGovAdvocate> CaseHearingGovAdvocates { get; set; }
        public virtual DbSet<CaseHearingResponse> CaseHearingResponses { get; set; }
        public virtual DbSet<GovernmentDepartmentDC> GovernmentDepartmentDC { get; set; }
        public virtual DbSet<GovernmentDepartmentDCContact> GovernmentDepartmentDCContacts { get; set; }

        public virtual DbSet<Purpose> Purposes { get; set; }
        public virtual DbSet<DisposalType> DisposalTypes { get; set; }
        public virtual DbSet<CauseListType> CauseListTypes { get; set; }
        public virtual DbSet<CauseList> CauseLists { get; set; }
        public virtual DbSet<DateWiseCauseList> DateWiseCauseLists { get; set; }
        public virtual DbSet<Category> Categorys { get; set; }
        public virtual DbSet<SubCategory> SubCategorys { get; set; }
        public virtual DbSet<Benche> Benches { get; set; }
        public virtual DbSet<BencheJudge> BencheJudges { get; set; }
        public virtual DbSet<Casedetail> Casedetails { get; set; }
        public virtual DbSet<CasedetailIa> Casedetailias { get; set; }
        public virtual DbSet<CasedetailPet> CasedetailPets { get; set; }
        public virtual DbSet<CasedetailResp> CasedetailResps { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            optionsBuilder.UseMySQL(connectionString);

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<StateLawOfficer>().HasKey(e => e.AdvId);
            modelBuilder.Entity<Notice>()
            .HasMany(a => a.Petitioners)
            .WithOne(b => b.NoticePetitioner)
           .HasForeignKey(e => e.NoticeId);


            modelBuilder.Entity<Notice>()
              .HasMany(a => a.Respondents)
              .WithOne(b => b.NoticeRespondent)
             .HasForeignKey(e => e.NoticeRespondentId);

            var enumConverter = new EnumListToStringValueConverter<PagePermissionType>();

            modelBuilder.Entity<PagePermission>().Property(e => e.PermissionTypes).HasConversion(enumConverter);
            modelBuilder.Entity<UserRolePagePermission>().Property(e => e.PermittedPermissionTypes).HasConversion(enumConverter);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Cascade;
            }

            foreach (var item in modelBuilder.Model.GetEntityTypes())
            {
                var p = item.FindPrimaryKey().Properties.FirstOrDefault(i => i.ValueGenerated != ValueGenerated.Never);
                if (p != null)
                {
                    p.ValueGenerated = ValueGenerated.Never;
                }

            }

        }

        public override int SaveChanges()
        {
            CheckEntityTracker();
            return base.SaveChanges();
        }

        public int SaveChangesWithAudit(HttpRequestBase httpRequest = null, string UserName = "Admin")
        {
            AuditTrail(httpRequest, UserName);
            return base.SaveChanges();
        }
        private void CheckEntityTracker()
        {
            var changes = this.ChangeTracker.Entries().Where(i => i.State != EntityState.Unchanged).ToList();
            var VirtualSessionId = Guid.NewGuid();
            var currentTimeStamp = Extended.CurrentIndianTime;

            foreach (var item in changes)
            {
                var tempETA = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerTableAttribute)) as EntityChangeTrackerTableAttribute;
                if (tempETA == null || !tempETA.EnableChangeLog) { continue; }

                var tempParentKeyAtt = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerParentKeyAttribute)) as EntityChangeTrackerParentKeyAttribute;
               
                Dictionary<string, object> KeyValues = new Dictionary<string, object>();
                Dictionary<string, object> CurrentValues = new Dictionary<string, object>();
                Dictionary<string, object> deltaChanges = new Dictionary<string, object>();
                string displayPropValue = null;
                string userNamePropValue = null;
                Dictionary<string, object> parentParentKeyValues = new Dictionary<string, object>();

                foreach (var property in item.Properties)
                {
                    var tempIgnoreAttribute = property.Metadata.PropertyInfo.GetCustomAttribute(typeof(EntityChangeTrackerIgnoreAttribute)) as EntityChangeTrackerIgnoreAttribute;

                    if (tempIgnoreAttribute != null && !tempIgnoreAttribute.KeepProp)
                    {
                        continue;
                    }

                    string propertyName = property.Metadata.Name;

                    if (tempETA.DisplayProp != null && tempETA.DisplayProp == propertyName)
                    {
                        displayPropValue = property.CurrentValue?.ToString();
                    }

                    if (tempETA.UserNameProp != null && tempETA.UserNameProp == propertyName)
                    {
                        userNamePropValue = property.CurrentValue?.ToString();
                    }

                    if (tempParentKeyAtt != null && tempParentKeyAtt.ParentPrimaryKeyList.Contains(propertyName) && property.CurrentValue != null)
                    {
                        parentParentKeyValues[propertyName] = property.CurrentValue;
                    }

                    if (property.Metadata.IsPrimaryKey())
                    {
                        KeyValues[propertyName] = property.CurrentValue;
                    }

                    Object propCurrentValueToWrite = property.CurrentValue;
                    if (tempIgnoreAttribute != null)
                    {
                        propCurrentValueToWrite = tempIgnoreAttribute.KeepPropDefaultValue;
                    }

                    CurrentValues[propertyName] = propCurrentValueToWrite;


                    IForeignKey foreignKey = null;
                    object NewFKNameValue = null;
                    PropertyInfo foreignKeyProp = null;
                    if (property.Metadata.IsForeignKey() && tempIgnoreAttribute == null && property.CurrentValue != null)
                    {
                        foreignKey = property.Metadata.GetContainingForeignKeys().Single();
                                                var dataNew = this.Find(foreignKey.PrincipalEntityType.ClrType, property.CurrentValue);
                        var tempAttribute = foreignKey.PrincipalEntityType.ClrType.GetCustomAttribute(typeof(EntityChangeTrackerTableAttribute)) as EntityChangeTrackerTableAttribute;
                                                if (tempAttribute == null)
                        {
                            throw new Exception($"All table should be covered by EntityChangeTrackerTableAttribute, missing in {foreignKey.PrincipalEntityType.DisplayName()}");
                            //continue;
                        }

                        if (tempAttribute.DisplayProp == null)
                        {
                            //This scenario should not be happen that and entity disply prop is null and it has direct forienkey, this prop will be only null in mapping table
                            continue;
                        }
                        foreignKeyProp = foreignKey.PrincipalEntityType.ClrType.GetProperty(tempAttribute.DisplayProp);
                        if (dataNew != null)
                        {
                            NewFKNameValue = foreignKeyProp.GetValue(dataNew);
                        }
                                                Dictionary<string, object> navigationData = new Dictionary<string, object>();
                        navigationData[tempAttribute.DisplayProp] = NewFKNameValue;
                        CurrentValues[foreignKey.DependentToPrincipal.Name] = navigationData;
                    }

                    if (property.IsModified)
                    {
                        Object OldFKName = null;
                        Object OldValue = property.OriginalValue;
                        if (tempIgnoreAttribute != null)
                        {
                            OldValue = tempIgnoreAttribute.KeepPropDefaultValue;
                        }
                        if (property.Metadata.IsForeignKey() && tempIgnoreAttribute == null && property.OriginalValue != null)
                        {                           
                            var dataOld = this.Find(foreignKey.PrincipalEntityType.ClrType, property.OriginalValue);
                                                        if (dataOld != null)
                            {
                                OldFKName = foreignKeyProp.GetValue(dataOld);
                            }
                        }
                        deltaChanges[propertyName] = new { Old = OldValue, OldFKName = OldFKName, New = propCurrentValueToWrite, NewFKName = NewFKNameValue };
                    }
                }
                if (item.State == EntityState.Modified && deltaChanges.Count == 0)
                {
                    continue;
                }

                var convertSetting = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };

                Guid? tempParentEntityId = null;
                if (!parentParentKeyValues.Count.IsZero())
                {
                    tempParentEntityId = Guid.Parse(parentParentKeyValues.First().Value.ToString());
                }

                var tempChange = new EntityChangeTrakerLog
                {
                    EntityChangeTrakerLogId = Guid.NewGuid(),
                    DisplayPropValue = displayPropValue,
                    TableName = item.Metadata.GetTableName(),
                    EntityId = Guid.Parse(KeyValues.Single().Value.ToString()),
                    EntryDate = currentTimeStamp,
                    CurrentData = CurrentValues.Count == 0 ? null : JsonConvert.SerializeObject(CurrentValues, convertSetting),
                    DeltaChanges = deltaChanges.Count == 0 ? null : JsonConvert.SerializeObject(deltaChanges, convertSetting),
                    EntryBy = userNamePropValue,
                    Action = item.State,
                    ParentEntityId = tempParentEntityId,
                    VirtualSessionId = VirtualSessionId
                };
                this.EntityChangeTrakerLogs.Add(tempChange);
            }
        }

        public void AuditTrail(HttpRequestBase httpRequest = null, string UserName = "Admin")
        {
            string browser = httpRequest.Browser.Browser;
            string ip = string.IsNullOrEmpty(httpRequest.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? httpRequest.ServerVariables["HTTP_X_FORWARDED_FOR"] : httpRequest.ServerVariables["REMOTE_ADDR"];
            if (this == null || httpRequest == null)
                return;
            var changes = this.ChangeTracker.Entries().Where(i => i.State != EntityState.Unchanged).ToList();
            var VirtualSessionId = Guid.NewGuid();
            var currentTimeStamp = Extended.CurrentIndianTime;

            foreach (var item in changes)
            {
                var tempETA = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerTableAttribute)) as EntityChangeTrackerTableAttribute;

                if (tempETA == null || !tempETA.EnableChangeLog)
                {
                    continue;
                }
                var tempParentKeyAtt = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerParentKeyAttribute)) as EntityChangeTrackerParentKeyAttribute;

                Dictionary<string, object> KeyValues = new Dictionary<string, object>();
                Dictionary<string, object> CurrentValues = new Dictionary<string, object>();
                Dictionary<string, object> deltaChanges = new Dictionary<string, object>();
                string displayPropValue = null;
                string userNamePropValue = null;
                Dictionary<string, object> parentParentKeyValues = new Dictionary<string, object>();

                foreach (var property in item.Properties)
                {
                    var tempIgnoreAttribute = property.Metadata.PropertyInfo.GetCustomAttribute(typeof(EntityChangeTrackerIgnoreAttribute)) as EntityChangeTrackerIgnoreAttribute;

                    //if ignore attribute is present no need to log prop name with default value like 'N/A'
                    if (tempIgnoreAttribute != null && !tempIgnoreAttribute.KeepProp)
                    {
                        continue;
                    }

                    string propertyName = property.Metadata.Name;

                    if (tempETA.DisplayProp != null && tempETA.DisplayProp == propertyName)
                    {
                        displayPropValue = property.CurrentValue?.ToString();
                    }

                    if (tempETA.UserNameProp != null && tempETA.UserNameProp == propertyName)
                    {
                        userNamePropValue = property.CurrentValue?.ToString();
                    }

                    if (tempParentKeyAtt != null && tempParentKeyAtt.ParentPrimaryKeyList.Contains(propertyName) && property.CurrentValue != null)
                    {
                        parentParentKeyValues[propertyName] = property.CurrentValue;
                    }

                    if (property.Metadata.IsPrimaryKey())
                    {
                        KeyValues[propertyName] = property.CurrentValue;
                    }

                    //if ignore attribute and need to log default value instead of actual
                    Object propCurrentValueToWrite = property.CurrentValue;
                    if (tempIgnoreAttribute != null)
                    {
                        propCurrentValueToWrite = tempIgnoreAttribute.KeepPropDefaultValue;
                    }

                    CurrentValues[propertyName] = propCurrentValueToWrite;

                    
                    if (property.IsModified)
                    {
                        // Object OldFKName = null;
                        Object OldValue = property.OriginalValue;
                        if (tempIgnoreAttribute != null)
                        {
                            OldValue = tempIgnoreAttribute.KeepPropDefaultValue;
                        }
                        #region          To Do for Forign Key
                        //if (property.Metadata.IsForeignKey() && tempIgnoreAttribute == null && property.OriginalValue != null)
                        //{
                        //    //var data = a1.PrincipalEntityType.FindPrimaryKey();                            
                        //    var dataOld = this.Find(foreignKey.PrincipalEntityType.ClrType, property.OriginalValue);

                        //    if (dataOld != null)
                        //    {
                        //        OldFKName = foreignKeyProp.GetValue(dataOld);
                        //    }
                        //}
                        #endregion

                        deltaChanges[propertyName] = new { Old = OldValue, New = propCurrentValueToWrite };
                    }
                }

                if (item.State == EntityState.Modified && deltaChanges.Count == 0)
                {
                    continue;
                }

                var convertSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

                Guid? tempParentEntityId = null;
                if (!parentParentKeyValues.Count.IsZero())
                {
                    tempParentEntityId = Guid.Parse(parentParentKeyValues.First().Value.ToString());
                }
                string table_name = item.Metadata.GetTableName() + "_audit";
                using (SqlDB mySqlDB = new SqlDB())
                {
                    string sqlQuery = @"SELECT 1 FROM information_schema.columns WHERE  TABLE_SCHEMA = '" + mySqlDB.DatabaseName + @"' and table_name='" + table_name + "';";
                    string ifexists = mySqlDB.ExecuteScalarString(sqlQuery);
                    if (string.IsNullOrEmpty(ifexists))
                    {
                        string create_table = @"CREATE TABLE `" + table_name + @"` (
                            `VirtualSessionId` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_general_ci', 
                            `action` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	                        `action_by` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	                        `browser` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	                        `ip` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	                        `log_date` DATETIME NULL DEFAULT NULL";
                        foreach (var i in CurrentValues)
                        {
                            create_table += ",`" + i.Key + "` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_general_ci' ";
                        }
                        create_table += @" )COLLATE = 'utf8_general_ci';";

                        mySqlDB.ExecuteScalarStringAsync(create_table);
                    }
                    else
                    {
                        //Check if all columns exists and add if not exists
                        string columns_all = "";
                        // get all column names comma separated
                        foreach (var i in CurrentValues) { columns_all += "'" + i.Key + "',"; }
                        columns_all = columns_all.Left(columns_all.Length - 1);
                        sqlQuery = "SELECT COLUMN_NAME FROM information_schema.columns WHERE  TABLE_SCHEMA = '" + mySqlDB.DatabaseName + "' and table_name='" + table_name + "' and COLUMN_NAME in (" + columns_all + ")";
                        //Get existing columns in table
                        var cols = mySqlDB.GetDataTable(sqlQuery);
                        //If the number of columns does not matches then create more columns.
                        if (cols.Rows.Count != CurrentValues.Count)
                        {
                            string[] data = cols.Rows.OfType<System.Data.DataRow>().Select(c => c[0].ToString()).ToArray();
                            foreach (var i in CurrentValues)
                            {
                                if (!data.Contains(i.Key))
                                {
                                    string alter_table = "ALTER TABLE `" + table_name + "` ADD `" + i.Key + "` VARCHAR(500) NULL DEFAULT NULL COLLATE 'utf8_general_ci' ";
                                    mySqlDB.ExecuteScalarStringAsync(alter_table);
                                }
                            }
                        }
                    }
                    string field_name = "VirtualSessionId,action,action_by,ip,browser,log_date";
                    string value = "'" + VirtualSessionId + "','" + item.State + "'," + "'" + UserName + "'," + "'" + ip + "'," + "'" + browser + "',now()";
                    foreach (var i in CurrentValues)
                    {
                        field_name += "," + i.Key;
                        value += ",'" + (i.Value == null ? "" : i.Value).ToString().Replace("'", "''") + "'";
                    }
                    string query = "insert into " + table_name + "(" + field_name + ")values(" + value + ");";
                    mySqlDB.ExecuteScalarStringAsync(query);
                }
            }
        }
    }


}
