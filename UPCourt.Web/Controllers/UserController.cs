﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class UserController : BaseMVCController
    {
        Paging pg = new Paging();
        IQueryable<User> dbData;
        List<UserSystemRole> rolenotin = new List<UserSystemRole> { UserSystemRole.AdvCivil, UserSystemRole.AdvCriminal,
             UserSystemRole.GovDeptHead , UserSystemRole.GovDeptDegUser , UserSystemRole.GovDeptOIC  };
        // GET: User
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanView)]
        public ActionResult Index(UserFilterMvcModel userFilterMvcModel)
        {
            InitilizeRegions(null);
            InitilizeDesignation(null);
            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(null);
            var model = new UserFilterMvcModel();
            SearchUserType(model);
            dbData = UserDataService.GetLite(db).Where(x => !rolenotin.Contains(x.UserRole.UserSystemRoleId)
            ).OrderByDescending(i => i.DisplayOrder);

            if (userFilterMvcModel.Name != null)
                dbData = dbData.Where(x => x.Name.ToLower().Trim() == userFilterMvcModel.Name.Trim().ToLower()).OrderBy(i => i.Name);

            if (userFilterMvcModel.Phone != null)
            {
                userFilterMvcModel.Phone = Regex.Replace(userFilterMvcModel.Phone, @"\s+", "");
                dbData = dbData.Where(x => x.Phone.Contains(userFilterMvcModel.Phone)).OrderBy(i => i.Name);
            }

            if (userFilterMvcModel.UserRoleId != null)
                dbData = dbData.Where(x => x.UserRoleId == userFilterMvcModel.UserRoleId).OrderBy(i => i.Name);

            if (userFilterMvcModel.AGDepartmentId != null)
                dbData = dbData.Where(x => x.AGDepartmentId == userFilterMvcModel.AGDepartmentId).OrderBy(i => i.Name);

            userFilterMvcModel.UserData = UserMvcModel.CopyFromEntityList(dbData, 0);

            return View(userFilterMvcModel);
        }

        [HttpPost]
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanCreate)]
        public ActionResult UsersFile(HttpPostedFileBase postedFile)
        {
            string filePath = string.Empty;
            if (postedFile != null)
            {
                if (Path.GetExtension(postedFile.FileName) == ".xlsx" || Path.GetExtension(postedFile.FileName) == ".xls")
                {
                    string path = Server.MapPath("~/Uploads/BulkUsers/");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    DataTable dt = ExcelImportHelper.ReadExcelFromFile(filePath);
                    try
                    {
                        var ObjAGDepartments = db.AGDepartments;
                        var ObjUserRoles = db.UserRoles;
                        var ObjRegions = db.Regions;

                        if (dt.Rows.Count > 0)
                        {
                            DataColumn dc2 = new DataColumn();
                            dc2.ColumnName = "UploadStatus";
                            dc2.DataType = typeof(string);

                            DataColumn dc3 = new DataColumn();
                            dc3.ColumnName = "Message";
                            dc3.DataType = typeof(string);
                            dt.Columns.AddRange(new DataColumn[] { dc2, dc3 });

                            int displayOrder = MasterBusinessService.GetUserDisplayOrder(db);

                            int RowsCount = 0;
                            foreach (DataRow row in dt.Rows)
                            {
                                try
                                {
                                    if (row["Password"].ToString().Length > 10)
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = "Password length is less than 10";
                                        continue;
                                    }
                                    if (row["Phone"].ToString().Length > 10)
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = "Phone length is less than 10";
                                        continue;
                                    }
                                    var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                                    if (regexItem.IsMatch(row["Name"].ToString()) == false)
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = "Invalid Name";
                                        continue;
                                    }
                                    var regexMobileNo = new Regex("^[0-9 ]*$");
                                    if (regexMobileNo.IsMatch(row["Phone"].ToString()) == false)
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = "Invalid Phone";
                                        continue;
                                    }

                                    var passWordSalt = PasswordHelper.GetPasswordSalt();
                                    var passwordHash = PasswordHelper.EncodePassword(row["Password"].ToString(), passWordSalt);


                                    User tempData = new User
                                    {
                                        UserId = Guid.NewGuid(),
                                        Name = row["Name"].ToString(),
                                        Email = row["Email"].ToString(),
                                        Address = row["Address"].ToString(),
                                        Phone = row["Phone"].ToString(),
                                        LoginId = row["Email"].ToString(),
                                        Status = row["Status"].ToString().ToLower() == "active" || row["Status"].ToString().ToLower() == "a" ? UserStatus.Active : UserStatus.Inactive,
                                        PasswordSalt = passWordSalt,
                                        PasswordHash = passwordHash,
                                        IsDefault = false,
                                        UserType = getUserType(row["UserType"].ToString()),
                                        AGDepartmentId = ObjAGDepartments.Where(x => x.Name.Trim() == row["AGDepartment"].ToString().Trim()).FirstOrDefault().AGDepartmentId,
                                        GovernmentDepartmentId = null,
                                        UserRoleId = ObjUserRoles.Where(x => x.Name.Trim() == row["Role"].ToString().Trim()).FirstOrDefault().UserRoleId,
                                        CreatedBy = AppUser.UserId,
                                        CreatedOn = DateTime.Now,
                                        DesignationId = null,
                                        RegionId = ObjRegions.Where(x => x.Name.Trim() == row["Region"].ToString().Trim()).FirstOrDefault().RegionId,
                                        DisplayOrder = displayOrder
                                    };
                                    var nameForCheck = tempData.Email.TrimX();
                                    var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.Email == nameForCheck);
                                    if (duplicateCheck == null)
                                    {
                                        try
                                        {
                                            db.Users.Add(tempData);
                                            db.SaveChangesWithAudit(Request, AppUser.UserName);
                                            displayOrder++;
                                            RowsCount++;
                                            row["UploadStatus"] = "Done";
                                            row["Message"] = "Inserted Successfully !!";
                                        }
                                        catch (Exception ex)
                                        {
                                            row["UploadStatus"] = "Not Done";
                                            row["Message"] = ex.Message;
                                        }
                                    }
                                    else
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = "Email : " + nameForCheck + "  Already exists !!";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    row["UploadStatus"] = "Not Done";
                                    row["Message"] = ex.Message;
                                }
                            }

                            DisplayMessage(MessageType.Success, CreatedMessage);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                dt.TableName = "UpdatedList";
                                var sheet1 = wb.Worksheets.Add(dt);
                                sheet1.Table(0).ShowAutoFilter = false;
                                sheet1.Table(0).Theme = XLTableTheme.None;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "UpdatedUserList.xlsx");
                                }
                            }

                        }
                        else
                        {
                            DisplayMessage(MessageType.Warning, $"No Record in Excel");
                        }
                    }
                    catch
                    {
                        DisplayMessage(MessageType.Error, $"Invalid file. Please download templet first.");
                    }
                }
                else
                {
                    DisplayMessage(MessageType.Error, $"Invalid file");
                }
            }
            return RedirectToAction("/");
        }

        private UserType getUserType(string userType)
        {
            switch (userType.ToLower().Trim())
            {
                case "aag":
                    return UserType.AAG;
                case "advocate":
                    return UserType.Advocate;
                case "ag":
                    return UserType.AG;
                case "agdepartment":
                    return UserType.AGDepartment;
                case "casestudyuser":
                    return UserType.CaseStudyUser;
                case "csc":
                    return UserType.CSC;
                case "governmentdepartment":
                    return UserType.GovernmentDepartment;
                case "usercredential":
                    return UserType.UserCrendial;
            }
            return UserType.UserCrendial;
        }

        private void SearchUserType(UserFilterMvcModel model)
        {
            model.UserTypeList = Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(F => new SelectListItem()
            {
                Text = F.GetDescription(),
                Value = F.ToInt().ToString()
            }).ToList();
        }


        // GET: User/Create
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            InitilizeRegions(null);
            InitilizeDesignation(null);

            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(null);
            var model = new UserMvcModel();
            model.Status = UserStatus.Active;
            InitilizeUserType(model, null);
            return View(model);
        }

        // POST: Country1/Create
        [HttpPost]
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanCreate)]
        public ActionResult Create(UserMvcModel model)
        {
            InitilizeRegions(model.RegionId);
            InitilizeUserRoles(model.UserRoleId);
            InitilizeAGDepartments(model.AGDepartmentId);
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            InitilizeDesignation(model.DesignationId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.Phone != null && model.Phone != "")
            {
                var count = UserDataService.GetDetail(db).Where(i => i.Phone == model.Phone).FirstOrDefault();
                if (count != null)
                {
                    ModelState.AddModelError(nameof(model.Phone), "This Moblie Number Already Registered");
                    return View(model);
                }
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            if (model.UserType == UserType.AGDepartment)
            {
                if (model.AGDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is required");
                    return View(model);
                }
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            
            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            User tempData = new User();
            tempData.UserId = Guid.NewGuid();
            tempData.Name = model.Name;
            tempData.Email = model.Email;
            tempData.Address = model.Address;
            tempData.Phone = model.Phone;
            tempData.LoginId = model.Email;
            tempData.Status = model.Status;
            tempData.PasswordSalt = passWordSalt;
            tempData.PasswordHash = passwordHash;
            tempData.IsDefault = model.IsDefault;
            tempData.UserType = model.UserType;
            tempData.AGDepartmentId = model.AGDepartmentId;
            tempData.GovernmentDepartmentId = model.GovernmentDepartmentId;
            tempData.UserRoleId = model.UserRoleId;
            tempData.CreatedBy = AppUser.UserId;
            tempData.CreatedOn = DateTime.Now;
            tempData.DesignationId = model.DesignationId;
            tempData.RegionId = model.RegionId;
            tempData.DisplayOrder = MasterBusinessService.GetUserDisplayOrder(db);
            db.Users.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Create");
        }

        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = UserDataService.GetLite(db).Where(i => i.UserId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }

        public void InitilizeRegions(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(mvc, nameof(RegionMvcModel.RegionId)
                , nameof(RegionMvcModel.Name), selectedValue);
        }
        public void InitilizeDesignation(object selectedValue)
        {
            var dbData = DesignationDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = DesignationResponseModel.CopyFromEntityList(dbData, 0);
            ViewBag.Designations = new SelectList(mvc, nameof(DesignationResponseModel.DesignationId)
                , nameof(DesignationResponseModel.Name), selectedValue);
        }



        public void InitilizeUserRoles(object selectedValue)
        {

            var dbData = UserRoleDataService.GetLite(db).Where(x => !rolenotin.Contains(x.UserSystemRoleId) && x.Status == UserRoleStatus.Active).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRoleMvcModel.Name), selectedValue);
        }
        public void InitilizeAGDepartments(object selectedValue)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(x => x.Status == DepartmentStatus.Active).OrderBy(i => i.Name).ToList();
            var mvc = AGDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AGDepartments = new SelectList(mvc, nameof(AGDepartmentMvcModel.AGDepartmentId)
                , nameof(AGDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).Where(x => x.Status == GovtDeparmentStatus.Active).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeUserType(UserMvcModel model, int? Id)
        {
            model.UserTypeList = Enum.GetValues(typeof(UserType)).Cast<UserType>().Where(x => x.ToInt() != 4 && x.ToInt() != 6).Select(F => new SelectListItem()
            {
                Text = F.GetDescription(),
                Value = F.ToInt().ToString(),
                Selected = Id.HasValue ? Id.Value == F.ToInt() : false
            }).ToList();
        }
        // GET: Country1/Edit/5
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeRegions(dbData.RegionId);
            InitilizeDesignation(dbData.DesignationId);
            InitilizeUserRoles(dbData.UserRoleId);
            InitilizeAGDepartments(dbData.AGDepartmentId);
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            var model = UserMvcModel.CopyFromEntity(dbData, 0);
            InitilizeUserType(model, Convert.ToInt32(model.UserType));
            return View(model);
        }

        // POST: Country1/Edit/5
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(UserMvcModel model)
        {
            InitilizeRegions(model.RegionId);
            InitilizeDesignation(model.DesignationId);
            InitilizeUserRoles(model.UserRoleId);
            InitilizeAGDepartments(model.AGDepartmentId);
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == model.UserId).FirstOrDefault();

            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            if (model.UserType == UserType.AGDepartment)
            {
                if (model.AGDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is required");
                    return View(model);
                }
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            if (model.UserType == UserType.GovernmentDepartment)
            {
                if (model.GovernmentDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is required");
                    return View(model);
                }
                if (model.AGDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            dbData.Name = model.Name;
            dbData.Email = model.Email;
            dbData.Address = model.Address;
            dbData.Phone = model.Phone;
            dbData.LoginId = model.Email;
            dbData.Status = model.Status;
            //If password changed
            if (!model.Password.IsNullOrEmpty())
            {
                var passwordSalt = PasswordHelper.GetPasswordSalt();
                var passwordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt);

                dbData.PasswordSalt = passwordSalt;
                dbData.PasswordHash = passwordHash;
            }
            dbData.IsDefault = model.IsDefault;
            dbData.UserType = model.UserType;
            dbData.UserRoleId = model.UserRoleId;
            dbData.AGDepartmentId = model.AGDepartmentId;
            dbData.GovernmentDepartmentId = model.GovernmentDepartmentId;
            dbData.DesignationId = model.DesignationId;
            dbData.RegionId = model.RegionId;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        private bool CommonForInsertUpdate(UserMvcModel model)
        {
            var emailForCheck = model.Email.TrimX();
            var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.UserId != model.UserId && i.Email == emailForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), "Email already exists");
                return false;
            }
            return true;
        }
        // GET: User/Details/5
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeRegions(dbData.RegionId);
            InitilizeDesignation(dbData.DesignationId);
            InitilizeUserRoles(dbData.UserRoleId);
            InitilizeAGDepartments(dbData.AGDepartmentId);
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            return View(UserMvcModel.CopyFromEntity(dbData, 0));
        }
    }
}
