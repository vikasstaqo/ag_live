﻿namespace UPCourt.Entity
{
    using System;
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class CourtJudgeMapping : BaseTransEntity
    {
        public Guid CourtJudgeMappingID { get; set; }
        public Guid CourtId { get; set; }
        public string Name { get; set; }
        public Guid BenchTypeID { get; set; }
        public Guid JudgeId { get; set; }
        public bool IsActive { get; set; }
    }
}
