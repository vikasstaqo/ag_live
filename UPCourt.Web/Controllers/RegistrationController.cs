﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    [AllowAnonymous]
    public class RegistrationController : BaseMVCController
    {

        public ActionResult Index()
        {
            var dbData = UserDataService.GetLite(db).ToList();
            return View();
        }
        

        public ActionResult Create()
        {
            UserMvcModelExternal model = new UserMvcModelExternal();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(UserMvcModelExternal model)
        {
            var dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.LoginId == model.LoginId);
            if (dbUser != null)
            {
                ModelState.AddModelError(nameof(UserMvcModelExternal.LoginId), "User already exist");
                return View(model);
            }
            string RoleName;
            if (model.LoginAs == LoginAS.InPerson) { RoleName = "Guest"; }
            else { RoleName = "Guest"; }

            var dbRole = UserRoleDataService.GetLite(db).Where(e => e.Name == RoleName).FirstOrDefault();
            var passwordSalt = PasswordHelper.GetPasswordSalt();

            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            User objUser = new User
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Aadhar = model.Aadhar,
                Email = model.Email,
                Gender = model.Gender.ToString(),
                Phone = model.Phone,
                LoginId = model.LoginId,
                PasswordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt),
                PasswordSalt = passwordSalt,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                Status = UserStatus.Active,
                UserRoleId = dbRole.UserRoleId,
                UserType = UserType.AG
            };
            db.Users.Add(objUser);
            db.SaveChangesWithAudit(Request, "");

            SucessMessage = $"User '{objUser.Name}' created successfully";

            TempData["ViewBagMsg"] = SucessMessage;
                      return RedirectToAction("PublicLogin", "Login");
                    }
        private bool CommonForInsertUpdate(UserMvcModelExternal model)
        {
            return true;
        }
    }
}