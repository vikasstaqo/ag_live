﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class BenchTypeDataService
    {
        public static IQueryable<BenchType> GetAll(DatabaseContext db)
        {
            return db.BenchType;
        }
        public static IQueryable<BenchType> GetLite(DatabaseContext db)
        {
            return db.BenchType.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<BenchType> GetDetail(DatabaseContext db)
        {
            return GetLite(db);
        }
    }
}