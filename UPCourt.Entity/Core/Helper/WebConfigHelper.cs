﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public static class WebConfigHelper
    {
        public static String ApplicationUrl
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(ApplicationUrl)];
            }
        }
        public static String MailAttachmentFolder
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailAttachmentFolder)];
            }
        }
        public static String SengGridApiToken
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(SengGridApiToken)];
            }
        }
        public static String FromEmail
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(FromEmail)];
            }
        }
        public static bool SendMailUsingSendGrid
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(SendMailUsingSendGrid)].ToBool();
            }
        }

        public static string MailSMTPHost
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailSMTPHost)];
            }
        }
        public static int MailSMTPPort
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailSMTPPort)].ToInt();
            }
        }
        public static bool MailEnableSsl
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailEnableSsl)].ToBool();
            }
        }
        public static string MailCredentialUserName
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailCredentialUserName)];
            }
        }
        public static string MailCredentialPassword
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(MailCredentialPassword)];
            }
        }
        public static string BulkUploadPath
        {
            get
            {
                string FullPath = string.Empty;
                try
                {
                    string RelativePath = ConfigurationManager.AppSettings[nameof(BulkUploadPath)];
                    FullPath = System.Web.HttpContext.Current.Server.MapPath(RelativePath);
                    if (!System.IO.Directory.Exists(FullPath))
                    {
                        System.IO.Directory.CreateDirectory(FullPath);
                    }
                }
                catch { }
                return FullPath;
            }
        }
        public static string DownloadPath
        {
            get
            {
                string RelativePath = ConfigurationManager.AppSettings[nameof(DownloadPath)];
                return System.Web.HttpContext.Current.Server.MapPath(RelativePath);
            }
        }
        public static int? DocumentMaxSizeInMB
        {
            get
            {
                return ConfigurationManager.AppSettings[nameof(DocumentMaxSizeInMB)].ToIntN();
            }
        }
    }
}
