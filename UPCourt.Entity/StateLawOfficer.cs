﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public class StateLawOfficer : BaseMasterEntity
    {
        public StateLawOfficer() { }
        public Guid AdvId { get; set; }
        public string AdvName { get; set; }
        public string AdvPost { get; set; }
        public string AdvLocation { get; set; }

        public string AdvEnrl { get; set; }
        public string AdvAor { get; set; }
    }
}
