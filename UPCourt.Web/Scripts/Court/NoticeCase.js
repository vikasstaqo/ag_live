﻿$(function () {

    $("#NoticeDataTable").DataTable({

        "bFilter": false,
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
        "ordering": true,
        "order": [],
    }).buttons().container().appendTo('#NoticeDataTable_wrapper .col-md-6:eq(0)');


    $("#NoticeToDate").change(function () {
        var startDate = $("#NoticeFromDate").val();
        var endDate = $("#NoticeToDate").val();
        var startYear = new Date($("#NoticeFromDate").val()).getFullYear()
        var endYear = new Date($("#NoticeToDate").val()).getFullYear()
        if (startYear > 2000 && endYear > 2000 && Date.parse(endDate) < Date.parse(startDate)) {
            ToastError("End date should not be less than Start date");
            $("#NoticeToDate").val('');
        }
    });

})

function showParentNoticeTypeDetails(obj) {

    $.ajax({
        type: "Get",
        async: false,
        url: "/NoticeCreate/GetNoticeTypes",
        data: { "pid": $(obj).val() },
        success: function (response) {
            var NoticeType = JSON.parse(response);
            $("#NoticeTypeId").empty();
            $("#NoticeTypeId").append('<option value>--Select--</option>');
            NoticeType.forEach(function (obj, idx) {
                $("#NoticeTypeId").append($("<option></option>").val(obj.CaseNoticeTypeId).html(obj.Name));
            })
        }
    });

}

function CheckCaseNo(CaseNotext) {

    var CaseNo = $(CaseNotext).val();
    if ($.trim(CaseNo) != '') {
        $.ajax({
            type: "Get",
            url: "/NoticeCase/CheckCaseNo/",
            data: { "CaseNo": CaseNo },
            success: function (response) {
                var Case = JSON.parse(response);
                if (Case.PetitionNo == CaseNo) {
                    ToastError("Case No : '" + CaseNo + "' already exists!!");
                    $(CaseNotext).val('');
                }
            }
        });
    }
    else {
        $(CaseNotext).val('');
    }
}

function ConfirmDelete(NoticeNo, NoticeId) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete Notice No '" + NoticeNo + "' !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      
        if (result.isConfirmed) {
            window.location.replace("/NoticeCase/Delete/" + NoticeId);
        }
    });
}
function ResetControl() {
    $("#NoticeNo").val("");
    $("#NoticeFromDate").val("");
    $("#NoticeToDate").val("");
    $('#NoticeTypeId').val("");
    $('#GovermentDepartmentId').val("");
    $("#Petitioner").val("");
    $("#Counselor").val("");
    $("#Respondent").val("");

}
