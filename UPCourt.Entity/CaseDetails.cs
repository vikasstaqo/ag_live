﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Case_no), UserNameProp: nameof(CreatedBy))]
    public class CaseDetail : BaseMasterEntity
    {
        public CaseDetail()
        {
            IA = new HashSet<CaseDetailIA>();
        }
        public Guid CaseDetailId { get; set; }
        public Guid NoticeId { get; set; }
        public Notice Notice { get; set; }
        public string PetName { get; set; }
        public int BenchType { get; set; }
        public int Cs_subject { get; set; }
        public int C_subject { get; set; }
        public int DispNature { get; set; }
        public int PurposeNext { get; set; }
        public string JudgeCode { get; set; }
        public DateTime DateOfDecision { get; set; }
        public string ResName { get; set; }
        public int CaseStateCode { get; set; }
        public int CaseDistCode { get; set; }
        public string Case_no { get; set; }
        public int CauselistType { get; set; }
        public int BranchId { get; set; }
        public int Status { get; set; }
        public ICollection<CaseDetailIA> IA { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ia_regno), UserNameProp: nameof(CreatedBy))]
    public class CaseDetailIA : BaseMasterEntity
    {
        public Guid CaseDetailIAId { get; set; }
        public Guid CaseDetailId { get; set; }
        public CaseDetail CaseDetail { get; set; }
        public int ia_regno { get; set; }
        public DateTime date_of_ia_registration { get; set; }
        public DateTime date_of_hearing { get; set; }
        public int court_no { get; set; }
        public string judge_code { get; set; }
        public int ia_regyear { get; set; }
        public DateTime date_of_order { get; set; }
        public int Status { get; set; }

    }

}
