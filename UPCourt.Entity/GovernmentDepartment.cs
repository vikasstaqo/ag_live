﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifiedBy))]
    public class GovernmentDepartment :BaseMasterEntity
    {
        public GovernmentDepartment()
        {
            GovernmentDepartmentContactNos = new HashSet<GovernmentDepartmentContactNo>();
            GovernmentDepartmentAddresses = new HashSet<GovernmentDepartmentAddress>();
            PartyAndRespondents = new HashSet<PartyAndRespondent>();
            GovernmentDesignations = new HashSet<GovernmentDesignation>();
        }
        public Guid GovernmentDepartmentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HeadOffice { get; set; }
        public string POCName { get; set; }
        public string POCContact { get; set; }
        public bool IsDefault { get; set; }
        public bool IsAssignUser { get; set; }
        public string POCFaxNo { get; set; }
        public string POCMobileNo { get; set; }
        public GovtDeparmentStatus Status { get; set; }

        public ICollection<GovernmentDepartmentContactNo> GovernmentDepartmentContactNos { get; set; }
        public ICollection<GovernmentDepartmentAddress> GovernmentDepartmentAddresses { get; set; }
        public ICollection<PartyAndRespondent> PartyAndRespondents { get; set; }
        public ICollection<GovernmentDesignation> GovernmentDesignations { get; set; }

        public User User { get; set; }
    }
    public enum GovtDeparmentStatus
    {
        Active = 1,
        Inactive = 0
    }
}
