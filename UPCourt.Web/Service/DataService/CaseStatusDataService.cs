﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CaseStatusDataService
    {
        public static IQueryable<CaseStatus> GetLite(DatabaseContext db)
        {
            return db.CaseStatuses.Where(x => x.IsDeleted == false); ;
        }
    }
}