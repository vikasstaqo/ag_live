﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Document_Name), UserNameProp: nameof(ModifyBy))]
    public class RequestDocument
    {
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Id { get; set; }
        public Guid? CaseId { get; set; }
        public Guid? DocumentId { get; set; }
        public DateTime RequestedAt { get; set; }
        public Guid? RequestedBy { get; set; }
        public DateTime? GivenAt { get; set; }
        public DateTime? LastSubmitDate { get; set; }
        public Guid? SharedBy { get; set; }
        public string CommentRequestor { get; set; }
        public string GovDeptComment { get; set; }
        public int DocumentStatus { get; set; }
        public string ModifyBy { get; set; }
        public string Document_Name { get; set; }
        public string DocumentPath { get; set; }
        public Case Case { get; set; }
        public ICollection<DocumentMaster> DocumentMaster { get; set; }
    }
}
