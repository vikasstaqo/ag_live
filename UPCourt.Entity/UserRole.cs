﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class UserRole : BaseMasterEntity
    {
        public UserRole()
        {
            this.UserRolePagePermissions = new HashSet<UserRolePagePermission>();
            this.Users = new HashSet<User>();
        }
        public Guid UserRoleId { get; set; }
        public string Name { get; set; }
        public UserRoleStatus Status { get; set; }
        public bool IsSytemCreated { get; set; }
        public UserSystemRole UserSystemRoleId { get; set; }
        public ICollection<UserRolePagePermission> UserRolePagePermissions { get; set; }
        public ICollection<User> Users { get; set; }
    }

    public enum UserRoleStatus
    {
        Inactive = 0,
        Active = 1,
    }

    public enum UserSystemRole
    {
        Admin = 1,
        AGAdmin = 2,
        Guest = 3,
        GovDeptHead = 4,
        GovDeptDegUser = 5,
        GovDeptOIC = 6,
        AdvCriminal = 7,
        AdvCivil = 8,
        DataEntryOperator = 9,
        CSC = 10,
        CGA = 11,
    }
}