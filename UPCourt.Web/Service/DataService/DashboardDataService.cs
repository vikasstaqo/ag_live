﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Models;

namespace UPCourt.Web.Service.DataService
{
    public class DashboardDataService
    {
        public static IQueryable<Notice> GetFilteredNotices(DatabaseContext db, Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, string HitFrom = "")
        {
            IQueryable<Notice> NoticeList = GetNoticeDetail(db);

            return CheckNoticess(UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, NoticeList);
        }

        public static IQueryable<Notice> GetDataEntryNotices(DatabaseContext db, Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, DateTime thisWeekStart, DateTime thisWeekEnd)
        {
            return CheckNoticess(UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, db.Notices);
        }

        private static IQueryable<Notice> CheckNoticess(Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, IQueryable<Notice> NoticeList)
        {


            if (UserSystemRoleId == UserSystemRole.Admin || UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                NoticeList = NoticeList.Where(x => x.RegionId == TempRegionId);
            }
            if (UserSystemRoleId == UserSystemRole.CSC || UserSystemRoleId == UserSystemRole.AdvCivil)
            {
                NoticeList = NoticeList.Where(x => x.NoticeType == ParentNoticeType.Civil);
            }
            else if (UserSystemRoleId == UserSystemRole.CGA || UserSystemRoleId == UserSystemRole.AdvCriminal)
            {
                NoticeList = NoticeList.Where(x => x.NoticeType == ParentNoticeType.Criminal);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptHead && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentDepartmentId == GovernmentDepartmentId).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptDegUser && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentDesignationId == DesignationId).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptOIC && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentdepartmentOICID == GovermentDepartmentOICID).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.DataEntryOperator)
            {
                NoticeList = NoticeList.Where(x => x.CreatedBy == UserId);
            }
            if (UserSystemRoleId == UserSystemRole.AdvCriminal || UserSystemRoleId == UserSystemRole.AdvCivil)
            {
                NoticeList = NoticeList.Where(x => x.Cases.Where(y => y.AdvocateId == UserId).Count() > 0);
            }
            return NoticeList.AsQueryable();
        }

        private static IQueryable<Notice> GetNoticeDetail(DatabaseContext db)
        {
            var notices = db.Notices.Where(i => i.IsDeleted == false)
                .Include(i => i.Cases).ThenInclude(i => i.CaseStatus)
                .Include(i => i.Cases).ThenInclude(i => i.CaseHearings).ThenInclude(i => i.CaseHearingRespondents)
                .Include(i => i.Cases).ThenInclude(i => i.CaseHearings).ThenInclude(i => i.CaseHearingResponses)
                .Include(i => i.Petitioners)
                .Include(i => i.Counselors)
                .Include(i => i.Respondents).ThenInclude(i => i.GovernmentDepartment)
                .Include(i => i.CaseNoticeType)
                .Include(i => i.CrimeDetail).ThenInclude(x => x.PoliceStation)
                .Include(i => i.NoticeSpecialcategorys)
                .Include(i => i.Group)
                .Include(i => i.Region);
            return notices;
        }
    }

    public class DataEntryNoticeCounts
    {
        public int TodayNotice { get; set; }
        public int WeeklyNotice { get; set; }
        public int MonthNotice { get; set; }
        public int YearlyNotice { get; set; }
    }



}