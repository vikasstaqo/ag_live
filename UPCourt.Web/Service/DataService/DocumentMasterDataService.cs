﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class DocumentMasterDataService
    {
        public static IQueryable<DocumentMaster> GetLite(DatabaseContext db)
        {
            var data = db.DocumentMaster;
            return data;
        }

        public static string GetName(Guid? docid)
        {
            DatabaseContext db = new DatabaseContext();
            var data = GetLite(db).Where(x => x.DocumentId == docid).FirstOrDefault();
            if (data != null)
                return data.DocumentName;
            else
                return "";
        }
    }
}