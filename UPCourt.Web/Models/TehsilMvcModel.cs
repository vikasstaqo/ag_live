﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class TehsilMvcModel : BaseEntityModel<Tehsil, TehsilMvcModel>
    {
        public Guid TehsilId { get; set; }
        public string TehsilName { get; set; }
        public TehsilStatus Status { get; set; }
    }
}