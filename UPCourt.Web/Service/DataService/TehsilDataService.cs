﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class TehsilDataService
    {
        public static IQueryable<Tehsil> GetLite(DatabaseContext db)
        {
            return db.Tehsil.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Tehsil> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}