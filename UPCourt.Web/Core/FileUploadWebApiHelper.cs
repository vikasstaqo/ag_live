﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Core
{
    public static class FileUploadWebApiHelper
    {
        const int OneMB = 1024 * 1024;
        const float MaxFileSizeInMB = 2;

        static IList<string> ValidImageFileExtensions = new List<string> { "jpg", "gif", "png", "jpeg" };
        static IList<string> ValidDocFileExtensions = new List<string> { "doc", "docx" };
        static IList<string> ValidExcelFileExtensions = new List<String> { "xlsx", "xls" };
        static IList<string> ValidPdfFileExtensions = new List<String> { "pdf" };
        const string ImageFolderPath = "/Shared/UploadImages/";

        public static Document UploadFileIfValid(BaseResultModel rModel, HttpPostedFile postedFile, DocumentFileType[] FileTypes, float? MaxSixeInMb = null)
        {
            //Assigning default
            if (!MaxSixeInMb.HasValue)
            {
                MaxSixeInMb = WebConfigHelper.DocumentMaxSizeInMB ?? MaxFileSizeInMB;
            }

            if (postedFile == null || postedFile.ContentLength <= 0)
            {
                rModel.Message = "Invalid file";
                return null;
            }

            var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
            var extension = ext.ToLower().Replace(".", "");

            List<string> allowedExtensions = new List<string>();
            foreach (var FileType in FileTypes)
            {
                switch (FileType)
                {
                    case DocumentFileType.Image:
                        allowedExtensions.AddRange(ValidImageFileExtensions);
                        break;
                    case DocumentFileType.Doc:
                        allowedExtensions.AddRange(ValidDocFileExtensions);
                        break;
                    case DocumentFileType.Excel:
                        allowedExtensions.AddRange(ValidExcelFileExtensions);
                        break;
                    case DocumentFileType.Pdf:
                        allowedExtensions.AddRange(ValidPdfFileExtensions);
                        break;
                    case DocumentFileType.Any:
                        allowedExtensions.AddRange(ValidImageFileExtensions);
                        allowedExtensions.AddRange(ValidDocFileExtensions);
                        allowedExtensions.AddRange(ValidExcelFileExtensions);
                        allowedExtensions.AddRange(ValidPdfFileExtensions);
                        break;
                    default:
                        break;
                }
            }

            if (!allowedExtensions.Contains(extension))
            {
                rModel.Message = "Please upload " + allowedExtensions.Join(',') + " extension files";
                return null;

            }


            if (postedFile.ContentLength > (OneMB * MaxSixeInMb))
            {
                rModel.Message = "Please upload a file upto " + MaxSixeInMb + " mb";
                return null;
            }



            var newFileInfo = GetServerPathNewFile(extension);

            postedFile.SaveAs(newFileInfo.Item1);


            var result = new Document
            {
                Name = postedFile.FileName,
                DocumentUrl = newFileInfo.Item3,
                Size = postedFile.ContentLength,
                DocumentId = Guid.NewGuid()
            };

            return result;
        }


        public static string GetServerFilePathFromUrl(String imageUrl)
        {
            var fileName = imageUrl;

            //if network image
            if (imageUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)
               || imageUrl.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {
                Uri uri = new Uri(imageUrl);
                fileName = uri.Segments[uri.Segments.Length - 1];
            }

            var MapablePath = ImageFolderPath + fileName;

           return HttpContext.Current.Server.MapPath("~" + MapablePath);
        }

        public static bool DeleteIfFileExists(String imageUrl)
        {

            var mapPath = GetServerFilePathFromUrl(imageUrl);
            if (!System.IO.File.Exists(mapPath))
            {
                return false;
            }
            try
            {
                System.IO.File.Delete(mapPath);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// First property full server path 
        /// Second property relative server path
        /// third provides full http path
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static ThreeProperty<String, String, string> GetServerPathNewFile(string extension)
        {
            string tempFileName;
            string MapablePath;
            string ServerPath;

            string UniqueFileName = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 24);
            tempFileName = UniqueFileName + "." + extension;
            MapablePath = ImageFolderPath + tempFileName;

            ServerPath = HttpContext.Current.Server.MapPath("~" + MapablePath);

            var f = new FileInfo(ServerPath);

            if (!Directory.Exists(f.DirectoryName))
            {
                Directory.CreateDirectory(f.DirectoryName);
            }

            ThreeProperty<string, string, string> result = new ThreeProperty<string, string, string>();
            result.Item1 = ServerPath;
            result.Item2 = MapablePath;
            result.Item3 = WebConfigHelper.ApplicationUrl + MapablePath;

            return result;
        }
    }
}