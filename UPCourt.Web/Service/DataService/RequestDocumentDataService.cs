﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class RequestDocumentDataService
    {
        public static IQueryable<RequestDocument> GetLite(DatabaseContext db)
        {
            return db.RequestDocument.Include(F => F.Case);
        }
        public static IQueryable<RequestDocument> GetFiltered(DatabaseContext db, User LoginUser)
        {
            List<RequestDocument> RequestDocumentList = GetLite(db).ToList();
            if ((LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptHead || LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptDegUser || LoginUser.UserRole.UserSystemRoleId == UserSystemRole.GovDeptOIC) && LoginUser.GovernmentDepartmentId != null)
            {
                RequestDocumentList = RequestDocumentList.Where(x => x.SharedBy == LoginUser.GovernmentDepartmentId).ToList();
            }
            return RequestDocumentList.AsQueryable();
        }

        public static IQueryable<RequestDocument> GetDocuments(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new Entity.RequestDocument
                {
                    Id = i.Id,
                    Document_Name = i.Document_Name,
                    Case = i.Case,
                    RequestedAt = i.RequestedAt,
                    GivenAt = i.GivenAt,
                    SharedBy = i.SharedBy,
                    CommentRequestor = i.CommentRequestor,
                    GovDeptComment = i.GovDeptComment,
                    DocumentStatus = i.DocumentStatus,
                    ModifyBy = i.ModifyBy,
                    DocumentPath = i.DocumentPath,
                    DocumentId = i.DocumentId,
                    CaseId = i.CaseId,
                    RequestedBy = i.RequestedBy
                });

         
            return retval;
        }
    }
}