﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CaseStageController : BaseMVCController
    {
        // GET: CaseStage
        public ActionResult Index()
        {
            var dbData = CaseStageDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder).ToList();
            return View(CaseStageMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = CaseStageDataService.GetLite(db).Where(i => i.CaseStageId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CaseStageMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Court/Create
        public ActionResult Create()
        {
            CaseStageMvcModel model = new CaseStageMvcModel();
            return View(model);
        }

        // POST: Court/Create
        [HttpPost]
        public ActionResult Create(CaseStageMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CaseStage tempCaseStage = new CaseStage();
            tempCaseStage.CaseStageId = Guid.NewGuid();
            tempCaseStage.DisplayOrder = MasterBusinessService.GetCaseStagesDisplayOrder(db);
            InitializeData(tempCaseStage, model, true);

            db.CaseStages.Add(tempCaseStage);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            //db.SaveChanges();
            SucessMessage = $"Court {tempCaseStage.Name} created successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create");
        }

        // GET: AGDepartment/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = CaseStageDataService.GetLite(db).Where(i => i.CaseStageId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CaseStageMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: CaseStatus/Edit/5
        [HttpPost]
        public ActionResult Edit(CaseStageMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = CaseStageDataService.GetLite(db).Where(i => i.CaseStageId == model.CaseStageId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitializeData(dbData, model, false);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Court {dbData.Name} update successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Index");
        }

        private void InitializeData(CaseStage tempCaseStage, CaseStageMvcModel model, bool IsNew)
        {
            tempCaseStage.Name = model.Name;
            tempCaseStage.IsActive = model.IsActive;
            tempCaseStage.ModifiedOn = DateTime.Now;
            tempCaseStage.ModifiedBy = AppUser.UserId;
            if(!IsNew)
            {
                tempCaseStage.CaseStageId = model.CaseStageId;
            }
        }

        public ActionResult Delete(Guid id)
        {
            var dbData = CaseStageDataService.GetLite(db).Where(i => i.CaseStageId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, $"Case Stage '{ dbData.Name }' Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("Index");
        }
    }
}
