﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class JudgeController : BaseMVCController
    {
        // GET: Judge
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(JudgeResponseModel.CopyFromEntityList(JudgeDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        // GET: Judge/Create
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new JudgeResponseModel();
            model.Status = JudgeStatus.Active;
            return View(model);
        }
        [HttpPost]
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanCreate)]
        public ActionResult Create(JudgeResponseModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Judge tempData = new Judge()
            {
                JudgeId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Status = model.Status,
                Phone = model.Phone,
                Address = model.Address,
                ModifiedBy = AppUser.UserId,
                DisplayOrder = MasterBusinessService.GetJudgesDisplayOrder(db)
            };
            db.Judges.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = JudgeDataService.GetDetail(db).Where(i => i.JudgeId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(JudgeResponseModel.CopyFromEntity(dbData, 0));
        }
        [HttpPost]
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanEdit)]
        public ActionResult Edit(JudgeResponseModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = JudgeDataService.GetDetail(db).Where(i => i.JudgeId == model.JudgeId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Email = model.Email;
            dbData.Status = model.Status;
            dbData.Phone = model.Phone;
            dbData.Address = model.Address;
            dbData.ModifiedBy = AppUser.UserId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = JudgeDataService.GetLite(db).Where(i => i.JudgeId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.Judge, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = JudgeDataService.GetLite(db).Where(i => i.JudgeId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(JudgeResponseModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(JudgeResponseModel model)
        {
            var duplicateCheck = JudgeDataService.GetLite(db).FirstOrDefault(i => i.JudgeId != model.JudgeId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }
            return true;
        }

    }
}
