﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CourtDataService
    {
        public static IQueryable<Court> GetLite(DatabaseContext db)
        {
            return db.Courts.Where(x => x.IsDeleted == false); ;
        }
    
        public static IQueryable<Court> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.CourtOpenCloseLogs)
                .Include(i => i.BenchType);
        }
    }
}