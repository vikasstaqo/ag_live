﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UPCourt.Entity.Migrations
{
    public partial class dbm2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("SET default_storage_engine=INNODB");
            migrationBuilder.CreateTable(
                name: "AGDepartments",
                columns: table => new
                {
                    AGDepartmentId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DepartmentHeadName = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AGDepartments", x => x.AGDepartmentId);
                });

            migrationBuilder.CreateTable(
                name: "CaseStatuses",
                columns: table => new
                {
                    CaseStatusId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HexColorCode = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseStatuses", x => x.CaseStatusId);
                });

            migrationBuilder.CreateTable(
                name: "Courts",
                columns: table => new
                {
                    CourtId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    CourtBench = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courts", x => x.CourtId);
                });

            migrationBuilder.CreateTable(
                name: "Designations",
                columns: table => new
                {
                    DesignationId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Designations", x => x.DesignationId);
                });

            migrationBuilder.CreateTable(
                name: "EntityChangeTrakerLogs",
                columns: table => new
                {
                    EntityChangeTrakerLogId = table.Column<byte[]>(nullable: false),
                    TableName = table.Column<string>(nullable: true),
                    DisplayPropValue = table.Column<string>(nullable: true),
                    EntityId = table.Column<byte[]>(nullable: false),
                    DeltaChanges = table.Column<string>(nullable: true),
                    CurrentData = table.Column<string>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    EntryBy = table.Column<string>(nullable: true),
                    Action = table.Column<int>(nullable: false),
                    ParentEntityId = table.Column<byte[]>(nullable: true),
                    VirtualSessionId = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntityChangeTrakerLogs", x => x.EntityChangeTrakerLogId);
                });

            migrationBuilder.CreateTable(
                name: "GovernmentDepartments",
                columns: table => new
                {
                    GovernmentDepartmentId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    HeadOffice = table.Column<string>(nullable: true),
                    POCName = table.Column<string>(nullable: true),
                    POCContact = table.Column<string>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GovernmentDepartments", x => x.GovernmentDepartmentId);
                });

            migrationBuilder.CreateTable(
                name: "NoticeCounselors",
                columns: table => new
                {
                    NoticeCounselorId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    RollNo = table.Column<string>(nullable: true),
                    EnrollmentNo = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NoticeCounselors", x => x.NoticeCounselorId);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationId = table.Column<byte[]>(nullable: false),
                    SourceEntityId = table.Column<byte[]>(nullable: false),
                    SourceEntityType = table.Column<int>(nullable: false),
                    TargetEntityId = table.Column<byte[]>(nullable: true),
                    TargetEntityType = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                });

            migrationBuilder.CreateTable(
                name: "PagePermissions",
                columns: table => new
                {
                    PagePermissionId = table.Column<byte[]>(nullable: false),
                    PermissionTypes = table.Column<string>(nullable: true),
                    PageName = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PagePermissions", x => x.PagePermissionId);
                });

            migrationBuilder.CreateTable(
                name: "Petitioners",
                columns: table => new
                {
                    PetitionerId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    IDProof = table.Column<int>(nullable: false),
                    IDProofNumber = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Petitioners", x => x.PetitionerId);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    RegionId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.RegionId);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsSytemCreated = table.Column<bool>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleId);
                });

            migrationBuilder.CreateTable(
                name: "CourtOpenCloseLogs",
                columns: table => new
                {
                    CourtOpenCloseLogId = table.Column<byte[]>(nullable: false),
                    CourtId = table.Column<byte[]>(nullable: false),
                    CloseFromDate = table.Column<DateTime>(nullable: true),
                    CloseToDate = table.Column<DateTime>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourtOpenCloseLogs", x => x.CourtOpenCloseLogId);
                    table.ForeignKey(
                        name: "FK_CourtOpenCloseLogs_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "CourtId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Advocates",
                columns: table => new
                {
                    AdvocateId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Status = table.Column<byte>(nullable: false),
                    DesignationId = table.Column<byte[]>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Advocates", x => x.AdvocateId);
                    table.ForeignKey(
                        name: "FK_Advocates_Designations_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "Designations",
                        principalColumn: "DesignationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GovernmentDepartmentAddresses",
                columns: table => new
                {
                    GovernmentDepartmentAddressId = table.Column<byte[]>(nullable: false),
                    GovernmentDepartmentId = table.Column<byte[]>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    IsHeadOfficeAddress = table.Column<bool>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GovernmentDepartmentAddresses", x => x.GovernmentDepartmentAddressId);
                    table.ForeignKey(
                        name: "FK_GovernmentDepartmentAddresses_GovernmentDepartments_Governme~",
                        column: x => x.GovernmentDepartmentId,
                        principalTable: "GovernmentDepartments",
                        principalColumn: "GovernmentDepartmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GovernmentDepartmentContactNos",
                columns: table => new
                {
                    GovernmentDepartmentContactNoId = table.Column<byte[]>(nullable: false),
                    GovernmentDepartmentId = table.Column<byte[]>(nullable: false),
                    ContactNo = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GovernmentDepartmentContactNos", x => x.GovernmentDepartmentContactNoId);
                    table.ForeignKey(
                        name: "FK_GovernmentDepartmentContactNos_GovernmentDepartments_Governm~",
                        column: x => x.GovernmentDepartmentId,
                        principalTable: "GovernmentDepartments",
                        principalColumn: "GovernmentDepartmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notices",
                columns: table => new
                {
                    NoticeId = table.Column<byte[]>(nullable: false),
                    PetitionerId = table.Column<byte[]>(nullable: false),
                    NoticeCounselorId = table.Column<byte[]>(nullable: true),
                    NoticeType = table.Column<int>(nullable: false),
                    SerialNo = table.Column<int>(nullable: false),
                    NoticeNo = table.Column<string>(nullable: true),
                    NoticeDate = table.Column<DateTime>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    IsContextOrder = table.Column<bool>(nullable: false),
                    NoticeBasicIndexId = table.Column<byte[]>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notices", x => x.NoticeId);
                    table.ForeignKey(
                        name: "FK_Notices_NoticeCounselors_NoticeCounselorId",
                        column: x => x.NoticeCounselorId,
                        principalTable: "NoticeCounselors",
                        principalColumn: "NoticeCounselorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Notices_Petitioners_PetitionerId",
                        column: x => x.PetitionerId,
                        principalTable: "Petitioners",
                        principalColumn: "PetitionerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRolePagePermissions",
                columns: table => new
                {
                    UserRolePagePermissionId = table.Column<byte[]>(nullable: false),
                    UserRoleId = table.Column<byte[]>(nullable: false),
                    PagePermissionId = table.Column<byte[]>(nullable: false),
                    PermittedPermissionTypes = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRolePagePermissions", x => x.UserRolePagePermissionId);
                    table.ForeignKey(
                        name: "FK_UserRolePagePermissions_PagePermissions_PagePermissionId",
                        column: x => x.PagePermissionId,
                        principalTable: "PagePermissions",
                        principalColumn: "PagePermissionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRolePagePermissions_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    LoginId = table.Column<string>(nullable: true),
                    PasswordSalt = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    UserRoleId = table.Column<byte[]>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false),
                    AGDepartmentId = table.Column<byte[]>(nullable: true),
                    GovernmentDepartmentId = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_AGDepartments_AGDepartmentId",
                        column: x => x.AGDepartmentId,
                        principalTable: "AGDepartments",
                        principalColumn: "AGDepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_GovernmentDepartments_GovernmentDepartmentId",
                        column: x => x.GovernmentDepartmentId,
                        principalTable: "GovernmentDepartments",
                        principalColumn: "GovernmentDepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdvocateCourts",
                columns: table => new
                {
                    AdvocateCourtId = table.Column<byte[]>(nullable: false),
                    AdvocateId = table.Column<byte[]>(nullable: false),
                    CourtId = table.Column<byte[]>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvocateCourts", x => x.AdvocateCourtId);
                    table.ForeignKey(
                        name: "FK_AdvocateCourts_Advocates_AdvocateId",
                        column: x => x.AdvocateId,
                        principalTable: "Advocates",
                        principalColumn: "AdvocateId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvocateCourts_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "CourtId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cases",
                columns: table => new
                {
                    CaseId = table.Column<byte[]>(nullable: false),
                    NoticeId = table.Column<byte[]>(nullable: false),
                    OrdinalNo = table.Column<int>(nullable: false),
                    PetitionNo = table.Column<string>(nullable: true),
                    CaseStatusId = table.Column<byte[]>(nullable: true),
                    AdvocateId = table.Column<byte[]>(nullable: true),
                    CourtId = table.Column<byte[]>(nullable: true),
                    RegionId = table.Column<byte[]>(nullable: true),
                    Status2 = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    PetitionerId = table.Column<byte[]>(nullable: false),
                    NoticeCounselorId = table.Column<byte[]>(nullable: true),
                    NextHearingDate = table.Column<DateTime>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cases", x => x.CaseId);
                    table.ForeignKey(
                        name: "FK_Cases_Advocates_AdvocateId",
                        column: x => x.AdvocateId,
                        principalTable: "Advocates",
                        principalColumn: "AdvocateId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_CaseStatuses_CaseStatusId",
                        column: x => x.CaseStatusId,
                        principalTable: "CaseStatuses",
                        principalColumn: "CaseStatusId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "CourtId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_NoticeCounselors_NoticeCounselorId",
                        column: x => x.NoticeCounselorId,
                        principalTable: "NoticeCounselors",
                        principalColumn: "NoticeCounselorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_Notices_NoticeId",
                        column: x => x.NoticeId,
                        principalTable: "Notices",
                        principalColumn: "NoticeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_Petitioners_PetitionerId",
                        column: x => x.PetitionerId,
                        principalTable: "Petitioners",
                        principalColumn: "PetitionerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cases_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "RegionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCredentialRequests",
                columns: table => new
                {
                    UserCredentialRequestId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    MobileNo = table.Column<string>(nullable: true),
                    PetitionNo = table.Column<string>(nullable: true),
                    CreateDate = table.Column<string>(nullable: true),
                    RegionId = table.Column<byte[]>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    UserId = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCredentialRequests", x => x.UserCredentialRequestId);
                    table.ForeignKey(
                        name: "FK_UserCredentialRequests_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "RegionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCredentialRequests_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLogInLogs",
                columns: table => new
                {
                    UserLogInLogId = table.Column<byte[]>(nullable: false),
                    UserId = table.Column<byte[]>(nullable: false),
                    SignIn = table.Column<DateTime>(nullable: false),
                    SessionExpireTime = table.Column<DateTime>(nullable: false),
                    SignOut = table.Column<DateTime>(nullable: true),
                    ApiSessionToken = table.Column<byte[]>(nullable: false),
                    IsExpired = table.Column<bool>(nullable: false),
                    IsForcedExpired = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogInLogs", x => x.UserLogInLogId);
                    table.ForeignKey(
                        name: "FK_UserLogInLogs_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CaseHearings",
                columns: table => new
                {
                    CaseHearingId = table.Column<byte[]>(nullable: false),
                    CaseId = table.Column<byte[]>(nullable: false),
                    HearingDate = table.Column<DateTime>(nullable: true),
                    NextHearingDate = table.Column<DateTime>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseHearings", x => x.CaseHearingId);
                    table.ForeignKey(
                        name: "FK_CaseHearings_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CaseTransfer",
                columns: table => new
                {
                    CaseTransferId = table.Column<byte[]>(nullable: false),
                    CaseId = table.Column<byte[]>(nullable: false),
                    CourtId = table.Column<byte[]>(nullable: false),
                    TransferDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseTransfer", x => x.CaseTransferId);
                    table.ForeignKey(
                        name: "FK_CaseTransfer_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CaseTransfer_Courts_CourtId",
                        column: x => x.CourtId,
                        principalTable: "Courts",
                        principalColumn: "CourtId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NoticeBasicIndexs",
                columns: table => new
                {
                    NoticeBasicIndexId = table.Column<byte[]>(nullable: false),
                    NoticeId = table.Column<byte[]>(nullable: true),
                    CaseId = table.Column<byte[]>(nullable: true),
                    NoticeOrCaseType = table.Column<int>(nullable: false),
                    Particulars = table.Column<string>(nullable: true),
                    Annexure = table.Column<string>(nullable: true),
                    SerialNo = table.Column<int>(nullable: false),
                    IndexDate = table.Column<DateTime>(nullable: true),
                    NoOfPages = table.Column<int>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NoticeBasicIndexs", x => x.NoticeBasicIndexId);
                    table.ForeignKey(
                        name: "FK_NoticeBasicIndexs_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NoticeBasicIndexs_Notices_NoticeId",
                        column: x => x.NoticeId,
                        principalTable: "Notices",
                        principalColumn: "NoticeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartyAndRespondents",
                columns: table => new
                {
                    PartyAndRespondentId = table.Column<byte[]>(nullable: false),
                    NoticePartyId = table.Column<byte[]>(nullable: true),
                    NoticeRespondentId = table.Column<byte[]>(nullable: true),
                    CasePartyId = table.Column<byte[]>(nullable: true),
                    CaseRespondentId = table.Column<byte[]>(nullable: true),
                    NoticeOrCaseType = table.Column<int>(nullable: false),
                    PartyAndRespondentType = table.Column<int>(nullable: false),
                    PartyType = table.Column<int>(nullable: false),
                    PartyName = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    GovernmentDepartmentId = table.Column<byte[]>(nullable: true),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyAndRespondents", x => x.PartyAndRespondentId);
                    table.ForeignKey(
                        name: "FK_PartyAndRespondents_Cases_CasePartyId",
                        column: x => x.CasePartyId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyAndRespondents_Cases_CaseRespondentId",
                        column: x => x.CaseRespondentId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyAndRespondents_GovernmentDepartments_GovernmentDepartme~",
                        column: x => x.GovernmentDepartmentId,
                        principalTable: "GovernmentDepartments",
                        principalColumn: "GovernmentDepartmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyAndRespondents_Notices_NoticePartyId",
                        column: x => x.NoticePartyId,
                        principalTable: "Notices",
                        principalColumn: "NoticeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyAndRespondents_Notices_NoticeRespondentId",
                        column: x => x.NoticeRespondentId,
                        principalTable: "Notices",
                        principalColumn: "NoticeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCases",
                columns: table => new
                {
                    UserCaseId = table.Column<byte[]>(nullable: false),
                    UserId = table.Column<byte[]>(nullable: false),
                    CaseId = table.Column<byte[]>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCases", x => x.UserCaseId);
                    table.ForeignKey(
                        name: "FK_UserCases_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "CaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserCases_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    DocumentId = table.Column<byte[]>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NoticeBasicIndexId = table.Column<byte[]>(nullable: true),
                    CaseHearingId = table.Column<byte[]>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    EntityType = table.Column<int>(nullable: false),
                    DocumentUrl = table.Column<string>(nullable: true),
                    Ordinal = table.Column<int>(nullable: false),
                    ModifyBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.DocumentId);
                    table.ForeignKey(
                        name: "FK_Documents_CaseHearings_CaseHearingId",
                        column: x => x.CaseHearingId,
                        principalTable: "CaseHearings",
                        principalColumn: "CaseHearingId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_NoticeBasicIndexs_NoticeBasicIndexId",
                        column: x => x.NoticeBasicIndexId,
                        principalTable: "NoticeBasicIndexs",
                        principalColumn: "NoticeBasicIndexId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdvocateCourts_AdvocateId",
                table: "AdvocateCourts",
                column: "AdvocateId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvocateCourts_CourtId",
                table: "AdvocateCourts",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_Advocates_DesignationId",
                table: "Advocates",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseHearings_CaseId",
                table: "CaseHearings",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_AdvocateId",
                table: "Cases",
                column: "AdvocateId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_CaseStatusId",
                table: "Cases",
                column: "CaseStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_CourtId",
                table: "Cases",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_Cases_NoticeCounselorId",
                table: "Cases",
                column: "NoticeCounselorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_NoticeId",
                table: "Cases",
                column: "NoticeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_PetitionerId",
                table: "Cases",
                column: "PetitionerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cases_RegionId",
                table: "Cases",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseTransfer_CaseId",
                table: "CaseTransfer",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseTransfer_CourtId",
                table: "CaseTransfer",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_CourtOpenCloseLogs_CourtId",
                table: "CourtOpenCloseLogs",
                column: "CourtId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CaseHearingId",
                table: "Documents",
                column: "CaseHearingId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_NoticeBasicIndexId",
                table: "Documents",
                column: "NoticeBasicIndexId");

            migrationBuilder.CreateIndex(
                name: "IX_GovernmentDepartmentAddresses_GovernmentDepartmentId",
                table: "GovernmentDepartmentAddresses",
                column: "GovernmentDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_GovernmentDepartmentContactNos_GovernmentDepartmentId",
                table: "GovernmentDepartmentContactNos",
                column: "GovernmentDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeBasicIndexs_CaseId",
                table: "NoticeBasicIndexs",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeBasicIndexs_NoticeId",
                table: "NoticeBasicIndexs",
                column: "NoticeId");

            migrationBuilder.CreateIndex(
                name: "IX_Notices_NoticeCounselorId",
                table: "Notices",
                column: "NoticeCounselorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Notices_PetitionerId",
                table: "Notices",
                column: "PetitionerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_CasePartyId",
                table: "PartyAndRespondents",
                column: "CasePartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_CaseRespondentId",
                table: "PartyAndRespondents",
                column: "CaseRespondentId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_GovernmentDepartmentId",
                table: "PartyAndRespondents",
                column: "GovernmentDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_NoticePartyId",
                table: "PartyAndRespondents",
                column: "NoticePartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAndRespondents_NoticeRespondentId",
                table: "PartyAndRespondents",
                column: "NoticeRespondentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCases_CaseId",
                table: "UserCases",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCases_UserId",
                table: "UserCases",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCredentialRequests_RegionId",
                table: "UserCredentialRequests",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCredentialRequests_UserId",
                table: "UserCredentialRequests",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserLogInLogs_UserId",
                table: "UserLogInLogs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRolePagePermissions_PagePermissionId",
                table: "UserRolePagePermissions",
                column: "PagePermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRolePagePermissions_UserRoleId",
                table: "UserRolePagePermissions",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AGDepartmentId",
                table: "Users",
                column: "AGDepartmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_GovernmentDepartmentId",
                table: "Users",
                column: "GovernmentDepartmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvocateCourts");

            migrationBuilder.DropTable(
                name: "CaseTransfer");

            migrationBuilder.DropTable(
                name: "CourtOpenCloseLogs");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "EntityChangeTrakerLogs");

            migrationBuilder.DropTable(
                name: "GovernmentDepartmentAddresses");

            migrationBuilder.DropTable(
                name: "GovernmentDepartmentContactNos");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "PartyAndRespondents");

            migrationBuilder.DropTable(
                name: "UserCases");

            migrationBuilder.DropTable(
                name: "UserCredentialRequests");

            migrationBuilder.DropTable(
                name: "UserLogInLogs");

            migrationBuilder.DropTable(
                name: "UserRolePagePermissions");

            migrationBuilder.DropTable(
                name: "CaseHearings");

            migrationBuilder.DropTable(
                name: "NoticeBasicIndexs");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "PagePermissions");

            migrationBuilder.DropTable(
                name: "Cases");

            migrationBuilder.DropTable(
                name: "AGDepartments");

            migrationBuilder.DropTable(
                name: "GovernmentDepartments");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Advocates");

            migrationBuilder.DropTable(
                name: "CaseStatuses");

            migrationBuilder.DropTable(
                name: "Courts");

            migrationBuilder.DropTable(
                name: "Notices");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.DropTable(
                name: "Designations");

            migrationBuilder.DropTable(
                name: "NoticeCounselors");

            migrationBuilder.DropTable(
                name: "Petitioners");
        }
    }
}
