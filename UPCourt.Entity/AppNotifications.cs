﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Message), UserNameProp: nameof(CreatedBy))]
    public class AppNotifications : BaseTransEntity
    {
     
        [System.ComponentModel.DataAnnotations.Key]
        public Guid Id { get; set; }
        /// <summary>
        /// Notification source: Case, General, Notice
        /// </summary>
        public int Source { get; set; }
        //public int TargetUserId { get; set; }
        public Guid? TargetUserId { get; set; }
        /// <summary>
        /// System = 1, Email = 2, SMS = 3
        /// </summary>
        public int NotificationType { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string RouteValue { get; set; }

        public Guid? HashId { get; set; }
        /// <summary>
        /// Save Notifications to database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int Create(AppNotifications model)
        {
            int retval = 0;
            try
            {
                DatabaseContext dbContext = new DatabaseContext();
                dbContext.AppNotifications.Add(model);
                retval = dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retval;
        }

        /// <summary>
        /// Returns list of al the saved Notifications
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<AppNotifications> GetNotifications()
        {
            DatabaseContext db = new DatabaseContext();
            var data = db.AppNotifications.ToList();
            return data;
        }

        /// <summary>
        /// Returns list of Notifications: if argument valus is 0 it will return all the notifications stored in database 
        /// else return all the notifications of a specific user.
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static IEnumerable<AppNotifications> GetNotifications(Guid userid)
        {
            DatabaseContext db = new DatabaseContext();
            #region Fetching all the Notifications List
            var data = (from n in db.AppNotifications
                        select new AppNotifications
                        {
                            Id = n.Id,
                            Source = n.Source,
                            TargetUserId = n.TargetUserId,
                            NotificationType = n.NotificationType,
                            Status = n.Status,
                            Message = n.Message,
                            CreatedBy = n.CreatedBy,
                            CreatedOn = n.CreatedOn,
                            RouteValue = n.RouteValue,
                            HashId = n.HashId
                        }).ToList();
            #endregion

            if (!string.IsNullOrEmpty(userid.ToString())) 
                data = data.Where(o => o.TargetUserId == userid && o.Status != 3).ToList();

            return data;
        }

        /// <summary>
        /// Returns list of Items: NotificationType from master.
        /// e.g: Email, System, SMS, etc.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<NotificationsType> GetNotificationType()
        {
            DatabaseContext db = new DatabaseContext();
            var data = db.NotificationsType.ToList();
            return data;
        }

        /// <summary>
        /// Returns list of items: NotificationSource
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<NotificationSource> GetNotificationSource()
        {
            DatabaseContext db = new DatabaseContext();
            var data = db.NotificationSources.ToList();
            return data;
        }

        /// <summary>
        /// Returns list of items: TargetAudience
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<NotificationTargetUsers> GetNotificationTargetUsers()
        {
            DatabaseContext db = new DatabaseContext();
            var data = db.NotificationTargetUsers.ToList();
            return data;
        }


        /// <summary>
        /// Convert integer value to GUID
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Guid Int2Guid(int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

        /// <summary>
        /// Convert GUID value to integer
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int Guid2Int(Guid value)
        {
            byte[] b = value.ToByteArray();
            int bint = BitConverter.ToInt32(b, 0);
            return bint;
        }

    }
}
