﻿$(function () {
    $("#example1").DataTable({
        "responsive": true,
        "lengthChange": true, "iDisplayLength": 20,
        "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
        "ordering": true,
        "lengthMenu": [[10, 20, 50, 100, 200, 500, -1], [10, 20, 50, 100, 200, 500, "All"]],
        "order": [],
        buttons: [
            { extend: 'pdf', exportOptions: { columns: "thead th:not(.noExport)" } },
            { extend: 'excel', exportOptions: { columns: "thead th:not(.noExport)" } },
            { extend: 'csv', exportOptions: { columns: "thead th:not(.noExport)" } },
            { extend: 'copy', exportOptions: { columns: "thead th:not(.noExport)" } },
            { extend: 'print', exportOptions: { columns: "thead th:not(.noExport)" } },
            { extend: 'colvis', exportOptions: { columns: "thead th:not(.noExport)" } }
        ]

    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "order": [],
    });
});

function ConfirmDelete(ID, Name, Controller) {
    debugger;
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete '" + Name + "' !",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
    }).then((result) => {
        //alert(result.isConfirmed);
        if (result.isConfirmed) {
            window.location.replace("/" + Controller + "/Delete/" + ID);
        }
    });
}