﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Service.BusinessService
{
    public class NoticeBusinessService : BaseBusinessService
    {
        public static int GetSerailNo(DatabaseContext db)
        {
            var lastNoticeSerialNo = NoticeDataService.GetLite(db).OrderBy(i => i.SerialNo).LastOrDefault();
            int InvoiceSerialNo = (lastNoticeSerialNo?.SerialNo ?? 0) + 1;
            return InvoiceSerialNo;
        }
        public static int GetPages_PDF(string args)
        {
            string ppath = args;
            PdfReader pdfReader = new PdfReader(ppath);
            int numberOfPages = pdfReader.NumberOfPages;

            return numberOfPages;
        }
    }
}