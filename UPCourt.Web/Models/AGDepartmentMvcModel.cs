﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class AGDepartmentMvcModel : BaseEntityModel<AGDepartment, AGDepartmentMvcModel>
    {
        public Guid AGDepartmentId { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Department Head Name")]
        public string DepartmentHeadName { get; set; }
        [Required]
        public DepartmentStatus Status { get; set; }
        public bool IsDefault { get; set; }
    }
}