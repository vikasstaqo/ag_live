﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;
using static UPCourt.Web.Models.ApiDataModel;

namespace UPCourt.Web.Helper
{
    public class DataFromApiHelper
    {
        private DatabaseContext db = new DatabaseContext();
        private Guid SystemGuid;

        public int SyncNoticeNo()
        {
            SystemGuid = UserDataService.GetDetail(db).Where(i => i.Name == "Admin").Select(u => u.UserId).FirstOrDefault();
            var dbData = NoticeDataService.GetDetail(db).Where(i => i.Cnr == null && i.CaseNoticeType != null);
            int cntr = 0;
            foreach (var Notice in dbData)
            {
                if (Regex.IsMatch(Notice.NoticeNo, @"^\d+$"))
                {
                    var filter = Notice.NoticeYear + "-" + Notice.NoticeNo + "&" + Notice.CaseNoticeType.case_type;
                    List<getNoticeNo> NoticeNos = new List<getNoticeNo>();
                    var content = GetDataModelAsync("cismaster/getNoticeNo/" + filter);
                    NoticeNos = JsonConvert.DeserializeObject<List<getNoticeNo>>(content.Result);

                    if (NoticeNos.Count() == 1)
                    {
                        Notice.RegCaseType = NoticeNos.FirstOrDefault().regcase_type;
                        Notice.RegNo = NoticeNos.FirstOrDefault().reg_no;
                        Notice.Cnr = NoticeNos.FirstOrDefault().cnr;
                        Notice.Ported = 0;
                        cntr++;
                    }
                }
            }
            db.SaveChanges();
            return cntr;
        }

        public int SyncNoticeCases()
        {
            SystemGuid = UserDataService.GetDetail(db).Where(i => i.Name == "Admin").Select(u => u.UserId).FirstOrDefault();
            var dbData = NoticeDataService.GetDetail(db).Where(i => i.Cnr != null && i.Ported == 0).Select(i => i.NoticeId).ToList();
            int cntr = 0;
            foreach (var noti in dbData)
            {
                Notice notice = NoticeDataService.GetDetail(db).Where(i => i.NoticeId == noti).FirstOrDefault();
                var filter = notice.Cnr;
                List<getCaseStatus> CaseStatus = new List<getCaseStatus>();
                var content = GetDataModelAsync("cismaster/getCaseStatus/" + filter);
                CaseStatus = JsonConvert.DeserializeObject<List<getCaseStatus>>(content.Result);

                if (CaseStatus.Count() == 1)
                {
                    cntr++;
                    getCaseDetails CaseDetail = new getCaseDetails();
                    var contentdetail = GetDataModelAsync("cismaster/getCaseDetails/" + filter);
                    CaseDetail = JsonConvert.DeserializeObject<List<getCaseDetails>>(contentdetail.Result).FirstOrDefault();

                    var _caseStatus = CaseStatus.FirstOrDefault();
                    Guid _CaseDetailId = Guid.NewGuid();
                    int _pet_adv_cd = 0;
                    int _res_adv_cd = 0;
                    if (CaseDetail != null)
                    {
                        _pet_adv_cd = CaseDetail.pet_adv_cd;
                        _res_adv_cd = CaseDetail.res_adv_cd;
                    }

                    string _fir_no = "";
                    int? _fir_year = 0;
                    string _dist_name = "";
                    string _police_st_name = "";

                    if (notice.CaseNoticeType.ParentNoticeType == ParentNoticeType.Criminal)
                    {
                        getCrimeDetails CrimeDetail = new getCrimeDetails();
                        var contentCrimedetail = GetDataModelAsync("cismaster/getCrimeDetails/" + filter);
                        CrimeDetail = JsonConvert.DeserializeObject<List<getCrimeDetails>>(contentCrimedetail.Result).FirstOrDefault();
                        if (CrimeDetail != null)
                        {
                            _fir_no = CrimeDetail.fir_no.Trim();
                            _fir_year = CrimeDetail.fir_year;
                            _dist_name = CrimeDetail.dist_name;
                            _police_st_name = CrimeDetail.police_st_name;
                        }
                    }


                    Casedetail _casedetail = new Casedetail()
                    {
                        CaseDetailId = _CaseDetailId,
                        NoticeId = notice.NoticeId,
                        PetName = _caseStatus.pet_name,
                        BenchType = _caseStatus.bench_type,
                        Cs_subject = _caseStatus.cs_subject,
                        C_subject = _caseStatus.c_subject,
                        DispNature = _caseStatus.disp_nature,
                        PurposeNext = _caseStatus.purpose_next,
                        JudgeCode = _caseStatus.judge_code,
                        DateOfDecision = _caseStatus.date_of_decision,
                        ResName = _caseStatus.res_name,
                        CaseStateCode = _caseStatus.case_state_code,
                        CaseDistCode = _caseStatus.case_dist_code,
                        Case_no = _caseStatus.case_no,
                        Short_order = _caseStatus.short_order,
                        CauselistType = _caseStatus.causelist_type,
                        BranchId = _caseStatus.branch_id,
                        pet_adv_cd = _pet_adv_cd,
                        res_adv_cd = _res_adv_cd,
                        fir_no = _fir_no,
                        fir_year = _fir_year,
                        dist_name = _dist_name,
                        police_st_name = _police_st_name,
                        Status = 1,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now
                    };
                    db.Casedetails.Add(_casedetail);

                    foreach (var _IA in _caseStatus.IA)
                    {
                        CasedetailIa _casedetailia = new CasedetailIa()
                        {
                            CaseDetailIAId = Guid.NewGuid(),
                            CaseDetailId = _CaseDetailId,
                            ia_regno = _IA.ia_regno,
                            date_of_ia_registration = _IA.date_of_ia_registration,
                            date_of_hearing = _IA.date_of_hearing,
                            court_no = _IA.court_no,
                            judge_code = _IA.judge_code,
                            ia_regyear = _IA.ia_regyear,
                            date_of_order = _IA.date_of_order,
                            Status = 1,
                            CreatedBy = SystemGuid,
                            CreatedOn = DateTime.Now,
                        };
                        db.Casedetailias.Add(_casedetailia);
                    }

                    foreach (var _Petitioner in _caseStatus.Petitioners)
                    {
                        CasedetailPet _casedetailPet = new CasedetailPet()
                        {
                            CaseDetailPetId = Guid.NewGuid(),
                            CaseDetailId = _CaseDetailId,
                            party_no = _Petitioner.party_no,
                            PetName = _Petitioner.name,
                            Status = 1,
                            CreatedBy = SystemGuid,
                            CreatedOn = DateTime.Now,
                        };
                        db.CasedetailPets.Add(_casedetailPet);
                    }

                    foreach (var _Respondent in _caseStatus.Respondents)
                    {
                        CasedetailResp _casedetailResp = new CasedetailResp()
                        {
                            CaseDetailRespId = Guid.NewGuid(),
                            CaseDetailId = _CaseDetailId,
                            party_no = _Respondent.party_no,
                            RespName = _Respondent.name,
                            Status = 1,
                            CreatedBy = SystemGuid,
                            CreatedOn = DateTime.Now,
                        };
                        db.CasedetailResps.Add(_casedetailResp);
                    }

                    var TotalCases = CaseDataService.GetLite(db).ToList().Count;
                    Case NewCase = new Case()
                    {
                        CaseId = Guid.NewGuid(),
                        NoticeId = notice.NoticeId,
                        OrdinalNo = TotalCases + 1,
                        PetitionNo = notice.RegNo.ToString(),
                        CaseStatusId = CaseStatusDataService.GetLite(db).Where(i => i.Name.Contains("Fresh Case")).FirstOrDefault().CaseStatusId,
                        Type = notice.NoticeType,
                        RegionId = notice.RegionId,
                        CreatedBy = SystemGuid,
                        CreatedOn = DateTime.Now
                    };
                    notice.BasicIndexs.ToList().ForEach(F => F.CaseId = NewCase.CaseId);

                    notice.Status = NoticeStatus.Accept;

                    db.Cases.Add(NewCase);
                    notice.Ported = 1;

                    db.SaveChanges();

                    //Create notification for the case created

                    AppNotificationHelper.CreateAppNotificationHelperObject(null).CreateNewCaseNotification(NewCase);

                    string description = $"Notice '{ notice.NoticeNo }' Converted to Case '{ NewCase.PetitionNo }' successfully";
                    TimelineBusinessService.SaveTimeLine(db, NewCase.NoticeId, NewCase.CaseId, EventType.NoticeToCase, SystemGuid, description, null);

                }
            }
            return cntr;
        }

        private Task<string> GetDataModelAsync(string url)
        {            
            HttpClient client = new HttpClient() { BaseAddress = new Uri("https://efiling-alld.allahabadhighcourt.in") };
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "YWhjOmFoY0A1NDMyMQ==");
            client.Timeout = TimeSpan.FromMinutes(10);          
            var result = client.GetAsync(url);
            result.Wait();
            var content = result.Result.Content.ReadAsStringAsync();
            return content;
        }
    }
}