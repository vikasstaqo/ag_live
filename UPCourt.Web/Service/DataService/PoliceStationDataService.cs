﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class PoliceStationDataService
    {
        public static IQueryable<PoliceStation> GetLite(DatabaseContext db)
        {
            return db.PoliceStations.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<PoliceStation> GetDetail(DatabaseContext db)
        {
            return GetLite(db).Include(i => i.Zone);
        }
    }

    public class ZoneDataService
    {
        public static IQueryable<Zone> GetLite(DatabaseContext db)
        {
            return db.Zones.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Zone> GetDetail(DatabaseContext db)
        {
            return GetDetail(db).Include(i => i.DistrictName);
        }
    }

    public class GroupDataService
    {
        public static IQueryable<Group> GetLite(DatabaseContext db)
        {
            return db.Groups.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Group> GetDetail(DatabaseContext db)
        {
            return GetDetail(db);
        }
    }

    public class ActTypeDataService
    {
        public static IQueryable<ActType> GetLite(DatabaseContext db)
        {
            return db.ActTypes;
        }
        public static IQueryable<ActType> GetDetail(DatabaseContext db)
        {
            return db.ActTypes;
        }
    }

    public class RangeDataService
    {
        public static IQueryable<Range> GetLite(DatabaseContext db)
        {
            return db.Ranges;
        }
        public static IQueryable<Range> GetDetail(DatabaseContext db)
        {
            return GetDetail(db).Include(i => i.RangeName);
        }
    }
}