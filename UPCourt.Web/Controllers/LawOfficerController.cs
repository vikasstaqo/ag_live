﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class LawOfficerController : BaseMVCController
    {
        // GET: LawOfficer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(StateLawOfficer model)
        {
            StateLawOfficer tempData = new StateLawOfficer
            {
                AdvId = Guid.NewGuid(),
                AdvName = model.AdvName,
                AdvEnrl = model.AdvEnrl,
                AdvAor = model.AdvAor,
                AdvLocation = model.AdvLocation,
                AdvPost = model.AdvPost,
                CreatedOn = DateTime.Now,
                CreatedBy = AppUser.UserId,
            };
            db.StateLawOfficer.Add(tempData);
            db.SaveChangesWithAudit(Request, "");
            SucessMessage = $"DC Govt. Department {tempData.AdvName} Created successfully";
            ViewBag.Msg = SucessMessage;
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create");

        }
        public ActionResult List()
        {
            StateLawOfficerIndexMvcModel model = new StateLawOfficerIndexMvcModel() { };

            var dbData = StateLawOfficerDataService.GetDetail(db).Where(F => F.AdvPost == "A.A.G").OrderByDescending(i => i.AdvName).ToList();
            model.StateLawOfficersDocs = StateLawOfficerModel.CopyFromEntityList(dbData, 0);

            var dbDataa = StateLawOfficerDataService.GetDetail(db).Where(F => F.AdvPost == "G.A").OrderByDescending(i => i.AdvName).ToList();
            model.StateLawOfficersDocsGA = StateLawOfficerModel.CopyFromEntityList(dbDataa, 0);

            var dbDataaa = StateLawOfficerDataService.GetDetail(db).Where(F => F.AdvPost == "CSE" || F.AdvPost== "CSE-I" || F.AdvPost== "CSE-II" || F.AdvPost== "CSE-V").OrderByDescending(i => i.AdvName).ToList();
            model.StateLawOfficersDocsCSE = StateLawOfficerModel.CopyFromEntityList(dbDataaa, 0);
            var dbDataaaa = StateLawOfficerDataService.GetDetail(db).Where(F => F.AdvPost == "A.CSE").OrderByDescending(i => i.AdvName).ToList();
            model.StateLawOfficersDocsACSE = StateLawOfficerModel.CopyFromEntityList(dbDataaaa, 0);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetList(string d)
        {
            var dbData = StateLawOfficerDataService.GetLite(db).Where(i => i.AdvPost == "A.A.G").ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }
    }
}