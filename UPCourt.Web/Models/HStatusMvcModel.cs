﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class HStatusMvcModel : BaseEntityModel<HStatus, HStatusMvcModel>
    {
        public Guid HStatusId { get; set; }
        public string HStatusName { get; set; }
    }
}