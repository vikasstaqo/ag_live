﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
   public class GovBlk : BaseMasterEntity
    {
        public GovBlk() { }
        public Guid GovBlklId { get; set; }
        public string GovBlklName { get; set; }
        public GovBlkStatus Status { get; set; }
    }
}
public enum GovBlkStatus
{
    Active = 1,
    Inactive = 0
}
