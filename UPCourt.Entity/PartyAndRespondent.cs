﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PartyName), UserNameProp: nameof(ModifyBy))]
    public class PartyAndRespondent
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
        public PartyType PartyType { get; set; }
        public string PartyName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string District { get; set; }           
        public string PinCode { get; set; }
        public string MobileNo { get; set; }
        public string ModifyBy { get; set; }
        public bool IsFirst { get; set; }
        public bool IsPartyDepartment { get; set; }
        public Notice NoticeRespondent { get; set; }
        public Guid? GovernmentDepartmentId { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }
        public Guid? GovernmentDesignationId { get; set; }
        public GovernmentDesignation GovDesignation { get; set; }
        public Guid? GovernmentdepartmentOICID { get; set; }
        public GovernmentDepartmentOic GovernmentDepartmentOic { get; set; }
        public int? SNo { get; set; }
        
    }

    public enum NoticeOrCaseType
    {
        Notice = 0,
        Case = 1
    }
    public enum PartyAndRespondentType
    {
        Party = 0,
        Respondent = 1
    }
    public enum PartyType
    {
        Individual = 0,
        Department = 1
    }
}
