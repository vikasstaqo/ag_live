﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Models;

namespace UPCourt.Web.Service.DataService
{
    public class NoticeDataService
    {
        public static IQueryable<Notice> GetAll(DatabaseContext db)
        {
            return db.Notices;
        }
        public static IQueryable<Notice> GetLite(DatabaseContext db)
        {
            return db.Notices.Where(i => i.IsDeleted == false);
        }
        public static IQueryable<Notice> GetDetail(DatabaseContext db, bool withRegion = true)
        {
            var notices = db.Notices.Where(i => i.IsDeleted == false)
                .Include(i => i.Petitioners)
                .Include(i => i.Counselors)
                .Include(i => i.Respondents).ThenInclude(i => i.GovernmentDepartment)
                .Include(i => i.BasicIndexs).ThenInclude(x => x.Documents)
                .Include(i => i.IPCs).ThenInclude(x => x.IPCSection)
                .Include(i => i.ActTypes)
                .Include(i => i.CaseNoticeType)
                .Include(i => i.CrimeDetail).ThenInclude(x => x.PoliceStation)
                .Include(i => i.NoticeSpecialcategorys)
                .Include(i => i.ImpungedAndSTDetails).ThenInclude(x => x.CaseNoticeType)
                .Include(i => i.ImpungedAndSTDetails).ThenInclude(x => x.SubordinateCourt)
                .Include(i => i.Group)
                .Include(i => i.CaseDetail).ThenInclude(x => x.IAs)
                .Include(i => i.CaseDetail).ThenInclude(x => x.Petitioners)
                .Include(i => i.CaseDetail).ThenInclude(x => x.Respondents)
                .Include(i => i.ECourtFees);
            if (withRegion)
                notices.Include(i => i.Region);
            return notices;
        }

        public static IQueryable<Notice> GetDBDetail(DatabaseContext db)
        {
            var notices = db.Notices.Where(i => i.IsDeleted == false)
                .Include(i => i.Petitioners)
                .Include(i => i.Counselors)
                .Include(i => i.Respondents).ThenInclude(i => i.GovernmentDepartment)
                .Include(i => i.CaseNoticeType)
                .Include(i => i.CrimeDetail).ThenInclude(x => x.PoliceStation)
                .Include(i => i.NoticeSpecialcategorys)
                .Include(i => i.Group)
                .Include(i => i.Region);
            return notices;
        }
        public static IQueryable<Notice> GetFiltered(DatabaseContext db, Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, string HitFrom = "")
        {
            IQueryable<Notice> NoticeList;
            if (HitFrom == "DB")
                NoticeList = GetDBDetail(db);
            else
                NoticeList = GetDetail(db);

            return CheckNoticess(UserId, UserSystemRoleId, TempRegionId, GovernmentDepartmentId, DesignationId, GovermentDepartmentOICID, NoticeList);

        }
        private static IQueryable<Notice> CheckNoticess(Guid UserId, UserSystemRole UserSystemRoleId, Guid? TempRegionId, Guid? GovernmentDepartmentId, Guid? DesignationId, Guid? GovermentDepartmentOICID, IQueryable<Notice> NoticeList)
        {


            if (UserSystemRoleId == UserSystemRole.Admin || UserSystemRoleId == UserSystemRole.AGAdmin)
            {
                NoticeList = NoticeList.Where(x => x.RegionId == TempRegionId);
            }
            if (UserSystemRoleId == UserSystemRole.CSC || UserSystemRoleId == UserSystemRole.AdvCivil)
            {
                NoticeList = NoticeList.Where(x => x.NoticeType == ParentNoticeType.Civil);
            }
            else if (UserSystemRoleId == UserSystemRole.CGA || UserSystemRoleId == UserSystemRole.AdvCriminal)
            {
                NoticeList = NoticeList.Where(x => x.NoticeType == ParentNoticeType.Criminal);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptHead && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentDepartmentId == GovernmentDepartmentId).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptDegUser && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentDesignationId == DesignationId).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.GovDeptOIC && GovernmentDepartmentId != null)
            {
                NoticeList = NoticeList.Where(x => x.Respondents.Where(y => y.GovernmentdepartmentOICID == GovermentDepartmentOICID).Count() > 0);
            }
            else if (UserSystemRoleId == UserSystemRole.DataEntryOperator)
            {
                NoticeList = NoticeList.Where(x => x.CreatedBy == UserId);
            }
            return NoticeList.AsQueryable();
        }
    }
    public class SpecialcategoryDataService
    {
        public static IQueryable<SpecialCategory> GetLite(DatabaseContext db)
        {
            return db.SpecialCategorys.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<SpecialCategory> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }

    public class SalutationDataService
    {
        public static IQueryable<Salutation> GetLite(DatabaseContext db)
        {
            return db.Salutations;
        }
        public static IQueryable<Salutation> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;

        }
    }

    public class PartyAndRespondentDataService
    {
        public static IQueryable<PartyAndRespondent> GetLite(DatabaseContext db)
        {
            return db.PartyAndRespondents;
        }
        public static IQueryable<PartyAndRespondent> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }

    public class NoticeBasicIndexDataService
    {
        public static IQueryable<NoticeBasicIndex> GetLite(DatabaseContext db)
        {
            return db.NoticeBasicIndexs;
        }
        public static IQueryable<NoticeBasicIndex> GetDetail(DatabaseContext db)
        {

            return db.NoticeBasicIndexs.Include(x => x.Documents);

        }
    }

    public class NoticeIPCDataService
    {
        public static IQueryable<NoticeIPC> GetLite(DatabaseContext db)
        {
            return db.NoticeIPCs;
        }
        public static IQueryable<NoticeIPC> GetDetail(DatabaseContext db)
        {
            return GetLite(db);
        }
    }

    public class NoticeActTypeDataService
    {
        public static IQueryable<NoticeActType> GetLite(DatabaseContext db)
        {
            return db.NoticeActTypes;
        }
        public static IQueryable<NoticeActType> GetDetail(DatabaseContext db)
        {
            return GetLite(db);
        }
    }

    public class PetitionerDataService
    {
        public static IQueryable<Petitioner> GetLite(DatabaseContext db)
        {
            return db.Petitioners;
        }
        public static IQueryable<Petitioner> GetDetail(DatabaseContext db)
        {
            return db.Petitioners;
        }
    }

    public class ImpungedAndSTDetailDataService
    {
        public static IQueryable<ImpungedAndSTDetail> GetLite(DatabaseContext db)
        {
            return db.ImpungedAndSTDetails;
        }
        public static IQueryable<ImpungedAndSTDetail> GetDetail(DatabaseContext db)
        {
            return db.ImpungedAndSTDetails.Include(x => x.SubordinateCourt);
        }
    }

    public class ECourtFeeDataService
    {
        public static IQueryable<ECourtFee> GetLite(DatabaseContext db)
        {
            return db.ECourtFees;
        }
        public static IQueryable<ECourtFee> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }

}
