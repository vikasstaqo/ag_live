﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    
    public class RegController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }
        public RegController() : this(new CustomUserManager())
        {
        }
        public RegController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }
        // GET: /Registration


        public class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }
            [JsonProperty("error-codes")]
            public List<string> ErrorMessage { get; set; }
        }

        public static CaptchaResponse ValidateCaptcha(string response)
        {
            string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }

        public ActionResult Index()
        {
            var model = new UserMvcModelExternal();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(UserMvcModelExternal model)
        {
            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);

            if (!response.Success)
            {
                ModelState.AddModelError(nameof(model.Name), $"Invalid Captch");
                return View(model);
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }


            var dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.LoginId == model.LoginId);
            if (dbUser != null)
            {
                ModelState.AddModelError(nameof(UserMvcModelExternal.LoginId), "User already exist");
                return View(model);
            }
            string RoleName;
            if (model.LoginAs == LoginAS.InPerson) { RoleName = "Guest"; }
            else { RoleName = "Guest"; }

            var dbRole = UserRoleDataService.GetLite(db).Where(e => e.Name == RoleName).FirstOrDefault();
            var passwordSalt = PasswordHelper.GetPasswordSalt();


            User objUser = new User
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Aadhar = model.Aadhar,
                Email = model.Email,
                Gender = model.Gender.ToString(),
                Phone = model.Phone,
                LoginId = model.LoginId,
                PasswordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt),
                PasswordSalt = passwordSalt,
                ModifiedBy = AppUser.UserId,
                ModifiedOn = DateTime.Now,
                Status = UserStatus.Active,
                UserRoleId = dbRole.UserRoleId,
                CreatedOn = DateTime.Now,
                UserType = UserType.AG
            };

            db.Users.Add(objUser);
            db.SaveChangesWithAudit(Request, "");

            SucessMessage = $"User {objUser.Name} created successfully";
            ViewBag.Msg = SucessMessage;
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create", "Registration");

        }
    }
}