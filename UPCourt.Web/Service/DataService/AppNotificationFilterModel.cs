﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class AppNotificationFilterModel : BaseEntityModel<Entity.AppNotifications, AppNotificationFilterModel>
    {
        [Display(Name = "Notification Date")]
        public DateTime CreationDate { get; set; }
        [Display(Name = "Status")]
        public int Status { get; set; }
        public int Source { get; set; }
        [Display(Name = "Notification Type")]
        public int NotificationType { get; set; }
        public ICollection<AppNotificationsMvcModel> NotificationData { get; set; }
    }
}