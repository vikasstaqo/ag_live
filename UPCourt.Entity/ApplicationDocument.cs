﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ApplicantName), UserNameProp: nameof(InsertedBy))]
    public class ApplicationDocument
    {

        public Guid ApplicationDocumentID { get; set; }

        public Guid ApplicationTypeID { get; set; }
        public ApplicationType ApplicationType { get; set; }
        public string ApplicantName { get; set; }

        public DateTime InsertedOn { get; set; }

        public string InsertedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        public bool IsActive { get; set; }

        public ICollection<ECourtFee> ECourtFees { get; set; }
        public ICollection<NoticeBasicIndex> BasicIndexs { get; set; }

        public ApplicationStatus FinalStatus { get; set; }
    }

    public enum ApplicationStatus
    {
        Draft = 0,
        FinalSubmit = 1
    }
}
