﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class UserCredentialRequestModel : BaseEntityModel<UserCredentialRequest, UserCredentialRequestModel>
    {
        public Guid UserCredentialRequestId { get; set; }
        [Required]

        public string Name { get; set; }
        public string Address { get; set; }
        [Required]
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        public string PetitionNo { get; set; }
        public UserCredentialRequestStatus Status { get; set; }
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        public RegionMvcModel Region { get; set; }

        public string RegionName { get; set; }

       
        public override UserCredentialRequestModel EntityToResponse(UserCredentialRequest entity, UserCredentialRequestModel model)
        {
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, 0);
            return model;
        }
    }
}