﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class TimelineMvcModel : BaseEntityModel<Timeline, TimelineMvcModel>
    {
        public Guid TimelineId { get; set; }
        public Guid? CaseId { get; set; }
        public Guid? NoticeId { get; set; }
        public EventType EventType { get; set; }
        public Guid UserId { get; set; }
        public DateTime TimeOfEvent { get; set; }
        public string Description { get; set; }
        public Guid? ParentTimelineId { get; set; }

        public Timeline ParentTimeline { get; set; }
        public UserMvcModel User { get; set; }
        public string EventTypeNameDisplay { get; set; }
        public string EventTypeNameIconType { get; set; }
        public override TimelineMvcModel EntityToResponse(Timeline entity, TimelineMvcModel model)
        {
            model.User = UserMvcModel.CopyFromEntity(entity.User, model.RecursiveLevel);
            model.EventTypeNameDisplay= entity.EventType.GetCustomAttribute<EventTypeAttribute>().DisplayText;
            model.EventTypeNameIconType = entity.EventType.GetCustomAttribute<EventTypeAttribute>().IconType;
            return model;
        }
    }

}