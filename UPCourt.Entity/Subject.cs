﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class Subject : BaseMasterEntity
    {
        public Subject()
        {
        }
        public Guid SubjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public ParentNoticeType ParentNoticeType { get; set; }
        public Guid? CaseNoticeTypeId { get; set; }
        public CaseNoticeType CaseNoticeType { get; set; }

    }
}
