﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(HStatusName), UserNameProp: nameof(CreatedBy))]
    public class HStatus : BaseMasterEntity
    {
        public HStatus() { }
        public Guid HStatusID { get; set; }
        public string HStatusName { get; set; }
    }
  
}
