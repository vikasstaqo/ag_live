﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class ZoneController : BaseMVCController
    {
        // GET: Zone
        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(ZoneMvcModel.CopyFromEntityList(ZoneDataService.GetLite(db).OrderByDescending(x => x.DisplayOrder), 0));
        }

        [HttpGet]
        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            InitilizeDistrict(null);
            var model = new ZoneMvcModel { IsActive = true };
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Subject, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(ZoneMvcModel model)
        {
            InitilizeDistrict(model.DistrictName);
            if (!ModelState.IsValid) { return View(model);  }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Zone tempData = new Zone()
            {
                ZoneID = Guid.NewGuid(),
                ZoneName = model.ZoneName,
                DistrictName = model.DistrictName,
                IsActive = model.IsActive,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetZonesDisplayOrder(db)
            };
            db.Zones.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }
        private void InitilizeDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName);
            ViewBag.Districts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }

        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = ZoneDataService.GetLite(db).Where(i => i.ZoneID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeDistrict(dbData.DistrictName);
            ZoneMvcModel model = ZoneMvcModel.CopyFromEntity(dbData, 0);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(ZoneMvcModel model)
        {
            InitilizeDistrict(model.DistrictName);
            if (!ModelState.IsValid) { return View(model); }
            var dbData = ZoneDataService.GetLite(db).Where(i => i.ZoneID == model.ZoneID).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            dbData.ZoneName = model.ZoneName;
            dbData.DistrictName = model.DistrictName;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = ZoneDataService.GetLite(db).Where(i => i.ZoneID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(ZoneMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(ZoneMvcModel model)
        {
            var duplicateCheck = ZoneDataService.GetLite(db).FirstOrDefault(i => i.ZoneName == model.ZoneName && i.DistrictName == model.DistrictName && i.ZoneID != model.ZoneID);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.ZoneName), $"{model.ZoneName} already exists");
                return false;
            }
            return true;
        }
        [PageRoleAuthorizarion(PageName.Zone, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = ZoneDataService.GetLite(db).Where(i => i.ZoneID == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}