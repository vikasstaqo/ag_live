﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Locality), UserNameProp: nameof(CreatedBy))]
    public class PoliceStation : BaseMasterEntity
    {
        public Guid PoliceStationId { get; set; }
        public string District { get; set; }
        public string Locality { get; set; }
        public string POC { get; set; }
        public string EmailID { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public bool IsActive { get; set; }
        public Guid? ZoneID { get; set; }
        public Guid? RangeID { get; set; }
        public Zone Zone { get; set; }
        public Range Range { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ZoneName), UserNameProp: nameof(CreatedBy))]
    public class Zone : BaseMasterEntity
    {
        public Guid ZoneID { get; set; }
        public string ZoneName { get; set; }
        public string DistrictName { get; set; }
        public bool IsActive { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(RangeName), UserNameProp: nameof(CreatedBy))]
    public class Range : BaseMasterEntity
    {
        public Guid RangeID { get; set; }
        public string RangeName { get; set; }
        public Guid? ZoneID { get; set; }
        public bool IsActive { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(GroupName), UserNameProp: nameof(CreatedBy))]
    public class Group : BaseMasterEntity
    {
        public Guid GroupID { get; set; }
        public string GroupName { get; set; }
        public bool IsActive { get; set; }

    }
}
