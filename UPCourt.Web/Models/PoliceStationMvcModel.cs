﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class PoliceStationFilterMvcModel : BaseEntityModel<PoliceStation, PoliceStationFilterMvcModel>
    {
        [Display(Name = "Zone")]
        public Guid? ZoneID { get; set; }

        [Display(Name = "Range")]
        public Guid? RangeID { get; set; }

        [Display(Name = "Locality")]
        public string Locality { get; set; }

        public ICollection<PoliceStationMvcModel> PoliceStation { get; set; }


    }
    public class PoliceStationMvcModel : BaseEntityModel<PoliceStation, PoliceStationMvcModel>
    {
        public Guid PoliceStationId { get; set; }
        public string District { get; set; }
        public string Locality { get; set; }
        [Display(Name = "Point Of Contact (POC)")]
        public string POC { get; set; }
        [Display(Name = "Email ID")]
        public string EmailID { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public bool IsActive { get; set; }
        [Display(Name = "Zone")]
        public Guid? ZoneID { get; set; }

        [Display(Name = "Range")]
        public Guid? RangeID { get; set; }
        public ZoneMvcModel Zone { get; set; }

        public RangeMvcModel Range { get; set; }
        public override PoliceStationMvcModel EntityToResponse(PoliceStation entity, PoliceStationMvcModel model)
        {
            model.Zone = ZoneMvcModel.CopyFromEntity(entity.Zone, model.RecursiveLevel);
            return model;
        }
    }
    public class ZoneMvcModel : BaseEntityModel<Zone, ZoneMvcModel>
    {
        public Guid ZoneID { get; set; }
        [Display(Name = "Zone Name")]
        [Required]
        public string ZoneName { get; set; }
        [Display(Name = "District Name")]
        [Required]
        public string DistrictName { get; set; }
        public bool IsActive { get; set; }

    }
    public class RangeMvcModel : BaseEntityModel<Range, RangeMvcModel>
    {
        public Guid RangeID { get; set; }
        public string RangeName { get; set; }
        public Guid? ZoneID { get; set; }
        public bool IsActive { get; set; }
    }
    public class GroupMvcModel : BaseEntityModel<Group, GroupMvcModel>
    {
        public Guid GroupID { get; set; }
        [Display(Name = "Group Name")]
        [Required]
        public string GroupName { get; set; }
        public bool IsActive { get; set; }
    }

}