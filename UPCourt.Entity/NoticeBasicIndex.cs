﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Annexure), UserNameProp: nameof(ModifyBy))]
    public class NoticeBasicIndex
    {
        public NoticeBasicIndex()
        {
            Documents = new HashSet<Document>();
        }
        public Guid NoticeBasicIndexId { get; set; }
        public Guid? NoticeId { get; set; }
        public Guid? CaseId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public string Particulars { get; set; }
        public string Annexure { get; set; }
        public int SerialNo { get; set; }
        public DateTime? IndexDate { get; set; }
        public int? NoOfPages { get; set; }        
        public string ModifyBy { get; set; } 
        public Notice Notice { get; set; }
        public Case Case { get; set; }
        public Guid? ApplicationDocumentID { get; set; }
        public ICollection<Document> Documents { get; set; }
    }
}
