﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CrimeDetailMvcModel : BaseEntityModel<CrimeDetail, CrimeDetailMvcModel>
    {
        public Guid CrimeDetailID { get; set; }
        public Guid NoticeId { get; set; }
        [Display(Name = "Crime No")]
        public string CrimeNo { get; set; }
        [Display(Name = "Crime Year")/*, Range(1980, 2021, ErrorMessage = "Invalid Year")*/]
        public int CrimeYear { get; set; }
        [Display(Name = "FIR No")]
        public string FIRNO { get; set; }
        [Display(Name = "FIR/Decision Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FIRDate { get; set; }

        [Display(Name = "IPC Section")]
        public Guid[] IPCSectionIdS { get; set; } 
        public Guid[] IPCSectionIds_Selected { get; set; }
        [Display(Name = "Section or Act")]
        public Guid[] ActtypeIDS { get; set; }

        [Display(Name = "Section or Act")]
        public string SectionOrAct { get; set; }
        public string District { get; set; }
        [Display(Name = "Police Station")]
        public Guid PoliceStationID { get; set; }
        [Display(Name = "Case No")]
        public string CaseNo { get; set; }
        [Display(Name = "Pending in Court of")]
        public string CaseNo_Court { get; set; }
        [Display(Name = "Trial No")]
        public string TrailNo { get; set; }
        [Display(Name = "Pending in Court of")]
        public string TrailNo_Court { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }
        public PoliceStationMvcModel PoliceStation { get; set; }

        public override CrimeDetailMvcModel EntityToResponse(CrimeDetail entity, CrimeDetailMvcModel model)
        {
            model.PoliceStation = PoliceStationMvcModel.CopyFromEntity(entity.PoliceStation, model.RecursiveLevel);
            return model;
        }
    }
}