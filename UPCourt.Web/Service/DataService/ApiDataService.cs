﻿namespace UPCourt.Web.Service.DataService
{
    using System.Linq;
    using UPCourt.Entity;

    public class PurposeDataService
    {

        public static IQueryable<Purpose> GetAll(DatabaseContext db)
        {
            return db.Purposes;
        }
        public static IQueryable<Purpose> GetLite(DatabaseContext db)
        {
            return db.Purposes.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Purpose> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }
    public class DisposalTypeDataService
    {
        public static IQueryable<DisposalType> GetAll(DatabaseContext db)
        {
            return db.DisposalTypes;
        }
        public static IQueryable<DisposalType> GetLite(DatabaseContext db)
        {
            return db.DisposalTypes.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<DisposalType> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }

    public class CauseListTypeDataService
    {
        public static IQueryable<CauseListType> GetAll(DatabaseContext db)
        {
            return db.CauseListTypes;
        }
        public static IQueryable<CauseListType> GetLite(DatabaseContext db)
        {
            return db.CauseListTypes.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<CauseListType> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }

    public class CauseListDataService
    {
        public static IQueryable<DateWiseCauseList> GetAllDateWise(DatabaseContext db)
        {
            return db.DateWiseCauseLists;
        }
        public static IQueryable<CauseList> GetAll(DatabaseContext db)
        {
            return db.CauseLists;
        }
        public static IQueryable<CauseList> GetLite(DatabaseContext db)
        {
            return db.CauseLists.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<DateWiseCauseList> GetLiteDateWise(DatabaseContext db)
        {
            return db.DateWiseCauseLists.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<CauseList> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
        public static IQueryable<DateWiseCauseList> GetDetailDateWise(DatabaseContext db)
        {
            var data = GetLiteDateWise(db);
            return data;
        }
    }

    public class CategoryDataService
    {
        public static IQueryable<Category> GetAll(DatabaseContext db)
        {
            return db.Categorys;
        }
        public static IQueryable<Category> GetLite(DatabaseContext db)
        {
            return db.Categorys.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Category> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }
    public class SubCategoryDataService
    {
        public static IQueryable<SubCategory> GetAll(DatabaseContext db)
        {
            return db.SubCategorys;
        }
        public static IQueryable<SubCategory> GetLite(DatabaseContext db)
        {
            return db.SubCategorys.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<SubCategory> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }
    public class BencheDataService
    {
        public static IQueryable<Benche> GetAll(DatabaseContext db)
        {
            return db.Benches;
        }
        public static IQueryable<Benche> GetLite(DatabaseContext db)
        {
            return db.Benches.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Benche> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }
    public class BenchejudgeService
    {
        public static IQueryable<BencheJudge> GetAll(DatabaseContext db)
        {
            return db.BencheJudges;
        }
        public static IQueryable<BencheJudge> GetLite(DatabaseContext db)
        {
            return db.BencheJudges.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<BencheJudge> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }

    public class CasedetailDataService
    {
        public static IQueryable<Casedetail> GetAll(DatabaseContext db)
        {
            return db.Casedetails;
        }
        public static IQueryable<Casedetail> GetLite(DatabaseContext db)
        {
            return db.Casedetails.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<Casedetail> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }

    public class CasedetailiaDataService
    {
        public static IQueryable<CasedetailIa> GetAll(DatabaseContext db)
        {
            return db.Casedetailias;
        }
        public static IQueryable<CasedetailIa> GetLite(DatabaseContext db)
        {
            return db.Casedetailias.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<CasedetailIa> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data;
        }
    }
}