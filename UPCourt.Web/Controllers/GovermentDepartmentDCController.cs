﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;



namespace UPCourt.Web.Controllers
{
    [AllowAnonymous]
    public class GovermentDepartmentDCController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }

        public GovermentDepartmentDCController() : this(new CustomUserManager())
        {
        }
        public GovermentDepartmentDCController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }
        // GET: Department
        [PageRoleAuthorizarion(PageName.GovermentDepartment, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            var dbData = GovermentDepartmentDCDataService.GetLite(db).ToList();
            return View(GovernmentDepartmentDCMvcModel.CopyFromEntityList(dbData, 0));
        }
        // GET: Department/Create
        public ActionResult Create()
        {
            InitilizeDistricts(null);
            GovernmentDepartmentDCMvcModel model = new GovernmentDepartmentDCMvcModel()
            {
                GovernmentDepartmentDCContacts = new List<GovernmentDepartmentDCContactMvcModel>()
            };
            model.GovernmentDepartmentDCContacts.Add(new GovernmentDepartmentDCContactMvcModel() { bActive = true });
            return View(model);
        }
        public class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }
            [JsonProperty("error-codes")]
            public List<string> ErrorMessage { get; set; }
        }
        public static CaptchaResponse ValidateCaptcha(string response)
        {
            string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            return JsonConvert.DeserializeObject<CaptchaResponse>(jsonResult.ToString());
        }

        [HttpPost]
        public ActionResult Create(GovernmentDepartmentDCMvcModel model)
        {
            InitilizeDistricts(model.District);
            // Code for validating the CAPTCHA
            CaptchaResponse response = ValidateCaptcha(Request["g-recaptcha-response"]);

            if (!response.Success)
            {
                ModelState.AddModelError(nameof(model.DepartmentShortName), $"Invalid Captch");
                return View(model);
            }

            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            GovernmentDepartmentDC tempData = new GovernmentDepartmentDC
            {
                GovernmentDepartmentDCId = Guid.NewGuid(),
                DepartmentShortName = model.DepartmentShortName,
                DepartmentFullName = model.DepartmentFullName,
                DepartmentType = model.DepartmentType,
                Address1 = model.Address1,
                Address2 = model.Address2,
                Division = model.Division,
                CreatedOn = DateTime.Now,
                District = model.District,
                Tehsil = model.Tehsil,
                GovBlk = model.GovBlk,
            };
            if (model.District == "Others")
                tempData.District = model.OtherDistrict;

            int seq = 1;

            foreach (var contact in model.GovernmentDepartmentDCContacts)
            {
                GovernmentDepartmentDCContact con = new GovernmentDepartmentDCContact()
                {
                    GovernmentDepartmentDCContactId = Guid.NewGuid(),
                    GovernmentDepartmentDCId = tempData.GovernmentDepartmentDCId,
                    ContactName = contact.ContactName,
                    CreatedOn = DateTime.Now,
                    Department = contact.Department,
                    Sequence = seq,
                    Designation = contact.Designation,
                    Email = contact.Email,
                    Mobile = contact.Mobile,
                    Phone = contact.Phone
                };
                tempData.GovernmentDepartmentDCContacts.Add(con);
                seq += 1;
            }


            db.GovernmentDepartmentDC.Add(tempData);
            db.SaveChangesWithAudit(Request, "");
            SucessMessage = $"DC Govt. Department {tempData.DepartmentShortName} Created successfully";
            ViewBag.Msg = SucessMessage;
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create");
        }

        private bool CommonForInsertUpdate(GovernmentDepartmentDCMvcModel model)
        {
            return true;
        }

        private void InitilizeDistricts(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
            var mvc = DistrictMvcModel.CopyFromEntityList(dbData, 0);
            mvc.Add(new DistrictMvcModel() { DistrictName = "Others" });
            var districts = new SelectList(mvc, nameof(DistrictMvcModel.DistrictName)
                , nameof(DistrictMvcModel.DistrictName), selectedValue);
            ViewBag.Districts = districts;
        }

        private void InitilizeDivision(object selectedValue)
        {
            var dbData = DivisionDataService.GetLite(db).OrderBy(i => i.DivisionName).ToList();
            var mvc = DivisionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Division = new SelectList(mvc, nameof(DivisionMvcModel.DivisionName)
                , nameof(DivisionMvcModel.DivisionName), selectedValue);
        }
        private void InitilizeTehsil(object selectedValue)
        {
            var dbData = TehsilDataService.GetLite(db).OrderBy(i => i.TehsilName).ToList();
            var mvc = TehsilMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Tehsil = new SelectList(mvc, nameof(TehsilMvcModel.TehsilName)
                , nameof(TehsilMvcModel.TehsilName), selectedValue);
        }
    }
}
