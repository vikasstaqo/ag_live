﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UPCourt.Web.Models
{
    public class ResponseModel
    {
        public e_StatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }

        public JsonResult ResponseJSON(e_StatusCode code, string sMessage, object oData, string sToken)
        {
            var oRetval = new ResponseModel()
            {
                StatusCode = code,
                Message = sMessage,
                Data = oData,
            };

            var result = new JsonResult();
            result.Data = oRetval;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.MaxJsonLength = int.MaxValue;
            result.ContentType = "application/json";

            return result;
        }


        public enum e_StatusCode
        {
            Success = 101,
            ValidationFails,
            NoDataFound,
            ExceptionOccurs,
            FileIsMissing,
            DuplicateRecords
        }
    }
}