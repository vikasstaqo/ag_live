﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Description), UserNameProp: nameof(CreatedBy))]
    public class CaseTransfer : BaseTransEntity
    {
        public Guid CaseTransferId { get; set; }
        public Guid CaseId { get; set; }
        public Guid CourtId { get; set; }
        public Guid? OldCourtId { get; set; }
        public DateTime? TransferDate { get; set; }
        public string Description { get; set; }
        public Case Case { get; set; }
        public Court Court { get; set; }


    }
}
