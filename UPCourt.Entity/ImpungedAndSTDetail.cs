﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog:true,DisplayProp:nameof(Judge),UserNameProp:nameof(ModifyBy))]
    public class ImpungedAndSTDetail
    { 
        public Guid ImpungedAndSTDetailID { get; set; }
        public Guid NoticeId { get; set; }
        public DetailsType DetailsType { get; set; }
        public CourtType CourtType { get; set; }
        public string District { get; set; }
        public Guid SubordinateCourtId { get; set; }
        public SubordinateCourt SubordinateCourt { get; set; }
        public Guid CaseNoticeTypeId { get; set; }
        public CaseNoticeType CaseNoticeType { get; set; }
        public int CaseYear { get; set; }
        public int CaseNo { get; set; }
        public string Judge { get; set; }
        public string CNRNo { get; set; }
        public DateTime DateOn { get; set; }
        public bool IsActive { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyOn { get; set; }
    }
    public enum CourtType
    {
        [Description("Lower Court")]
        LowerCourt = 1,
        [Description("Higher Court")]
        HighCourt = 2,
    }

    public enum DetailsType
    {
        [Description("Impunged Order")]
        ImpungedOrder = 1,
        [Description("ST Detail")]
        STDetail = 2,
    }
}
