﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class SalutationController : BaseMVCController
    {
        // GET: Salutation
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(SalutationMvcModel.CopyFromEntityList(SalutationDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new SalutationMvcModel
            {
                Status = SalutationStatus.Active
            };
            return View(model);
        }
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(SalutationMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Salutation tempData = new Salutation()
            {
                SalutationId = Guid.NewGuid(),
                Name = model.Name,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetSalutationsDisplayOrder(db)
            };
            db.Salutations.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = SalutationDataService.GetDetail(db).Where(i => i.SalutationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(SalutationMvcModel.CopyFromEntity(dbData, 0));
        }
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(SalutationMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = SalutationDataService.GetDetail(db).Where(i => i.SalutationId == model.SalutationId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.Name = model.Name;
            dbData.Status = model.Status;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        private bool CommonForInsertUpdate(SalutationMvcModel model)
        {
            var duplicateCheck = SalutationDataService.GetLite(db).FirstOrDefault(i => i.SalutationId != model.SalutationId && i.Name == model.Name);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }
            return true;
        }
        [PageRoleAuthorizarion(PageName.Salutation, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = SalutationDataService.GetLite(db).Where(i => i.SalutationId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
