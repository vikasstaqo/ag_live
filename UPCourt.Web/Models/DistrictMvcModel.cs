﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DistrictMvcModel: BaseEntityModel<District, DistrictMvcModel>
    {
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public DistrictStatus Status { get; set; }
    }
}