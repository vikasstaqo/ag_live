﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace UPCourt.Web.Core
{
    public static class RegularExpressionHelper
    {
        public const string PanValidationExpression = @"^[A-Z]{3}P[A-Z]{1}\d{4}[A-Z]{1}$";
        public const string TanValidationExpression = @"^[A-Z]{4}[0-9]{5}[A-Z]{1}$";
        public const string AadhaarValidationExpression = @"^[2-9]{1}[0-9]{3}\\s[0-9]{4}\\s[0-9]{4}$";
        public const string EmailValidationExpression = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        public const string PinCodeValidationExpression = @"^\d{6}$";
        public const string PhoneNoValidationExpression = @"\+?[0-9-]+$";
        public const string BankAcNoValidationExpression = @"^\d{9,18}$";
        public const string IFSCodeValidationExpression = @"^[A-Z]{4}0[A-Z0-9]{6}$";
        public const string AlphabateOnlyWithSpace = @"^[a-zA-Z][a-zA-Z\. ]*$";

        public static bool IsValidExpression(string StringToCompare, string ValidationExpressionPattern)
        {
            bool isValid = false;

            if ((ValidationExpressionPattern != null) && (ValidationExpressionPattern.Trim().Length > 0) &&
                (StringToCompare != null) && (StringToCompare.Trim().Length > 0))
            {
                try
                {
                    Regex regex = new Regex(ValidationExpressionPattern);
                    Match match = regex.Match(StringToCompare);
                    isValid = match.Success;
                }
                catch (Exception)
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }
    }
}