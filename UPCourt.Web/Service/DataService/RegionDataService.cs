﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class RegionDataService
    {
        public static IQueryable<Region> GetLite(DatabaseContext db)
        {
            return db.Regions.Where(x => x.IsDeleted == false);
        }
    }
}