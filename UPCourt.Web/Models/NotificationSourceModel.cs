﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class NotificationSourceModel : BaseEntityModel<Entity.NotificationSource, NotificationSourceModel>
    {
        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public bool IsActive { get; set; }
    }
}