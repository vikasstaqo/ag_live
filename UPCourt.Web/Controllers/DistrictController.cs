﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class DistrictController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(DistrictMvcModel.CopyFromEntityList(DistrictDataService.GetLite(db).OrderByDescending(x => x.DisplayOrder), 0));
        }

        [HttpGet]
        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new DistrictMvcModel();
            model.Status = DistrictStatus.Active;
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(DistrictMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            District tempData = new District()
            {
                DistrictId = Guid.NewGuid(),
                DistrictName = model.DistrictName,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetDistrictDisplayOrder(db)
            };
            db.Districts.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }


        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = DistrictDataService.GetLite(db).Where(i => i.DistrictId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            DistrictMvcModel model = DistrictMvcModel.CopyFromEntity(dbData, 0);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(DistrictMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = DistrictDataService.GetLite(db).Where(i => i.DistrictId == model.DistrictId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.DistrictName = model.DistrictName;
            dbData.Status = model.Status;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        private bool CommonForInsertUpdate(DistrictMvcModel model)
        {
            var duplicateCheck = DistrictDataService.GetLite(db).FirstOrDefault(i => i.DistrictId != model.DistrictId && i.DistrictName == model.DistrictName);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.DistrictName), $"{model.DistrictName} already exists");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.District, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = DistrictDataService.GetLite(db).Where(i => i.DistrictId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}