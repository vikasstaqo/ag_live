﻿
$(document).ready(function () {

    setTimeout(function () {
        $('#Petitioners_0__Name').focus();
    });
    $(document).ajaxStart(function () { ShowLoader(); });
    $(document).ajaxComplete(function () { HideLoader(); });

    $("#CreateNoticeForm").on("submit", function () { ShowLoader(); });
    $("#EditNoticeForm").on("submit", function () { ShowLoader(); });


    var stepper = new Stepper($('.bs-stepper')[0])


    $('.IPCSectionIdS').tokenize2({
        sortable: true
    });

 
    $(".Department").hide();

    $(".NextBtn").click(function () {

        var ButtonId = $(this).attr('id');
        var parent_card;
        if (ButtonId == "NextBtn1")
            parent_card = $('#basic-details');
        else if (ButtonId == "NextBtn2")
            parent_card = $('#case-details');
        else
            parent_card = $('#basic-index');

        var next_step = true;

        parent_card.find('.required').each(function () {
            if ($(this).is(":visible")) {
                if ($(this).val() == "") {
                    $(this).addClass('input-error');
                    $(this).siblings('label').addClass("red");
                    $(this).parents('.input-group').siblings('label').addClass("red");
                    ToastError("Required : " + $(this).siblings('label').html());
                    next_step = false;
                    return false;
                }
                else {
                    $(this).removeClass('input-error');
                    $(this).parents('.input-group').siblings('label').removeClass("red");
                    $(this).siblings('label').removeClass("red");

                    parent_card.find('.PhoneNumber').each(function () {
                        if ($(this).val() != "" && !validatePhoneNo($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Invalid PhoneNo In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });
                    parent_card.find('.EmailAddress').each(function () {
                        if ($(this).val() != "" && !validateEmail($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Invalid Email Address In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });
                    parent_card.find('.OnlyAlphabate').each(function () {
                        if ($(this).val() != "" && !validateAlphabateOnlyWithSpace($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Only Alphabate In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });
                    parent_card.find('.AadhaarNo').each(function () {
                        if ($(this).val() != "" && !validateAadhaar($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Only 12 digit Adhar No In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });
                    parent_card.find('.PanNo').each(function () {
                        if ($(this).val() != "" && !validatePanNo($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Invalid Pan No In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });
                    parent_card.find('.Year').each(function () {
                        if ($(this).val() != "" && !validateYear($(this).val())) {
                            $(this).addClass('input-error');
                            $(this).parents('.input-group').siblings('label').addClass("red");
                            ToastError("Invalid Year In " + $(this).siblings('label').html());
                            next_step = false;
                            return false;
                        }
                        else {
                            $(this).removeClass('input-error');
                            $(this).parents('.input-group').siblings('label').removeClass("red");
                            $(this).siblings('label').removeClass("red");
                        }
                    });

                }
            }
        });

        if (next_step) {
            if (ButtonId == "NextBtn1") {
                stepper.next();
                $('#RegionId').focus();
            }
            else if (ButtonId == "NextBtn2") {
                stepper.next();
                $('#NoticeBasicIndexs_0__Particulars').focus();
            }
        }
    });
    $(".previousBtn").click(function () {
        stepper.previous();
    });


});

var max = 10;


function RefreshDeparment() {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetDepartmentList",
        async: false,
        data: { "pid": 1 },
        success: function (response) {
            var GovDept = JSON.parse(response);
            $('.DDLGovDept').empty();
            $('.DDLGovDept').append('<option value>--Select--</option>');
            GovDept.forEach(function (obj, idx) {
                $('.DDLGovDept').append($("<option></option>").val(obj.GovernmentDepartmentId).html(obj.Name));
            });
        }
    });

}


function showParentNoticeTypeDetails(obj) {
    debugger;
    $.ajax({
        type: "Get",
        async: false,
        url: "/NoticeCreate/GetNoticeTypes",
        data: { "pid": $(obj).val() },
        success: function (response) {
            var Notice = JSON.parse(response);
            $("#CaseNoticeTypeId").empty();
            $("#CaseNoticeTypeId").append('<option value>--Select--</option>');
            Notice.forEach(function (obj, idx) {
                $("#CaseNoticeTypeId").append($("<option></option>").val(obj.CaseNoticeTypeId).html(obj.Name));
            })
        }
    });

   

    if ($(obj).val() == "2") {
        $("#dvCrimeDetails").show();
    }
    else {
        $("#dvCrimeDetails").hide();
    }

}

function getSubjects(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetSubjects",
        data: { "ntid": $(obj).val() },
        success: function (response) {
            var Subj = JSON.parse(response);
            $("#Subject").empty();
            $("#Subject").append('<option value>--Select--</option>');
            Subj.forEach(function (obj, idx) {
                $("#Subject").append($("<option></option>").val(obj.Name).html(obj.Name));
            })
        }
    });
}

function getPoliceStations(obj) {
    $.ajax({
        type: "Get",
        url: "/NoticeCreate/GetPoliceStations",
        data: { "d": $(obj).val() },
        success: function (response) {
            var Notice = JSON.parse(response);
            $("#CrimeDetail_PoliceStationID").empty();
            $("#CrimeDetail_PoliceStationID").append('<option value>--Select--</option>');
            Notice.forEach(function (obj, idx) {
                $("#CrimeDetail_PoliceStationID").append($("<option></option>").val(obj.PoliceStationId).html(obj.Locality + ' Police'));
            })
        }
    });
}

function IDProofChange(IDProof) {
    var IDProofVal = $(IDProof).val();
    var IDProofNumbertxt = $('#' + $(IDProof).attr('id').replace('IDProof', 'IDProofNumber'));
    IDProofNumbertxt.removeClass("AadhaarNo");
    IDProofNumbertxt.removeClass("PanNo");
    if (IDProofVal == 0) {
        IDProofNumbertxt.addClass("AadhaarNo");
    }
    else if (IDProofVal == 2) {
        IDProofNumbertxt.addClass("PanNo");
    }


}

function CheckNoticeNo(NoticeNotext) {

    var NoticeId = $('#NoticeId').val();
    var NoticeNo = $(NoticeNotext).val();
    if (NoticeNo.length > 40) {
        ToastWarning("Max 40 character allowed for Notice No");
        $(NoticeNotext).val(NoticeNo.substring(0, 40));
    }
    else {
        $.ajax({
            type: "Get",
            async: true,
            url: "/NoticeCreate/CheckNoticeNo",
            data: { "NoticeNo": NoticeNo, "NoticeId": NoticeId },
            success: function (response) {
                var Notice = JSON.parse(response);

                if (Notice != null && Notice != undefined && Notice.NoticeNo == NoticeNo) {
                    ToastError("NoticeNo : '" + NoticeNo + "' already exists!!");
                    $(NoticeNotext).val('');
                }
            }
        });
    }
}

function AddCounselor() {

    var rowCount = $('.CounselorRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {
        var newCounselorRow = $('#CounselorEliment_' + LastRowCount).clone();

        newCounselorRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));

        });
        newCounselorRow.find(".IsFirstRow").css("display", "none");
        newCounselorRow.find(".Removebtn").css("display", "block");
        newCounselorRow.find(".NewId").val('');

        $('#DivCounselor').append("<div class='CounselorRow' id='CounselorEliment_" + rowCount + "' style='border: #7d7d7dee thin dashed;  margin-bottom:15px; background:#eeeeee4a; padding:10px;'>" + newCounselorRow.html() + "</div>");
    }
}

function AddPetitioner() {
    var rowCount = $('.PetitionerRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {
        var newPetitionerRow = $('#PetitionerEliment_' + LastRowCount).clone();

        newPetitionerRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));
        });
        newPetitionerRow.find(".IsFirstRow").css("display", "none");
        newPetitionerRow.find(".Removebtn").css("display", "block");
        newPetitionerRow.find(".NewId").val('');
        $('#DivPetitioner').append("<div class='PetitionerRow' id='PetitionerEliment_" + rowCount + "' style='border: #7d7d7dee thin dashed;  margin-bottom:15px; background:#eeeeee4a; padding:10px;'>" + newPetitionerRow.html() + "</div>");
    }
}

function AddRespondent() {
    var rowCount = $('.RespondentRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {
        var newRespondentRow = $('#RespondentEliment_' + LastRowCount).clone();
        newRespondentRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));
            $(this).removeAttr("readonly", "");
        });
        newRespondentRow.find(".IsFirstRow").css("display", "none");
        newRespondentRow.find(".Removebtn").css("display", "block");

        newRespondentRow.find("#DepartmentEliment_" + LastRowCount).attr("id", "DepartmentEliment_" + rowCount).css("display", "none");
        newRespondentRow.find("#GovDegignationEliment_" + LastRowCount).attr("id", "GovDegignationEliment_" + rowCount).css("display", "none");
        newRespondentRow.find("#NonDepartmentEliment_" + LastRowCount).attr("id", "NonDepartmentEliment_" + rowCount).css("display", "none");
        newRespondentRow.find("#NonDepartmentEliment1_" + LastRowCount).attr("id", "NonDepartmentEliment1_" + rowCount).css("display", "block");
        newRespondentRow.find("#NonDepartmentEliment2_" + LastRowCount).attr("id", "NonDepartmentEliment2_" + rowCount).css("display", "block");
        newRespondentRow.find(".NewId").val('');

        $('#DivRespondent').append("<div class='RespondentRow' id='RespondentEliment_" + rowCount + "' style='border: #7d7d7dee thin dashed;  margin-bottom:15px; background:#eeeeee4a; padding:10px;'>" + newRespondentRow.html() + "</div>");
    }
}

function chked(radio) {

    var DepartmentDiv = $('#' + $(radio).attr("id").replace("NoticeRespondents", "DepartmentEliment").replace("__IsPartyDepartment", ""));
    var GovDegignationDiv = $('#' + $(radio).attr("id").replace("NoticeRespondents", "GovDegignationEliment").replace("__IsPartyDepartment", ""));
    var NonDepartmentDiv = $('#' + $(radio).attr("id").replace("NoticeRespondents", "NonDepartmentEliment").replace("__IsPartyDepartment", ""));
    var NonDepartmentDiv1 = $('#' + $(radio).attr("id").replace("NoticeRespondents", "NonDepartmentEliment1").replace("__IsPartyDepartment", ""));
    var NonDepartmentDiv2 = $('#' + $(radio).attr("id").replace("NoticeRespondents", "NonDepartmentEliment2").replace("__IsPartyDepartment", ""));
   

    var DepartmentDDL = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "GovernmentDepartmentId"));
    var PartyName = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "PartyName"));
    var ContactNo = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "ContactNo"));
    var Email = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "Email"));
    var MobileNo = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "MobileNo"));
    var FaxNo = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "FaxNo"));
    var Address = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "Address"));
    var City = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "Pincode"));
    var City = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "City"));
    var City = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "Pincode"));
  
    var GovDegignationDDL = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "GovernmentDesignationId"));
    var DistrictDDL = $('#' + $(radio).attr("id").replace("IsPartyDepartment", "District"));
    var Checked = $(radio).is(':checked');
    if (Checked) {
        DepartmentDiv.show();
        GovDegignationDiv.show();
        NonDepartmentDiv.hide();
        NonDepartmentDiv1.hide();
        NonDepartmentDiv2.hide();
        PartyName.attr("readonly", "readonly");
        ContactNo.attr("readonly", "readonly");
        Email.attr("readonly", "readonly");
        MobileNo.attr("readonly", "readonly");
        FaxNo.attr("readonly", "readonly");

        DepartmentDDL.val('');
        DepartmentDDL.addClass('required');
        GovDegignationDDL.val('');
        GovDegignationDDL.addClass('required');
        DistrictDDL.val('');
        DistrictDDL.addClass('required');
        PartyName.val('');
        ContactNo.val('');
        Email.val('');
        MobileNo.val('');
        FaxNo.val('');
        Address.val('');
        City.val('');
    } else {
        DepartmentDiv.hide();
        GovDegignationDiv.hide();
        NonDepartmentDiv.show();
        NonDepartmentDiv1.show();
        NonDepartmentDiv2.show();
        DepartmentDDL.removeClass('required');
        GovDegignationDDL.removeClass('required');
        DepartmentDDL.val('');
        GovDegignationDDL.val('');
        DistrictDDL.val('');
        DistrictDDL.removeClass('required');
        PartyName.val('');
        ContactNo.val('');
        Email.val('');
        MobileNo.val('');
        FaxNo.val('');
        Address.val('');
        City.val('');
        PartyName.removeAttr("readonly", "");
        ContactNo.removeAttr("readonly", "");
        Email.removeAttr("readonly", "");
    }
}

function GetDepartmentDetail(GovDept) {

    var DepartmentId = $(GovDept).val();
    var PartyName = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "PartyName"));
    var ContactNo = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "ContactNo"));
    var Email = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "Email"));
    var MobileNo = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "MobileNo"));
    var FaxNo = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "FaxNo"));
    var GovDegignationDDL = $('#' + $(GovDept).attr("id").replace("GovernmentDepartmentId", "GovernmentDesignationId"));

    $.ajax({
        type: "Get",
        async: false,
        url: "/NoticeCreate/GetDepartment/",
        data: { "GovtDepartmentId": DepartmentId },
        success: function (response) {
            if (response != null && response != "null") {
                var depdate = JSON.parse(response);
                PartyName.val(depdate.POCName);
                Email.val(depdate.Email);
                ContactNo.val(depdate.POCContact);
                MobileNo.val(depdate.POCMobileNo);
                FaxNo.val(depdate.POCFaxNo);
            }
            else {
                PartyName.val('');
                Email.val('');
                ContactNo.val('');
                MobileNo.val('');
                FaxNo.val('');
            }
        }
    });

    $.ajax({
        type: "Get",
        async: false,
        url: "/NoticeCreate/GetDepartmentDesignation/",
        data: { "GovtDepartmentId": DepartmentId },
        success: function (response) {
            var Degignation = JSON.parse(response);
            GovDegignationDDL.empty();
            GovDegignationDDL.append('<option value>--Select--</option>');
            Degignation.forEach(function (obj, idx) {
                GovDegignationDDL.append($("<option></option>").val(obj.GovernmentDesignationId).html(obj.DesignationName));
            });
        }
    });
}

function AddBasicIndex() {

    var rowCount = $('.BasicIndexRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {

        var newBasicIndexRow = $('#BasicIndexEliment_' + LastRowCount).clone();
        newBasicIndexRow.find("select, textarea, input, span, label, a").each(function () {
            if (this.tagName === 'A' && this.id !== 'RemoveBasicIndexEliment_' + LastRowCount) {
                $(this).remove();
                return;
            }
            if ($(this).attr('type') === 'file')
                $(this).show();
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));
            if ($(this).attr('onclick') != undefined)
                $(this).attr('onclick', $(this).attr("onclick").replace(LastRowCount, rowCount));
            if ($(this).attr('id') !== 'NoticeBasicIndexs_' + rowCount + '__bActive')
                $(this).attr('value', '');

        });
        newBasicIndexRow.find(".Removebtn").css("display", "block");
        newBasicIndexRow.find(".NewId").val('');
        $('#DivBasicIndex').append("<tr class='BasicIndexRow' id='BasicIndexEliment_" + rowCount + "' >" + newBasicIndexRow.html() + "</tr>");
    }
}


function AddIPCSection() {

    var rowCount = $('.IPCSectionRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {

        var newIPCSectionRow = $('#IPCSectionEliment_' + LastRowCount).clone();
        newIPCSectionRow.find("select, textarea, input, span, label, a").each(function () {
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));

        });
        newIPCSectionRow.find(".Removebtn").css("display", "block");
        $('#DivIPCSection').append("<tr class='IPCSectionRow' id='IPCSectionEliment_" + rowCount + "' >" + newIPCSectionRow.html() + "</tr>");
    }
}

function RemoveAddedItem(btn) {
    var id = $(btn).attr('id');
    $('#' + id.replace('Remove', '')).hide();
    $('#' + id.replace('Remove', '')).find("[id$=bActive]").val("false");
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validatePhoneNo(Phone) {
    var re = /^\+?[0-9-]+$/;
    return re.test(Phone);
}

function validateAlphabateOnlyWithSpace(Name) {
    var re = /^[a-zA-Z][a-zA-Z\. ]*$/;
    return re.test(Name);
}

function validateAadhaar(AadhaarNo) {
    var re = /^\d{12}$/;
    return re.test(AadhaarNo);
}

function validatePanNo(PanNo) {
    var re = /^[A-Z]{3}P[A-Z]{1}\d{4}[A-Z]{1}$/;
    return re.test(PanNo);
}

function validateYear(year) {
    var re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    return re.test(year);
}


function ValidateUpdFile(val) {

    var ret = false;
    var fuData = document.getElementById(val);
    var FileUploadPath = fuData.value;
    var validFileSize = 120 * 1024 * 1024;
    var sizeoffile = fuData.files[0].size;
    var origanalpath = fuData.valueOf();

    if (FileUploadPath == '') {
        ret = false;
    }
    else {
        if (sizeoffile !== 0 && sizeoffile <= validFileSize) {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
            if (Extension == "exe" || Extension == "rar" || Extension == "zip") {
                ret = false;
                ToastWarning("Invalid File Format " + Extension + " is not allowed");
                document.getElementById(val).value = '';
            }
            else {
                ret = true;
            }
        }
        else {
            ret = false;
            ToastWarning("File Size Should be Greater than 0 and less than 120 MB");
            document.getElementById(val).value = '';
        }

    }

    return ret;
}

$(document).ready(function () {
    $("#CreateNoticeForm, #EditNoticeForm").submit(function () {
        $(".IPCSectionIdS").closest('div').find('.tokens-container li.token').each(function (obj) {
            $('#crime_ipcs').append($('<option></option>').val($(this).data('value')).html($(this).text()).prop('selected', true));
        });
    });
});
