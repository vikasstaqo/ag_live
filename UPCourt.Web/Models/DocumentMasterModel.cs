﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DocumentMasterModel : BaseEntityModel<Entity.DocumentMaster, DocumentMasterModel>
    {
        public Guid DocumentId { get; set; }
        [Display(Name = "Document Name")]
        public string DocumentName { get; set; }
        public string Description { get; set; }
        public string ModifyBy { get; set; }
    }
}