﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPCourt.Web.Models
{
    public class ApiDataModel
    {
        public class getBenchTypes
        {
            public int bench_type_code { get; set; }
            public string bench_type_name { get; set; }
        }
        public class getJudges
        {
            public int judge_code { get; set; }
            public string judge_name { get; set; }
            public string short_judge_name { get; set; }
        }

        public class getOIPC
        {
            public int actcode { get; set; }
            public string actname { get; set; }
        }

        public class getCaseTypes
        {
            public int case_type { get; set; }
            public string full_form { get; set; }
            public string type_name { get; set; }
            public string type_flag { get; set; }
        }

        public class getPurpose
        {
            public int purpose_code { get; set; }
            public string purpose_name { get; set; }
        }

        public class getDisposalType
        {
            public int disp_type { get; set; }
            public string disp_name { get; set; }
        }

        public class getCauseListType
        {
            public int cause_list_type_id { get; set; }
            public string cause_list_type { get; set; }
        }

        public class getCauseListDateWise
        {
            public string pet_name { get; set; }
            public int bench_id { get; set; }
            public string cause_list_eliminate { get; set; }
            public string elimination { get; set; }
            public int causelist_id { get; set; }
            public string res_name { get; set; }
            public string cause_list_status { get; set; }
            public int causelist_sr_no { get; set; }
            public string case_no { get; set; }
            public string published { get; set; }
            public DateTime causelist_date { get; set; }
            public int causelist_type { get; set; }
            public int court_no { get; set; }
            public int for_bench_id { get; set; }
        }
        public class getCategorySubCategory
        {
            public int subject_code { get; set; }
            public string subject_name { get; set; }

            public int subnature1_cd { get; set; }
            public string subnature1_desc { get; set; }
        }
        public class getCategory
        {
            public int subject_code { get; set; }
            public string subject_name { get; set; }
        }
        public class getSubCategory
        {
            public int subject_code { get; set; }
            public int subnature1_cd { get; set; }
            public string subnature1_desc { get; set; }
        }


        public class getBenches
        {
            public int for_bench_id { get; set; }
            public int court_no { get; set; }
            public string room_no { get; set; }
            public List<getJudges> judges { get; set; }
        }
        public class getNoticeNo
        {
            public int reg_year { get; set; }
            public int regcase_type { get; set; }
            public int reg_no { get; set; }
            public string cnr { get; set; }
        }

        public class getCaseDetail
        {
            public string pet_email { get; set; }
            public string pet_adv { get; set; }
            public string pet_adv_cd { get; set; }
            public string pet_name { get; set; }
            public string res_mobile { get; set; }
            public string pet_age { get; set; }
            public string res_age { get; set; }
            public string res_sex { get; set; }
            public string res_adv_cd { get; set; }
            public string pet_father_name { get; set; }
            public string res_name { get; set; }
            public string pet_mobile { get; set; }
            public string pet_sex { get; set; }
            public string res_email { get; set; }
            public string res_father_name { get; set; }
            public string case_no { get; set; }
            public string cino { get; set; }
            public string res_adv { get; set; }
            public CaseTypeMvcModel CaseType { get; internal set; }
        }


        public class CaseStatusAppDetails
        {
            public DateTime? date_of_order { get; set; }
            public int ia_regyear { get; set; }
            public string judge_code { get; set; }
            public int court_no { get; set; }
            public DateTime? date_of_hearing { get; set; }
            public DateTime? date_of_ia_registration { get; set; }
            public int ia_regno { get; set; }
        }

        public class AdvocateDetails
        {
            public string adv_phone1 { get; set; }
            public int adv_code { get; set; }
            public string off_add { get; set; }
            public string adv_reg { get; set; }
            public string email { get; set; }
            public string address { get; set; }
            public string adv_full_name { get; set; }
            public string adv_mobile { get; set; }
            public string adv_name { get; set; }
            public string adv_phone { get; set; }
        }

        public class Case_Respondents
        {
            public string name { get; set; }
            public int party_no { get; set; }
        }

        public class Case_Petitioners
        {
            public string name { get; set; }
            public int party_no { get; set; }
        }

        public class CaseStatus_IA
        {
            public DateTime? date_of_order { get; set; }
            public int ia_regyear { get; set; }
            public string judge_code { get; set; }
            public int court_no { get; set; }
            public DateTime? date_of_hearing { get; set; }
            public DateTime? date_of_ia_registration { get; set; }
            public int ia_regno { get; set; }
        }

        public class getCaseStatus
        {
            public DateTime? date_first_list { get; set; }
            public int case_dist_code { get; set; }
            public string pet_name { get; set; }
            public int bench_type { get; set; }
            public CaseStatusBenchType benchType { get; set; }
            public List<Case_Petitioners> Petitioners { get; set; }
            public int cs_subject { get; set; }
            public int disp_nature { get; set; }
            public int purpose_next { get; set; }
            public string judge_code { get; set; }
            public DateTime? date_of_decision { get; set; }
            public string res_name { get; set; }
            public int case_state_code { get; set; }
            public List<Case_Respondents> Respondents { get; set; }
            public int c_subject { get; set; }
            public List<CaseStatus_IA> IA { get; set; }
            public string case_no { get; set; }
            public string short_order { get; set; }
            public int causelist_type { get; set; }
            public int branch_id { get; set; }
            public CaseDetailMvcModel CaseDetail { get; set; }
            public CaseStatusSubject Subject { get; set; }
        }

        public class getCrimeDetails
        {
            public string fir_no { get; set; }
            public int fir_year { get; set; }
            public string dist_name { get; set; }
            public string police_st_name { get; set; }
        }
        public class getCaseDetails
        {
            public string pet_email { get; set; }
            public string pet_adv { get; set; }
            public int pet_adv_cd { get; set; }
            public string pet_name { get; set; }
            public string res_mobile { get; set; }
            public string pet_age { get; set; }
            public string res_age { get; set; }
            public string res_sex { get; set; }
            public int res_adv_cd { get; set; }
            public string pet_father_name { get; set; }
            public string res_name { get; set; }
            public string pet_mobile { get; set; }
            public string pet_sex { get; set; }
            public string res_email { get; set; }
            public string res_father_name { get; set; }
            public string case_no { get; set; }
            public string cino { get; set; }
            public string res_adv { get; set; }
            public CaseTypeMvcModel CaseType { get; internal set; }
        }
    }
}