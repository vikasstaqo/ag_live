﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Entity.Core;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using System.Configuration;

namespace UPCourt.Web.Controllers
{
    public class ReportController : BaseMVCController
    {
        // GET: Report
        [PageRoleAuthorizarion(PageName.AuditTrail, PagePermissionType.CanView)]
        public ActionResult Audit()
        {
            return View();
        }

        [HttpPost]
        [PageRoleAuthorizarion(PageName.AuditTrail, PagePermissionType.CanView)]
        public ActionResult AuditList([System.Web.Http.FromBody] AuditMvcModels model)
        {
            //
            using (SqlDB mySqlDB = new SqlDB())
            {
                string sqlQuery = @"SELECT GROUP_CONCAT(COLUMN_NAME) FROM information_schema.columns 
                                        WHERE TABLE_SCHEMA = '" + mySqlDB.DatabaseName + @"'  
                                              And TABLE_NAME='" + model.Audit_Type.Replace("'", "") + @"' 
                                              And COLUMN_NAME not in ('VirtualSessionId','action','action_by', 'browser','ip','log_date')  
                                    ";
                string columns = mySqlDB.ExecuteScalarString(sqlQuery);
                if (!string.IsNullOrEmpty(columns))
                {
                    sqlQuery = "select " + columns + ",action As Action,action_by as User_Name," +
                        "DATE_FORMAT(log_date, '%a, %D %b %Y') AS Action_Date, TIME_FORMAT(log_date, '%r') AS Action_Time," +
                        "browser as Browser,ip as IP_Address  from " + model.Audit_Type.Replace("'", "") + " where 1=1 ";
                }
                if (model.from.ToString("yyyy-MM-dd") != "0001-01-01")
                {
                    sqlQuery += " and date(log_date)>= '" + model.from.ToString("yyyy-MM-dd") + "'";
                }
                if (model.to.ToString("yyyy-MM-dd") != "0001-01-01")
                {
                    sqlQuery += " and date(log_date)<= '" + model.to.ToString("yyyy-MM-dd") + "'";
                }
                if (model.Action_by != null)
                    sqlQuery += " and action_by= '" + model.Action_by.Replace("'", "") + "'";
                if (model.Action != "0")
                    sqlQuery += " and action= '" + model.Action.Replace("'", "") + "'";
                sqlQuery += ";";
                DataTable ds = mySqlDB.GetDataTable(sqlQuery);
                return Json(JsonConvert.SerializeObject(ds), JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult BindDropDown()
        {
            using (SqlDB mySqlDB = new SqlDB())
            {
                string sqlQuery = @"Select 
                                        table_name 
                                    FROM 
                                        information_schema.tables 
                                    WHERE 
                                        TABLE_SCHEMA = '" + mySqlDB.DatabaseName + @"'  
                                        And TABLE_NAME LIKE '%_audit%';

                                    Select 
                                        userid,name 
                                    FROM 
                                        users 
                                    ORDER BY name";
                DataSet ds = mySqlDB.GetDataSet(sqlQuery);
                return Json(JsonConvert.SerializeObject(ds), JsonRequestBehavior.AllowGet);
            }
        }
    }
}