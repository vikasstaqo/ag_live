﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UPCourt.Web.Models
{
    public class ChangePasswordModel
    {

        [Required]
        [Display(Name = "User Name")]
        public string LoginName { get; set; }
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [RegularExpression(@"^[a-zA-Z0-9'@&#.\s]{7,10}$", ErrorMessage = "Password length must be between 7 to 10 characters which contain at least one numeric digit and a special character")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

    }
}