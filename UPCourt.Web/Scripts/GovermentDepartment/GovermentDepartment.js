﻿

var max = 1000;

function AddPhoneNo() {
    if (ValidAddPhoneNo()) {
        var rowCount = $('.PhoneNoRow').length;
        var LastRowCount = rowCount - 1;
        if (LastRowCount <= max) {
            var newPhoneNoRow = $('#PhoneNoEliment_' + LastRowCount).clone();

            newPhoneNoRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
                if ($(this).attr("id") != undefined)
                    $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
                if ($(this).attr("name") != undefined)
                    $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
                if ($(this).attr("data-valmsg-for") != undefined)
                    $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
                if ($(this).attr("for") != undefined)
                    $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));
                $(this).val('');
                $(this).attr('value', '');
                
            });
            newPhoneNoRow.find(".IsFirstRow").css("display", "none");
            newPhoneNoRow.find(".Removebtn").css("display", "block");
            newPhoneNoRow.find(".NewId").val('');

            $('#DivPhoneNo').append("<div class='PhoneNoRow ' id='PhoneNoEliment_" + rowCount + "' >" + newPhoneNoRow.html() + "</div>");
        }
    }
    else {
        ToastError("Please enter Required fields in PhoneNo First.");
    }
}

function ValidAddPhoneNo() {
    var res = true;
    $('.PhoneNoRow input[required="true"]').each(function () {
        if ($(this).val() == "") {
            res = false;
        }
    });
    $('.PhoneNoRow select[required="true"]').each(function () {
        if ($(this).val() == "") {
            res = false;
        }
    });
    return res;
}

function AddAddress() {
    if (ValidAddAddress()) {
        var rowCount = $('.AddressRow').length;
        var LastRowCount = rowCount - 1;
        if (LastRowCount <= max) {
            var newAddressRow = $('#AddressEliment_' + LastRowCount).clone();

            newAddressRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
                if ($(this).attr("id") != undefined)
                    $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
                if ($(this).attr("name") != undefined)
                    $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
                if ($(this).attr("data-valmsg-for") != undefined)
                    $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
                if ($(this).attr("for") != undefined)
                    $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));
                $(this).val('');
                $(this).attr('value', '');
            });
            newAddressRow.find(".IsFirstRow").css("display", "none");
            newAddressRow.find(".Removebtn").css("display", "block");
            newAddressRow.find(".NewId").val('');

            $('#DivAddress').append("<div class='AddressRow ' id='AddressEliment_" + rowCount + "' >" + newAddressRow.html() + "</div>");
        }
    }
    else {
        ToastError("Please enter Required fields in Address First.");
    }
}

function ValidAddAddress() {
    var res = true;
    $('.AddressRow input[required="true"]').each(function () {
        if ($(this).val() == "") {
            res = false;
        }
    });
    $('.AddressRow select[required="true"]').each(function () {
        if ($(this).val() == "") {
            res = false;
        }
    });
    return res;
}

function AddDesignation() {

    var rowCount = $('.DesignationRow').length;
    var LastRowCount = rowCount - 1;
    if (LastRowCount <= max) {
        var newDesignationRow = $('#DesignationEliment_' + LastRowCount).clone();

        newDesignationRow.find("checkbox, select, textarea, input, span, label, a").each(function () {
            if ($(this).attr("id") != undefined)
                $(this).attr("id", $(this).attr("id").replace(LastRowCount, rowCount));
            if ($(this).attr("name") != undefined)
                $(this).attr("name", $(this).attr("name").replace(LastRowCount, rowCount));
            if ($(this).attr("data-valmsg-for") != undefined)
                $(this).attr("data-valmsg-for", $(this).attr("data-valmsg-for").replace(LastRowCount, rowCount));
            if ($(this).attr("for") != undefined)
                $(this).attr("for", $(this).attr("for").replace(LastRowCount, rowCount));

        });

        newDesignationRow.find(".Removebtn").css("display", "block");
        newDesignationRow.find(".NewId").val('');

        $('#DivDesignation').append("<div class='DesignationRow ' id='DesignationEliment_" + rowCount + "' >" + newDesignationRow.html() + "</div>");
    }
}

function RemoveAddedItem(btn) {
    var id = $(btn).attr('id');
    $('#' + id.replace('Remove', '')).hide();
    $('#' + id.replace('Remove', '')).find("[id$=bActive]").val("false");
}