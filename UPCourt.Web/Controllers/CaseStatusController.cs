﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CaseStatusController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanView)]
        // GET: CaseStatus
        public ActionResult Index()
        {
            return View(CaseStatusMvcModel.CopyFromEntityList(CaseStatusDataService.GetLite(db).OrderByDescending(i => i.DisplayOrder), 0));
        }

        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanView)]
        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CaseStatusMvcModel.CopyFromEntity(dbData, 0));
        }

        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanCreate)]
        // GET: CaseStatus/Create
        public ActionResult Create()
        {
            var model = new CaseStatusMvcModel();
            return View(model);
        }
        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(CaseStatusMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            CaseStatus tempCaseStatus = new CaseStatus();
            tempCaseStatus.CaseStatusId = Guid.NewGuid();
            InitializeData(tempCaseStatus, model);

            tempCaseStatus.ModifiedBy = AppUser.UserId;
            tempCaseStatus.ModifiedOn = DateTime.Now;

            db.CaseStatuses.Add(tempCaseStatus);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }


        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanEdit)]
        // GET: CaseStatus/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(CaseStatusMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: CaseStatus/Edit/5
        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(CaseStatusMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == model.CaseStatusId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            InitializeData(dbData, model);
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        [PageRoleAuthorizarion(PageName.CaseStatus, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
        private void InitializeData(CaseStatus tempCaseStatus, CaseStatusMvcModel model)
        {
            tempCaseStatus.Name = model.Name;
            tempCaseStatus.HexColorCode = model.HexColorCode;
        }
        private bool CommonForInsertUpdate(CaseStatusMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = CaseStatusDataService.GetLite(db).FirstOrDefault(i => i.CaseStatusId != model.CaseStatusId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Case Status name already exists");
                return false;
            }
            var ClourCodeForCheck = model.HexColorCode.TrimX();
            var duplicateCheckColour = CaseStatusDataService.GetLite(db).FirstOrDefault(i => i.CaseStatusId != model.CaseStatusId && i.HexColorCode == ClourCodeForCheck);

            if (duplicateCheckColour != null)
            {
                ModelState.AddModelError(nameof(model.HexColorCode), "Case color code already exists");
                return false;
            }
            return true;
        }
    }
}
