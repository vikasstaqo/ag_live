﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public static class DatabaseContextHelper
    {
        public static void EnsureSeedDataForContext(this DatabaseContext db)
        {

            const string SystemName = "System";
            Guid SystemGuid = Guid.NewGuid();
            DateTime currentTime = Extended.CurrentIndianTime;

                 var allEnumPages = Enum.GetValues(typeof(PageName));

            foreach (Enum tempEnum in allEnumPages)
            {
                var pageEnum = (PageName)tempEnum;
                var dbPage = db.PagePermissions.FirstOrDefault(i => i.PageName == pageEnum);
                var allPermissions = tempEnum.GetType()
                .GetField(tempEnum.ToString())
                .GetCustomAttribute(typeof(PagePermissionAttribute)) as PagePermissionAttribute;

                if (dbPage == null)
                {
                    dbPage = new PagePermission();
                    dbPage.PagePermissionId = Guid.NewGuid();
                    dbPage.ModifyBy = SystemName;
                    dbPage.PageName = pageEnum;
                    dbPage.PermissionTypes = allPermissions.PermissionTypes;
                    db.PagePermissions.Add(dbPage);
                }
                if (!dbPage.PermissionTypes.SequenceEqual(allPermissions.PermissionTypes))
                {
                    dbPage.PermissionTypes = allPermissions.PermissionTypes;
                    dbPage.ModifyBy = SystemName;
                }
            }

            db.SaveChanges();

            //check if default admin role exists
                        var dbRoleDefault = db.UserRoles.FirstOrDefault(i => i.IsSytemCreated);

            if (dbRoleDefault == null)
            {
                dbRoleDefault = new UserRole();
                dbRoleDefault.UserRoleId = Guid.NewGuid();
                dbRoleDefault.Name = "Admin";
                dbRoleDefault.CreatedBy = SystemGuid;
                dbRoleDefault.CreatedOn = currentTime;
                dbRoleDefault.IsSytemCreated = true;
                dbRoleDefault.Status = UserRoleStatus.Active;
                db.UserRoles.Add(dbRoleDefault);
            }
            db.SaveChanges();

            //Confirming admin has all page permissions

            var dbAllPages = db.PagePermissions.ToList();

            foreach (var dbPage in dbAllPages)
            {
                var dbURPP = db.UserRolePagePermissions.FirstOrDefault(i => i.UserRoleId == dbRoleDefault.UserRoleId && i.PagePermissionId == dbPage.PagePermissionId);

                if (dbURPP == null)
                {
                    dbURPP = new UserRolePagePermission();

                    dbURPP.ModifiedBy = SystemGuid;
                    dbURPP.ModifiedOn = currentTime;
                    dbURPP.PermittedPermissionTypes = dbPage.PermissionTypes;
                    dbURPP.PagePermissionId = dbPage.PagePermissionId;
                    dbURPP.UserRoleId = dbRoleDefault.UserRoleId;
                    dbURPP.UserRolePagePermissionId = Guid.NewGuid();
                    db.UserRolePagePermissions.Add(dbURPP);
                }

                if (!dbURPP.PermittedPermissionTypes.SequenceEqual(dbPage.PermissionTypes))
                {
                    dbURPP.PermittedPermissionTypes = dbPage.PermissionTypes;
                    dbURPP.ModifiedBy = SystemGuid;
                    dbURPP.ModifiedOn = currentTime;
                }
            }

            db.SaveChanges();

            if (!db.Users.Any())
            {
                var passWordSalt = PasswordHelper.GetPasswordSalt();
                var passwordHash = PasswordHelper.EncodePassword("123", passWordSalt);

                User adminuser = new User
                {
                    UserId = Guid.NewGuid(),
                    Name = "Sales @ Gmail",
                    Email = "sales@gmail.com",
                    Phone = "0000000000",
                    LoginId = "sales@gmail.com",
                    Address = "India",
                    UserRoleId = dbRoleDefault.UserRoleId,
                    Status = UserStatus.Active,
                    CreatedBy = SystemGuid,
                    CreatedOn = currentTime,
                    PasswordSalt = passWordSalt,
                    PasswordHash = passwordHash
                };
                db.Users.Add(adminuser);
                db.SaveChanges();
            }
            CaseStatus disposeType = db.CaseStatuses.FirstOrDefault(i => i.Type == CaseStatusType.Dispose);
            if (disposeType == null)
            {
                //Default status for cases
                disposeType = new CaseStatus();
                disposeType.CaseStatusId = Guid.NewGuid();
                disposeType.Name = "Dispose";
                disposeType.Type = CaseStatusType.Dispose;
                disposeType.HexColorCode = "#FF0000";
                disposeType.ModifiedBy = SystemGuid;
                disposeType.ModifiedOn = currentTime;
                db.CaseStatuses.Add(disposeType);
                db.SaveChanges();
            }
        }
    }
}
