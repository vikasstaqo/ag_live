﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class AdvocateController : BaseMVCController
    {
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            var dbAdvocate = AdvocateDataService.GetDetail(db, AppUser).OrderByDescending(i => i.CreatedOn).ToList();
            return View(UserMvcModel.CopyFromEntityList(dbAdvocate, 0));
        }

        [HttpPost]
        [PageRoleAuthorizarion(PageName.User, PagePermissionType.CanCreate)]
        public ActionResult UsersFile(HttpPostedFileBase postedFile)
        {
            string filePath = string.Empty;
            if (postedFile != null)
            {
                if (Path.GetExtension(postedFile.FileName) == ".xlsx" || Path.GetExtension(postedFile.FileName) == ".xls")
                {

                    string path = Server.MapPath("~/Uploads/BulkUsers/");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    DataTable dt = ExcelImportHelper.ReadExcelFromFile(filePath);
                    try
                    {
                        int RowsCount = 0;
                        if (dt.Rows.Count > 0)
                        {
                            DataColumn dc2 = new DataColumn();
                            dc2.ColumnName = "UploadStatus";
                            dc2.DataType = typeof(string);

                            DataColumn dc3 = new DataColumn();
                            dc3.ColumnName = "Message";
                            dc3.DataType = typeof(string);
                            dt.Columns.AddRange(new DataColumn[] { dc2, dc3 });
                            int displayOrder = MasterBusinessService.GetUserDisplayOrder(db);
                            foreach (DataRow row in dt.Rows)
                            {
                                var passWordSalt = PasswordHelper.GetPasswordSalt();
                                var passwordHash = PasswordHelper.EncodePassword(row["Password"].ToString(), passWordSalt);
                                User tempData = new User
                                {
                                    UserId = Guid.NewGuid(),
                                    Name = row["Name"].ToString(),
                                    EnrollmentNo = row["EnrollmentNo"].ToString(),
                                    Email = row["Email"].ToString(),
                                    Address = row["Address"].ToString(),
                                    Phone = row["Phone"].ToString(),
                                    LoginId = row["Email"].ToString(),
                                    Status = row["Status"].ToString().ToLower() == "active" || row["Status"].ToString().ToLower() == "a" ? UserStatus.Active : UserStatus.Inactive,
                                    PasswordSalt = passWordSalt,
                                    PasswordHash = passwordHash,
                                    IsDefault = false,
                                    UserRoleId = db.UserRoles.Where(y => y.Name.Trim() == row["Role"].ToString().Trim()).FirstOrDefault().UserRoleId,
                                    CreatedBy = AppUser.UserId,
                                    CreatedOn = DateTime.Now,
                                    RegionId = null,
                                    DisplayOrder = displayOrder
                                };

                                var nameForCheck = tempData.Email.TrimX();
                                var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.Email == nameForCheck);
                                if (duplicateCheck == null)
                                {
                                    try
                                    {
                                        db.Users.Add(tempData);
                                        db.SaveChangesWithAudit(Request, AppUser.UserName);
                                        displayOrder++;
                                        RowsCount++;
                                        row["UploadStatus"] = "Done";
                                        row["Message"] = "Inserted Successfully !!";
                                    }
                                    catch (Exception ex)
                                    {
                                        row["UploadStatus"] = "Not Done";
                                        row["Message"] = ex.Message;
                                        continue;
                                    }
                                }
                                else
                                {
                                    row["UploadStatus"] = "Not Done";
                                    row["Message"] = "Email : " + nameForCheck + "  Already exists !!";
                                    continue;
                                }
                            }
                            DisplayMessage(MessageType.Success, CreatedMessage);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                dt.TableName = "UpdatedList";
                                var sheet1 = wb.Worksheets.Add(dt);
                                sheet1.Table(0).ShowAutoFilter = false;
                                sheet1.Table(0).Theme = XLTableTheme.None;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "UpdatedAdvocateList.xlsx");
                                }
                            }
                        }
                        else
                        {
                            DisplayMessage(MessageType.Warning, $"No Record in Excel");
                        }
                    }
                    catch
                    {
                        DisplayMessage(MessageType.Error, $"Invalid file. Please download templet first.");
                    }
                }
                else
                {
                    DisplayMessage(MessageType.Error, $"Invalid file");
                }
            }
            return RedirectToAction("/");
        }

        // GET: Advocate/Create
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanCreate)]
        public ActionResult Create()
        {
            var model = new UserMvcModel
            {
                Status = UserStatus.Active,
                RegionId = AppUser.TempRegionId
            };
            InitilizeUserRoles(null);
            // InitilizeCourt(null);
            return View(model);
        }
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(UserMvcModel model)
        {
            InitilizeUserRoles(model.UserRoleId);
            if (!ModelState.IsValid) { return View(model); }
            var count = UserDataService.GetDetail(db).Where(i => i.Phone == model.Phone).FirstOrDefault();
            if (count != null)
            {
                ModelState.AddModelError(nameof(model.Phone), "This Moblie Number Already Registered");
                return View(model);
            }
            count = UserDataService.GetDetail(db).Where(i => i.EnrollmentNo == model.EnrollmentNo).FirstOrDefault();
            if (count != null)
            {
                ModelState.AddModelError(nameof(model.EnrollmentNo), "This Enrollment No Already exists for another Advocate");
                return View(model);
            }

            if (!CommonForInsertUpdate(model)) { return View(model); }

            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            User advocate = new User
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Address = model.Address,
                Phone = model.Phone,
                LoginId = model.Email,
                Status = model.Status,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = model.IsDefault,
                UserType = model.UserType,
                UserRoleId = model.UserRoleId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetUserDisplayOrder(db),
                RegionId = model.RegionId,
                EnrollmentNo = model.EnrollmentNo
            };           
            db.Users.Add(advocate);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }
        public void InitilizeUserRoles(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).Where(x => x.Name.ToLower().Contains("advocate")).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId), nameof(UserRoleMvcModel.Name), selectedValue);
        }
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = AdvocateDataService.GetDetail(db, AppUser).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeUserRoles(dbData.UserRoleId);
            InitializeRegions(AppUser.User);
            // InitilizeCourt(null);
            return View(UserMvcModel.CopyFromEntity(dbData, 0));
        }

        private void InitializeRegions(object selected)
        {
            ViewBag.Regions = new SelectList(AppUser.Regions, nameof(Region.RegionId), nameof(Region.Name), selected);
        }

        [HttpPost]
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanEdit)]
        public ActionResult Edit(UserMvcModel model)
        {
            InitilizeUserRoles(model.UserRoleId);
            InitializeRegions(AppUser.TempRegionId);
            if (!ModelState.IsValid) { return View(model); }
            var dbData = AdvocateDataService.GetDetail(db, AppUser).Where(i => i.UserId == model.UserId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            var count = UserDataService.GetDetail(db).Where(i => i.EnrollmentNo == model.EnrollmentNo && i.UserId != model.UserId).FirstOrDefault();
            if (count != null)
            {
                ModelState.AddModelError(nameof(model.EnrollmentNo), "This Enrollment No Already exists for another Advocate");
                return View(model);
            }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            dbData.Name = model.Name;
            dbData.Phone = model.Phone;
            dbData.Status = model.Status;
            dbData.Address = model.Address;
            dbData.UserRoleId = model.UserRoleId;

            //If password changed
            if (!model.Password.IsNullOrEmpty())
            {
                var passwordSalt = PasswordHelper.GetPasswordSalt();
                var passwordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt);
                dbData.PasswordSalt = passwordSalt;
                dbData.PasswordHash = passwordHash;
            }
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            dbData.EnrollmentNo = model.EnrollmentNo;
            dbData.RegionId = model.RegionId;

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }
        public void InitilizeCourt(List<AdvocateCourt> selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CourtMvcModel.CopyFromEntityList(dbData, 0);
            if (!selectedValue.IsEmptyCollection())
            {
                var dbData2 = new List<Court>();
                foreach (var item in selectedValue.ToList())
                {
                    var data = dbData.Find(i => i.CourtId == item.CourtId);
                    dbData2.Add(data);
                }
                mvc = CourtMvcModel.CopyFromEntityList(dbData2, 0);
            }

            ViewBag.Courts = new MultiSelectList(mvc, nameof(CourtMvcModel.CourtId), nameof(CourtMvcModel.Name));
        }
        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = AdvocateDataService.GetDetail(db, AppUser).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeUserRoles(dbData.UserRoleId);
            return View(UserMvcModel.CopyFromEntity(dbData, 0));
        }
        private bool CommonForInsertUpdate(UserMvcModel model)
        {
            var nameForCheck = model.Email.TrimX();
            var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.UserId != model.UserId && i.Email == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), "Email already exists");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.Advocate, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = AdvocateDataService.GetDetail(db, AppUser).Where(i => i.UserId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
