﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NotificationsController : BaseMVCController
    {
        // GET: Notifications
        public ActionResult Index()
        {
            var data = GetItems();

            return View(AppNotificationsMvcModel.CopyFromEntityList(data, 0));  
        }

        public ActionResult Search(AppNotificationFilterModel filterModel)
        {
            InitializeNotificationType(null);
            InitializeTargetSource(null);
            InitializeUserRole(null);

            var model = new AppNotificationFilterModel();

            var data = GetItems();

            if (filterModel.CreatedOn != null)
            {
                data = data.Where(o => o.CreatedOn == filterModel.CreatedOn).ToList();
            }

            if (filterModel.Source > 0)
            {
                data = data.Where(o => o.Source == filterModel.Source).ToList();
            }

            if (filterModel.NotificationType > 0)
            {
                data = data.Where(o => o.NotificationType == filterModel.NotificationType).ToList();
            }

            model.NotificationData = AppNotificationsMvcModel.CopyFromEntityList(data, 0);

            return View(model);

        }

        protected List<AppNotifications> GetItems()
        {
            var data = AppNotificationsDataService.GetLite(db).OrderBy(i => i.CreatedOn).ToList();
            return data;
        }

        protected List<Notification> GetItems(Guid userid)
        {
            var data = NotificationsDataService.GetNotifications(db).ToList();
            return data;
        }

        [HttpGet]
        public JsonResult Items()
        {
            ResponseModel oResponse = new ResponseModel();
            try
            {
                var data = GetItems();

                if (data.Count > 0)
                {
                    return oResponse.ResponseJSON(ResponseModel.e_StatusCode.Success, "Success!", data, null);
                }
                else
                {
                    return oResponse.ResponseJSON(ResponseModel.e_StatusCode.NoDataFound, ResponseModel.e_StatusCode.NoDataFound.ToString(), data, null);
                }
            }
            catch (Exception ex)
            {
                return oResponse.ResponseJSON(ResponseModel.e_StatusCode.ExceptionOccurs, ex.Message, null, null);
            }
        }

        [HttpGet]
        public JsonResult UserNotifications(Guid userid)
        {
            ResponseModel oResponse = new ResponseModel();
            try
            {
               
                var data = AppNotifications.GetNotifications(userid).ToList();

                if (data.Count > 0)
                {
                    return oResponse.ResponseJSON(ResponseModel.e_StatusCode.Success, "Success!", data, null);
                }
                else
                {
                    return oResponse.ResponseJSON(ResponseModel.e_StatusCode.NoDataFound, ResponseModel.e_StatusCode.NoDataFound.ToString(), data, null);
                }
            }
            catch (Exception ex)
            {
                return oResponse.ResponseJSON(ResponseModel.e_StatusCode.ExceptionOccurs, ex.Message, null, null);
            }
        }

        /// <summary>
        /// GET: Notifications/Create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            InitializeTargetAudience(null);
            InitializeNotificationType(null);
            InitializeTargetSource(null);
            InitializeUserRole(null);
            var model = new AppNotificationsMvcModel();
            return View(model);
        }

        /// <summary>
        /// POST: Notifications/Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(AppNotificationsMvcModel model)
        {
            InitializeTargetAudience(model.TargetUserId);
            InitializeNotificationType(model.NotificationType);
            InitializeTargetSource(model.Source);
            InitializeUserRole(model.UserType);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            #region Loop to the list items: AssignedUsers, to save the Notifications
            foreach (var item in model.AssignedUsers)
            {
                AppNotifications tempAppNotify = new AppNotifications();
                tempAppNotify.Id = Guid.NewGuid();
                model.TargetUserId = item;
                InitializeData(tempAppNotify, model);
                var iResult = AppNotifications.Create(tempAppNotify);
            }
            #endregion

            SucessMessage = $"Notification created successfully";

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Initialize input data to save notifications: Save in table notifications
        /// </summary>
        /// <param name="tempNotify"></param>
        /// <param name="model"></param>
        private void InitializeData(Notification tempNotify, NotificationsModel model)
        {
            tempNotify.SourceEntityId = model.SourceEntityId;
            tempNotify.SourceEntityType = model.SourceEntityType;
            tempNotify.TargetEntityId = model.TargetEntityId;
            tempNotify.TargetEntityType = model.TargetEntityType;
            tempNotify.Status = model.Status;
            tempNotify.Message = model.Message;
            tempNotify.ModifyBy = AppUser.UserName;
            tempNotify.CreationDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// Ininitialize input data to save notifications: Save in AppNotifications table(New)
        /// </summary>
        /// <param name="tempNotify"></param>
        /// <param name="model"></param>
        private void InitializeData(AppNotifications tempNotify, AppNotificationsMvcModel model)
        {
            tempNotify.Source = model.Source;
            tempNotify.TargetUserId = model.TargetUserId;
            tempNotify.NotificationType = model.NotificationType;
            tempNotify.Status = 1;
            tempNotify.Message = model.Message;
            tempNotify.CreatedBy = AppUser.UserId;
            tempNotify.CreatedOn = Extended.CurrentIndianTime;
            tempNotify.RouteValue = model.RouteValue;
        }

        private void InitializeData(NotificationTargetUsers tempNotify, NotificationTargetUsersModel model)
        {
            tempNotify.EntityName = model.EntityName;
            tempNotify.IsActive = true;
        }

        /// <summary>
        /// Initializing data from Enum: NotificationSource and set the resultset into ViewState
        /// </summary>
        /// <param name="selectedValue"></param>
        public void InitializeSourceEntityType(object selectedValue)
        {
            var enumData = from Entity.NotificationSourceEntityType e in Enum.GetValues(typeof(Entity.NotificationSourceEntityType))
                           select new
                           {
                               Id = (int)e,
                               Name = e.ToString()
                           };
            ViewBag.SourceEntityType = new SelectList(enumData, nameof(NotificationsMvcModel.Id), nameof(NotificationsMvcModel.Name), selectedValue);
           
        }

        /// <summary>
        /// Initializing data from Enum: NotificationTargetEntity and set the resultset into ViewState
        /// </summary>
        /// <param name="selectedValue"></param>
        public void InitializeTargetEntityType(object selectedValue)
        {
            var enumData = from Entity.NotificationTargetEntityType e in Enum.GetValues(typeof(Entity.NotificationTargetEntityType))
                           select new
                           {
                               Id = (int)e,
                               Name = e.ToString()
                           };
                       ViewBag.TargetEntityType = new SelectList(enumData, nameof(NotificationsMvcModel.Id), nameof(NotificationsMvcModel.Name), selectedValue);
        }

        /// <summary>
        /// Returns list of items from database: NotificationTarget and set the resultset into ViewState
        /// </summary>
        /// <param name="selectedValue"></param>
        public void InitializeTargetAudience(object selectedValue)
        {
            var dbData = NotificationTargetUserService.GetLite(db).OrderBy(i => i.EntityName).ToList();
            var mvc = NotificationTargetUsersModel.CopyFromEntityList(dbData, 0);
            ViewBag.TargetUsers = new SelectList(mvc, nameof(NotificationTargetUsersModel.EntityId)
                , nameof(NotificationTargetUsers.EntityName), selectedValue);
        }

        /// <summary>
        /// Returns list of items from database: NotificationSource and set the resultset into ViewState
        /// </summary>
        /// <param name="selectedValue"></param>
        public void InitializeTargetSource(object selectedValue)
        {
            var dbData = NotificationSourceService.GetLite(db).OrderBy(i => i.SourceName).ToList();
            var mvc = NotificationSourceModel.CopyFromEntityList(dbData, 0);
            ViewBag.TargetSource = new SelectList(mvc, nameof(NotificationSourceModel.SourceId)
                , nameof(NotificationSource.SourceName), selectedValue);
        }

        /// <summary>
        /// Returns list of items from database: NotificationType and set the resultset into ViewState
        /// </summary>
        /// <param name="selectedValue"></param>
        public void InitializeNotificationType(object selectedValue)
        {
            var dbData = NotificationTypeService.GetLite(db).OrderBy(i => i.Type).ToList();
            var mvc = NotificationTypeModel.CopyFromEntityList(dbData, 0);
            ViewBag.NotificationType = new SelectList(mvc, nameof(NotificationTypeModel.NotificationTypeId)
                , nameof(NotificationsType.Type), selectedValue);
        }

        public void InitializeUserRole(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRole.Name), selectedValue);
        }

        public static string Test()
        {
            return "{\"Name\": Sushant, \"Address\":\"Gurgaon\"}";
        }

        public JsonResult GetUsers(Guid user_Type)
        {
            var data = UserDataService.GetDetail(db).ToList();
            data = data.Where(o => o.UserRoleId.ToString() == user_Type.ToString()).OrderBy(i => i.Name).ToList();

            var selectFields = data.Select(A => new User
            {
                UserId = A.UserId,
                UserRoleId = A.UserRoleId,
                Name = A.Name
            });

            return Json(selectFields, JsonRequestBehavior.AllowGet);
        }
    }

    public class UserNotificationsController : BaseMVCController
    {
        // GET: Notifications
        public ActionResult Index()
        {
            var data = GetItems();
            var model = AppNotificationsMvcModel.CopyFromEntityList(data, 0);
            return View(model);
        }

        protected List<AppNotifications> GetItems()
        {
            var data = AppNotificationsDataService.GetLite(db).Where(x => x.TargetUserId == AppUser.UserId).OrderBy(o => o.Status).ThenByDescending(o => o.CreatedOn).ToList();
            return data;
        }

        public ActionResult Show(Guid? id)
        {
            var model = AppNotificationHelper.CreateAppNotificationHelperObject(this).UpdateNotificationStatus(id, NotificationStatusEnum.IsRead);
            model.Where(m => m.Id == id).FirstOrDefault().ShowDefaultNotification = true;
            return View(model);
        }
    }
}