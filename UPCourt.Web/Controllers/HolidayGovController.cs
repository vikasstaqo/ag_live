﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class HolidayGovController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }
        public HolidayGovController() : this(new CustomUserManager())
        {
        }
        public HolidayGovController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }
        // GET: HolidayGov
        public ActionResult Index()
        {
            var dbData = HolidayListDataService.GetLite(db).ToList();
            return View(HolidayMvcModel.CopyFromEntityList(dbData, 0));
        }
        public ActionResult Create()
        {
            Initilizeholiday(null);
            InitilizHStatus(null);
            return View();
        }
        [HttpPost]
        public ActionResult Create(HolidayMvcModel model)
        {
            // Code for validating the CAPTCHA
            InitilizHStatus(model.HolidayName);
            Holiday tempData = new Holiday
            {
                HolidayId = Guid.NewGuid(),
                HolidayName = model.HolidayName,
                Hdate = model.Hdate,
                Hyear = model.Hyear,
                CreatedOn = DateTime.Now,
                CreatedBy = AppUser.UserId,
                HStatus = model.HStatus,
            };
            db.Holiday.Add(tempData);
            db.SaveChangesWithAudit(Request, "");
            SucessMessage = $"DC Govt. Department {tempData.HolidayName} Created successfully";
            ViewBag.Msg = SucessMessage;
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("Create");

        }

        public ActionResult List()
        {
            InitilizHStatus(null);
            HolidayIndexMvcModel model = new HolidayIndexMvcModel() { };

            var dbData = HolidayListDataService.GetDetail(db).Where(F => F.CreatedBy == AppUser.UserId).OrderByDescending(i => i.HStatus).ToList();
            model.HolidayDocs = HolidayMvcModel.CopyFromEntityList(dbData, 0);

            return View(model);
        }

        [HttpGet]
        public ActionResult GetList(string d)
        {
            var dbData = HolidayListDataService.GetLite(db).Where(i => i.HStatus == d).ToList();
            return Json(JsonConvert.SerializeObject(dbData), JsonRequestBehavior.AllowGet);
        }
        private bool CommonForInsertUpdate(HolidayMvcModel model)
        {
            return true;
        }

        private void InitilizeDistricts(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
            var mvc = DistrictMvcModel.CopyFromEntityList(dbData, 0);
            mvc.Add(new DistrictMvcModel() { DistrictName = "Others" });
            var districts = new SelectList(mvc, nameof(DistrictMvcModel.DistrictName)
                , nameof(DistrictMvcModel.DistrictName), selectedValue);
            ViewBag.Districts = districts;
        }

        private void Initilizeholiday(object selectedValue)
        {
            var dbData = HolidayListDataService.GetLite(db).OrderBy(i => i.HolidayName).ToList();
            var mvc = HolidayMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.HolidayList = new SelectList(mvc, nameof(HolidayMvcModel.HolidayName)
                , nameof(HolidayMvcModel.HolidayName), selectedValue);
        }
        private void InitilizHStatus(object selectedValue)
        {
            var dbData = HStatusDataService.GetLite(db).OrderBy(i => i.HStatusName).ToList();
            var mvc = HStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.HStatus = new SelectList(mvc, nameof(HStatusMvcModel.HStatusName)
                , nameof(HStatusMvcModel.HStatusName), selectedValue);
        }
    }
}