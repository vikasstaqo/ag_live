﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Core;

namespace UPCourt.Web.Controllers
{
    public class FreshCaseDashbordController : BaseMVCController
    {
        // GET: FreshCaseDashbord
        public ActionResult Index(CaseFilterMvcModel caseFilterMvcModel, string CurrentTab)
        {
            InitilizeCouncil(null);
            InitilizeCourt(null);
            InitilizeCourtBench(null);
            InitilizeContextOrder(caseFilterMvcModel.IsContextOrder);
            if (CurrentTab == null)
                CurrentTab = "AcctiveCases";

            ViewBag.CurrentTab = CurrentTab;
            ViewBag.UserRoleName = AppUser.User.UserRole.Name;
            List<Case> dbData = new List<Case>();
            List<Case> ActiveCases = new List<Case>();
            List<Case> DisposedCases = new List<Case>();
            List<Case> CaseTransfer = new List<Case>();
            dbData = CaseDataService.GetDetail(db).Where(x => x.RegionId == AppUser.TempRegionId).OrderByDescending(i => i.Notice.NoticeDate).ToList();

            if (AppUser.User.UserRole.Name.ToLower() == "csc" || AppUser.User.UserRole.Name.ToLower() == "advocate civil")
                dbData = dbData.Where(x => x.Notice.NoticeType == ParentNoticeType.Civil).ToList();

            if (AppUser.User.UserRole.Name.ToLower() == "cga" || AppUser.User.UserRole.Name.ToLower() == "advocate criminal")
                dbData = dbData.Where(x => x.Notice.NoticeType == ParentNoticeType.Criminal).ToList();

            ActiveCases = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Normal).ToList();
            CaseTransfer = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Normal).ToList();
            DisposedCases = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Dispose).ToList();

            if (CurrentTab == "AcctiveCases")
            {
                if (caseFilterMvcModel.CaseStatusActive != null)
                    InitilizeCaseStatus(caseFilterMvcModel.CaseStatusActive);
                else
                    InitilizeCaseStatus(null);

                if (caseFilterMvcModel.ParentNoticeTypeActive != null)
                    InitilizeCaseType(caseFilterMvcModel.ParentNoticeTypeActive, AppUser.User.UserRole.Name);
                else
                    InitilizeCaseType(null, AppUser.User.UserRole.Name);

                InitilizeContextOrder(caseFilterMvcModel.IsContextOrderActive);

                if (AppUser.User.UserRole.Name.ToLower() == "admin")
                {
                    if (caseFilterMvcModel.IsContextOrderActive == 2)
                        ActiveCases = ActiveCases.Where(x => x.Notice.IsContextOrder == false).ToList();
                    if (caseFilterMvcModel.IsContextOrderActive == 1)
                        ActiveCases = ActiveCases.Where(x => x.Notice.IsContextOrder == true).ToList();
                }

                if (caseFilterMvcModel.CaseStatusActive != null && caseFilterMvcModel.CaseStatusActive.ToString() != "")
                    ActiveCases = ActiveCases.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusActive).ToList();

                if (caseFilterMvcModel.ParentNoticeTypeActive != null && caseFilterMvcModel.ParentNoticeTypeActive != "99" && caseFilterMvcModel.ParentNoticeTypeActive != "")
                    ActiveCases = ActiveCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.ParentNoticeTypeActive).ToList();

                if (caseFilterMvcModel.CaseDateActive != null)
                    ActiveCases = ActiveCases.Where(x => x.Notice.NoticeDate == caseFilterMvcModel.CaseDateActive).ToList();
            }
            else
            {
                InitilizeCaseStatus(null);
                InitilizeCaseType(null, AppUser.User.UserRole.Name);
                InitilizeContextOrder(null);
            }
            caseFilterMvcModel.AcctiveCases = CaseMvcModel.CopyFromEntityList(ActiveCases, 0);
            caseFilterMvcModel.DisposedCases = CaseMvcModel.CopyFromEntityList(DisposedCases, 0);
            caseFilterMvcModel.CaseTransfer = CaseMvcModel.CopyFromEntityList(CaseTransfer, 0);
            return View(caseFilterMvcModel);
        }
        [HttpPost]
        public ActionResult AssignDuty(Guid CaseGuid, Guid Advocate)
        {
            Case dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == CaseGuid).FirstOrDefault();
            var OldAdvocatename = UserDataService.GetLite(db).Where(x => x.UserId == dbcase.AdvocateId).FirstOrDefault()?.Name;

            dbcase.AdvocateId = Advocate;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string newAdvocatename = UserDataService.GetLite(db).Where(x => x.UserId == Advocate).FirstOrDefault().Name;

            if (OldAdvocatename != null && OldAdvocatename != "")
            {
                string description = $" Advocate is changed from '{ OldAdvocatename }' to '{ newAdvocatename }' successfully";
                TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.AdvocateChange, AppUser.UserId, description, null);
            }
            else
            {
                string description = $"New Advocate '{ newAdvocatename }' is assigned successfully";
                TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.AdvocateAssignment, AppUser.UserId, description, null);
            }
            SucessMessage = $" Advocate assigned to case :- {dbcase.PetitionNo} successfully";
            return RedirectToAction("Index", new { CurrentTab = "AcctiveCases" });
        }
        [HttpPost]
        public ActionResult ReOpenCase(Guid ReCaseGuid)
        {
            Case dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == ReCaseGuid).FirstOrDefault();
            var CaseOldStatus = CaseStatusDataService.GetLite(db).Where(x => x.CaseStatusId == dbcase.CaseStatusId).FirstOrDefault().Name;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Re-Opened").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string description = $" Case status changed from '{ CaseOldStatus }' to 'Re-opened' successfully";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Reopen, AppUser.UserId, description, null);

            SucessMessage = $"Case :- {dbcase.PetitionNo} Re-Opened successfully";

            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        public ActionResult GetCourtsByBench(Guid BenchId)
        {
            var dbData = CourtDataService.GetLite(db).Where(c => c.BenchTypeID == BenchId).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            SelectList courts = new SelectList(AllCourt, "CourtId", "Name", null);
            return Json(new { data = courts.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReAppealCase(UpdateDisposedCaseMvcModal model)
        {
            Case dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == model.CaseGuid).FirstOrDefault();
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Re-Appeal").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            addNewCase(dbcase, model);
            SucessMessage = $"Case :- {dbcase.PetitionNo} Re-Appeal done successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        public ActionResult MoveToSupremeCourt(UpdateDisposedCaseMvcModal model)
        {
            Case dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == model.CaseGuid).FirstOrDefault();
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Moved to Supreme Court").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            addNewCase(dbcase, model);
            SucessMessage = $"Case :- {dbcase.PetitionNo} Moved to Supreme Court successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        private void addNewCase(Case dbcase, UpdateDisposedCaseMvcModal model)
        {
            Case newCase = getNewCase(dbcase, model);
            db.Cases.Add(newCase);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
        }

        private Case getNewCase(Case model, UpdateDisposedCaseMvcModal uModel)
        {
            var TotalCases = CaseDataService.GetLite(db).ToList().Count;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Fresh Case").FirstOrDefault();
            Case NewCase = new Case()
            {
                CaseId = Guid.NewGuid(),
                NoticeId = model.NoticeId,
                OrdinalNo = TotalCases + 1,
                Type = model.Type,
                CreatedBy = AppUser.UserId,
                CaseStatusId = dbstatus.CaseStatusId,
                AdvocateId = model.AdvocateId,
                Status2 = model.Status2,
                PetitionNo = uModel.CaseNo,
                ParentCaseId = uModel.CaseGuid,
                CourtId = uModel.CourtId
            };

            return NewCase;
        }

        [HttpPost]
        public ActionResult DisposeCase(Guid DisposeCaseGuid)
        {

            Case dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == DisposeCaseGuid).FirstOrDefault();
            var CaseOldStatus = CaseStatusDataService.GetLite(db).Where(x => x.CaseStatusId == dbcase.CaseStatusId).FirstOrDefault().Name;
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Disposed").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            string description = $" Case status changed from '{ CaseOldStatus }' to 'Disposed' successfully";
            TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.Disposed, AppUser.UserId, description, null);

            SucessMessage = $"Case :- {dbcase.PetitionNo} disposed successfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });

        }

        [HttpPost]
        public ActionResult MoveToOtherCourt(string CaseTransferIds, Guid NewCourt, string Description)
        {
            string[] caseids = CaseTransferIds.Split(',');
            foreach (string cas in caseids)
            {
                if (cas != "" && cas != null)
                {
                    var dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == Guid.Parse(cas)).FirstOrDefault();
                    var CaseOldCourt = CourtDataService.GetLite(db).Where(x => x.CourtId == dbcase.CourtId).FirstOrDefault()?.Name;
                    CaseTransfer caseTransfer = new CaseTransfer()
                    {
                        CaseTransferId = Guid.NewGuid(),
                        CaseId = Guid.Parse(cas),
                        CourtId = NewCourt,
                        OldCourtId = dbcase.CourtId,
                        TransferDate = DateTime.Now,
                        Description = Description,
                        CreatedBy = AppUser.UserId
                    };

                    dbcase.CaseTransfers.Add(caseTransfer);
                    dbcase.CourtId = NewCourt;
                    db.SaveChangesWithAudit(Request, AppUser.UserName);

                    var CaseNewCourt = CourtDataService.GetLite(db).Where(x => x.CourtId == NewCourt).FirstOrDefault().Name;
                    if (CaseOldCourt != null && CaseOldCourt != "")
                    {
                        string description = $" Court is changed from '{ CaseOldCourt }' to '{ CaseNewCourt }' successfully";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.CourtChange, AppUser.UserId, description, null);
                    }
                    else
                    {
                        string description = $"New Court '{ CaseNewCourt }' is assigned successfully";
                        TimelineBusinessService.SaveTimeLine(db, null, dbcase.CaseId, EventType.CourtAssignment, AppUser.UserId, description, null);
                    }
                }
            }

            SucessMessage = $"Case transfered successfully";
            return RedirectToAction("Index", new { CurrentTab = "CaseTransfer" });
        }
        public void InitilizeCouncil(object selectedValue)
        {
            var dbData = UserDataService.GetLite(db).Where(x => x.UserRole.Name.Contains("advocate"))?.ToList();
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Council = new SelectList(mvc, nameof(UserMvcModel.UserId), nameof(UserMvcModel.Name), selectedValue);
        }

        public void InitilizeRegion(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var allRegions = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(allRegions, "RegionId", "Name", selectedValue);
        }

        public void InitilizeCourt(object selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AllCourt = new SelectList(AllCourt, "CourtId", "Name", selectedValue);


            dbData = dbData.Where(x => x.IsClose == false && x.Status == CourtStatus.Active).ToList();
            var OpenedCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.OpenedCourt = new SelectList(OpenedCourt, "CourtId", "Name", selectedValue);
        }
        public void InitilizeCourtBench(object selectedValue)
        {
            var dbData = BenchTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var benchs = BenchTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Benchs = new SelectList(benchs, "BenchTypeID", "Name", selectedValue);
        }
        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Normal).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }

        public void InitilizeCaseType(object selectedValue, string UserRoleName)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (!(UserRoleName.ToLower() == "csc" || UserRoleName.ToLower() == "advocate civil"))
                items.Add(new SelectListItem { Text = "Criminal", Value = "Criminal" });

            if (!(UserRoleName.ToLower() == "cga" || UserRoleName.ToLower() == "advocate criminal"))
                items.Add(new SelectListItem { Text = "Civil", Value = "Civil" });

            if (UserRoleName.ToLower() == "admin" || UserRoleName.ToLower() == "ag" || UserRoleName.ToUpper() == "aag")
                items.Add(new SelectListItem { Text = "All", Value = "All" });

            ViewBag.CaseType = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitilizeContextOrder(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Yes", Value = "1" });
            items.Add(new SelectListItem { Text = "No", Value = "2" });
            ViewBag.ContextOrder = new SelectList(items, "Value", "Text", selectedValue);
        }
        public ActionResult CaseView(Guid id)
        {

            InitilizeDocument(null);
            InitilizeDepartment(null, id);

            var dbData = CaseDataService.GetDetail(db).Where(x => x.CaseId == id).FirstOrDefault();
            CaseMvcModel CaseMobelobj = CaseMvcModel.CopyFromEntity(dbData, 1);
            if (CaseMobelobj.Notice.CrimeDetail != null)
                if (CaseMobelobj.Notice.CrimeDetail.PoliceStation == null && CaseMobelobj.Notice.CrimeDetail.PoliceStationID != null)
                {
                    CaseMobelobj.Notice.CrimeDetail.PoliceStation = PoliceStationMvcModel.CopyFromEntity(PoliceStationDataService.GetLite(db).Where(x => x.PoliceStationId == CaseMobelobj.Notice.CrimeDetail.PoliceStationID).FirstOrDefault(), 0);
                }
            CaseMobelobj.Timelines = TimelineMvcModel.CopyFromEntityList(TimelineBusinessService.GetCaseTimeline(db, id), 0);
            DocumentMvcModel obj = new DocumentMvcModel();
            CaseMobelobj.NewCaseHearing = new NewCaseHearingMvcModelList();

            CaseMobelobj.NewCaseHearing.DocumentMvcModel = new List<DocumentMvcModel>();
            CaseMobelobj.NewCaseHearing.DocumentMvcModel.Add(new DocumentMvcModel { Name = "" });

            InitilizeCaseStatus(CaseMobelobj.CaseStatusId);
            InitilizeCouncil(CaseMobelobj.AdvocateId);
            InitilizeCourt(CaseMobelobj.CourtId);
            InitilizeRegion(CaseMobelobj.RegionId);

            return View(CaseMobelobj);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveBasicIndex(CaseMvcModel DocumentMVCModel)
        {


            var dbData = CaseDataService.GetDetail(db).Where(x => x.CaseId == DocumentMVCModel.CaseId).FirstOrDefault();

            dbData.AdvocateId = DocumentMVCModel.AdvocateId;
            dbData.CaseStatusId = DocumentMVCModel.CaseStatus.CaseStatusId;
            dbData.CourtId = DocumentMVCModel.Court.CourtId;
            dbData.Status2 = DocumentMVCModel.Status2;
            dbData.RegionId = DocumentMVCModel.Region.RegionId;

            db.Cases.Update(dbData);


            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Basic Index Saved successfully";

            return RedirectToAction("CaseView", new { id = DocumentMVCModel.CaseId });
        }

        private bool IsValidPermission(object rModel, PageName @case, PagePermissionType canView)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CaseView(CaseMvcModel DocumentMVCModel)
        {

            CaseHearing ChObj = new CaseHearing();
            ChObj.CaseHearingId = Guid.NewGuid();
            ChObj.CaseId = DocumentMVCModel.CaseId;
            ChObj.HearingDate = DocumentMVCModel.NewCaseHearing.Hearing.HearingDate;
            ChObj.NextHearingDate = DocumentMVCModel.NewCaseHearing.Hearing.NextHearingDate;
            ChObj.Comments = DocumentMVCModel.NewCaseHearing.Hearing.Comments;
            int OrderNo = 1;
            foreach (var Doc in DocumentMVCModel.NewCaseHearing.DocumentMvcModel)
            {
                Document DocNewObj = new Document();
                DocNewObj.DocumentId = Guid.NewGuid();
                DocNewObj.CaseHearingId = ChObj.CaseHearingId;
                DocNewObj.Name = Doc.Name;
                DocNewObj.DocumentUrl = SaveDocument(Doc.UploadFile);
                DocNewObj.FileType = DocumentFileType.Any;
                DocNewObj.EntityType = DocumentEntityType.CaseHearing;
                DocNewObj.Ordinal = OrderNo;
                DocNewObj.Size = Doc.UploadFile.ContentLength;
                DocNewObj.Status = DocumentStatus.Added;
                OrderNo++;
                db.Documents.Add(DocNewObj);
            }
            db.CaseHearings.Add(ChObj);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Case Hearing Added successfully";
            return RedirectToAction("CaseView", DocumentMVCModel.CaseId);
        }
        string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                if (ext.ToUpper() == ".DOC" || ext.ToUpper() == ".DOCX" || ext.ToUpper() == ".PDF" || ext.ToUpper() == ".XLS" || ext.ToUpper() == ".JPEG" || ext.ToUpper() == ".JPG")
                {
                    int FileSize = FileUpload.ContentLength;
                    if (FileSize > 5048576)
                    {
                        return ".jpeg, .jpg, .doc, .docx, .xls and .pdf  with Maximum file size 5 mb will be uploaded";
                    }
                    else
                    {
                        string fileName = "", UploadName = "", FilePath = "";

                        fileName = Path.GetFileName(FileUpload.FileName);
                        Guid guid;
                        guid = Guid.NewGuid();

                        UploadName = guid + "_" + fileName;
                        FilePath = Path.Combine(Server.MapPath("~/Document/"), UploadName);
                        FileUpload.SaveAs(FilePath);
                        return UploadName;
                    }
                }
            }

            return "";
        }

        public ActionResult CaseHearingDOC(Guid CaseHearID)
        {
            var dbData = CaseDataService.GetDetail(db).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault();

            return PartialView("CaseHearingDOC", CaseMvcModel.CopyFromEntity(dbData, 0));
        }
        public ActionResult CaseHearingComment(Guid CaseHearID)
        {
            var obj = CaseDataService.GetDetail(db).Where(x => x.CaseHearings.Any(m => m.CaseHearingId == CaseHearID)).FirstOrDefault();
            var Commenst = obj.CaseHearings.Where(m => m.CaseHearingId == CaseHearID).Select(x => x.Comments).FirstOrDefault();
            return PartialView("CaseHearingComment", Commenst);
        }

        [HttpPost]
        public ActionResult GetDocument(DocumentMaster model)
        {
            var dbData = CaseDataService.GetDetail(db).ToList();
            return Json(new { ddllist = dbData });
        }

        [HttpPost]
        public ActionResult GetRequestData(Guid id)
        {
            var dbdata = RequestDocDataService.GetDetail(db).Where(x => x.CaseId == id).OrderBy(i => i.DocumentId).ToList();
            var ep = (from gd in dbdata
                      join pr in db.GovernmentDepartments on gd.SharedBy equals pr.GovernmentDepartmentId
                      join c in db.DocumentMaster on gd.DocumentId equals c.DocumentId
                      select new
                      {
                          pr.Name,
                          c.DocumentName,
                          gd.CommentRequestor,
                          gd.GovDeptComment,
                          gd.Document_Name,
                          gd.DocumentStatus,
                          gd.DocumentPath,
                          gd.CaseId
                      });
            return Json(ep);

        }

        [HttpPost]

        public void InitilizeDocument(object selectedValue)
        {
            var dbData = CaseDataService.GetDocument(db).ToList();
            var mvc = DocumentMVCModel.CopyFromEntityList(dbData, 0);
            ViewBag.Document = new SelectList(mvc, nameof(DocumentMVCModel.DocumentId), nameof(DocumentMVCModel.DocumentName), selectedValue);
        }
        public void InitilizeDepartment(object selectedValue, Guid? caseid)
        {
            var dbData = CaseDataService.GetDepartment(db, caseid).ToList();
            ViewBag.Departmemt = new SelectList(dbData, "GovernmentDepartmentId", "Name", selectedValue);
        }
    }
}
