﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class NotificationsDataService
    {
        public static IQueryable<Notification> GetLite(DatabaseContext db)
        {
            return db.Notifications;
        }
        public static IQueryable<Notification> GetDetail(DatabaseContext db)
        {
            var retval = GetLite(db).Select(i => new Notification
            {
                Message = i.Message,
                ModifyBy = i.ModifyBy,
                SourceEntityId = i.SourceEntityId,
                SourceEntityType = i.SourceEntityType,
                TargetEntityId = i.TargetEntityId,
                TargetEntityType = i.TargetEntityType,
                Status = i.Status
            });

            return retval;
        }

        public static IEnumerable<Notification> GetNotifications(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new Notification
                {
                    Message = i.Message,
                    ModifyBy = i.ModifyBy,
                    SourceEntityId = i.SourceEntityId,
                    SourceEntityType = i.SourceEntityType,
                    TargetEntityId = i.TargetEntityId,
                    TargetEntityType = i.TargetEntityType,
                    Status = i.Status,
                    CreationDate = i.CreationDate,
                });

            return retval;
        }
    }

    public class NotificationSourceService
    {
        public static IQueryable<NotificationSource> GetLite(DatabaseContext db)
        {
            return db.NotificationSources;
        }

        public static IEnumerable<NotificationSource> GetNotifications(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new NotificationSource
                {
                    SourceId = i.SourceId,
                    SourceName = i.SourceName,
                    IsActive = i.IsActive
                });

            return retval;
        }
    }

    public class NotificationTargetUserService
    {
        public static IQueryable<NotificationTargetUsers> GetLite(DatabaseContext db)
        {
            return db.NotificationTargetUsers;
        }

        public static IEnumerable<NotificationTargetUsers> GetNotifications(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new NotificationTargetUsers
                {
                    EntityId = i.EntityId,
                    EntityName = i.EntityName,
                    IsActive = i.IsActive
                });

            return retval;
        }
    }

    public class NotificationTypeService
    {
        public static IQueryable<NotificationsType> GetLite(DatabaseContext db)
        {
            return db.NotificationsType;
        }

        public static IEnumerable<NotificationsType> GetNotifications(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new NotificationsType
                {
                    NotificationTypeId = i.NotificationTypeId,
                    Type = i.Type,
                    IsActive = i.IsActive
                });

            return retval;
        }
    }
}