﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
   
    public class JudgeResponseModel : BaseEntityModel<Judge, JudgeResponseModel>
    {
        public Guid JudgeId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public JudgeStatus Status { get; set; }
      

    }

   

}