﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DepartmentShortName), UserNameProp: "")]
    public class GovernmentDepartmentDC
    {
        public GovernmentDepartmentDC()
        {
            GovernmentDepartmentDCContacts = new HashSet<GovernmentDepartmentDCContact>();
        }
        public Guid GovernmentDepartmentDCId { get; set; }
        public GovernmentDeparmentType DepartmentType { get; set; }
        public string DepartmentShortName { get; set; }
        public string DepartmentFullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Division { get; set; }

        public string Tehsil { get; set; }

        public string GovBlk { get; set; }
       // public string Block { get; set; }
        public ICollection<GovernmentDepartmentDCContact> GovernmentDepartmentDCContacts { get; set; }
        
    }


    public enum GovernmentDeparmentType
    {
        [Description("Police Department")]
        PoliceDepartment = 1,
        [Description("Other Department")]
        OtherDepartment = 2
    }

    public class GovernmentDepartmentDCContact
    {
        public Guid GovernmentDepartmentDCContactId { get; set; }
        public Guid GovernmentDepartmentDCId { get; set; }
        public int Sequence { get; set; }
        public string ContactName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
