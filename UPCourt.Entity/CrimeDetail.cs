﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CrimeNo), UserNameProp: nameof(CreatedBy))]
    public class CrimeDetail :BaseTransEntity
    {
        public CrimeDetail() 
        {
            
        }
        public Guid CrimeDetailID { get; set; }
        public Guid NoticeId { get; set; }
        public string CrimeNo { get; set; }
        public int CrimeYear { get; set; }
        public string FIRNO { get; set; }
        public DateTime FIRDate { get; set; }
        public Guid IPCSectionId { get; set; }
        public string District { get; set; }
        public Guid PoliceStationID { get; set; }
        public PoliceStation PoliceStation { get; set; }
        public string CaseNo { get; set; }
        public string CaseNo_Court { get; set; }
        public string TrailNo { get; set; }
        public string TrailNo_Court { get; set; }
        public bool IsActive { get; set; }

    }
}
