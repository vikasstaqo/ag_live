﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class IPCSectionDataService
    {
        public static IQueryable<IPCSection> GetAll(DatabaseContext db)
        {
            return db.IPCSections;
        }
        public static IQueryable<IPCSection> GetLite(DatabaseContext db)
        {
            return db.IPCSections.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<IPCSection> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }

   
}