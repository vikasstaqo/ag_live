﻿using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class ApplicationDocumentController : BaseMVCController
    {
        // GET: ApplicationDocument
        int numberOfPages = 0;
        public ActionResult Index(Guid? id)

        {
            if (TempData["CurrentTab"] == null)
                TempData["CurrentTab"] = "app";


            if (id == null)
            {
                InitializeData(null);
                var model = new ApplicationDocumentMvcModel
                {
                    ECourtFees = new List<ECourtFeeMvcModel> { new ECourtFeeMvcModel() },
                    NoticeBasicIndexs = new List<NoticeBasicIndexMvcModel> { new NoticeBasicIndexMvcModel() },
                    InsertedOn = DateTime.Now,

                };
                return View(model);
            }
            else
            {
                TempData["NewCase"] = "0";
                var dbData = ApplicationDocumentDataService.GetDetail(db).Where(i => i.ApplicationDocumentID == id).FirstOrDefault();
                var model = ApplicationDocumentMvcModel.CopyFromEntity(dbData, 0);
                model.ECourtFees.Insert(0, new ECourtFeeMvcModel());
                model.NoticeBasicIndexs.Insert(0, new NoticeBasicIndexMvcModel());
                InitializeData(model);

                List<ApplicationDocument> list = new List<ApplicationDocument>();
                list.Add(dbData);

                TempData["ListOfApp"] = model;
                TempData.Keep("ListOfApp");
                ViewBag.ListOfApp = list;

                return View(model);
            }
        }

        private void InitializeData(ApplicationDocumentMvcModel model)
        {
            InitilizeApplicationTypes(null);
        }

        private void InitilizeApplicationTypes(object selectedValue)
        {
            var dbData = ApplicationTypeDataService.GetLite(db).ToList();
            var mvc = ApplicationTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.ApplicationTypes = new SelectList(mvc, nameof(ApplicationType.ApplicationTypeID), nameof(ApplicationType.ATName), selectedValue);
        }

        #region Save Application Document
        public ActionResult SaveApplicationDocument(ApplicationDocumentMvcModel model)
        {
            var newapplicationdocumentID = Guid.NewGuid();
            ApplicationDocument Insert_ApplicationDocumentModel = new ApplicationDocument
            {
                ApplicationDocumentID = newapplicationdocumentID,
                ApplicationTypeID = model.ApplicationTypeID,
                ApplicantName = model.ApplicantName,
                InsertedOn = DateTime.Now,
                InsertedBy = AppUser.UserName,
                IsActive = true,
                FinalStatus = ApplicationStatus.Draft
            };


            db.ApplicationDocuments.Add(Insert_ApplicationDocumentModel);

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            TempData["CurrentTab"] = "e-court";
            string description = $"ApplicationDocument Saved successfully";
            TimelineBusinessService.SaveTimeLine(db, newapplicationdocumentID, null, EventType.ApplicationDocumentCreated, AppUser.UserId, description, null);
            DisplayMessage(MessageType.Success, $"ApplicationDocument Saved successfully");



            return RedirectToAction("Index", "ApplicationDocument", new { id = Insert_ApplicationDocumentModel.ApplicationDocumentID });
        }

        public ActionResult DeleteApplicationDocument(Guid ApplicationDocumentID)
        {
            var dbdata = ApplicationDocumentDataService.GetDetail(db).Where(x => x.ApplicationDocumentID == ApplicationDocumentID).FirstOrDefault();
            db.ApplicationDocuments.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "app";
            DisplayMessage(MessageType.Error, $"ApplicationDocument ", "Deleted", "successfully");
            return RedirectToAction("Index", "ApplicationDocument");
        }

        #endregion

        #region ECourtFee
        public ActionResult SaveECourtFee(ApplicationDocumentMvcModel model)
        {
            var i = 0;
            if (model.ApplicationDocumentID != null)
            {
                ECourtFee InsertECourtFee = new ECourtFee
                {
                    eCourtFeeID = Guid.NewGuid(),
                    ApplicationDocumentID = model.ApplicationDocumentID,
                    ReceiptNo = model.ECourtFees[i].ReceiptNo,
                    Amount = model.ECourtFees[i].Amount,
                    EDate = model.ECourtFees[i].EDate,
                    InsertedBy = AppUser.UserName,
                    InsertedDate = DateTime.Now,
                    Status = true
                };

                db.ECourtFees.Add(InsertECourtFee);
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "upload";
                DisplayMessage(MessageType.Success, $"Application ECourtFee Saved successfully");
                return RedirectToAction("Index", "ApplicationDocument", new { id = model.ApplicationDocumentID });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }

        public ActionResult DeleteECourtFee(Guid id, Guid ApplicationDocumentID)
        {
            var dbdata = ECourtFeeDataService.GetDetail(db).Where(x => x.eCourtFeeID == id).FirstOrDefault();
            db.ECourtFees.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "e-court";
            DisplayMessage(MessageType.Error, $"Application Document E Court Fee Deleted Successfully ", "Deleted", "successfully");
            //return RedirectToAction("Index", "ApplicationDocument", new { id = ApplicationDocumentID });
            return RedirectToAction("List");
        }
        #endregion

        #region Upload Document
        public ActionResult SaveBasicIndex(ApplicationDocumentMvcModel model)
        {
            var i = 0;
            if (model.ApplicationDocumentID != null)
            {
                NoticeBasicIndex Insert_NoticeBasicIndex = new NoticeBasicIndex
                {
                    NoticeBasicIndexId = Guid.NewGuid(),
                    ApplicationDocumentID = model.ApplicationDocumentID,
                    Particulars = model.NoticeBasicIndexs[i].Particulars,
                    Annexure = model.NoticeBasicIndexs[i].Annexure,
                    IndexDate = model.NoticeBasicIndexs[i].IndexDate
                };
                Insert_NoticeBasicIndex.IndexDate = model.NoticeBasicIndexs[i].IndexDate;
                // Insert_NoticeBasicIndex.NoOfPages = 10;
                if (model.NoticeBasicIndexs[i].BasicIndexFile.ContentLength > 0)
                {
                    Document Insert_Doc = new Document
                    {
                        DocumentId = Guid.NewGuid(),
                        NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId,
                        Name = Insert_NoticeBasicIndex.Annexure,
                        DocumentUrl = SaveDocument(model.NoticeBasicIndexs[i].BasicIndexFile),
                        FileType = DocumentFileType.Any,
                        EntityType = DocumentEntityType.CaseHearing,
                        Size = model.NoticeBasicIndexs[i].BasicIndexFile.ContentLength,
                        Status = DocumentStatus.Added,
                        CreatedBy = AppUser.UserId
                    };
                    Insert_NoticeBasicIndex.Documents.Add(Insert_Doc);
                    Insert_NoticeBasicIndex.NoOfPages = numberOfPages;
                }

                db.NoticeBasicIndexs.Add(Insert_NoticeBasicIndex);
                db.SaveChangesWithAudit(Request, AppUser.UserName);
                TempData["CurrentTab"] = "upload";
                DisplayMessage(MessageType.Success, $"ApplicationDocument Document Saved successfully");
                return RedirectToAction("Index", "ApplicationDocument", new { id = model.ApplicationDocumentID });
            }
            else
            {
                return RedirectToAction("Index", "FreshCases");
            }
        }
        string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                if (ext.ToUpper() == ".PDF")
                {
                    int FileSize = FileUpload.ContentLength;
                    if (FileSize > 5048576)
                    {
                        return ".pdf  with Maximum file size 5 mb will be uploaded";
                    }
                    else
                    {
                        string fileName = Path.GetFileName(FileUpload.FileName);
                        Guid guid;
                        guid = Guid.NewGuid();

                        string UploadName = guid + "_" + fileName;
                        string FilePath = Path.Combine(Server.MapPath("~/Document/ApplicationDocument/"), UploadName);
                        //string FilePath1 = Server.MapPath("~/Document/ApplicationDocument/" + fileName + "");
                        FileUpload.SaveAs(FilePath);
                        //FileUpload.SaveAs(FilePath1);


                        PdfReader pdfReader = new PdfReader(FilePath);
                        numberOfPages = pdfReader.NumberOfPages;


                        return UploadName;
                    }
                }
            }

            return "";
        }


        public ActionResult DeleteBasicIndex(Guid id, Guid ApplicationDocumentID)
        {
            var dbdata = NoticeBasicIndexDataService.GetDetail(db).Where(x => x.NoticeBasicIndexId == id).FirstOrDefault();
            db.NoticeBasicIndexs.Remove(dbdata);
            db.SaveChanges();
            TempData["CurrentTab"] = "app";
            DisplayMessage(MessageType.Error, $"ApplicationDocument  Document Removed Successfully ", "Deleted", "successfully");
            //return RedirectToAction("Index", "ApplicationDocument", new { id = ApplicationDocumentID });
            return RedirectToAction("List");
        }
        #endregion

        #region List
        public ActionResult List()
        {
            ApplicationDocumentIndexMvcModel model = new ApplicationDocumentIndexMvcModel()
            {
                //NoticeNo = string.Empty,
                //NoticeFromDate = DateTime.Now.AddMonths(-1),
                //NoticeToDate = DateTime.Now.AddDays(10)
            };

            // InitilizeCaseNoticeTypes(null, 0db

            var dbData = ApplicationDocumentDataService.GetDetail(db)
                .Where(F => F.InsertedBy == AppUser.UserName)
                .OrderByDescending(i => i.InsertedOn).ToList();


            //var dbData = (from s in db.ApplicationDocuments
            //              join sa in db.ApplicationTypes on s.ApplicationTypeID equals sa.ApplicationTypeID
            //              where s.InsertedBy == AppUser.UserName
            //              orderby s.InsertedOn
            //              select new
            //              {
            //                  ApplicationTypeID = sa.ATName,
            //                  ApplicantName = s.ApplicantName,
            //                  FinalStatus = s.FinalStatus,
            //                  ApplicationDocumentID=s.ApplicationDocumentID
            //              }  );
            model.ApplicationDocuments = ApplicationDocumentMvcModel.CopyFromEntityList(dbData, 0);

            return View(model);
        }

        //private List<SelectListItem> GetAllStatus()
        //{
        //    var AllStatuses = CaseStatusDataService.GetLite(db).ToList();
        //    var CaseStatuses = new List<SelectListItem>();
        //    for (int i = 0; i < AllStatuses.Count; i++)
        //    {
        //        CaseStatuses.Add(new SelectListItem()
        //        {
        //            Text = AllStatuses[i].Name,
        //            Value = AllStatuses[i].CaseStatusId.ToString(),
        //            // Selected = AllStatuses.Where(x => x.Name == "Active").FirstOrDefault().CaseStatusId == AllStatuses[i].CaseStatusId
        //        });
        //    }
        //    return CaseStatuses;
        //}
        #endregion

        #region delete
        public ActionResult Delete(Guid id)
        {
            var dbData = ApplicationDocumentDataService.GetLite(db).Where(i => i.ApplicationDocumentID == id).FirstOrDefault();
            db.ApplicationDocuments.Remove(dbData);
            db.SaveChanges();
            db.SaveChangesWithAudit(Request, AppUser.UserName);

            DisplayMessage(MessageType.Success, $"ApplicationDocument  Deleted Successfully ", "Deleted", "successfully");
            return RedirectToAction("List");
        }
        #endregion

        #region FinalSubmit
        public ActionResult ConfirmFinalSubmit(Guid id)
        {


            var dbData = ApplicationDocumentDataService.GetLite(db).Where(i => i.ApplicationDocumentID == id).FirstOrDefault();

            dbData.FinalStatus = ApplicationStatus.FinalSubmit;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"ApplicationDocument Finally Submit successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("List");
        }
        #endregion

        #region Edit
        public ActionResult Edit(Guid id)
        {

            var dbData = ApplicationDocumentDataService.GetLite(db).Where(i => i.ApplicationDocumentID == id).FirstOrDefault();
            var dbData1 = ECourtFeeDataService.GetDetail(db).Where(i => i.ApplicationDocumentID == id).ToList();
            var dbData2 = NoticeBasicIndexDataService.GetDetail(db).Where(i => i.ApplicationDocumentID == id).ToList();
            if (dbData1.Count == 0)
            {
                return RedirectToAction("Index", "ApplicationDocument", new { id = id });
            }
            InitilizeApplicationTypes(dbData.ApplicationTypeID);
            ApplicationDocumentMvcModel model = ApplicationDocumentMvcModel.CopyFromEntity(dbData, 0);

            model.ECourtFees = ECourtFeeMvcModel.CopyFromEntityList(dbData1, 0);
            model.NoticeBasicIndexs = NoticeBasicIndexMvcModel.CopyFromEntityList(dbData2, 0);
            return View(model);
        }

        public ActionResult EditApplicationDocument(ApplicationDocumentMvcModel model)
        {
            int Ecourt = 1;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = ApplicationDocumentDataService.GetDetail(db).Where(i => i.ApplicationDocumentID == model.ApplicationDocumentID).FirstOrDefault();

            InitilizeApplicationTypes(dbData.ApplicationTypeID);
            if (dbData == null)
            {
                return RedirectToAction("List");
            }

            dbData.ApplicationTypeID = model.ApplicationTypeID;
            dbData.ApplicantName = model.ApplicantName;
            dbData.ModifiedBy = AppUser.UserName;
            dbData.ModifiedOn = DateTime.Now;
            dbData.IsActive = true;
            dbData.FinalStatus = ApplicationStatus.Draft;


            foreach (var Item_ECourtFee in model.ECourtFees.Where(x => x.bActive == true))
            {

                ECourtFee ECF = dbData.ECourtFees.Where(e => e.eCourtFeeID == Item_ECourtFee.eCourtFeeID).FirstOrDefault();

                ECF.Amount = Item_ECourtFee.Amount;
                ECF.ReceiptNo = Item_ECourtFee.ReceiptNo;
                ECF.ModifyBy = AppUser.UserName;
                ECF.EDate = Item_ECourtFee.EDate;
                ECF.ModifyDate = DateTime.Now;


                Ecourt += 1;
            }



            if (!model.NoticeBasicIndexs.IsEmptyCollection())
            {
                foreach (var item_NoticeBasicIndex in model.NoticeBasicIndexs.Where(x => x.bActive == true && x.Particulars != null && x.Particulars != ""))
                {
                    bool newNoticeBasicIndex = false;
                    NoticeBasicIndex Insert_NoticeBasicIndex = dbData.BasicIndexs.Where(b => b.NoticeBasicIndexId == item_NoticeBasicIndex.NoticeBasicIndexId).FirstOrDefault();
                    if (Insert_NoticeBasicIndex == null)
                    {
                        newNoticeBasicIndex = true;
                        Insert_NoticeBasicIndex = new NoticeBasicIndex();
                        Insert_NoticeBasicIndex.NoticeBasicIndexId = Guid.NewGuid();
                        Insert_NoticeBasicIndex.ApplicationDocumentID = dbData.ApplicationDocumentID;
                    }

                    Insert_NoticeBasicIndex.Particulars = item_NoticeBasicIndex.Particulars;
                    Insert_NoticeBasicIndex.Annexure = item_NoticeBasicIndex.Annexure;
                    Insert_NoticeBasicIndex.IndexDate = item_NoticeBasicIndex.IndexDate;
                    Insert_NoticeBasicIndex.NoOfPages = 100;
                    bool newDoc = false;
                    if (item_NoticeBasicIndex.BasicIndexFile != null && item_NoticeBasicIndex.BasicIndexFile.ContentLength > 0)
                    {
                        if (!item_NoticeBasicIndex.Documents.IsEmptyCollection())
                        {
                            if (Insert_NoticeBasicIndex.Documents.Count > 0)
                            {
                                foreach (var uDoc in Insert_NoticeBasicIndex.Documents)
                                {
                                    Document doc_Update = Insert_NoticeBasicIndex.Documents.Where(d => d.DocumentId == uDoc.DocumentId).FirstOrDefault();
                                    if (doc_Update == null)
                                    {
                                        newDoc = true;
                                        doc_Update = new Document();
                                        doc_Update.DocumentId = Guid.NewGuid();
                                        doc_Update.FileType = DocumentFileType.Any;
                                        doc_Update.EntityType = DocumentEntityType.CaseHearing;

                                    }
                                    
                                    doc_Update.NoticeBasicIndexId = uDoc.NoticeBasicIndexId;
                                    doc_Update.Name = uDoc.Name;
                                    doc_Update.DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile);
                                    doc_Update.Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength;
                                    doc_Update.Status = DocumentStatus.Added;
                                    doc_Update.CreatedBy = AppUser.UserId;
                                    if (newDoc)
                                        Insert_NoticeBasicIndex.Documents.Add(doc_Update);

                                }
                            }
                            else
                            {
                                newDoc = true;
                                Document doc_Update = new Document();
                                doc_Update.DocumentId = Guid.NewGuid();
                                doc_Update.FileType = DocumentFileType.Any;
                                doc_Update.EntityType = DocumentEntityType.CaseHearing;
                                doc_Update.NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId;
                                doc_Update.Name = item_NoticeBasicIndex.Documents[0].Name;
                                doc_Update.DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile);
                                doc_Update.Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength;
                                doc_Update.Status = DocumentStatus.Added;
                                doc_Update.CreatedBy = AppUser.UserId;
                                if (newDoc)
                                    Insert_NoticeBasicIndex.Documents.Add(doc_Update);
                            }
                        }
                        else if (item_NoticeBasicIndex.BasicIndexFile.ContentLength > 0)
                        {
                            Document Insert_Doc = new Document
                            {
                                DocumentId = Guid.NewGuid(),
                                NoticeBasicIndexId = Insert_NoticeBasicIndex.NoticeBasicIndexId,
                                Name = Insert_NoticeBasicIndex.Annexure,
                                DocumentUrl = SaveDocument(item_NoticeBasicIndex.BasicIndexFile),
                                FileType = DocumentFileType.Any,
                                EntityType = DocumentEntityType.CaseHearing,
                                Size = item_NoticeBasicIndex.BasicIndexFile.ContentLength,
                                Status = DocumentStatus.Added,
                                CreatedBy = AppUser.UserId
                            };
                            Insert_NoticeBasicIndex.Documents.Add(Insert_Doc);
                        }
                    }
                    if (newNoticeBasicIndex)
                        dbData.BasicIndexs.Add(Insert_NoticeBasicIndex);
                }
            }
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            SucessMessage = $"Application Document updated successfully";
            DisplayMessage(MessageType.Success, SucessMessage);
            return RedirectToAction("List");
        }

        public ActionResult EditECourtFee(Guid id)
        {
            var dbData = ECourtFeeDataService.GetLite(db).Where(i => i.eCourtFeeID == id).FirstOrDefault();

            if (dbData == null)
            {
                return RedirectToAction("List");
            }
            return RedirectToAction("Edit", "ApplicationDocument", new { id = id });
        }
        #endregion
    }
}