﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CaseMvcModel : BaseEntityModel<Case, CaseMvcModel>
    {
        public Guid CaseId { get; set; }
        public Guid NoticeId { get; set; }
        public string LastHearingDate { get; set; }
        public int HearingStatus { get; set; }
        [Required]
        [Display(Name = "Notice Type")]
        public string PetitionNo { get; set; }
        [Display(Name = "Case Status")]
        public Guid? CaseStatusId { get; set; }
        [Display(Name = "Advocate")]
        public Guid? AdvocateId { get; set; }
        [Display(Name = "Docs Prepration")]
        public Guid? DocsPreprationId { get; set; }
        [Display(Name = "Court")]
        public Guid? CourtId { get; set; }
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        [Display(Name = "Status2")]
        public CaseMovementStatus? Status2 { get; set; }
        public Guid? ParentCaseId { get; set; }
        public int Type { get; set; }
        [Required]
        [Display(Name = "Petitioner")]
        public Guid PetitionerId { get; set; }
        [Display(Name = "Notice Counselor")]
        public Guid? NoticeCounselorId { get; set; }
        [Display(Name = "Next Hearing Date")]
        public DateTime? NextHearingDate { get; set; }
        public Guid? DocumentId { get; set; }
        public Guid? SharedBy { get; set; }
        public DateTime? LastSubmitDate { get; set; }
        public string ModifyBy { get; set; }
        public string CommentRequestor { get; set; }
        public CaseStatusMvcModel CaseStatus { get; set; }
        public UserMvcModel Advocate { get; set; }
        public UserMvcModel DocsPrepration { get; set; }
        public CourtMvcModel Court { get; set; }
        public RegionMvcModel Region { get; set; }
        public NoticeMvcModel Notice { get; set; }
        public bool IsCurrentAdvocate { get; set; }
        public NewCaseHearingMvcModelList NewCaseHearing { get; set; }
        public ICollection<UserCaseMvcModel> UserCases { get; set; }
        public ICollection<NoticeBasicIndexMvcModel> BasicIndexs { get; set; }
        public ICollection<CaseHearingMvcModel> CaseHearings { get; set; }
        public ICollection<CaseTransferMvcModel> CaseTransfers { get; set; }
        public ICollection<TimelineMvcModel> Timelines { get; set; }
        public override CaseMvcModel EntityToResponse(Case entity, CaseMvcModel model)
        {
            model.Advocate = UserMvcModel.CopyFromEntity(entity.Advocate, model.RecursiveLevel);
            model.DocsPrepration = UserMvcModel.CopyFromEntity(entity.DocsPrepration, model.RecursiveLevel);
            model.Court = CourtMvcModel.CopyFromEntity(entity.Court, model.RecursiveLevel);
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, model.RecursiveLevel);
            model.Notice = NoticeMvcModel.CopyFromEntity(entity.Notice, model.RecursiveLevel);
            model.UserCases = UserCaseMvcModel.CopyFromEntityList(entity.UserCases, model.RecursiveLevel);
            model.BasicIndexs = NoticeBasicIndexMvcModel.CopyFromEntityList(entity.BasicIndexs, model.RecursiveLevel);
            model.CaseHearings = CaseHearingMvcModel.CopyFromEntityList(entity.CaseHearings, model.RecursiveLevel);
            model.CaseTransfers = CaseTransferMvcModel.CopyFromEntityList(entity.CaseTransfers, model.RecursiveLevel);
            model.CaseStatus = CaseStatusMvcModel.CopyFromEntity(entity.CaseStatus, model.RecursiveLevel);
            model.Timelines = TimelineMvcModel.CopyFromEntityList(entity.Timelines, model.RecursiveLevel);
            return model;
        }


    }
    public class CaseHearingMvcModel : BaseEntityModel<CaseHearing, CaseHearingMvcModel>
    {
        public Guid CaseHearingId { get; set; }
        public Guid CaseId { get; set; }
        [Display(Name = "Date of Hearing")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? HearingDate { get; set; }

        [Display(Name = "Next Hearing Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? NextHearingDate { get; set; }

        [AllowHtml]
        public string Comments { get; set; }
        public CaseMvcModel Case { get; set; }
        [Display(Name = "Supporting document")]
        public DocumentMvcModel SupportingDocuments { get; set; }
        [Display(Name = "Existing Supporting document")]
        public List<DocumentMvcModel> ExistingSupportingDocs { get; set; }
        public List<DocumentMvcModel> Documents { get; set; }

        [Display(Name = "Type of Hearing")]
        public CaseHearingType HearingType { get; set; }
        [Display(Name = "H'ble Justice Name")]
        public Guid JudgeId { get; set; }
        [Display(Name = "H'ble Justice Name")]
        public JudgeResponseModel Judge { get; set; }
        [Display(Name = "Sr. no of case in cause list")]
        public string CauseListSrNo { get; set; }
        [Display(Name = "Reason as mentioned in the cause list")]
        public string CauseListReason { get; set; }
        [Display(Name = "Instruction from the court")]
        public string CourtInstructions { get; set; }

        public CaseHearingStatus HearingStatus { get; set; }
        [Display(Name = "Is certified copy received")]
        public CertifiedCopyReceived IsCertifiedCopyReceived { get; set; }
        [Display(Name = "Is Time Bound Compliance")]
        public TimeBoundCompliance IsTimeBoundCompliance { get; set; }
        [Display(Name = "Time bound Compliance  Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? TimeBoundComplianceDate { get; set; }
        public string CertifiedCopyFileName { get; set; }
        public Guid CertifiedCopyDocumentId { get; set; }
        [Display(Name = "Certified copy of the judgement")]
        public DocumentMvcModel CertifiedCopy { get; set; }
        public List<CaseHearingRespondentMvcModel> CaseHearingRespondents { get; set; }

        public List<CaseHearingResponseMvcModel> CaseHearingResponses { get; set; }

        public CaseHearingResponseMvcModel CaseHearingResponse { get; set; }
        public ICollection<CaseHearingGovAdvocateMvcModel> CaseHearingGovAdvocates { get; set; }
        [Display(Name = "Name of Govt Advocate to appear")]
        public Guid[] AdvocateIds { get; set; }
        public Guid[] AdvocateIds_Selected { get; set; }
        public List<NoticeRespondentMvcModel> AddRespondents { get; set; }
        public List<Guid> RemoveRespondents { get; set; }
        public override CaseHearingMvcModel EntityToResponse(CaseHearing entity, CaseHearingMvcModel model)
        {
            model.Case = CaseMvcModel.CopyFromEntity(entity.Case, model.RecursiveLevel);
            model.Documents = DocumentMvcModel.CopyFromEntityList(entity.Documents, model.RecursiveLevel);
            model.CaseHearingRespondents = CaseHearingRespondentMvcModel.CopyFromEntityList(entity.CaseHearingRespondents, model.RecursiveLevel);
            model.CaseHearingResponses = CaseHearingResponseMvcModel.CopyFromEntityList(entity.CaseHearingResponses, model.RecursiveLevel);
            model.CaseHearingGovAdvocates = CaseHearingGovAdvocateMvcModel.CopyFromEntityList(entity.CaseHearingGovAdvocates, model.RecursiveLevel);
            model.Judge = JudgeResponseModel.CopyFromEntity(entity.Judge, model.RecursiveLevel);
            return model;
        }

        public UserMvcModel CreatedByUser { get; set; }
    }

    public class CaseHearingRespondentMvcModel : BaseEntityModel<CaseHearingRespondent, CaseHearingRespondentMvcModel>
    {

        public Guid CaseHearingRespondentId { get; set; }
        public Guid CaseHearingId { get; set; }

        [Display(Name = "Government Department")]
        public Guid GovernmentDepartmentId { get; set; }
        [Display(Name = "Document type")]
        public Guid DocumentType { get; set; }
        [Display(Name = "Last compliance date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastComplianceDate { get; set; }
        public Priority Priority { get; set; }
        public bool bActive { get; set; }
        public string Comments { get; set; }
        [Display(Name = "Respondent Departments")]
        public GovermentDepartmentMvcModel GovernmentDepartment { get; set; }
    }

    public class CaseHearingGovAdvocateMvcModel : BaseEntityModel<CaseHearingGovAdvocate, CaseHearingGovAdvocateMvcModel>
    {
        public Guid CaseHearingGovAdvocateId { get; set; }
        public Guid CaseHearingId { get; set; }
        public Guid AdvocateId { get; set; }
        public UserMvcModel Advocate { get; set; }
        public override CaseHearingGovAdvocateMvcModel EntityToResponse(CaseHearingGovAdvocate entity, CaseHearingGovAdvocateMvcModel model)
        {
            model.Advocate = UserMvcModel.CopyFromEntity(entity.Advocate, model.RecursiveLevel);
            return model;
        }
    }


    public class CaseHearingResponseMvcModel : BaseEntityModel<CaseHearingResponse, CaseHearingResponseMvcModel>
    {
        public Guid CaseHearingResponseId { get; set; }
        public Guid CaseId { get; set; }
        public Guid CaseHearingId { get; set; }
        public Guid? GovDepartmentId { get; set; }
        public Guid? DocumentId { get; set; }
        public Guid? AdvocateId { get; set; }
        public Document Document { get; set; }
        public string Comments { get; set; }
        public HttpPostedFileBase UploadDocument { get; set; }
    }
    public class CaseTransferMvcModel : BaseEntityModel<CaseTransfer, CaseTransferMvcModel>
    {
        public Guid CaseTransferId { get; set; }
        public Guid CaseId { get; set; }
        public Guid CourtId { get; set; }
        public DateTime? TransferDate { get; set; }
        public string Description { get; set; }
        public CaseMvcModel Case { get; set; }
        public CourtMvcModel Court { get; set; }
        public override CaseTransferMvcModel EntityToResponse(CaseTransfer entity, CaseTransferMvcModel model)
        {
            model.Case = CaseMvcModel.CopyFromEntity(entity.Case, model.RecursiveLevel);
            model.Court = CourtMvcModel.CopyFromEntity(entity.Court, model.RecursiveLevel);
            return model;
        }
    }

    public class NewCaseHearingMvcModel
    {
        public CaseHearingMvcModel Hearing { get; set; }
        public DocumentMvcModel Document { get; set; }
        [Required]
        [Display(Name = "Hearing case documents")]
        public System.Web.HttpPostedFileBase UploadFile { get; set; }
    }
    public class NewCaseHearingMvcModelList
    {
        public CaseHearingMvcModel Hearing { get; set; }
        public List<DocumentMvcModel> DocumentMvcModel { get; set; }

    }

    public class UpdateDisposedCaseMvcModal
    {
        public Guid CaseGuid { get; set; }
        public string CaseNo { get; set; }
        public Guid BenchId { get; set; }
        public Guid CourtId { get; set; }
    }

    public class CauseListFilterMVCModel : BaseEntityModel<CauseList, CauseListFilterMVCModel>
    {
        [Display(Name = "Cause List Date")]
        public DateTime CauseListDate { get; set; }
        public ICollection<CauseListMvcModel> CauseLists { get; set; }
    }

    public class CaseFilterMvcModel : BaseEntityModel<Case, CaseFilterMvcModel>
    {
        [Display(Name = "Case Hearing Status")]
        public string CaseHearingStatusActive { get; set; }

        [Display(Name = "Gov Department")]
        public Guid? GovDepartmentIdActive { get; set; }
        [Display(Name = "Case Status")]
        public Guid? CaseStatusActive { get; set; }

        [Display(Name = "Case Status")]
        public Guid? CaseStatusTransfer { get; set; }

        [Display(Name = "Case Type")]
        public string ParentNoticeTypeActive { get; set; }

        [Display(Name = "Case Type")]
        public Guid? NoticeTypeActive { get; set; }

        [Display(Name = "Case Type")]
        public string CaseTypeDispose { get; set; }
        [Display(Name = "Case Type")]
        public string CaseTypeTransfer { get; set; }
        [Display(Name = "Case No")]
        public string CaseNoActive { get; set; }
        [Display(Name = "Petitioner Name")]
        public string CasePetitionerNameActive { get; set; }

        [Display(Name = "Date"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateActive { get; set; }
        [Display(Name = "Date"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateDispose { get; set; }
        [Display(Name = "Date"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateTransfer { get; set; }
        [Display(Name = "Advocate")]
        public Guid? AdvocateId { get; set; }
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        [Display(Name = "Department")]
        public Guid? GovDeptId { get; set; }
        [Display(Name = "Document Required")]
        public Guid? DocRequiredId { get; set; }
        [Display(Name = "Hearning")]
        public string HearningType { get; set; }
        [Display(Name = "Context Order")]
        public int IsContextOrderActive { get; set; }
        [Display(Name = "Context Order")]
        public int IsContextOrderDispose { get; set; }
        [Display(Name = "Context Order")]
        public int IsContextOrderTransfer { get; set; }
        [Display(Name = "Context Order")]
        public int IsContextOrder { get; set; }
        public AdvocateMvcModel Advocate { get; set; }

        public AdvocateMvcModel DocsPrepration { get; set; }
        public AdvocateMvcModel OldAdvocate { get; set; }
        public CourtMvcModel OldCourt { get; set; }
        public CourtMvcModel NewCourt { get; set; }
        public string Description { get; set; }
        [Display(Name = "Counselor Name")]
        public string Counselor { get; set; }
        [Display(Name = "Petitioner Name")]
        public string Petitioner { get; set; }
        [Display(Name = "Respondent Name")]
        public string Respondent { get; set; }
        public ICollection<CaseMvcModel> AcctiveCases { get; set; }
        public ICollection<CaseMvcModel> DisposedCases { get; set; }
        public ICollection<CaseMvcModel> CaseTransfer { get; set; }
        public ICollection<CauseListMvcModel> CauseLists { get; set; }

        [Display(Name = "Date From"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateFrom { get; set; }
        [Display(Name = "Date To"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateTo { get; set; }
        public override CaseFilterMvcModel EntityToResponse(Case entity, CaseFilterMvcModel model)
        {
            return model;
        }
        public string submitButton { get; set; }
        public ExportType Export { get; set; }
        public Guid GovOICId { get; set; }
        public GovOICMvcModel GovOIC { get; set; }
    }
    public class DocumentMasterMvcModel : BaseEntityModel<DocumentMaster, DocumentMasterMvcModel>
    {
        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
    }


    public class AdvocateCaseFilterMvcModel : BaseEntityModel<Case, AdvocateCaseFilterMvcModel>
    {
        [Display(Name = "Case Status")]
        public Guid? CaseStatusActive { get; set; }

        [Display(Name = "Case Status")]
        public Guid? CaseStatusTransfer { get; set; }

        [Display(Name = "Case Type")]
        public string CaseTypeActive { get; set; }

        [Display(Name = "Case Type")]
        public string CaseTypeDispose { get; set; }
        [Display(Name = "Case Type")]
        public string CaseTypeTransfer { get; set; }

        [Display(Name = "Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateActive { get; set; }
        [Display(Name = "Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateDispose { get; set; }
        [Display(Name = "Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateTransfer { get; set; }
        [Display(Name = "Advocate")]
        public Guid? AdvocateId { get; set; }

        [Display(Name = "Advocate")]
        public Guid? UserId { get; set; }
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        [Display(Name = "Department")]
        public Guid? GovDeptId { get; set; }
        [Display(Name = "Document Required")]
        public Guid? DocRequiredId { get; set; }
        [Display(Name = "Hearning")]
        public string HearningType { get; set; }
        [Display(Name = "Context Order")]
        public int IsContextOrder { get; set; }
        public AdvocateMvcModel Advocate { get; set; }
        public CourtMvcModel OldCourt { get; set; }
        public CourtMvcModel NewCourt { get; set; }
        public string Description { get; set; }

        public ICollection<CaseMvcModel> AcctiveCases { get; set; }
        public ICollection<CaseMvcModel> DisposedCases { get; set; }
        public ICollection<CaseMvcModel> CaseTransfer { get; set; }

        [Display(Name = "Date From"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateFrom { get; set; }
        [Display(Name = "Date To"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CaseDateTo { get; set; }
        public override AdvocateCaseFilterMvcModel EntityToResponse(Case entity, AdvocateCaseFilterMvcModel model)
        {
            return model;
        }
        public IList<AdvocateCaseCount> CaseCountList { get; set; }




    }

    public class AdvocateCaseCount
    {
        public string CaseStatus { get; set; }
        public int? Count { get; set; }
    }
}
