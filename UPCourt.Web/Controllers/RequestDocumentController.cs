﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;
using static UPCourt.Web.Models.ResponseModel;

namespace UPCourt.Web.Controllers
{
    public class RequestDocumentController : BaseMVCController
    {
        // GET: RequestDocument
        public ActionResult Index()
        {
            var data = GetItems();
            var dbData = RequestDocumentModel.CopyFromEntityList(data, 0);
          
            return View(dbData);
        }

        public ActionResult Search()
        {
            var baseURL = Request.Url.GetLeftPart(UriPartial.Authority);
           
            var data = GetItems();
           var dbData = RequestDocumentDataService.GetFiltered(db, AppUser.User).OrderBy(i => i.DocumentId).ToList();
            var retval = (from gd in dbData
                          join pr in db.GovernmentDepartments on gd.SharedBy equals pr.GovernmentDepartmentId
                          join c in db.DocumentMaster on gd.DocumentId equals c.DocumentId
                          join ca in db.Cases on gd.CaseId equals ca.CaseId
                          select new
                          {
                              gd.Id,
                              pr.Name,
                              ca.PetitionNo,
                              c.DocumentName,
                              gd.CommentRequestor,
                              gd.GovDeptComment,
                              gd.Document_Name,
                              gd.DocumentStatus,
                              gd.DocumentPath,
                              gd.CaseId,
                              gd.RequestedAt,
                              gd.LastSubmitDate,
                              gd.GivenAt
                          }).ToList();
            var listDocs = new List<RequestDocumentMvcModel>();

            if (retval.Count > 0)
            {
                foreach (var item in retval)
                {
                    listDocs.Add(new RequestDocumentMvcModel
                    {
                        Id = item.Id,
                        Name = item.Name,
                        CaseNo = item.PetitionNo,
                        DocumentName = item.DocumentName,
                        CommentRequestor = item.CommentRequestor,
                        GovDeptComment = item.GovDeptComment,
                        Document_Name = item.Document_Name,
                        CaseId = item.CaseId,
                        RequestedAt = item.RequestedAt,
                        LastSubmitDate = item.LastSubmitDate,
                        GivenAt = item.GivenAt,
                        DocumentPath = string.IsNullOrEmpty(item.DocumentPath) ? "" : item.DocumentPath.Replace("~/", baseURL + "/")
                    });
                }
            }

            return View(listDocs);
        }

        protected List<Entity.RequestDocument> GetItems()
        {
            var data = RequestDocumentDataService.GetDocuments(db).ToList();
            return data;
        }

        public ActionResult Create()
        {
            InitializeDocumentMasterList(null);
            InitializeDepartmentList(null);

            var model = new RequestDocumentModel();
            return View(model);
        }

        public void InitializeDocumentMasterList(object selectedValue)
        {
            var dbData = DocumentMasterDataService.GetLite(db).OrderBy(i => i.DocumentName).ToList();
            var mvc = DocumentMasterModel.CopyFromEntityList(dbData, 0);
            ViewBag.DocumentMasterList = new SelectList(mvc, nameof(RequestDocumentModel.Id)
                , nameof(Entity.DocumentMaster.DocumentName), selectedValue);
        }

        public void InitializeDepartmentList(object selectedValue)
        {
            var dbData = DocumentMasterDataService.GetLite(db).OrderBy(i => i.DocumentName).ToList();
            var mvc = DocumentMasterModel.CopyFromEntityList(dbData, 0);
            ViewBag.DepartmentList = new SelectList(mvc, nameof(RequestDocumentModel.Id)
                , nameof(Entity.DocumentMaster.DocumentName), selectedValue);
        }

   

        public JsonResult GetDocuments(Guid userId)
        {
            var dbdata = RequestDocumentDataService.GetLite(db).OrderBy(i => i.DocumentId).ToList();
            var ep = (from gd in dbdata
                      join pr in db.GovernmentDepartments on gd.SharedBy equals pr.GovernmentDepartmentId
                      join c in db.DocumentMaster on gd.DocumentId equals c.DocumentId
                      select new { pr.Name, c.DocumentName, gd.CommentRequestor, gd.GovDeptComment, gd.Document_Name, gd.DocumentStatus, gd.DocumentPath, gd.CaseId }); var abc = from g in dbdata
                                                                                                                                                                                  join doc in db.DocumentMaster on g.DocumentId equals doc.DocumentId
                                                                                                                                                                                  select new
                                                                                                                                                                                  {
                                                                                                                                                                                      g.Id,
                                                                                                                                                                                      doc.DocumentName,
                                                                                                                                                                                      g.CommentRequestor,
                                                                                                                                                                                      g.GovDeptComment
                                                                                                                                                                                  };

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(ep);

            var jsonParse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RequestDocumentModel>>(jsonString);

            return Json(ep, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadFile(RequestDocumentMvcModel model)
        {
            var response = new ResponseModel();
            var files = Request.Files;

            response = SaveToPhysicalLocation(files[0], model.Id.ToString());
            if (response.StatusCode == e_StatusCode.Success)
            {
                var newdoc = RequestDocumentDataService.GetDocuments(db).Where(x => x.Id == model.Id).FirstOrDefault();
                newdoc.Id = model.Id;
                newdoc.GivenAt = DateTime.Now;
                newdoc.ModifyBy = AppUser.UserName;
                newdoc.DocumentPath = response.Message;
                newdoc.GovDeptComment = model.GovDeptComment;
                newdoc.Document_Name = files[0].FileName.ToString();
                newdoc.DocumentStatus = 1;
                db.RequestDocument.Update(newdoc);
                int iresult = db.SaveChangesWithAudit(Request);

                if (iresult > 0)
                {
                    var modelNotify = new AppNotifications
                    {
                        Id = Guid.NewGuid(),
                        Source = (int)NotificationSourceEntityType.Case,
                        TargetUserId = newdoc.RequestedBy,
                        NotificationType = 1,
                        Status = 1,
                        Message = "Document Uploaded for case: " + newdoc.Case.PetitionNo,
                        CreatedBy = AppUser.UserId,
                        CreatedOn = Extended.CurrentIndianTime,
                        RouteValue = ""
                    };
                    var iResult = AppNotifications.Create(modelNotify);

                    string DocumentTypeName = DocumentMasterDataService.GetLite(db).Where(x => x.DocumentId == newdoc.DocumentId).FirstOrDefault().DocumentName;
                    string CaseName = CaseDataService.GetLite(db).Where(x => x.CaseId == newdoc.CaseId).FirstOrDefault().PetitionNo;

                    string description = $"{ DocumentTypeName } Document uploaded Successfully for Case No { CaseName } ";
                    TimelineBusinessService.SaveTimeLine(db, null, model.CaseId, EventType.RequestDocumentUploaded, AppUser.UserId, description, null);

                    response.StatusCode = e_StatusCode.Success;
                    response.Message = "Document uploaded successfully.";
                    response.Data = null;
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private ResponseModel SaveToPhysicalLocation(HttpPostedFileBase file, string fileSavingDirectoryUQ)
        {
            var response = new ResponseModel();
            string retval = string.Empty;
            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(file.FileName);

                    if (fileExtension.ToUpper() == ".DOC" || fileExtension.ToUpper() == ".DOCX" || fileExtension.ToUpper() == ".PDF"
                        || fileExtension.ToUpper() == ".XLS" || fileExtension.ToUpper() == ".XLSX" || fileExtension.ToUpper() == ".JPEG"
                        || fileExtension.ToUpper() == ".JPG" || fileExtension.ToUpper() == ".PNG")
                    {
                        int FileSize = file.ContentLength;
                        if (FileSize > 5048576)
                        {
                            retval = ".jpeg, .jpg, .png, .doc, .docx, .xls, .xlsx and .pdf  with Maximum file size 5 mb will be uploaded";
                            response.Message = retval;
                            response.StatusCode = e_StatusCode.ValidationFails;
                        }
                        else
                        {
                            string sFileSavePath = Server.MapPath("~/Document/Request");
                            if (!Directory.Exists(sFileSavePath))
                                Directory.CreateDirectory(sFileSavePath);

                            Guid guid;
                            guid = Guid.NewGuid();

                            var fileName = Path.GetFileName(file.FileName);
                            var path = Path.Combine(sFileSavePath + "/", guid + "_" + fileName);
                            file.SaveAs(path);
                            retval = "~/Document/Request/" + guid + "_" + fileName;
                            response.Message = retval;
                            response.StatusCode = e_StatusCode.Success;
                        }
                    }
                    else
                    {
                        retval = "This file format is not supported. Accepted file formats are: doc/docx/xls/xlsx/pdf/jpg/png.";
                        response.Message = retval;
                        response.StatusCode = e_StatusCode.ValidationFails;
                    }
                }
            }
            else
            {
                retval = "Select a file to upload.";
                response.Message = retval;
                response.StatusCode = e_StatusCode.ValidationFails;
            }

            return response;
        }
    }
}