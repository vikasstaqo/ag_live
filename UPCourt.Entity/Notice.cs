﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeNo), UserNameProp: nameof(CreatedBy))]
    public class Notice : BaseTransEntity
    {
        public Notice()
        {
            Petitioners = new HashSet<Petitioner>();
            Counselors = new HashSet<NoticeCounselor>();
            Respondents = new HashSet<PartyAndRespondent>();
            BasicIndexs = new HashSet<NoticeBasicIndex>();
            IPCs = new HashSet<NoticeIPC>();
            ActTypes = new HashSet<NoticeActType>();
            NoticeSpecialcategorys = new HashSet<NoticeSpecialcategory>();
            Cases = new HashSet<Case>();
        }
        public Guid NoticeId { get; set; }
        public ParentNoticeType NoticeType { get; set; }
        public Guid? CaseNoticeTypeId { get; set; }
        public CaseNoticeType CaseNoticeType { get; set; }
        public Guid? GroupID { get; set; }
        public Group Group { get; set; }
        public CrimeDetail CrimeDetail { get; set; }
        public Casedetail CaseDetail { get; set; }
        public int SerialNo { get; set; }
        public string NoticeNo { get; set; }
        public DateTime? NoticeDate { get; set; }
        public DateTime NoticeCreateDate { get; set; }
        public int NoticeYear { get; set; }
        //case details
        public string Subject { get; set; }
        public bool IsContextOrder { get; set; }
        public Guid RegionId { get; set; }
        public NoticeStatus Status { get; set; }
        public NoticeFinalStatus FinalSubmit { get; set; }
        public string District { get; set; }
        public NoticeFrom NoticeFrom { get; set; }
        public ICollection<ECourtFee> ECourtFees { get; set; }
        public ICollection<Petitioner> Petitioners { get; set; }
        public ICollection<NoticeCounselor> Counselors { get; set; }
        public ICollection<PartyAndRespondent> Respondents { get; set; }
        public ICollection<NoticeBasicIndex> BasicIndexs { get; set; }
        public ICollection<NoticeIPC> IPCs { get; set; }
        public ICollection<NoticeActType> ActTypes { get; set; }
        public ICollection<NoticeSpecialcategory> NoticeSpecialcategorys { get; set; }
        public ICollection<ImpungedAndSTDetail> ImpungedAndSTDetails { get; set; }
        public Region Region { get; set; }
        public ICollection<Case> Cases { get; set; }
        public string SectionOrAct { get; set; }

        public DateTime ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }

        public int? RegCaseType { get; set; }
        public int? RegNo { get; set; }
        public string Cnr { get; set; }
        public int? Ported { get; set; }
        public string ReferenceCaseNo { get; set; }

    }

    public enum NoticeStatus
    {
        Pending = 0,
        Accept = 1,
        Reject = 2
    }

    public enum NoticeFinalStatus
    {
        Pending = 0,
        Submited = 1,
    }
    public enum NoticeFrom
    {
        InternalSystem = 1,
        UserSystem = 2
    }


}
