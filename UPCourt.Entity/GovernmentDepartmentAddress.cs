﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Address),UserNameProp:nameof(ModifyBy))]
    public class GovernmentDepartmentAddress
    {
        public Guid GovernmentDepartmentAddressId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string Address { get; set; }
        public bool IsHeadOfficeAddress { get; set; } 
        public string ModifyBy { get; set; } 
        public GovernmentDepartment GovernmentDepartment { get; set; }
    }
}
