﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class GroupController : BaseMVCController
    {
        // GET: Group
        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanView)]
        public ActionResult Index()
        {
            return View(GroupMvcModel.CopyFromEntityList(GroupDataService.GetLite(db).OrderByDescending(x => x.DisplayOrder), 0));
        }


        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanCreate)]
        [HttpGet]
        public ActionResult Create()
        {
            var model = new GroupMvcModel();
            model.IsActive = true;
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanCreate)]
        [HttpPost]
        public ActionResult Create(GroupMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Group tempData = new Group()
            {
                GroupID = Guid.NewGuid(),
                GroupName = model.GroupName,
                IsActive = model.IsActive,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetGroupsDisplayOrder(db)
            };
            db.Groups.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }


        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanEdit)]
        public ActionResult Edit(Guid id)
        {
            var dbData = GroupDataService.GetLite(db).Where(i => i.GroupID == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            GroupMvcModel model = GroupMvcModel.CopyFromEntity(dbData, 0);
            return View(model);
        }

        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanEdit)]
        [HttpPost]
        public ActionResult Edit(GroupMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = GroupDataService.GetLite(db).Where(i => i.GroupID == model.GroupID).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            dbData.GroupName = model.GroupName;
            dbData.IsActive = model.IsActive;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        private bool CommonForInsertUpdate(GroupMvcModel model)
        {
            var duplicateCheck = GroupDataService.GetLite(db).FirstOrDefault(i => i.GroupID != model.GroupID && i.GroupName == model.GroupName);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.GroupName), $"{model.GroupName} already exists");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.Group, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = GroupDataService.GetLite(db).Where(i => i.GroupID == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}