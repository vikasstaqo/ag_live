﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Core
{
    public class BaseMVCCaseController : BaseMVCController
    {
        // GET: BaseMVCCase
        protected CaseMvcModel GetCaseMvcModel(Guid id)
        {
            CaseMvcModel CaseMobelobj;
            
            var dbData = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, id).Where(x => x.CaseId == id).FirstOrDefault();
            CaseMobelobj = CaseMvcModel.CopyFromEntity(dbData, 1);
            if (CaseMobelobj.Notice.CrimeDetail != null)
                if (CaseMobelobj.Notice.CrimeDetail.PoliceStation == null && CaseMobelobj.Notice.CrimeDetail.PoliceStationID != null)
                {
                    CaseMobelobj.Notice.CrimeDetail.PoliceStation = PoliceStationMvcModel.CopyFromEntity(PoliceStationDataService.GetLite(db).Where(x => x.PoliceStationId == CaseMobelobj.Notice.CrimeDetail.PoliceStationID).FirstOrDefault(), 0);
                }
            var dbUsers = UserDataService.GetLite(db).ToList();
            foreach (var caseHearing in CaseMobelobj.CaseHearings)
            {
                caseHearing.CreatedByUser = UserMvcModel.CopyFromEntity(dbUsers.Where(x => x.UserId == (Guid)caseHearing.CreatedBy).FirstOrDefault(), 0);
                caseHearing.CaseHearingGovAdvocates = caseHearing.CaseHearingGovAdvocates.Where(x => x.IsDeleted == false).ToList();
                caseHearing.Documents = caseHearing.Documents.Where(d => d.IsDeleted == false).ToList();
            }
            if (CaseMobelobj.CaseHearings.Count > 0)
            {
                if (CaseMobelobj.CaseHearings.LastOrDefault().HearingDate != null && CaseMobelobj.CaseHearings.LastOrDefault().HearingDate != DateTime.MinValue)
                    CaseMobelobj.LastHearingDate = CaseMobelobj.CaseHearings.LastOrDefault().HearingDate?.ToString("yyyy-MM-dd");
                else
                    CaseMobelobj.LastHearingDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
            }
            else
            {
                CaseMobelobj.LastHearingDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
            }
            CaseMobelobj.Timelines = TimelineMvcModel.CopyFromEntityList(TimelineBusinessService.GetCaseTimeline(db, id), 0);
            DocumentMvcModel obj = new DocumentMvcModel();
            CaseMobelobj.NewCaseHearing = new NewCaseHearingMvcModelList();

            CaseMobelobj.NewCaseHearing.DocumentMvcModel = new List<DocumentMvcModel>();
            CaseMobelobj.NewCaseHearing.DocumentMvcModel.Add(new DocumentMvcModel { Name = "" });

            if (CaseMobelobj.Notice.CaseDetail != null)
            {
                CaseMobelobj.Notice.CaseDetail.Bench = BenchTypeMvcModel.CopyFromEntity(db.BenchType.Where(x => x.bench_type_code == CaseMobelobj.Notice.CaseDetail.BenchType).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.Category = CategoryMvcModel.CopyFromEntity(db.Categorys.Where(x => x.CategoryCode == CaseMobelobj.Notice.CaseDetail.C_subject).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.SubCategory = SubCategoryMvcModel.CopyFromEntity(db.SubCategorys.Where(x => x.CategoryCode == CaseMobelobj.Notice.CaseDetail.C_subject && x.SubCategoryCode == CaseMobelobj.Notice.CaseDetail.Cs_subject).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.CauselistTypes = CauseListTypeMvcModel.CopyFromEntity(db.CauseListTypes.Where(x => x.Id == CaseMobelobj.Notice.CaseDetail.CauselistType).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.District = DistrictMvcModel.CopyFromEntity(db.Districts.Where(x => x.DistCode == CaseMobelobj.Notice.CaseDetail.CaseDistCode).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.PetAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == CaseMobelobj.Notice.CaseDetail.pet_adv_cd).FirstOrDefault(), 0);
                CaseMobelobj.Notice.CaseDetail.RespAdvocate = ApiAdvocateMvcModel.CopyFromEntity(db.Advocates.Where(x => x.AdvCode == CaseMobelobj.Notice.CaseDetail.res_adv_cd).FirstOrDefault(), 0);
            }
            CaseMobelobj.IsCurrentAdvocate = CaseMobelobj.AdvocateId == AppUser.UserId;
            return CaseMobelobj;
        }

        protected string SaveDocument(HttpPostedFileBase FileUpload)
        {
            if (FileUpload != null)
            {
                string ext = System.IO.Path.GetExtension(FileUpload.FileName);
                if (ext.ToUpper() == ".DOC" || ext.ToUpper() == ".DOCX" || ext.ToUpper() == ".PDF" || ext.ToUpper() == ".XLS" || ext.ToUpper() == ".JPEG" || ext.ToUpper() == ".JPG" || ext.ToUpper() == ".PNG")
                {
                    int FileSize = FileUpload.ContentLength;
                    if (FileSize > 5048576)
                    {
                        return ".jpeg, .jpg, .doc, .docx, .xls and .pdf  with Maximum file size 5 mb will be uploaded";
                    }
                    else
                    {
                        string fileName = "", UploadName = "", FilePath = "";

                        fileName = Path.GetFileName(FileUpload.FileName);
                        Guid guid;
                        guid = Guid.NewGuid();

                        UploadName = guid + "_" + fileName;
                        FilePath = Path.Combine(Server.MapPath("~/Document/"), UploadName);
                        FileUpload.SaveAs(FilePath);
                        return UploadName;
                    }
                }
            }

            return "";
        }
    }
}