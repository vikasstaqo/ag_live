﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{

    public class SubjectMvcModel : BaseEntityModel<Subject, SubjectMvcModel>
    {
        public Guid SubjectId { get; set; }
        [Required]
        [Display(Name = "Subject")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public ParentNoticeType ParentNoticeType { get; set; }
        [Display(Name = "Notice Type")]
        public Guid? CaseNoticeTypeId { get; set; }
        public CaseNoticeTypeMvcModel CaseNoticeType { get; set; }
        public override SubjectMvcModel EntityToResponse(Subject entity, SubjectMvcModel model)
        {
            model.CaseNoticeType = CaseNoticeTypeMvcModel.CopyFromEntity(entity.CaseNoticeType, model.RecursiveLevel);
            return model;
        }

    }
}