﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Models
{
    public class CustomUserManager : UserManager<ApplicationUser>
    {
        public CustomUserManager()
            : base(new CustomUserSore<ApplicationUser>())
        {

        }

        public override Task<ApplicationUser> FindAsync(string userName, string password)
        {
            var taskInvoke = Task<ApplicationUser>.Factory.StartNew(() =>
            {
                if (userName == "username" && password == "password")
                {
                    return new ApplicationUser { Id = "NeedsAnId", UserName = "UsernameHere" };
                }
                return null;
            });

            return taskInvoke;
        }

        public override Task<ClaimsIdentity> CreateIdentityAsync(ApplicationUser user, string authenticationType)
        {
            var strOrganisationId = user.OrganisationId.HasValue ? user.OrganisationId.Value.ToString() : "";
            var strTempRegionId = user.TempRegionId.HasValue ? user.TempRegionId.Value.ToString() : "";
            var identity = base.CreateIdentityAsync(user, authenticationType);
            identity.Result.AddClaim(new Claim(ClaimTypes.Role, user.UserType.ToString()));
            identity.Result.AddClaim(new Claim(ClaimTypes.GroupSid, strOrganisationId));
            identity.Result.AddClaim(new Claim(ClaimTypes.PrimaryGroupSid, strTempRegionId));
            return identity;
        }
    }
    public class ApplicationUser : IUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public UserType UserType { get; set; }
        public Guid? OrganisationId { get; set; }
        public Guid? DepartmentId { get; set; }
        public Guid? AgDepartmentId { get; set; }
        public Guid? RegionId { get; set; }
        public Guid? TempRegionId { get; set; }
        public DateTime LastLogin { get; set; }
        public Guid UserId { get { return Guid.Parse(Id); } }
        public User User
        {
            get
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    return Service.DataService.UserDataService.GetDetail(db).FirstOrDefault(F => F.UserId == UserId);
                }
            }
        }

        public List<Region> Regions
        {
            get
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    var UserRoleName = User.UserRole.UserSystemRoleId;
                    if (UserRoleName == UserSystemRole.Admin || UserRoleName == UserSystemRole.AGAdmin)
                        return Service.DataService.RegionDataService.GetLite(db).Where(x => x.Status == RegionStatus.Active).ToList();
                    else
                        return Service.DataService.RegionDataService.GetLite(db).Where(x => x.Status == RegionStatus.Active && x.RegionId == User.RegionId).ToList();
                }
            }
        }
        public static ApplicationUser GetUserFromIdentity(IPrincipal principal)
        {
            if (principal == null) { return null; }
            var identity = (ClaimsIdentity)principal.Identity;
            var type = identity.FindFirst(ClaimTypes.Role);
            var organisationId = identity.FindFirst(ClaimTypes.GroupSid);
            var TempRegionId = identity.FindFirst(ClaimTypes.PrimaryGroupSid);
            if (type == null) { return null; }
            ApplicationUser user = new ApplicationUser();
            user.Id = identity.GetUserId();
            user.UserName = identity.GetUserName();
            user.UserType = type.Value.ToEnum<UserType>();
            if (organisationId != null && !organisationId.Value.IsNullOrEmpty())
            {
                user.OrganisationId = Guid.Parse(organisationId.Value);
            }
            if (TempRegionId != null && !TempRegionId.Value.IsNullOrEmpty())
            {
                user.TempRegionId = Guid.Parse(TempRegionId.Value);
            }
            return user;
        }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User Name")]

        public string LoginName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool OTPVerify { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }


    public class ChangePasswordViewModel
    {
        [Required]
        [Display(Name = "User Name")]

        public string LoginName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string Password { get; set; }


    }


    public class CustomUserSore<T> : IUserStore<T> where T : ApplicationUser
    {
        void IDisposable.Dispose()
        {
        }

        public Task CreateAsync(T user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(T user)
        {
            throw new NotImplementedException();
        }

        public Task<T> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<T> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }
    }
}