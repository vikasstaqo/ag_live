﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifiedBy))]
    public class Judge : BaseMasterEntity
    {
        public Guid JudgeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public JudgeStatus Status { get; set; }
        public int judge_code { get; set; }
        public string short_judge_name { get; set; }

    }
    public enum JudgeStatus
    {
        Active = 1,
        Inactive = 0
    }
}
