﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Service.BusinessService
{
    public class MasterBusinessService : BaseBusinessService
    {
        public static int GetCasedetailDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Casedetails.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetCasedetailiaDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Casedetailias.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetIPSectionDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.IPCSections.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetCaseOrdinalNo(DatabaseContext db)
        {
            var DisplayOrderNo = db.Cases.OrderBy(i => i.OrdinalNo).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.OrdinalNo ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetUserDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Users.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetAGDepartmentDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.AGDepartments.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetUserRolesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.UserRoles.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetCourtsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Courts.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetDesignationsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Designations.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetGovernmentDepartmentsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.GovernmentDepartments.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetGovernmentDepartmentOICsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.GovernmentDepartmentOics.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetRegionsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Regions.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetJudgesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Judges.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetPurposeDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Purposes.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetDisposalTypeDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.DisposalTypes.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetCauseListTypeDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.CauseListTypes.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetCategoryDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Categorys.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetSubCategoryDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.SubCategorys.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetBenchTypeDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.BenchType.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetBencheDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Benches.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetBencheJudgesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.BencheJudges.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetCaseNoticeTypesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.CaseNoticeTypes.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetCaseStagesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.CaseStages.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetNatureOfDisposalsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.NatureOfDisposals.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetSubjectsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Subjects.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetPoliceStationsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.PoliceStations.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetGovernmentDesignationsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.GovernmentDesignations.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetSalutationsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Salutations.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetSpecialCategorysDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.SpecialCategorys.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetGroupsDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Groups.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }
        public static int GetZonesDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Zones.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

        public static int GetDistrictDisplayOrder(DatabaseContext db)
        {
            var DisplayOrderNo = db.Districts.OrderBy(i => i.DisplayOrder).LastOrDefault();
            int NewDisplayOrderNo = (DisplayOrderNo?.DisplayOrder ?? 0) + 1;
            return NewDisplayOrderNo;
        }

    }
}