﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DivisionName), UserNameProp: nameof(CreatedBy))]
    public class Division : BaseMasterEntity
    {
        public Division() { }
        public Guid DivisionId { get; set; }
        public string DivisionName { get; set; }
        public DivisionStatus Status { get; set; }
    }

}
public enum DivisionStatus
{
    Active = 1,
    Inactive = 0
}
