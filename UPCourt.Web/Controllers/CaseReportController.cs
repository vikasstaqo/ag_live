﻿using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CaseReportController : BaseMVCController
    {
        // GET: CaseReport
        [PageRoleAuthorizarion(PageName.CaseReport, PagePermissionType.CanView)]
        public ActionResult Index(CaseFilterMvcModel cfmm)
        {

            InitilizeCaseStatus(cfmm.CaseStatusActive);
            InitilizeCaseType(cfmm.ParentNoticeTypeActive);
            InitilizeAdvocate(cfmm.AdvocateId);
            InitilizeRegion(cfmm.RegionId);
            InitilizeDepartment(cfmm.GovDeptId);
            InitilizeDocReq(cfmm.DocRequiredId);
            InitilizeContextOrder(cfmm.IsContextOrder);
            InitilizeCaseHearingStatus(cfmm.CaseHearingStatusActive);

            cfmm.AcctiveCases = searchCases(cfmm);

            if (cfmm.submitButton == "ExportOption")
            {
                if (cfmm.AcctiveCases != null)
                {
                    AddCaseDetails(cfmm.AcctiveCases);
                    var sb = new StringBuilder();
                    var data = from c in cfmm.AcctiveCases
                               select new
                               {
                                   CaseNo = c.PetitionNo,
                                   Petitioner = PetitionerDetail(c.Notice.Petitioners),
                                   Respondent = RespondentDetail(c.Notice.NoticeRespondents),
                                   CaseType = c.Notice.NoticeType,
                                   CaseStatus = c.CaseStatus.Name,
                                   CaseHearingStatus = c.CaseHearings.LastOrDefault()?.HearingStatus,
                                   Region = c.Region?.Name,
                                   AdvocateAGoffice = c.Advocate?.Name,
                                   PetitonerAdvocate = c.Notice.NoticeCounselors.FirstOrDefault()?.Name,
                                   CNRNo = c.Notice.Cnr,
                                   BenchType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Bench != null ? c.Notice.CaseDetail.Bench.Name : "",
                                   CauseListType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.CauselistTypes != null ? c.Notice.CaseDetail.CauselistTypes.Name : "",
                                   District = c.Notice.CaseDetail != null && c.Notice.CaseDetail.District != null ? c.Notice.CaseDetail.District.DistrictName : "",
                                   Category = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Category != null ? c.Notice.CaseDetail.Category.CategoryName + " (" + c.Notice.CaseDetail.Category.CategoryCode + ")" : "",
                                   SubCategory = c.Notice.CaseDetail != null && c.Notice.CaseDetail.SubCategory != null ? c.Notice.CaseDetail.SubCategory.SubCategoryName + " (" + c.Notice.CaseDetail.SubCategory.SubCategoryCode + ")" : "",
                                   NoticeNumber = c.Notice.NoticeNo,
                                   FlilingDate = "No Data",
                                   LastHearingDate = c.CaseHearings.LastOrDefault()?.HearingDate?.ToString("dd/MM/yyyy"),
                                   ContextOrder = c.Notice.IsContextOrder ? "Yes" : "No"
                               };
                    var list = data.ToList();
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = list;
                    grid.DataBind();

                    Response.ClearContent();

                    if (cfmm.Export == ExportType.Excel)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                        grid.RenderControl(htw);
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.xls");
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.Write(sw.ToString());
                    }
                    else if (cfmm.Export == ExportType.PDF)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                        grid.RenderControl(htw);
                        StringReader reader = new StringReader(sw.ToString());
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        pdfDoc.AddHeader("Case Report", "Cases");
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                        pdfDoc.Close();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.PDF");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                    }

                    Response.End();
                }

            }
            return View(cfmm);
        }


        #region Drop Down Lists 

        private string PetitionerDetail(List<PetitionerMvcModel> Pet)
        {
            return Pet != null && Pet.Count > 0 ? Pet.FirstOrDefault().Name : "";
        }
        private string RespondentDetail(List<NoticeRespondentMvcModel> Res)
        {
            string res = "";
            if (Res != null && Res.Count > 0)
            {
                foreach (NoticeRespondentMvcModel objNRM in Res)
                {
                    if (objNRM?.GovernmentDepartmentId != null)
                        res = res + "," + objNRM?.GovermentDepartment.POCName + "(" + objNRM?.GovermentDepartment.Name + ") ";
                    else
                        res = res + "," + objNRM.PartyName;
                }
                res = res.Remove(0, 1);
            }
            return res;
        }
        private List<CaseMvcModel> searchCases(CaseFilterMvcModel caseFilterMvcModel)
        {
            if (caseFilterMvcModel != null)
            {
                List<Case> acctiveCases = new List<Case>();
                acctiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseDateFrom != null)
                    acctiveCases = acctiveCases.Where(x => x.Notice.NoticeDate >= caseFilterMvcModel.CaseDateFrom).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseDateTo != null)
                    acctiveCases = acctiveCases.Where(x => x.Notice.NoticeDate <= caseFilterMvcModel.CaseDateTo).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseStatusActive != null && caseFilterMvcModel.CaseStatusActive.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusActive).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.ParentNoticeTypeActive != null && caseFilterMvcModel.ParentNoticeTypeActive.ToLower() != "all" && caseFilterMvcModel.ParentNoticeTypeActive != "")
                    acctiveCases = acctiveCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.ParentNoticeTypeActive).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.AdvocateId != null && caseFilterMvcModel.AdvocateId.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.Advocate?.UserId == caseFilterMvcModel.AdvocateId).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.RegionId != null && caseFilterMvcModel.RegionId.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.Region?.RegionId == caseFilterMvcModel.RegionId).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.GovDeptId != null && caseFilterMvcModel.GovDeptId.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.Notice.Respondents.FirstOrDefault().GovernmentDepartment?.GovernmentDepartmentId == caseFilterMvcModel.GovDeptId).OrderBy(i => i.Notice.NoticeDate).ToList();

                
                if (caseFilterMvcModel.Petitioner != null && caseFilterMvcModel.Petitioner != "")
                    acctiveCases = acctiveCases.Where(x => x.Notice.Petitioners.Where(p => p.Name.ToLower().Contains(caseFilterMvcModel.Petitioner.Trim().ToLower())).ToList().Count > 0).ToList();

                if (caseFilterMvcModel.Counselor != null && caseFilterMvcModel.Counselor.Trim().Length > 0)
                    acctiveCases = acctiveCases.Where(x => x.Notice.Counselors.Where(c => c.Name.ToLower().Contains(caseFilterMvcModel.Counselor.Trim().ToLower())).ToList().Count > 0).ToList();

                if (caseFilterMvcModel.Respondent != null && caseFilterMvcModel.Respondent.Trim().Length > 0)
                    acctiveCases = acctiveCases.Where(x => x.Notice.Respondents.Where(r => (r.PartyName != null && r.PartyName.ToLower().Contains(caseFilterMvcModel.Respondent.Trim().ToLower()))
                         || r.GovernmentDepartment != null && (r.GovernmentDepartment.Name.ToLower().Contains(caseFilterMvcModel.Respondent.Trim().ToLower())
                         || r.GovernmentDepartment.POCName.ToLower().Contains(caseFilterMvcModel.Respondent.Trim().ToLower()))
                     ).ToList().Count > 0).ToList();

                if (caseFilterMvcModel.CaseHearingStatusActive != null && caseFilterMvcModel.CaseHearingStatusActive.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.HearingStatus == Convert.ToInt32(caseFilterMvcModel.CaseHearingStatusActive)).ToList();

                if (caseFilterMvcModel.IsContextOrder == 2)
                    acctiveCases = acctiveCases.Where(x => x.Notice.IsContextOrder == false).OrderBy(i => i.Notice.NoticeDate).ToList();
                if (caseFilterMvcModel.IsContextOrder == 1)
                    acctiveCases = acctiveCases.Where(x => x.Notice.IsContextOrder == true).OrderBy(i => i.Notice.NoticeDate).ToList();

                AddTimeLines(acctiveCases);

                return CaseMvcModel.CopyFromEntityList(acctiveCases, 0);
            }

            return null;
        }


        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, nameof(CaseStatusMvcModel.CaseStatusId), nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeCaseType(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (!(AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.CSC || AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.AdvCivil))
                items.Add(new SelectListItem { Text = "Criminal", Value = "2" });

            if (!(AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.CGA || AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.AdvCriminal))
                items.Add(new SelectListItem { Text = "Civil", Value = "1" });

            if (AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.Admin || AppUser.User.UserRole.UserSystemRoleId == UserSystemRole.AGAdmin)
                items.Add(new SelectListItem { Text = "All", Value = "99" });

            ViewBag.CaseType = new SelectList(items, "Value", "Text", selectedValue);
        }
        public void InitilizeAdvocate(object selectedValue)
        {
            var dbData = AdvocateDataService.GetLite(db, AppUser).OrderBy(i => i.Name).ToList();
            var mvc = UserMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Advocate = new SelectList(mvc, nameof(UserMvcModel.UserId), nameof(UserMvcModel.Name), selectedValue);
        }
        public void InitilizeRegion(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Region = new SelectList(mvc, nameof(RegionMvcModel.RegionId), nameof(RegionMvcModel.Name), selectedValue);
        }
        public void InitilizeDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Department = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId), nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeDocReq(object selectedValue)
        {

            var dbData = DocumentMasterDataService.GetLite(db).OrderBy(i => i.DocumentName).ToList();
            var mvc = DocumentMasterMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.DocReq = new SelectList(mvc, nameof(DocumentMasterMvcModel.DocumentId), nameof(DocumentMasterMvcModel.DocumentName), selectedValue);
        }
        public void InitilizeHearingType(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Today", Value = "Today" });
            items.Add(new SelectListItem { Text = "Within 2 Days", Value = "Within2Day" });
            items.Add(new SelectListItem { Text = "Within 5 Days", Value = "Within5Day" });
            items.Add(new SelectListItem { Text = "Within a week", Value = "WithinaWeek" });
            items.Add(new SelectListItem { Text = "After a week", Value = "AfteraWeek" });
            ViewBag.HearingType = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitilizeCaseHearingStatus(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Admission", Value = "1" });
            items.Add(new SelectListItem { Text = "Motion Hearing", Value = "2" });
            items.Add(new SelectListItem { Text = "Final Order", Value = "3" });
            ViewBag.CaseHearingStatus = new SelectList(items, "Value", "Text", selectedValue);
        }
        public void InitilizeContextOrder(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Yes", Value = "1" });
            items.Add(new SelectListItem { Text = "No", Value = "2" });
            ViewBag.ContextOrder = new SelectList(items, "Value", "Text", selectedValue);
        }

        public void InitilizeNoticeType(object selectedValue, string ParentnoticeType)
        {
            var dbData = NoticeTypeDataService.GetLite(db).Where(n => n.ParentNoticeType == (ParentNoticeType)Convert.ToInt32(ParentnoticeType) && n.IsActive == true).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);

        }
        #endregion

        [PageRoleAuthorizarion(PageName.DocRequestedCaseReport, PagePermissionType.CanView)]
        public ActionResult DocRequestedCase(CaseFilterMvcModel cfmm)
        {
            InitilizeDocReq(cfmm.DocRequiredId);
            InitilizeCaseHearingStatus(cfmm.CaseHearingStatusActive);

            List<Case> acctiveCases = new List<Case>();
            acctiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderBy(i => i.Notice.NoticeDate).ToList();
            acctiveCases = acctiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingRespondents.Count() > 0).Count() > 0).OrderBy(i => i.Notice.NoticeDate).ToList();
            AddTimeLines(acctiveCases);
            if (cfmm.DocRequiredId != null && cfmm.DocRequiredId.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingRespondents.Where(z => z.DocumentType == cfmm.DocRequiredId).Count() > 0).Count() > 0).ToList();

            if (cfmm.CaseDateFrom != null && cfmm.CaseDateFrom != DateTime.MinValue && cfmm.CaseDateTo != null && cfmm.CaseDateTo != DateTime.MinValue)
                acctiveCases = acctiveCases.Where(x => x.CaseHearings.Where(y => y.CaseHearingRespondents.Where(z => z.CreatedOn >= cfmm.CaseDateFrom && z.CreatedOn <= cfmm.CaseDateTo).Count() > 0).Count() > 0).ToList();

            if (cfmm.CaseHearingStatusActive != null && cfmm.CaseHearingStatusActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.HearingStatus == Convert.ToInt32(cfmm.CaseHearingStatusActive)).ToList();

            cfmm.AcctiveCases = CaseMvcModel.CopyFromEntityList(acctiveCases, 0);
            if (cfmm.submitButton == "ExportOption")
            {
                if (cfmm.AcctiveCases != null)
                {
                    AddCaseDetails(cfmm.AcctiveCases);
                    var sb = new StringBuilder();
                    var data = from c in cfmm.AcctiveCases
                               select new
                               {
                                   CaseNo = c.PetitionNo,
                                   Petitioner = PetitionerDetail(c.Notice.Petitioners),
                                   Respondent = RespondentDetail(c.Notice.NoticeRespondents),
                                   CaseType = c.Notice.NoticeType,
                                   CaseStatus = c.CaseStatus.Name,
                                   HearingStatus = c.CaseHearings.LastOrDefault()?.HearingStatus,
                                   Region = c.Region?.Name,
                                   AdvocateAGoffice = c.Advocate?.Name,
                                   PetitonerAdvocate = c.Notice.NoticeCounselors.FirstOrDefault()?.Name,
                                   CNRNo = c.Notice.Cnr,
                                   BenchType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Bench != null ? c.Notice.CaseDetail.Bench.Name : "",
                                   CauseListType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.CauselistTypes != null ? c.Notice.CaseDetail.CauselistTypes.Name : "",
                                   District = c.Notice.CaseDetail != null && c.Notice.CaseDetail.District != null ? c.Notice.CaseDetail.District.DistrictName : "",
                                   Category = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Category != null ? c.Notice.CaseDetail.Category.CategoryName + " (" + c.Notice.CaseDetail.Category.CategoryCode + ")" : "",
                                   SubCategory = c.Notice.CaseDetail != null && c.Notice.CaseDetail.SubCategory != null ? c.Notice.CaseDetail.SubCategory.SubCategoryName + " (" + c.Notice.CaseDetail.SubCategory.SubCategoryCode + ")" : "",
                                   NoticeNumber = c.Notice.NoticeNo,
                                   FlilingDate = "No Data",
                                   LastHearingDate = c.CaseHearings.LastOrDefault()?.HearingDate?.ToString("dd/MM/yyyy"),
                                   ContextOrder = c.Notice.IsContextOrder ? "Yes" : "No"
                               };
                    var list = data.ToList();
                    var grid = new System.Web.UI.WebControls.GridView { DataSource = list };
                    grid.DataBind();
                    Response.ClearContent();

                    if (cfmm.Export == ExportType.Excel)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                        grid.RenderControl(htw);
                        Response.AddHeader("content-disposition", "attachment; filename=DocRequestedCaseReport.xls");
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.Write(sw.ToString());
                    }
                    else if (cfmm.Export == ExportType.PDF)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                        grid.RenderControl(htw);
                        StringReader reader = new StringReader(sw.ToString());
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        pdfDoc.AddHeader("Case Report", "Cases");
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                        
                        pdfDoc.Close();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment; filename=DocRequestedCaseReport.PDF");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                    }

                    Response.End();
                }
            }
            return View(cfmm);
        }

        [PageRoleAuthorizarion(PageName.CategoryDisposedCaseReport, PagePermissionType.CanView)]
        public ActionResult CategoryDisposedCase(CaseFilterMvcModel cfmm)
        {
            InitilizeCaseType(cfmm.ParentNoticeTypeActive);
            InitilizeNoticeType(cfmm.NoticeTypeActive, cfmm.ParentNoticeTypeActive);
            InitilizeCaseHearingStatus(cfmm.CaseHearingStatusActive);
            List<Case> acctiveCases = new List<Case>();
            acctiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderBy(i => i.Notice.NoticeDate).ToList();
            acctiveCases = acctiveCases.Where(x => x.CaseStatusId == CaseStatusDataService.GetLite(db).Where(y => y.Type == CaseStatusType.Dispose).FirstOrDefault().CaseStatusId).ToList();
            AddTimeLines(acctiveCases);
            if (cfmm.ParentNoticeTypeActive != null && cfmm.ParentNoticeTypeActive.ToLower() != "0" && cfmm.ParentNoticeTypeActive != "")
                acctiveCases = acctiveCases.Where(x => x.Notice.NoticeType == (ParentNoticeType)Convert.ToInt32(cfmm.ParentNoticeTypeActive)).ToList();

            if (cfmm.NoticeTypeActive != null && cfmm.NoticeTypeActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.Notice.CaseNoticeTypeId == cfmm.NoticeTypeActive).ToList();

            if (cfmm.CaseDateFrom != null && cfmm.CaseDateFrom != DateTime.MinValue && cfmm.CaseDateTo != null && cfmm.CaseDateTo != DateTime.MinValue)
                acctiveCases = acctiveCases.Where(x => x.Timelines.Where(z => z.EventType == EventType.Disposed && z.TimeOfEvent >= cfmm.CaseDateFrom && z.TimeOfEvent <= cfmm.CaseDateTo).Count() > 0).ToList();

            if (cfmm.CaseHearingStatusActive != null && cfmm.CaseHearingStatusActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.HearingStatus == Convert.ToInt32(cfmm.CaseHearingStatusActive)).ToList();

            cfmm.AcctiveCases = CaseMvcModel.CopyFromEntityList(acctiveCases, 0);

            if (cfmm.submitButton == "ExportOption")
            {
                if (cfmm.AcctiveCases != null)
                {
                    AddCaseDetails(cfmm.AcctiveCases);
                    var sb = new StringBuilder();
                    var data = from c in cfmm.AcctiveCases
                               select new
                               {
                                   CaseNo = c.PetitionNo,
                                   Petitioner = PetitionerDetail(c.Notice.Petitioners),
                                   Respondent = RespondentDetail(c.Notice.NoticeRespondents),
                                   CaseType = c.Notice.NoticeType,
                                   CaseStatus = c.CaseStatus.Name,
                                   HearingStatus = c.CaseHearings.LastOrDefault()?.HearingStatus,
                                   Region = c.Region?.Name,
                                   AdvocateAGoffice = c.Advocate?.Name,
                                   PetitonerAdvocate = c.Notice.NoticeCounselors.FirstOrDefault()?.Name,
                                   CNRNo = c.Notice.Cnr,
                                   BenchType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Bench != null ? c.Notice.CaseDetail.Bench.Name : "",
                                   CauseListType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.CauselistTypes != null ? c.Notice.CaseDetail.CauselistTypes.Name : "",
                                   Category = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Category != null ? c.Notice.CaseDetail.Category.CategoryName + " (" + c.Notice.CaseDetail.Category.CategoryCode + ")" : "",
                                   District = c.Notice.CaseDetail != null && c.Notice.CaseDetail.District != null ? c.Notice.CaseDetail.District.DistrictName : "",
                                   SubCategory = c.Notice.CaseDetail != null && c.Notice.CaseDetail.SubCategory != null ? c.Notice.CaseDetail.SubCategory.SubCategoryName + " (" + c.Notice.CaseDetail.SubCategory.SubCategoryCode + ")" : "",
                                   NoticeNumber = c.Notice.NoticeNo,
                                   FlilingDate = "No Data",
                                   DisposedDate = c.Timelines.Where(z => z.EventType == EventType.Disposed).Count() > 0 ? c.Timelines.Where(z => z.EventType == EventType.Disposed).FirstOrDefault()?.TimeOfEvent.ToString("dd/MM/yyyy") : "",
                                   ContextOrder = c.Notice.IsContextOrder ? "Yes" : "No"
                               };
                    var list = data.ToList();
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = list;
                    grid.DataBind();


                    Response.ClearContent();


                    if (cfmm.Export == ExportType.Excel)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                        grid.RenderControl(htw);
                        Response.AddHeader("content-disposition", "attachment; filename=CategoryWiseDisposedCaseReport.xls");
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.Write(sw.ToString());
                    }
                    else if (cfmm.Export == ExportType.PDF)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                        grid.RenderControl(htw);
                        StringReader reader = new StringReader(sw.ToString());
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        pdfDoc.AddHeader("Case Report", "Cases");
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                        pdfDoc.Close();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment; filename=CategoryWiseDisposedCaseReport.PDF");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                    }

                    Response.End();
                }
            }
            return View(cfmm);
        }

        [PageRoleAuthorizarion(PageName.CategoryPendingCaseReport, PagePermissionType.CanView)]
        public ActionResult CategoryPendingCase(CaseFilterMvcModel cfmm)
        {
            InitilizeCaseType(cfmm.ParentNoticeTypeActive);
            InitilizeNoticeType(cfmm.NoticeTypeActive, cfmm.ParentNoticeTypeActive);
            InitilizeCaseHearingStatus(cfmm.CaseHearingStatusActive);
            List<Case> acctiveCases = new List<Case>();
            acctiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderBy(i => i.Notice.NoticeDate).ToList();
            acctiveCases = acctiveCases.Where(x => x.CaseStatusId == CaseStatusDataService.GetLite(db).Where(y => y.Type == CaseStatusType.Pending).FirstOrDefault().CaseStatusId).ToList();
            AddTimeLines(acctiveCases);
            if (cfmm.ParentNoticeTypeActive != null && cfmm.ParentNoticeTypeActive.ToLower() != "0" && cfmm.ParentNoticeTypeActive != "")
                acctiveCases = acctiveCases.Where(x => x.Notice.NoticeType == (ParentNoticeType)Convert.ToInt32(cfmm.ParentNoticeTypeActive)).ToList();

            if (cfmm.NoticeTypeActive != null && cfmm.NoticeTypeActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.Notice.CaseNoticeTypeId == cfmm.NoticeTypeActive).ToList();

            if (cfmm.CaseDateFrom != null && cfmm.CaseDateFrom != DateTime.MinValue && cfmm.CaseDateTo != null && cfmm.CaseDateTo != DateTime.MinValue)
                acctiveCases = acctiveCases.Where(x => x.Timelines.Where(z => z.EventType == EventType.Pending && z.TimeOfEvent >= cfmm.CaseDateFrom && z.TimeOfEvent <= cfmm.CaseDateTo).Count() > 0).ToList();

            if (cfmm.CaseHearingStatusActive != null && cfmm.CaseHearingStatusActive.ToString() != "")
                acctiveCases = acctiveCases.Where(x => x.HearingStatus == Convert.ToInt32(cfmm.CaseHearingStatusActive)).ToList();

            cfmm.AcctiveCases = CaseMvcModel.CopyFromEntityList(acctiveCases, 0);
            if (cfmm.submitButton == "ExportOption")
            {
                if (cfmm.AcctiveCases != null)
                {
                    AddCaseDetails(cfmm.AcctiveCases);
                    var sb = new StringBuilder();
                    var data = from c in cfmm.AcctiveCases
                               select new
                               { 
                                   CaseNo = c.PetitionNo,
                                   Petitioner = PetitionerDetail(c.Notice.Petitioners),
                                   Respondent = RespondentDetail(c.Notice.NoticeRespondents),
                                   CaseType = c.Notice.NoticeType,
                                   CaseStatus = c.CaseStatus.Name,
                                   HearingStatus = c.CaseHearings.LastOrDefault()?.HearingStatus,
                                   Region = c.Region?.Name,
                                   AdvocateAGoffice = c.Advocate?.Name,
                                   PetitonerAdvocate = c.Notice.NoticeCounselors.FirstOrDefault()?.Name,
                                   CNRNo = c.Notice.Cnr,
                                   BenchType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Bench != null ? c.Notice.CaseDetail.Bench.Name : "",
                                   CauseListType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.CauselistTypes != null ? c.Notice.CaseDetail.CauselistTypes.Name : "",
                                   District = c.Notice.CaseDetail != null && c.Notice.CaseDetail.District != null ? c.Notice.CaseDetail.District.DistrictName : "",
                                   Category = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Category != null ? c.Notice.CaseDetail.Category.CategoryName + " (" + c.Notice.CaseDetail.Category.CategoryCode + ")" : "",
                                   SubCategory = c.Notice.CaseDetail != null && c.Notice.CaseDetail.SubCategory != null ? c.Notice.CaseDetail.SubCategory.SubCategoryName + " (" + c.Notice.CaseDetail.SubCategory.SubCategoryCode + ")" : "",
                                   NoticeNumber = c.Notice.NoticeNo,
                                   FlilingDate = "No Data",
                                   LastHearingDate = c.CaseHearings.LastOrDefault()?.HearingDate?.ToString("dd/MM/yyyy"),
                                   ContextOrder = c.Notice.IsContextOrder ? "Yes" : "No"
                               };
                    var list = data.ToList();
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = list;
                    grid.DataBind();


                    Response.ClearContent();



                    if (cfmm.Export == ExportType.Excel)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                        grid.RenderControl(htw);
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.xls");
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.Write(sw.ToString());
                    }
                    else if (cfmm.Export == ExportType.PDF)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                        grid.RenderControl(htw);
                        StringReader reader = new StringReader(sw.ToString());
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        pdfDoc.AddHeader("Case Report", "Cases");
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                        pdfDoc.Close();
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.PDF");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                    }

                    Response.End();
                }
            }
            return View(cfmm);
        }

        [PageRoleAuthorizarion(PageName.FinalHearingCaseReport, PagePermissionType.CanView)]
        public ActionResult FinalHearingCase(CaseFilterMvcModel cfmm)
        {
            List<Case> ActiveCases = new List<Case>();
            ActiveCases = CaseDataService.GetFiltered(db, AppUser.UserId, AppUser.User.UserRole.UserSystemRoleId, AppUser.TempRegionId, AppUser.User.GovernmentDepartmentId, AppUser.User.DesignationId, AppUser.User.GovermentDepartmentOICID, "DB").OrderByDescending(i => i.CreatedOn).ToList();

            ActiveCases = ActiveCases.Where(x => x.CaseStatusId == CaseStatusDataService.GetLite(db).Where(y => y.Type == CaseStatusType.Pending).FirstOrDefault().CaseStatusId &&
                                                 x.CaseHearings.Where(y => y.HearingType == CaseHearingType.FinalOrder).Count() > 0).ToList();

            if (cfmm.CaseDateFrom != null && cfmm.CaseDateFrom != DateTime.MinValue && cfmm.CaseDateTo != null && cfmm.CaseDateTo != DateTime.MinValue)
                ActiveCases = ActiveCases.Where(x => x.CaseHearings.LastOrDefault().HearingDate >= cfmm.CaseDateFrom && x.CaseHearings.LastOrDefault().HearingDate <= cfmm.CaseDateTo).ToList();

            if (cfmm.CasePetitionerNameActive != null && cfmm.CasePetitionerNameActive != "")
                ActiveCases = ActiveCases.Where(x => x.Notice.Petitioners.Where(y => y.Name.ToLower().Contains(cfmm.CasePetitionerNameActive.ToLower())).Count() > 0).ToList();

            if (cfmm.CaseNoActive != null && cfmm.CaseNoActive != "")
                ActiveCases = ActiveCases.Where(x => x.PetitionNo.ToLower().Contains(cfmm.CasePetitionerNameActive.ToLower())).ToList();
            AddTimeLines(ActiveCases);
            cfmm.AcctiveCases = CaseMvcModel.CopyFromEntityList(ActiveCases, 0);

            if (cfmm.submitButton == "ExportOption")
            {
                if (cfmm.AcctiveCases != null)
                {
                    AddCaseDetails(cfmm.AcctiveCases);
                    var sb = new StringBuilder();
                    var data = from c in cfmm.AcctiveCases
                               select new
                               {
                                   CaseNo = c.PetitionNo,
                                   Petitioner = PetitionerDetail(c.Notice.Petitioners),
                                   Respondent = RespondentDetail(c.Notice.NoticeRespondents),
                                   CaseType = c.Notice.NoticeType,
                                   CaseStatus = c.CaseStatus.Name,
                                   HearingStatus = c.CaseHearings.LastOrDefault()?.HearingStatus,
                                   Region = c.Region?.Name,
                                   AdvocateAGoffice = c.Advocate?.Name,
                                   PetitonerAdvocate = c.Notice.NoticeCounselors.FirstOrDefault()?.Name,
                                   CNRNo = c.Notice.Cnr,
                                   BenchType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Bench != null ? c.Notice.CaseDetail.Bench.Name : "",
                                   CauseListType = c.Notice.CaseDetail != null && c.Notice.CaseDetail.CauselistTypes != null ? c.Notice.CaseDetail.CauselistTypes.Name : "",
                                   District = c.Notice.CaseDetail != null && c.Notice.CaseDetail.District != null ? c.Notice.CaseDetail.District.DistrictName : "",
                                   Category = c.Notice.CaseDetail != null && c.Notice.CaseDetail.Category != null ? c.Notice.CaseDetail.Category.CategoryName + " (" + c.Notice.CaseDetail.Category.CategoryCode + ")" : "",
                                   SubCategory = c.Notice.CaseDetail != null && c.Notice.CaseDetail.SubCategory != null ? c.Notice.CaseDetail.SubCategory.SubCategoryName + " (" + c.Notice.CaseDetail.SubCategory.SubCategoryCode + ")" : "",
                                   NoticeNumber = c.Notice.NoticeNo,
                                   FlilingDate = "No Data",
                                   DisposedDate = c.Timelines.Where(z => z.EventType == EventType.Disposed).Count() > 0 ? c.Timelines.Where(z => z.EventType == EventType.Disposed).FirstOrDefault()?.TimeOfEvent.ToString("dd/MM/yyyy") : "",
                                   ContextOrder = c.Notice.IsContextOrder ? "Yes" : "No"
                               };
                    var list = data.ToList();
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = list;
                    grid.DataBind();
                    Response.ClearContent();
                    if (cfmm.Export == ExportType.Excel)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "20");
                        grid.RenderControl(htw);
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.xls");
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.Write(sw.ToString());
                    }
                    else if (cfmm.Export == ExportType.PDF)
                    {
                        StringWriter sw = new StringWriter();
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        grid.Style.Add(System.Web.UI.HtmlTextWriterStyle.FontSize, "8");
                        grid.RenderControl(htw);
                        StringReader reader = new StringReader(sw.ToString());
                        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                        pdfDoc.Open();
                        pdfDoc.AddHeader("Case Report", "Cases");
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, reader);
                        pdfDoc.Close();

                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment; filename=CaseReport.PDF");
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDoc);
                    }

                    Response.End();
                }
            }
            return View(cfmm);
        }
    }
}