﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class DistrictDataService
    {
        public static IQueryable<District> GetLite(DatabaseContext db)
        {
            return db.Districts.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<District> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}