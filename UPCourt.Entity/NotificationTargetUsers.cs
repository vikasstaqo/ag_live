﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(EntityName), UserNameProp: nameof(EntityName))]
    public class NotificationTargetUsers
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public bool IsActive { get; set; }
    }
}
