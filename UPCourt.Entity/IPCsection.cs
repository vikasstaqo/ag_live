﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(SectionName), UserNameProp: nameof(CreatedBy))]
    public class IPCSection : BaseMasterEntity
    {
        public IPCSection() { }
        public Guid IPCSectionId { get; set; }
        public string SectionName { get; set; }
        public string Description { get; set; }
        public IPCSectionStatus Status { get; set; }

    }

    public enum IPCSectionStatus
    {
        Active = 1,
        Inactive = 0
    }

}
