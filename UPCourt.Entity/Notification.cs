﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Message), UserNameProp: nameof(ModifyBy))]
    public class Notification
    {
        public System.Guid NotificationId { get; set; }
        public System.Guid SourceEntityId { get; set; }
        public int SourceEntityType { get; set; }
        public Guid? TargetEntityId { get; set; }
        public Guid TargetEntityType { get; set; }
        public NotificationStatus Status { get; set; }
        public string Message { get; set; }
        public string ModifyBy { get; set; }
        public string CreationDate { get; set; }
        public int NotificationType { get; set; }
    }

    public enum NotificationSourceEntityType
    {
        General = 1,
        Case,
        Notice
    }
    public enum NotificationTargetEntityType
    {
        AG = 0,
        AIG = 1,
        AGDepartment = 2
    }
    public enum NotificationStatus
    {
        Created = 1,
        Delivered = 2,
        Read = 3
    }
}
