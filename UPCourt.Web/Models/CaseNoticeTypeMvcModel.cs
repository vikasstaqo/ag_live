﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    

    public class CaseNoticeTypeMvcModel : BaseEntityModel<CaseNoticeType, CaseNoticeTypeMvcModel>
    {
        public Guid CaseNoticeTypeId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        public string type_name { get; set; }
        public ParentNoticeType ParentNoticeType { get; set; }
    }





}