﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class CaseNoticeType : BaseMasterEntity
    {
        public Guid CaseNoticeTypeId { get; set; }
        public string Name { get; set; }
        public int? case_type { get; set; }
        public string type_name { get; set; }
        public ParentNoticeType ParentNoticeType { get; set; }
        public bool IsActive { get; set; }
    }

    public enum ParentNoticeType
    {
        [PatentNoticeTypeAttribute("Civil")]
        Civil = 1,
        [PatentNoticeTypeAttribute("Criminal")]
        Criminal = 2,
    }


    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class PatentNoticeTypeAttribute : Attribute
    {
        public PatentNoticeTypeAttribute(string DisplayText)
        {
            _DisplayText = DisplayText;

        }
        string _DisplayText;
        public string DisplayText => _DisplayText;

    }
}
