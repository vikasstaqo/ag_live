﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class AGDepartmentDataService
    {
        public static IQueryable<AGDepartment> GetLite(DatabaseContext db)
        {
            return db.AGDepartments.Where(x=> x.IsDeleted==false);
        }
    }
}