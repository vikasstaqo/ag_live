﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UPCourt.Entity;
using UPCourt.Web.Models;

namespace UPCourt.Web.Core
{
    [AppAuthorizarion]
    public class BaseMVCController : Controller
    {

        public DatabaseContext db = new DatabaseContext();

        protected BaseResultModel rModel = new BaseResultModel();


        public JsonResult RModelResult()
        {
            return Json(rModel, JsonRequestBehavior.AllowGet);
        }

        protected void AddTimeLines(List<Case> acctiveCases)
        {
            var allTimeLines = Service.DataService.TimelineDataService.GetLite(db).Where(x => x.EventType == EventType.Disposed || x.EventType == EventType.Pending).ToList();

            foreach (var c in acctiveCases)
            {
                c.Timelines = allTimeLines.Where(x => x.CaseId == c.CaseId).ToList();
            }
        }

        protected void AddCaseDetails(ICollection<CaseMvcModel> FilterCases)
        {
            //Get all Masters used below
            var allCasedetails = db.Casedetails.ToList();
            var allBenchType = db.BenchType.ToList();
            var allCategorys = db.Categorys.ToList();
            var allSubCategorys = db.SubCategorys.ToList();
            var allCauseListTypes = db.CauseListTypes.ToList();
            var allDistricts = db.Districts.ToList();

            //attach Details based on cases
            foreach (var fc in FilterCases)
            {
                fc.Notice.CaseDetail = CaseDetailsMvcModel.CopyFromEntity(allCasedetails.Where(x => x.NoticeId == fc.NoticeId).FirstOrDefault(), 0);
                if (fc.Notice.CaseDetail != null)
                {
                    fc.Notice.CaseDetail.Bench = BenchTypeMvcModel.CopyFromEntity(allBenchType.Where(x => x.bench_type_code == fc.Notice.CaseDetail.BenchType).FirstOrDefault(), 0);
                    fc.Notice.CaseDetail.Category = CategoryMvcModel.CopyFromEntity(allCategorys.Where(x => x.CategoryCode == fc.Notice.CaseDetail.C_subject).FirstOrDefault(), 0);
                    fc.Notice.CaseDetail.SubCategory = SubCategoryMvcModel.CopyFromEntity(allSubCategorys.Where(x => x.CategoryCode == fc.Notice.CaseDetail.C_subject && x.SubCategoryCode == fc.Notice.CaseDetail.Cs_subject).FirstOrDefault(), 0);
                    fc.Notice.CaseDetail.CauselistTypes = CauseListTypeMvcModel.CopyFromEntity(allCauseListTypes.Where(x => x.Id == fc.Notice.CaseDetail.CauselistType).FirstOrDefault(), 0);
                    fc.Notice.CaseDetail.District = DistrictMvcModel.CopyFromEntity(allDistricts.Where(x => x.DistCode == fc.Notice.CaseDetail.CaseDistCode).FirstOrDefault(), 0);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public string SucessMessage { set { TempData[nameof(SucessMessage)] = value; } get { var value = TempData[nameof(SucessMessage)]; return value == null ? null : value.ToString(); } }

        public string CreatedMessage = ConfigurationManager.AppSettings["CreatedMessage"];
        public string UpdatedMessage = ConfigurationManager.AppSettings["UpdatedMessage"];
        public string DeletedMessage = ConfigurationManager.AppSettings["DeletedMessage"];

        private ApplicationUser _AppUser;
        public ApplicationUser AppUser
        {
            get { if (_AppUser == null) { _AppUser = GetAppUser(User); } return _AppUser; }
        }


        public static ApplicationUser GetAppUser(IPrincipal user)
        {
            return ApplicationUser.GetUserFromIdentity(user);
        }

        public ActionResult GetDefaultActionByUser(UserType UserType)
        {
            //TODO: create three dashboard for 3 usertype
            switch (UserType)
            {
                case UserType.AG:
                    return RedirectToAction("Index", "Home");
                default:
                    return RedirectToAction("Index", "Home");

            }
        }
        public ActionResult GetDefaultLoginActionByUser(UserType UserType)
        {
            switch (UserType)
            {
                case UserType.AG:
                    return RedirectToAction("Index", "Login");
                default:
                    return RedirectToAction("Index", "Login");

            }
        }
        protected bool IsValidPermission(BaseResultModel rModel, PageName Page, PagePermissionType[] Permisions)
        {
            foreach (var Permision in Permisions)
            {
                //If any one permission satified then all good
                if (IsValidPermission(rModel, Page, Permisions))
                {
                    return true;
                }
            }

            return false;
        }
        protected bool IsValidPermission(BaseResultModel rModel, PageName Page, PagePermissionType Permision)
        {
            User user = GetUser();
            var pagePermission = user.UserRole.UserRolePagePermissions.FirstOrDefault(i => i.PagePermission.PageName == Page);

            if (pagePermission == null || pagePermission.PermittedPermissionTypes == null || !pagePermission.PermittedPermissionTypes.Contains(Permision))
            {
                rModel.Message = "You don't have permission to perform this opearation";
                return false;
            }
            return true;
        }
        protected List<Document> FileUploadCommon(BaseResultModel rModel, IEnumerable<Document> dbCurrentDocuments,
            IEnumerable<DocumentMvcModel> modelDocuments, int MinFileCount, int MaxFileCount,
            DocumentFileType FileType, float? MaxFileSizeInMB = null)
        {
            return this.FileUploadCommon(rModel, dbCurrentDocuments, modelDocuments, MinFileCount, MaxFileCount, new[] { FileType }, MaxFileSizeInMB);
        }
        protected List<Document> FileUploadCommon(BaseResultModel rModel, IEnumerable<Document> dbCurrentDocuments,
            IEnumerable<DocumentMvcModel> modelDocuments, int? MinFileCount, int MaxFileCount,
            DocumentFileType[] FileType, float? MaxFileSizeInMB = null)
        {

            var httpRequest = System.Web.HttpContext.Current.Request;

            if (httpRequest.Files.Count < MinFileCount)
            {
                rModel.Message = "No file uploaded";
                return null;
            }

            if (httpRequest.Files.Count > MaxFileCount)
            {
                rModel.Message = string.Format("File max limit {0} exceeds", MaxFileCount);
                return null;
            }

            //if min count is 0 and no file defination present in model
            if (MinFileCount == 0 && modelDocuments.IsEmptyCollection())
            {
                rModel.IsSuccess = true;
                return new List<Document>();
            }
            List<Document> rDocList = new List<Document>();
            var nextOrdinal = dbCurrentDocuments.Select(i => i.Ordinal).DefaultIfEmpty().Max() + 1;

            List<HttpPostedFile> allHttpFiles = new List<HttpPostedFile>(httpRequest.Files.Count);

            for (int count = 0; count < httpRequest.Files.Count; count++)
            {
                var file = httpRequest.Files[count];
                if (file.FileName.IsNotNullOrEmpty())
                    allHttpFiles.Add(file);
            }

            foreach (var httpFile in allHttpFiles)
            {
                var result = FileUploadWebApiHelper.UploadFileIfValid(rModel, httpFile, FileType, MaxFileSizeInMB);
                if (!rModel.Message.IsNullOrEmpty())
                {
                    return null;
                }
                result.Ordinal = nextOrdinal++;
                rDocList.Add(result);
            }

            foreach (var fromModel in modelDocuments)
            {
                var httpFile = allHttpFiles.FirstOrDefault(i => i.FileName == fromModel.Name && (i.ContentLength == 0 || i.ContentLength == fromModel.Size));
                if (fromModel == null)
                {
                    rModel.Message = fromModel.Name + " does not exists in uploaded files";
                    return null;
                }

                if (fromModel.DocumentId != null)
                {
                    var dbTempDocument = dbCurrentDocuments.FirstOrDefault(i => i.DocumentId == fromModel.DocumentId);
                    if (dbTempDocument == null)
                    {
                        rModel.Message = dbTempDocument.Name + " does not exists in records";
                        return null;
                    }

                    //while file can not be edited so continue no futher processing with existing files
                    continue;
                }


                var result = FileUploadWebApiHelper.UploadFileIfValid(rModel, httpFile, FileType, MaxFileSizeInMB);
                result.Ordinal = nextOrdinal++;

               
                if (!rModel.Message.IsNullOrEmpty())
                {
                    break;
                }
                rDocList.Add(result);
            }


            if (rModel.Errors.Count.IsZero() && rModel.Message.IsNullOrEmpty())
            {
                rModel.IsSuccess = true;
            }

            if (!rModel.IsSuccess)
            {
                foreach (var item in rDocList)
                {
                    //if the image was pre existing then delete                        
                    FileUploadWebApiHelper.DeleteIfFileExists(item.DocumentUrl);
                }
                return null;
            }

            //mark removed files
            var removedDocList = dbCurrentDocuments.Where(i => !modelDocuments.Any(d => d.DocumentId == i.DocumentId));
            foreach (var item in removedDocList)
            {
                item.Status = DocumentStatus.Removed;
            }
            //this are new files
            return rDocList;

        }


        public bool IsAccessAuthorised(PageName Page, PagePermissionType Permision)
        {
            var pagePermission = GetUser().UserRole.UserRolePagePermissions.FirstOrDefault(i => i.PagePermission.PageName == Page);
            if (pagePermission == null || !pagePermission.PermittedPermissionTypes.Contains(Permision))
                return false;
            return true;
        }
        public User GetUser()
        {
            return Service.DataService.UserDataService.GetDetail(db).FirstOrDefault(F => F.UserId == AppUser.UserId);
        }
        public bool HasUserAccess()
        {
            User user = GetUser();
            string CurrentController = UIHelper.GetControllerName();
            string CurrentAction = UIHelper.GetActionName();

            var PagePermission = user.UserRole.UserRolePagePermissions.ToList().FirstOrDefault(F =>
                F.PagePermission.PageName.GetCustomAttribute<PagePermissionAttribute>().ControllerName == CurrentController
            );
            if (PagePermission.IsNull())
                return false;
            var AllowedAction = PagePermission.PermittedPermissionTypes.ToList().FirstOrDefault(F => F.GetCustomAttribute<PagePermissionTypeAttribute>().ActionName == CurrentAction);
            if (AllowedAction.IsNull())
                return false;

            return true;
        }
        public void DisplayMessage(MessageType Type, string Message, string Title = "", string SubTitle = "")
        {
            List<ToastMessage> ToastMessages;
            if (TempData["SystemMessage"] == null)
                ToastMessages = new List<ToastMessage>();
            else
                ToastMessages = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ToastMessage>>(TempData["SystemMessage"].ToString());

            ToastMessage ToastMessage = new ToastMessage(Type, Message, Title, SubTitle);
            ToastMessages.Add(ToastMessage);
            TempData["SystemMessage"] = Newtonsoft.Json.JsonConvert.SerializeObject(ToastMessages, Newtonsoft.Json.Formatting.None);
        }
    }
    public class AppAuthorizarionAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            BaseMVCController controller = filterContext.Controller as BaseMVCController;
            if (controller.AppUser != null)
            {
                filterContext.Result = controller.GetDefaultActionByUser(controller.AppUser.UserType);
            }
            else
            {
                filterContext.Result = controller.GetDefaultLoginActionByUser(UserType.AG);
            }
        }
    }

    public class PageRoleAuthorizarionAttribute : AuthorizeAttribute
    {
        public PageName PageName { get; set; }
        public IEnumerable<PagePermissionType> PagePermissions { get; set; }
        public PageRoleAuthorizarionAttribute(PageName pageName, params PagePermissionType[] pagePermissions)
        {
            this.PageName = pageName;
            this.PagePermissions = pagePermissions;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var userInfo = BaseMVCController.GetAppUser(filterContext.HttpContext.User);

            if (userInfo == null)
            {
                filterContext.Result = (ActionResult)new RedirectToRouteResult(new RouteValueDictionary() { { "Controller", (object)"Login" }, { "Action", (object)"Index" } });
                return;
            }
            var db = (filterContext.Controller as BaseMVCController).db;
            var dbUSerInfo = Service.DataService.UserDataService.GetDetail(db).FirstOrDefault(i => i.UserId == userInfo.UserId);
            //User not found
            if (dbUSerInfo != null)
            {
                var UserpagePermission = userInfo.User.UserRole.UserRolePagePermissions.FirstOrDefault(i => i.PagePermission.PageName == PageName);
                if (UserpagePermission != null && UserpagePermission.PermittedPermissionTypes != null)
                {
                    bool IsAllowed = false;
                    foreach (var UserPermittedPermissionTypes in UserpagePermission.PermittedPermissionTypes.ToList())
                    {
                        foreach (var CalledPermissionTypes in PagePermissions.ToList())
                        {
                            if (UserPermittedPermissionTypes == CalledPermissionTypes)
                            {
                                IsAllowed = true; break;
                            }
                        }
                    }
                    if (!IsAllowed)
                    {
                        filterContext.Result = (ActionResult)new RedirectToRouteResult(new RouteValueDictionary()
                        {
                            { "Controller", (object)"DashBoard" }, { "Action", (object)"UnuthorizedAccess" }
                        });
                    }
                }
                else
                {
                    filterContext.Result = (ActionResult)new RedirectToRouteResult(new RouteValueDictionary()
                        {
                            { "Controller", (object)"DashBoard" }, { "Action", (object)"UnuthorizedAccess" }
                        });
                }
            }
        }
    }

    public class ToastMessage
    {
        public ToastMessage(MessageType Type, string Message, string Title = "", string SubTitle = "")
        {
            this.Type = Type;
            this.Message = Message;
            this.Title = Title.IsNullOrEmpty() ? GetTitle(Type) : Title;
            this.SubTitle = SubTitle.IsNullOrEmpty() ? GetSubTitle(Type) : SubTitle;
        }
        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public MessageType Type { get; private set; }
        public string MessageType { get { return Type.ToString(); } }
        public string Message { get; private set; }
        public string Title { get; private set; }
        public string SubTitle { get; private set; }
        private string GetTitle(MessageType Type)
        {
            string Title = string.Empty;
            switch (Type)
            {
                case Core.MessageType.Success:
                    Title = "Successful Operation";
                    break;
                case Core.MessageType.Information:
                    Title = "Information Message";
                    break;
                case Core.MessageType.Warning:
                    Title = "Warning Message!!!";
                    break;
                case Core.MessageType.Error:
                    Title = "Error Occurred";
                    break;
            }
            return Title;
        }
        private string GetSubTitle(MessageType Type)
        {
            string SubTitle = string.Empty;
            switch (Type)
            {
                case Core.MessageType.Success:
                    SubTitle = "Performed successfully";
                    break;
                case Core.MessageType.Information:
                    SubTitle = "Just as information";
                    break;
                case Core.MessageType.Warning:
                    SubTitle = "This is to concern!!!";
                    break;
                case Core.MessageType.Error:
                    SubTitle = "Error!!! please check";
                    break;
            }
            return SubTitle;
        }
    }
    [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum MessageType
    {
        [System.Runtime.Serialization.EnumMember(Value = "bg-success")]
        Success,
        [System.Runtime.Serialization.EnumMember(Value = "bg-info")]
        Information,
        [System.Runtime.Serialization.EnumMember(Value = "bg-warning")]
        Warning,
        [System.Runtime.Serialization.EnumMember(Value = "bg-danger")]
        Error
    }
}