﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class GovernmentDepartmentDCMvcModel : BaseEntityModel<GovernmentDepartmentDC, GovernmentDepartmentDCMvcModel>
    {
        public Guid GovernmentDepartmentDCId { get; set; }

        public GovernmentDeparmentType DepartmentType { get; set; }
        [MaxLength(50)]
        [Display(Name = "Department Short Name (Max 50 chars)")]
        public string DepartmentShortName { get; set; }
        [MaxLength(200)]
        [Display(Name = "Department Full Name (Max 200 chars)")]
        public string DepartmentFullName { get; set; }
        [MaxLength(200)]
        [Display(Name = "Address Line 1 (Max 200 chars)")]
        public string Address1 { get; set; }
  
        [Display(Name = "Address Line 2 (Max 200 chars)")]
        public string Address2 { get; set; }
        [MaxLength(100)]
        [Display(Name = "City/Village (Max 100 chars)")]
        public string City { get; set; }
        [MaxLength(200)]

        public string District { get; set; }
        [Display(Name = "Other District (Max 200 chars)")]
        [MaxLength(200)]
        public string OtherDistrict { get; set; }

        public string Division { get; set; }

        public string Tehsil { get; set; }

        [Display(Name = "Block Name")]
        public string GovBlk { get; set; }
        public List<GovernmentDepartmentDCContactMvcModel> GovernmentDepartmentDCContacts { get; set; }
        public override GovernmentDepartmentDCMvcModel EntityToResponse(GovernmentDepartmentDC entity, GovernmentDepartmentDCMvcModel model)
        {
            model.GovernmentDepartmentDCContacts = GovernmentDepartmentDCContactMvcModel.CopyFromEntityList(entity.GovernmentDepartmentDCContacts, model.RecursiveLevel);
            return model;
        }
    }

    public class GovernmentDepartmentDCContactMvcModel : BaseEntityModel<GovernmentDepartmentDCContact, GovernmentDepartmentDCContactMvcModel>
    {
        public Guid GovernmentDepartmentDCContactId { get; set; }
        public Guid GovernmentDepartmentDCId { get; set; }
        public int Sequence { get; set; }
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        [RegularExpression(RegularExpressionHelper.EmailValidationExpression, ErrorMessage = "Entered Email format is not valid.")]
        public string Email { get; set; }
        [RegularExpression(RegularExpressionHelper.PhoneNoValidationExpression, ErrorMessage = "Entered phone format is not valid.")]
        public string Mobile { get; set; }
       public string Phone { get; set; }
        public bool bActive { get; set; }
    }

}