﻿namespace UPCourt.Web.Service.DataService
{
    using System;
    using System.Linq;
    using UPCourt.Entity;

    public class SubordinateCourtDataService
    {
        public static IQueryable<SubordinateCourt> GetLite(DatabaseContext db)
        {
            return db.SubordinateCourts;
        }
    }
}