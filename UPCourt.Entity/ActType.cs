﻿using System;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ActBelongName), UserNameProp: nameof(CreatedBy))]
    public class ActType : BaseMasterEntity
    {
        public Guid ActTypeID { get; set; }
        public string ActBelongName { get; set; }
        public bool IsActive { get; set; }
        public ParentActBelongType ParentActBelongType { get; set; }
    }

    public enum ParentActBelongType
    {
        [ParentActBelongType("Criminal Act")]
        CriminalAct = 1,
        [ParentActBelongType("Civil Act")]
        CivilAct = 2
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class ParentActBelongTypeAttribute : Attribute
    {
        public ParentActBelongTypeAttribute(string DisplayText)
        {
            _DisplayText = DisplayText;

        }
        string _DisplayText;
        public string DisplayText => _DisplayText;

    }
}
