﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class IPCSectionController : BaseMVCController
    {
        // GET: IPCSection
        public ActionResult Index()
        {
            return View(IPCSectionMvcModel.CopyFromEntityList(IPCSectionDataService.GetDetail(db).OrderByDescending(i => i.DisplayOrder), 0));
        }
        public ActionResult Create()
        {
            var model = new IPCSectionMvcModel();
            model.Status = IPCSectionStatus.Active;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(IPCSectionMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            IPCSection tempData = new IPCSection()
            {
                IPCSectionId = Guid.NewGuid(),
                SectionName = model.SectionName,
                Description = model.Description,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetIPSectionDisplayOrder(db)

            };
            db.IPCSections.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var dbData = IPCSectionDataService.GetDetail(db).Where(i => i.IPCSectionId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(IPCSectionMvcModel.CopyFromEntity(dbData, 0));
        }
        [HttpPost]

        public ActionResult Edit(IPCSectionMvcModel model)
        {
            if (!ModelState.IsValid) { return View(model); }
            var dbData = IPCSectionDataService.GetDetail(db).Where(i => i.IPCSectionId == model.IPCSectionId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            dbData.SectionName = model.SectionName;
            dbData.Description = model.Description;
            dbData.Status = model.Status;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }

        [PageRoleAuthorizarion(PageName.IPCSection, PagePermissionType.CanView)]
        public ActionResult Details(Guid id)
        {
            var dbData = IPCSectionDataService.GetLite(db).Where(i => i.IPCSectionId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(IPCSectionMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(IPCSectionMvcModel model)
        {
            var duplicateCheck = IPCSectionDataService.GetLite(db).FirstOrDefault(i => i.IPCSectionId != model.IPCSectionId && i.SectionName == model.SectionName);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.SectionName), $"{model.SectionName} already exists");
                return false;
            }
            return true;
        }
        [PageRoleAuthorizarion(PageName.IPCSection, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = IPCSectionDataService.GetLite(db).Where(i => i.IPCSectionId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
