﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PurposeName), UserNameProp: nameof(ModifiedBy))]
    public class Purpose : BaseMasterEntity

    {
        public Guid PurposeId { get; set; }
        public int Purposecode { get; set; }
        public string PurposeName { get; set; }
        public PurposeStatus Status { get; set; }
    }
    public enum PurposeStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DispTypeName), UserNameProp: nameof(ModifiedBy))]
    public class DisposalType : BaseMasterEntity
    {
        public Guid DisposalTypeId { get; set; }
        public int DispType { get; set; }
        public string DispTypeName { get; set; }
        public DisposalTypeStatus Status { get; set; }
    }
    public enum DisposalTypeStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifiedBy))]
    public class CauseListType : BaseMasterEntity
    {
        public Guid CauseListTypeId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public CauseListTypeStatus Status { get; set; }
    }
    public enum CauseListTypeStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CategoryName), UserNameProp: nameof(ModifiedBy))]
    public class Category : BaseMasterEntity

    {
        public Guid CategoryId { get; set; }
        public int CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public CategoryStatus Status { get; set; }
    }
    public enum CategoryStatus
    {
        Active = 1,
        Inactive = 0
    }


    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(SubCategoryName), UserNameProp: nameof(ModifiedBy))]
    public class SubCategory : BaseMasterEntity

    {
        public Guid SubCategoryId { get; set; }
        public int SubCategoryCode { get; set; }
        public string SubCategoryName { get; set; }
        public int CategoryCode { get; set; }
        public SubCategoryStatus Status { get; set; }
    }
    public enum SubCategoryStatus
    {
        Active = 1,
        Inactive = 0
    }


    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ForBenchId), UserNameProp: nameof(ModifiedBy))]
    public class Benche : BaseMasterEntity
    {
        public Guid BencheId { get; set; }
        public int ForBenchId { get; set; }
        public int CourtNo { get; set; }
        public string RoomNo { get; set; }
        public BencheStatus Status { get; set; }
    }
    public enum BencheStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ForBenchId), UserNameProp: nameof(ModifiedBy))]
    public class BencheJudge : BaseMasterEntity
    {
        public Guid BencheJudgeId { get; set; }
        public int ForBenchId { get; set; }
        public string JudgeName { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeId), UserNameProp: nameof(CreatedBy))]
    public class Casedetail : BaseMasterEntity
    {
        public Guid CaseDetailId { get; set; }
        public Guid NoticeId { get; set; }
        public Notice Notice { get; set; }
        public string PetName { get; set; }
        public int BenchType { get; set; }
        public int Cs_subject { get; set; }
        public int C_subject { get; set; }
        public int DispNature { get; set; }
        public int PurposeNext { get; set; }
        public string JudgeCode { get; set; }
        public DateTime? DateOfDecision { get; set; }
        public string ResName { get; set; }
        public int CaseStateCode { get; set; }
        public int CaseDistCode { get; set; }
        public string Case_no { get; set; }
        public string Short_order { get; set; }
        public int CauselistType { get; set; }
        public int BranchId { get; set; }
        public int? pet_adv_cd { get; set; }
        public int? res_adv_cd { get; set; }

        public string fir_no { get; set; }
        public int? fir_year { get; set; }
        public string dist_name { get; set; }
        public string police_st_name { get; set; }

        public int Status { get; set; }
        public ICollection<CasedetailIa> IAs { get; set; }
        public ICollection<CasedetailPet> Petitioners { get; set; }
        public ICollection<CasedetailResp> Respondents { get; set; }
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CaseDetailId), UserNameProp: nameof(CreatedBy))]
    public class CasedetailIa : BaseMasterEntity
    {
        public Guid CaseDetailIAId { get; set; }
        public Guid CaseDetailId { get; set; }
        public int ia_regno { get; set; }
        public DateTime? date_of_ia_registration { get; set; }
        public DateTime? date_of_hearing { get; set; }
        public int court_no { get; set; }
        public string judge_code { get; set; }
        public int ia_regyear { get; set; }
        public DateTime? date_of_order { get; set; }
        public int Status { get; set; }

    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CaseDetailId), UserNameProp: nameof(CreatedBy))]
    public class CasedetailPet : BaseMasterEntity
    {
        public Guid CaseDetailPetId { get; set; }
        public Guid CaseDetailId { get; set; }
        public int party_no { get; set; }
        public string PetName { get; set; }
        public int Status { get; set; }

    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CaseDetailId), UserNameProp: nameof(CreatedBy))]
    public class CasedetailResp : BaseMasterEntity
    {
        public Guid CaseDetailRespId { get; set; }
        public Guid CaseDetailId { get; set; }
        public int party_no { get; set; }
        public string RespName { get; set; }
        public int Status { get; set; }

    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CauseListId), UserNameProp: nameof(CreatedBy))]
    public class CauseList : BaseMasterEntity
    {
        public Guid CauseListId { get; set; }
        public Guid CauseListTypeId { get; set; }
        public string PetName { get; set; }
        public int BenchId { get; set; }
        public string cause_list_eliminate { get; set; }
        public string elimination { get; set; }
        public int CauseList_Id { get; set; }
        public string ResName { get; set; }
        public string CauseListStatus { get; set; }
        public int causelist_sr_no { get; set; }
        public string Case_no { get; set; }
        public string published { get; set; }
        public DateTime CauseListDate { get; set; }
        public int CauseList_Type { get; set; }
        public int Court_no { get; set; }
        public int for_bench_id { get; set; }
        public CauseListStatus Status { get; set; }
    }

    public enum CauseListStatus
    {
        Active = 1,
        Inactive = 0
    }

    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(DateWiseCauseListId), UserNameProp: nameof(CreatedBy))]
    public class DateWiseCauseList : BaseMasterEntity //All Cause Lists will contain causeLists for 7 days and previous ones will be deleted.
    {
        public Guid DateWiseCauseListId { get; set; }
        public Guid CauseListTypeId { get; set; }
        public string PetName { get; set; }
        public int BenchId { get; set; }
        public string cause_list_eliminate { get; set; }
        public string elimination { get; set; }
        public int CauseList_Id { get; set; }
        public string ResName { get; set; }
        public string CauseListStatus { get; set; }
        public int causelist_sr_no { get; set; }
        public string Case_no { get; set; }
        public string published { get; set; }
        public DateTime CauseListDate { get; set; }
        public int CauseList_Type { get; set; }
        public int Court_no { get; set; }
        public int for_bench_id { get; set; }
        public CauseList_DateWiseStatus Status { get; set; }
    }

    public enum CauseList_DateWiseStatus
    {
        Active = 1,
        Inactive = 0
    }
}
