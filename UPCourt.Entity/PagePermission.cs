﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PageName), UserNameProp: nameof(ModifyBy))]
    public class PagePermission
    {
        public PagePermission()
        {
            UserRolePagePermissions = new HashSet<UserRolePagePermission>();
        }
        public Guid PagePermissionId { get; set; }
        public IEnumerable<PagePermissionType> PermissionTypes { get; set; }
        public PageName PageName { get; set; }
        public string ModifyBy { get; set; }
        public ICollection<UserRolePagePermission> UserRolePagePermissions { get; set; }
    }

    public enum PageName
    {
        [Description("Dashboard")]
        [PagePermission("Dashboard", "DashBoard", ParentMenue.StandAlone, "nav-icon fas fa-tachometer-alt", 1,
            PagePermissionType.CanView)]
        Dashboard = 0,
        [Description("User")]
        [PagePermission("User", "User", ParentMenue.Master, "far fa-circle nav-icon", 2,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        User = 1,

        [Description("AG Department")]
        [PagePermission("AG Department", "AGDepartment", ParentMenue.Master, "far fa-circle nav-icon", 3,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        AGDepartment = 2,

        [Description("User Role")]
        [PagePermission("User Role", "UserRole", ParentMenue.Master, "far fa-circle nav-icon", 4,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        UserRole = 3,

        [Description("Advocate")]
        [PagePermission("Advocate", "Advocate", ParentMenue.Master, "far fa-circle nav-icon", 5,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Advocate = 4,

        [Description("Case Status")]
        [PagePermission("Case Status", "CaseStatus", ParentMenue.Master, "far fa-circle nav-icon", 6,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        CaseStatus = 5,

        [Description("Court")]
        [PagePermission("Court", "Court", ParentMenue.Master, "far fa-circle nav-icon", 7,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Court = 6,

        [Description("Designation")]
        [PagePermission("Designation", "Designation", ParentMenue.Master, "far fa-circle nav-icon", 8,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Designation = 7,

        [Description("Goverment Department")]
        [PagePermission("Goverment Department", "GovermentDepartment", ParentMenue.Master, "far fa-circle nav-icon", 9,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        GovermentDepartment = 8,

        [Description("Region")]
        [PagePermission("Region", "Region", ParentMenue.Master, "far fa-circle nav-icon", 10,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Region = 9,

        [Description("Judge")]
        [PagePermission("Judge", "Judge", ParentMenue.Master, "far fa-circle nav-icon", 11,
       PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        Judge = 10,

        [Description("Bench Type")]
        [PagePermission("Bench Type", "BenchType", ParentMenue.Master, "far fa-circle nav-icon", 12,
        PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        BenchType = 11,

        [Description("Notice Type")]
        [PagePermission("Notice Type", "NoticeType", ParentMenue.Master, "far fa-circle nav-icon", 13,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        NoticeType = 12,

        [Description("Subject")]
        [PagePermission("Subject", "Subject", ParentMenue.Master, "far fa-circle nav-icon", 16,
      PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Subject = 15,

        [Description("IPC Section")]
        [PagePermission("IPC Section", "IPCSection", ParentMenue.Master, "far fa-circle nav-icon", 17,
          PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        IPCSection = 16,

        [Description("District")]
        [PagePermission("District", "District", ParentMenue.Master, "far fa-circle nav-icon", 18,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        District = 17,

        [Description("Police Station")]
        [PagePermission("Police Station", "PoliceStation", ParentMenue.Master, "far fa-circle nav-icon", 19,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        PoliceStation = 18,

        [Description("Govt Designation")]
        [PagePermission("Govt Designation", "GovDesignation", ParentMenue.Master, "far fa-circle nav-icon", 20,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        GovDesignation = 19,
        [Description("Govt OIC")]
        [PagePermission("Govt OIC", "GovOIC", ParentMenue.Master, "far fa-circle nav-icon", 21,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        GovOIC = 20,

        [Description("Salutation")]
        [PagePermission("Salutation", "Salutation", ParentMenue.Master, "far fa-circle nav-icon", 22,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Salutation = 21,

        [Description("Special Category")]
        [PagePermission("Special Category", "SpecialCategory", ParentMenue.Master, "far fa-circle nav-icon", 23,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        SpecialCategory = 22,

        [Description("Group")]
        [PagePermission("Group", "Group", ParentMenue.Master, "far fa-circle nav-icon", 24,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Group = 23,

        [Description("Zone")]
        [PagePermission("Zone", "Zone", ParentMenue.Master, "far fa-circle nav-icon", 25,
      PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Zone = 24,

        [Description("User Credential Request")]
        [PagePermission("User Credential Request", "UserCredentialRequest", ParentMenue.UserCredentialRequest, "far fa-circle nav-icon", 2,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        UserCredentialRequest = 30,

        [Description("Notice List")]
        [PagePermission("Notice List", "NoticeCase", ParentMenue.NoticeManagement, "far fa-circle nav-icon", 1,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        NoticeCase = 31,

        [Description("Add New Notice")]
        [PagePermission("Add New Notice", "NoticeCreate", ParentMenue.NoticeManagement, "far fa-circle nav-icon", 2,
        PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        NoticeCreate = 32,

        [Description("Case")]
        [PagePermission("Case", "Case", ParentMenue.CaseManagement, "far fa-circle nav-icon", 1,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        Case = 41,
        [Description("Department Case")]
        [PagePermission("Department-Case", "DepartmentCase", ParentMenue.CaseManagement, "far fa-circle nav-icon", 2,
           PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        DepartmentCase = 42,

        [Description("Audit Trail")]
        [PagePermission("Audit Trail", "Report/Audit", ParentMenue.AuditTrail, "far fa-circle nav-icon", 1,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanCreate, PagePermissionType.CanDelete)]
        AuditTrail = 61,

        [Description("Notice Report")]
        [PagePermission("Notice Report", "NoticeReport", ParentMenue.Reports, "far fa-circle nav-icon", 1, PagePermissionType.CanView)]
        NoticeReport = 71,
        [Description("Case Report")]
        [PagePermission("Case Report", "CaseReport", ParentMenue.Reports, "far fa-circle nav-icon", 2, PagePermissionType.CanView)]
        CaseReport = 72,
        [Description("Doc Requested Wise Case Report")]
        [PagePermission("Doc Requested Wise Case", "CaseReport/DocRequestedCase", ParentMenue.Reports, "far fa-circle nav-icon", 3, PagePermissionType.CanView)]
        DocRequestedCaseReport = 73,
        [Description("Category wise disposed case Report")]
        [PagePermission("Category Wise Disposed Case", "CaseReport/CategoryDisposedCase", ParentMenue.Reports, "far fa-circle nav-icon", 4, PagePermissionType.CanView)]
        CategoryDisposedCaseReport = 74,
        [Description("Category wise Pending case Report")]
        [PagePermission("Category Wise Pending Case", "CaseReport/CategoryPendingCase", ParentMenue.Reports, "far fa-circle nav-icon", 5, PagePermissionType.CanView)]
        CategoryPendingCaseReport = 75,
        [Description("Final Hearing Cases Report")]
        [PagePermission("Final Hearing Cases", "CaseReport/FinalHearingCase", ParentMenue.Reports, "far fa-circle nav-icon", 6, PagePermissionType.CanView)]
        FinalHearingCaseReport = 76,

        [Description("ChangePassword")]
        [PagePermission("ChangePassword", "ChangePassword", ParentMenue.ChangePassword, "far fa-circle nav-icon", 2,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanViewHistory)]
        ChangePassword = 100,

        [Description("Fresh Case")]
        [PagePermission("e-Notice", "FreshCases/List", ParentMenue.FreshCases, "nav-icon fas fa-plus", 1,
        PagePermissionType.CanView, PagePermissionType.CanEdit, PagePermissionType.CanDelete, PagePermissionType.CanCreate)]
        FreshCase = 201,

        [Description("Registration")]
        [PagePermission("Registration", "Registration/Create", ParentMenue.Registration, "far fa-circle nav-icon", 9,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanDelete)]
        Registration = 202,
    }
    public enum PagePermissionType
    {
        [Description("Create")]
        [PagePermissionTypeAttribute("Create", "Create")]
        CanCreate = 0,
        [Description("Edit")]
        [PagePermissionTypeAttribute("Edit", "Edit")]
        CanEdit = 1,
        [Description("Delete")]
        [PagePermissionTypeAttribute("Delete", "Delete")]
        CanDelete = 2,
        [Description("View")]
        [PagePermissionTypeAttribute("View", "Index")]
        CanView = 3,
        [Description("Audit Log")]
        [PagePermissionTypeAttribute("Audit Log", "Log")]
        CanViewHistory = 4,
        [Description("Can approve")]
        [PagePermissionTypeAttribute("Can approve", "Approval")]
        User_CanApprove = 5,
    }
}
