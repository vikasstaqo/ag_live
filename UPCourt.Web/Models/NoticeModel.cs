﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;


namespace UPCourt.Web.Models
{
    public class NoticeMvcModel : BaseEntityModel<Notice, NoticeMvcModel>
    {
        public Guid NoticeId { get; set; }
        public Guid PetitionerId { get; set; }
        public Guid? NoticeCounselorId { get; set; }
        [Required]
        [Display(Name = "Notice Type")]
        public ParentNoticeType NoticeType { get; set; }
        [Required(ErrorMessage = "Notice Type is required")]
        [Display(Name = "Notice Type")]
        public Guid? CaseNoticeTypeId { get; set; }
        public CaseNoticeTypeMvcModel CaseNoticeType { get; set; }
        public List<CaseMvcModel> Cases { get; set; }
        public Guid? GroupID { get; set; }
        public GroupMvcModel Group { get; set; }
        public CrimeDetailMvcModel CrimeDetail { get; set; }
        public CaseDetailsMvcModel CaseDetail { get; set; }
        public int SerialNo { get; set; }
        [Required(ErrorMessage = "Notice No is required"), StringLength(50)]
        [Display(Name = "Notice No.")]
        public string NoticeNo { get; set; }
        [Required(ErrorMessage = "Notice Date is required")]
        [Display(Name = "Notice Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? NoticeDate { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Year")]
        public int NoticeYear { get; set; }
        
        public string Subject { get; set; }
        public string District { get; set; }

        public string SectionOrAct { get; set; }
        public Guid[] SpecialCategories { get; set; }
        public List<NoticeSpecialcategoryMvcModel> NoticeSpecialCategories { get; set; }
       
        [Display(Name = "Is Context Order")]
        public bool IsContextOrder { get; set; }
        public string Cnr { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        [Required]
        public NoticeStatus Status { get; set; }
        [Display(Name = "Parent Notice Type")]
        public ParentNoticeType ParentNoticeType { get; set; }
        [Display(Name = "Court")]
        public Guid RegionId { get; set; }
        public RegionMvcModel Region { get; set; }
        public List<PetitionerMvcModel> Petitioners { get; set; }
        public List<NoticeCounselorMvcModel> NoticeCounselors { get; set; }
        public Guid NoticeRespondentId { get; set; }
        public List<NoticeRespondentMvcModel> NoticeRespondents { get; set; }
        public List<NoticeBasicIndexMvcModel> NoticeBasicIndexs { get; set; }
        public Guid ImpungedAndSTDetailID { get; set; }
        public List<ImpungedAndSTDetailMvcModel> ImpungedAndSTDetails { get; set; }
        public List<CrimeDetail> Crimedetailslist { get; set; }
        public List<NoticeIPCMvcModel> NoticeIPCs { get; set; }
        public Guid NoticeActTypeId { get; set; }
        public List<NoticeActTypeMvcModel> NoticeActTypes { get; set; }
        public List<ECourtFeeMvcModel> ECourtFees { get; set; }
        [Display(Name = "Reference Case No (Optional)")]
        public string ReferenceCaseNo { get; set; }
        public string BtnCommand { get; set; }
        public NoticeFinalStatus FinalSubmit { get; set; }
        public override NoticeMvcModel EntityToResponse(Notice entity, NoticeMvcModel model)
        {
            model.Petitioners = PetitionerMvcModel.CopyFromEntityList(entity.Petitioners.OrderBy(x => x.SNo), model.RecursiveLevel);
            model.NoticeCounselors = NoticeCounselorMvcModel.CopyFromEntityList(entity.Counselors.OrderBy(x => x.SNo), model.RecursiveLevel);
            model.NoticeRespondents = NoticeRespondentMvcModel.CopyFromEntityList(entity.Respondents.OrderBy(x => x.SNo), model.RecursiveLevel);
            model.NoticeBasicIndexs = NoticeBasicIndexMvcModel.CopyFromEntityList(entity.BasicIndexs, model.RecursiveLevel);
            model.NoticeIPCs = NoticeIPCMvcModel.CopyFromEntityList(entity.IPCs.OrderBy(x => x.SNo), model.RecursiveLevel);
            model.NoticeActTypes = NoticeActTypeMvcModel.CopyFromEntityList(entity.ActTypes.OrderBy(x => x.SNo), model.RecursiveLevel);
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, model.RecursiveLevel);
            model.CaseNoticeType = CaseNoticeTypeMvcModel.CopyFromEntity(entity.CaseNoticeType, model.RecursiveLevel);
            model.Group = GroupMvcModel.CopyFromEntity(entity.Group, model.RecursiveLevel);
            model.ImpungedAndSTDetails = ImpungedAndSTDetailMvcModel.CopyFromEntityList(entity.ImpungedAndSTDetails, model.RecursiveLevel);
            model.CrimeDetail = CrimeDetailMvcModel.CopyFromEntity(entity.CrimeDetail, model.RecursiveLevel);
            model.CaseDetail = CaseDetailsMvcModel.CopyFromEntity(entity.CaseDetail, model.RecursiveLevel);
            
            model.NoticeSpecialCategories = NoticeSpecialcategoryMvcModel.CopyFromEntityList(entity.NoticeSpecialcategorys, model.RecursiveLevel);
            model.ECourtFees = ECourtFeeMvcModel.CopyFromEntityList(entity.ECourtFees, model.RecursiveLevel);
            model.Cases = CaseMvcModel.CopyFromEntityList(entity.Cases, model.RecursiveLevel);

            return model;
        }

        private Guid[] getSpecialCategoriesIds(Notice entity)
        {
            Guid[] categories = new Guid[entity.NoticeSpecialcategorys.Count];
            var i = 0;
            foreach (var cat in entity.NoticeSpecialcategorys)
            {
                categories[i] = cat.NoticeSpecialcategoryId;
                i++;
            }
            return categories;
        }
    }
    public class PetitionerMvcModel : BaseEntityModel<Petitioner, PetitionerMvcModel>
    {
        public Guid PetitionerId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }
        public string Email { get; set; }
        [Display(Name = "Petitioner District")]
        public string District { get; set; }
        [Display(Name = "Case District")]
        public string CaseDistrict { get; set; }

        [Display(Name = "ID Proof")]
        public IDProofType IDProof { get; set; }

        [Display(Name = "ID Proof Number")]

        public string IDProofNumber { get; set; }
        [Display(Name = "Salutation")]
        public Guid? SalutationId { get; set; }
        [Display(Name = "Petitioner's Age")]
        public int? Age { get; set; }
        [Display(Name = "Salutations")]
        public Guid? ParantageSalutationId { get; set; }


        [Required]

        [Display(Name = "Parantage Name")]
        public string ParantageName { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobile No")]
      [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Mobile Number.")]
        public string MobileNo { get; set; }
        [Display(Name = "Pincode"), RegularExpression(RegularExpressionHelper.PinCodeValidationExpression, ErrorMessage = "Invalid Pincode")]
        public string Pincode { get; set; }
        public bool bActive { get; set; }
        [Display(Name = "First Petitioner")]
        public bool IsFirst { get; set; }

        public int? SNo { get; set; }
        public override PetitionerMvcModel EntityToResponse(Petitioner entity, PetitionerMvcModel model)
        {
            return model;
        }
    }
    public class NoticeCounselorMvcModel : BaseEntityModel<NoticeCounselor, NoticeCounselorMvcModel>
    {
        public Guid NoticeCounselorId { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Roll No.")]
        public string RollNo { get; set; }

        [Display(Name = "Enrollment No.")]
        public string EnrollmentNo { get; set; }
        [Display(Name = "Contact No.")]
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public bool bActive { get; set; }
        public int? SNo { get; set; }
    }
    public class NoticePartyMvcModel : BaseEntityModel<PartyAndRespondent, NoticePartyMvcModel>
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticePartyId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public Guid? CasePartyId { get; set; }
        public Guid? CaseRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
     
        public PartyType PartyType { get; set; }
        [Display(Name = "Party Name")]
        public string PartyName { get; set; }
        [Display(Name = "Contact No.")]
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
  
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        [Display(Name = "If Party Department")]
        public bool IsPartyDepartment { get; set; }
        public bool DeleteNoticeParty { get; set; }
        [FilterUIHint("GovermentDepartmentForNoticeMvcModel")]
        public GovermentDepartmentForNoticeMvcModel GovermentDepartment { get; set; }
        public override NoticePartyMvcModel EntityToResponse(PartyAndRespondent entity, NoticePartyMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentForNoticeMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            return model;
        }

        public bool bActive { get; set; }
    }
    public class NoticeRespondentMvcModel : BaseEntityModel<PartyAndRespondent, NoticeRespondentMvcModel>
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticePartyId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public Guid? CasePartyId { get; set; }
        public Guid? CaseRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
     
        public CaseMvcModel Case { get; set; }
        public PartyType PartyType { get; set; }
        [Display(Name = "Party Name"), DataType(DataType.Text)]
        public string PartyName { get; set; }
        [Display(Name = "Contact No."), DataType(DataType.PhoneNumber)]
        public string ContactNo { get; set; }
        [Display(Name = "Email Id."), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "District")]
        public string District { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Pincode"), RegularExpression(RegularExpressionHelper.PinCodeValidationExpression, ErrorMessage = "Invalid Pincode")]
        public string Pincode { get; set; }

        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        [Display(Name = "If Party Department")]
        public bool IsPartyDepartment { get; set; }
        public bool DeleteRespondentParty { get; set; }

        public bool bActive { get; set; }
        [Display(Name = "First Respondent")]
        public bool IsFirst { get; set; }
        [FilterUIHint("GovermentDepartmentForNoticeMvcModel")]
        public GovermentDepartmentForNoticeMvcModel GovermentDepartment { get; set; }
        [Display(Name = "Gov Designation")]
        public Guid? GovernmentDesignationId { get; set; }
        public GovDesignationMvcModel GovDesignation { get; set; }

        [Display(Name = "Mobile No")]

        public string MobileNo { get; set; }

        [Display(Name = "Fax No")]
        public string FaxNo { get; set; }
        public int? SNo { get; set; }
        public Guid GovernmentdepartmentOICID { get; set; }
        public GovOICMvcModel GovOIC { get; set; }
        public override NoticeRespondentMvcModel EntityToResponse(PartyAndRespondent entity, NoticeRespondentMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentForNoticeMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            model.GovDesignation = GovDesignationMvcModel.CopyFromEntity(entity.GovDesignation, model.RecursiveLevel);
            model.GovOIC = GovOICMvcModel.CopyFromEntity(entity.GovernmentDepartmentOic, model.RecursiveLevel);
            return model;
        }

    }
    public class GovermentDepartmentForNoticeMvcModel : BaseEntityModel<GovernmentDepartment, GovermentDepartmentForNoticeMvcModel>
    {
        public Guid GovernmentDepartmentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HeadOffice { get; set; }
        public string POCName { get; set; }
        public string POCContact { get; set; }
    }

    public class NoticeCaseIndexMvcModel
    {
        public Guid NoticeId { get; set; }
        [Display(Name = "Notice No.")]
        public string NoticeNo { get; set; }

        [Display(Name = "Notice From")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? NoticeFromDate { get; set; }

        [Display(Name = "Notice To")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? NoticeToDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter case no.")]
        [Display(Name = "Case No")]
        public string CaseNo { get; set; }
        public List<SelectListItem> CaseStatuses { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select case status")]
        [Display(Name = "Case Status")]
        public Guid CaseStatusId { get; set; }
        [Display(Name = "Goverment Department")]

        public Guid? GovermentDepartmentId { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Display(Name = "User")]
        public Guid? CreatedBy { get; set; }
        [Display(Name = "Notice Type")]
        public Guid NoticeTypeId { get; set; }
        [Display(Name = "Parent Notice Type")]
        public ParentNoticeType ParentNoticeType { get; set; }
        [Display(Name = "Counselor Name")]
        public string Counselor { get; set; }
        [Display(Name = "Petitioner Name")]
        public string Petitioner { get; set; }
        [Display(Name = "Respondent Name")]
        public string Respondent { get; set; }
        [Display(Name = "Notice Type")]
        public Guid CaseNoticeTypeId { get; set; }
        public string Subject { get; set; }
        [Display(Name = "Group")]
        public Guid GroupId { get; set; }
        [Display(Name = "Region")]
        public Guid RegionId { get; set; }
        [Display(Name = "Crime No")]
        public string CrimeNo { get; set; }

        [Display(Name = "Fir No")]
        public string FIRNO { get; set; }

        [Display(Name = "Notice From")]
        public NoticeFrom NoticeFrom { get; set; }
        public List<IGrouping<string, NoticeMvcModel>> Notices_Subjects { get; set; }
        public List<NoticeMvcModel> Notices { get; set; }
        public ExportType Export { get; set; }
    }

    public enum ExportType
    {
        [Display(Name = "To Excel")]
        Excel,
        [Display(Name = "To PDF")]
        PDF
    }

    public class NoticeIPCMvcModel : BaseEntityModel<NoticeIPC, NoticeIPCMvcModel>
    {
        public Guid NoticeIpcId { get; set; }
        public Guid NoticeId { get; set; }
        public Guid IPCSectionId { get; set; }
        public IPCSectionMvcModel IPCSection { get; set; }
        public Guid CrimeDetailId { get; set; }
        public bool bActive { get; set; }
        public int? SNo { get; set; }
        public override NoticeIPCMvcModel EntityToResponse(NoticeIPC entity, NoticeIPCMvcModel model)
        {
            model.IPCSection = IPCSectionMvcModel.CopyFromEntity(entity.IPCSection, model.RecursiveLevel);
            return model;
        }
    }
    public class NoticeActTypeMvcModel : BaseEntityModel<NoticeActType, NoticeActTypeMvcModel>
    {
        public Guid NoticeActTypeId { get; set; }
        public Guid NoticeId { get; set; }
        public string BelongsTo { get; set; }
        public string ActTitle { get; set; }
        public string Section { get; set; }
        public string RuleTitle { get; set; }
        public string RuleNumber { get; set; }
        public string ModifyBy { get; set; }
        public int? SNo { get; set; }
        public bool bActive { get; set; }

    }
    public class NoticeSpecialcategoryMvcModel : BaseEntityModel<NoticeSpecialcategory, NoticeSpecialcategoryMvcModel>
    {
        public Guid NoticeSpecialcategoryId { get; set; }
        public Guid NoticeId { get; set; }
        public Guid SpecialCategoryId { get; set; }
        public SpecialCategoryMvcModel SpecialCategory { get; set; }
        public Guid CrimeDetailId { get; set; }
        public bool bActive { get; set; }
        public override NoticeSpecialcategoryMvcModel EntityToResponse(NoticeSpecialcategory entity, NoticeSpecialcategoryMvcModel model)
        {
            model.SpecialCategory = SpecialCategoryMvcModel.CopyFromEntity(entity.SpecialCategory, model.RecursiveLevel);
            return model;
        }
    }

    public class SpecialCategoryMvcModel : BaseEntityModel<SpecialCategory, SpecialCategoryMvcModel>
    {
        public Guid SpecialCategoryId { get; set; }
        public string Name { get; set; }
        public SpecialCategoryStatus Status { get; set; }

    }
    public class NoticeBasicIndexMvcModel : BaseEntityModel<NoticeBasicIndex, NoticeBasicIndexMvcModel>
    {
        public Guid NoticeBasicIndexId { get; set; }
        public Guid? NoticeId { get; set; }
        public Guid? CaseId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        [Display(Name = "Document Name")]
        public string Particulars { get; set; }
        public string Annexure { get; set; }
        public int SerialNo { get; set; }
        [Display(Name = "Index Date")]
        public DateTime? IndexDate { get; set; }
        [Display(Name = "No. Of Pages")]
        public int? NoOfPages { get; set; }
        public bool bActive { get; set; }
        public List<DocumentMvcModel> Documents { get; set; }
        [Display(Name = "Upload Document")]
        public HttpPostedFileBase BasicIndexFile { get; set; }
        public override NoticeBasicIndexMvcModel EntityToResponse(NoticeBasicIndex entity, NoticeBasicIndexMvcModel model)
        {
            model.Documents = DocumentMvcModel.CopyFromEntityList(entity.Documents.OrderByDescending(x => x.DocumentId), model.RecursiveLevel);
            return model;
        }
    }
    public class DocumentMvcModel : BaseEntityModel<Document, DocumentMvcModel>
    {
        public Guid DocumentId { get; set; }
        [Display(Name = "Upload Document Name")]
        [Required]
        public string Name { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        public Guid? CaseHearingId { get; set; }
        public DocumentFileType FileType { get; set; }
        [Display(Name = "Document type")]
        public DocumentType Type { get; set; }
        public DocumentEntityType EntityType { get; set; }
        public string DocumentUrl { get; set; }
        public int Ordinal { get; set; }
        public DocumentStatus Status { get; set; }
        public int Size { get; set; }
        public int PageCount { get; set; }
        public string PageRange { get; set; }
        [Display(Name = "Comments")]
        [AllowHtml]
        public string Description { get; set; }
        [Display(Name = "Certified copy of the judgement *")]
        public HttpPostedFileBase UploadFile { get; set; }

        public List<HttpPostedFileBase> MultiUploadFiles { get; set; }
    }

    public class ActTypeMvcModel : BaseEntityModel<ActType, ActTypeMvcModel>
    {
        public Guid ActTypeID { get; set; }
        public string ActBelongName { get; set; }
        public bool IsActive { get; set; }
        public ParentActBelongType ParentActBelongType { get; set; }
        public DateTime InsertedDate { get; set; }
        public string InsertedBy { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
    }

    public class ECourtFeeMvcModel : BaseEntityModel<ECourtFee, ECourtFeeMvcModel>
    {
        public Guid eCourtFeeID { get; set; }
        public Guid? NoticeId { get; set; }
        [Display(Name = "Receipt No.")]
        public string ReceiptNo { get; set; }
        public decimal Amount { get; set; }
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? EDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public string InsertedBy { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool Status { get; set; }

        public bool bActive { get; set; }
    }
}