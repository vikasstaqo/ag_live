﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public class Tehsil : BaseMasterEntity
    {
        public Tehsil() { }
        public Guid TehsilId { get; set; }
        public string TehsilName { get; set; }
        public TehsilStatus Status { get; set; }
    }
}
public enum TehsilStatus
{
    Active = 1,
    Inactive = 0
}

