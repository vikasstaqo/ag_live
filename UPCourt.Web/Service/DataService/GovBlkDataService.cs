﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovBlkDataService
    {
        public static IQueryable<GovBlk> GetLite(DatabaseContext db)
        {
            return db.GovBlk.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<GovBlk> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}