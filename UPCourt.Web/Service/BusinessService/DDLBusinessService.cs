﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Service.BusinessService
{
    public class DDLBusinessService : BaseMVCController
    {
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeCaseNoticeTypes(object selectedValue)
        {
            var dbData = NoticeTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseNoticeTypes = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);
        }
        public void InitilizeRegions(object selectedValue)
        {

            if (AppUser.User.RegionId != null)
            {
                var dbData = RegionDataService.GetLite(db).Where(x => x.RegionId == AppUser.User.RegionId).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }
            else
            {
                var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
                var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
                ViewBag.Regions = new SelectList(mvc, nameof(Region.RegionId), nameof(Region.Name), selectedValue);
            }

        }
        public void InitilizeIPCSections(object selectedValue)
        {
            var dbData = IPCSectionDataService.GetLite(db).OrderBy(i => i.SectionName).ToList();
            var mvc = IPCSectionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.IPCSections = new SelectList(mvc, nameof(IPCSection.IPCSectionId), nameof(IPCSection.SectionName), selectedValue);
        }
        public void InitilizeDistrict(object selectedValue)
        {
            var dbData = DistrictDataService.GetLite(db).OrderBy(i => i.DistrictName).ToList();
        
            ViewBag.Districts = new SelectList(dbData, nameof(District.DistrictName), nameof(District.DistrictName), selectedValue);
        }


        public void InitilizeNoticeType(object selectedValue)
        {
            var dbData = NoticeTypeDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CaseNoticeTypeMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.NoticeType = new SelectList(mvc, nameof(CaseNoticeType.CaseNoticeTypeId), nameof(CaseNoticeType.Name), selectedValue);

        }
      
        public void InitilizeDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Department = new SelectList(mvc, "GovernmentDepartmentId", "Name", selectedValue);
        }
    }
}