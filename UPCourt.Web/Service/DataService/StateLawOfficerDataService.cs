﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class StateLawOfficerDataService
    {
        public static IQueryable<StateLawOfficer> GetLite(DatabaseContext db)
        {
            return db.StateLawOfficer;
        }
        public static IQueryable<StateLawOfficer> GetDetail(DatabaseContext db)
        {
            var data = GetLite(db);
            return data; ;

        }
    }
}