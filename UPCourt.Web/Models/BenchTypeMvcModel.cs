﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class BenchTypeMvcModel : BaseEntityModel<BenchType, BenchTypeMvcModel>
    {
        public Guid BenchTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public int bench_type_code { get; set; }
        public int NumberOfJudge { get; set; }

        

    }
    public class BenchDetails
    {
        public Guid BenchTypeID { get; set; }
        public string Name { get; set; }
        public int NumberOfJudge { get; set; }
        public string selectedValue { get; set; }
    }
}