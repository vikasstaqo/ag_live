﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class GovDesignationController : BaseMVCController
    {
        // GET: GovDesignation
        public ActionResult Index()
        {
            return View(GovDesignationMvcModel.CopyFromEntityList(GovDesignationDataService.GetFiltered(db, AppUser.User).OrderByDescending(i => i.DisplayOrder), 0));
        }
        public ActionResult Create()
        {
            InitilizeGovermentDepartments(AppUser.User.GovernmentDepartmentId);
            var model = new GovDesignationMvcModel
            {
                Status = GovernmentDesignationStatus.Active,
                GovernmentDepartmentId = (Guid)(AppUser.User.GovernmentDepartmentId != null ? AppUser.User.GovernmentDepartmentId : Guid.Empty)
            };
            return View(model);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetFiltered(db, AppUser.User).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, "GovernmentDepartmentId", "Name", selectedValue);
        }

        [HttpPost]
        public ActionResult Create(GovDesignationMvcModel model)
        {
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            if (!ModelState.IsValid) { return View(model); }
            if (!CommonForInsertUpdate(model)) { return View(model); }
            Guid GovernmentDesignationId = Guid.NewGuid();
            GovernmentDesignation tempData = new GovernmentDesignation()
            {
                GovernmentDesignationId = GovernmentDesignationId,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                DesignationName = model.DesignationName,
                POCName = model.POCName,
                POCEmail = model.POCEmail,
                POCMobileNo = model.POCMobileNo,
                POCContact = model.POCContact,
                POCFaxNo = model.POCFaxNo,
                Status = model.Status,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now,
                DisplayOrder = MasterBusinessService.GetGovernmentDesignationsDisplayOrder(db)
            };

            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passWordSalt);
            User tempUser = new User()
            {
                UserId = Guid.NewGuid(),
                Name = model.DesignationName,
                Email = model.POCEmail,
                Phone = model.POCMobileNo,
                LoginId = model.POCEmail,
                Status = UserStatus.Active,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = true,
                UserType = UserType.GovernmentDepartment,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                DesignationId = GovernmentDesignationId,
                UserRoleId = UserRoleDataService.GetLite(db).Where(x => x.UserSystemRoleId == UserSystemRole.GovDeptDegUser).FirstOrDefault().UserRoleId,
                CreatedBy = AppUser.UserId,
                CreatedOn = DateTime.Now
            };
            db.Users.Add(tempUser);
            db.GovernmentDesignations.Add(tempData);
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, CreatedMessage);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var dbData = GovDesignationDataService.GetDetail(db).Where(i => i.GovernmentDesignationId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            return View(GovDesignationMvcModel.CopyFromEntity(dbData, 0));
        }
        [HttpPost]

        public ActionResult Edit(GovDesignationMvcModel model)
        {
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            if (!ModelState.IsValid) { return View(model); }
            var dbData = GovDesignationDataService.GetDetail(db).Where(i => i.GovernmentDesignationId == model.GovernmentDesignationId).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            if (!CommonForInsertUpdate(model)) { return View(model); }

            dbData.DesignationName = model.DesignationName;
            dbData.GovernmentDepartmentId = model.GovernmentDepartmentId;
            dbData.Status = model.Status;
            dbData.POCName = model.POCName;
            dbData.POCMobileNo = model.POCMobileNo;
            dbData.POCContact = model.POCContact;
            dbData.POCFaxNo = model.POCFaxNo;
            dbData.ModifiedBy = AppUser.UserId;
            dbData.ModifiedOn = DateTime.Now;

            var GovDesuser = UserDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId
            && i.DesignationId == model.GovernmentDesignationId
            && i.UserRole.UserSystemRoleId == UserSystemRole.GovDeptDegUser).FirstOrDefault();

            if (GovDesuser == null)
            {
                User tempData = new User()
                {
                    UserId = Guid.NewGuid(),
                    Name = model.DesignationName,
                    Email = model.POCEmail,
                    Phone = model.POCMobileNo,
                    LoginId = model.POCEmail,
                    Status = model.Status == GovernmentDesignationStatus.Active ? UserStatus.Active : UserStatus.Inactive,
                    IsDefault = true,
                    UserType = UserType.GovernmentDepartment,
                    GovernmentDepartmentId = model.GovernmentDepartmentId,
                    DesignationId = model.GovernmentDesignationId,
                    UserRoleId = UserRoleDataService.GetLite(db).Where(x => x.UserSystemRoleId == UserSystemRole.GovDeptDegUser).FirstOrDefault().UserRoleId,
                    CreatedBy = AppUser.UserId,
                    CreatedOn = DateTime.Now
                };

                if (!model.POCPassword.IsNullOrEmpty())
                {
                    var passwordSalt = PasswordHelper.GetPasswordSalt();
                    var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passwordSalt);
                    tempData.PasswordSalt = passwordSalt;
                    tempData.PasswordHash = passwordHash;
                }

                db.Users.Add(tempData);
                dbData.IsAssignUser = true;
            }
            else
            {
                GovDesuser.Status = model.Status == GovernmentDesignationStatus.Active ? UserStatus.Active : UserStatus.Inactive;
                if (!model.POCPassword.IsNullOrEmpty())
                {
                    var passwordSalt = PasswordHelper.GetPasswordSalt();
                    var passwordHash = PasswordHelper.EncodePassword(model.POCPassword, passwordSalt);
                    GovDesuser.PasswordSalt = passwordSalt;
                    GovDesuser.PasswordHash = passwordHash;
                }
            }

            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, UpdatedMessage);
            return RedirectToAction("Index");
        }


        public ActionResult Details(Guid id)
        {
            var dbData = GovDesignationDataService.GetLite(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null) { return RedirectToAction("Index"); }
            return View(GovDesignationMvcModel.CopyFromEntity(dbData, 0));
        }

        private bool CommonForInsertUpdate(GovDesignationMvcModel model)
        {
            var duplicateCheck = GovDesignationDataService.GetLite(db).FirstOrDefault(i => i.GovernmentDesignationId != model.GovernmentDesignationId && i.GovernmentDepartmentId == model.GovernmentDepartmentId && i.DesignationName == model.DesignationName);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.DesignationName), $"{ model.DesignationName } already exists with Same Deparment ");
                return false;
            }
            var duplicateEmailCheck = GovDesignationDataService.GetLite(db).FirstOrDefault(i => i.GovernmentDesignationId != model.GovernmentDesignationId && i.POCEmail == model.POCEmail);
            if (duplicateEmailCheck != null)
            {
                ModelState.AddModelError(nameof(model.POCEmail), "Email already exists");
                return false;
            }

            var duplicateEmailUserCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.DesignationId != model.GovernmentDesignationId && i.Email == model.POCEmail);
            if (duplicateEmailUserCheck != null)
            {
                ModelState.AddModelError(nameof(model.POCEmail), "Email already exists in user master.");
                return false;
            }
            return true;
        }

        [PageRoleAuthorizarion(PageName.GovDesignation, PagePermissionType.CanDelete)]
        public ActionResult Delete(Guid id)
        {
            var dbData = GovDesignationDataService.GetLite(db).Where(i => i.GovernmentDesignationId == id).FirstOrDefault();
            dbData.IsDeleted = true;
            db.SaveChangesWithAudit(Request, AppUser.UserName);
            DisplayMessage(MessageType.Success, DeletedMessage);
            return RedirectToAction("Index");
        }
    }
}
