﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;

namespace UPCourt.Web.Core
{
    public static class ExcelImportHelper
    {
        public static DataTable ReadExcelFromFile(string filePath)
        {
            string extension = Path.GetExtension(filePath);
            string conString = string.Empty;
            if (extension == ".xls")
                conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
            else if (extension == ".xlsx")
                conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;

            DataTable dt = new DataTable();
            conString = string.Format(conString, filePath);
            using (OleDbConnection connExcel = new OleDbConnection(conString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get first sheet name
                        connExcel.Open();
                        DataTable dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        //connExcel.Close();

                        //Get first Sheet data
                        cmdExcel.CommandText = "Select * from [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();

                    }
                }
            }

            return dt;
        }
    }
}