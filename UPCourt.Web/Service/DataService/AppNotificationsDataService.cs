﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class AppNotificationsDataService
    {
        public static IQueryable<AppNotifications> GetLite(DatabaseContext db)
        {
            return db.AppNotifications;
        }
    
        public static IEnumerable<AppNotifications> GetNotifications(DatabaseContext db)
        {
            var retval = GetLite(db)
                .Select(i => new AppNotifications
                {
                    Source = i.Source,
                    TargetUserId = i.TargetUserId,
                    NotificationType = i.NotificationType,
                    Status = i.Status,
                    Message = i.Message,
                    CreatedBy = i.CreatedBy,
                    CreatedOn = i.CreatedOn,
                    ModifiedDate = i.ModifiedDate,
                    RouteValue = i.RouteValue
               });

            return retval;
        }
    }
}