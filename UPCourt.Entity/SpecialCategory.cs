﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(CreatedBy))]
    public class SpecialCategory : BaseMasterEntity
    {
        public SpecialCategory() { }
        public Guid SpecialCategoryId { get; set; }
        public string Name { get; set; }
        public SpecialCategoryStatus Status { get; set; }

    }

    public enum SpecialCategoryStatus
    {
        Active = 1,
        Inactive = 0
    }

}
