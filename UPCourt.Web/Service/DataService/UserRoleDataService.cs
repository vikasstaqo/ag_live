﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class UserRoleDataService
    {
        public static IQueryable<UserRole> GetLite(DatabaseContext db)
        {
            return db.UserRoles.Where(x => x.IsDeleted == false);
        }
        public static IQueryable<UserRole> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
               .Include(i => i.UserRolePagePermissions)
               .ThenInclude(i => i.PagePermission);
        }
    }
}