﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using UPCourt.Web.Error;

namespace UPCourt.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_EndRequest()
        {
            // removing excessive headers. They don't need to see this.
            Response.Headers.Remove("Server");
        }
        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception objErr = Server.GetLastError().GetBaseException();
            var lineNumber = 0;
            var frame = new StackTrace(objErr, true).GetFrame(0); // where the error originated
            if (frame != null)
                lineNumber = frame.GetFileLineNumber();

            string err = "Error Line:" + Convert.ToString(lineNumber) + "  Error in: " + Request.Url.ToString() +
                              ". Error Message:" + objErr.Message.ToString();


            // Log the error
            ErrHandler.WriteError(err);
            // Response.Redirect("Error.html");
        }
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new NoCacheGlobalActionFilter());
        }
    }
    public class NoCacheGlobalActionFilter : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {

            HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
            cache.SetCacheability(HttpCacheability.NoCache);

            base.OnResultExecuted(filterContext);
        }
    }
}
